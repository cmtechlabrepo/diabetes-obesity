<?php
return [
    'Theme' => [
        'title' => 'Diabetes Obesity Clinic',
        'logo' => [
            'mini' => '<b>D</b>OC',
            'normal' => '<img src="https://sandbox.cmtechlab.com/medfile/img/logo.png" alt="Diabetes OC" />',
            'large' => '<img src="https://sandbox.cmtechlab.com/medfile/img/logo.png" style="width: 100%;" alt="Diabetes OC" />'
        ],
        'login' => [
            'show_remember' => false,
            'show_register' => false,
            'show_social' => false
        ],
        'folder' => ROOT,
        'skin' => 'blue'
    ]
];