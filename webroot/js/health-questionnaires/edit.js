$( document ).ready(function() {     
    $('#datepicker1,#datepicker2,#datepicker3,.started, .dateillness, .datesurgery, .datehospitalization,.bonedate,#signature_date').datepicker({
        autoclose: true
    });

    var idMedications = $('#medication-list tr:last').attr('id');
    var maxMedications = idMedications.replace(/[^0-9\.]+/g, '');

    var idIllness = $('#illness-list tr:last').attr('id');
    var maxIllness = idIllness.replace(/[^0-9\.]+/g, '');

    $('#medication-list').on('click','.add-medication',function(e){
        $(".started").datepicker("destroy");
        var nextMedication = parseInt(maxMedications) + 1;
        maxMedications = nextMedication;
        var maketr = "<tr><td><input name='medicationname"+nextMedication+"' type='text' id='medicationname"+nextMedication+"'></td>";
        maketr += "<td><input name='dose"+nextMedication+"' type='text' id='dose"+nextMedication+"'></td>";
        maketr += "<td><input name='oftentaken"+nextMedication+"' type='text' id='oftentaken"+nextMedication+"'></td>";            
        maketr += "<td><input name='purpose"+nextMedication+"' type='text' id='purpose"+nextMedication+"'></td>";
        maketr += "<td><input name='started"+nextMedication+"' type='text' class='started' id='started"+nextMedication+"'></td>";
        maketr += "<td><input name='medication_brand"+nextMedication+"' type='text' id='medication_brand"+nextMedication+"'></td>";
        maketr += "<td>";
        maketr += "<a href='#' class='add add-medication' ><i class='fa fa-fw fa-plus'></i></a>";
        maketr += "<a href='#' class='remove remove-medication' ><i class='fa fa-fw fa-times'></i></a>";
        maketr += "</td></tr>";
        $('#medication-list tbody').append(maketr);
        
        $('.started').datepicker({
            autoclose: true
        });
        
        e.preventDefault();
    });
    
    $('#medication-list').on('click','.remove-medication',function(e){
        $(this).parent().parent().remove();
        e.preventDefault();
    });

    var idIllness = $('#illness-list tr:last').attr('id');
    var maxIllness = idIllness.replace(/[^0-9\.]+/g, '');

    $('#illness-list').on('click','.add-illness',function(e){
        $(".dateillness").datepicker("destroy");
        var nextillness = parseInt(maxIllness) + 1;
        maxIllness = nextillness;
        var maketr = "<tr><td><input class='dateillness' name='dateillness"+nextillness+"' type='text' id='dateillness"+nextillness+"'></td>";
        maketr += "<td><input name='illness"+nextillness+"' type='text' id='illness"+nextillness+"'></td>";
        maketr += "<td><input name='treatment"+nextillness+"' type='text' id='treatment"+nextillness+"'></td>";            
        maketr += "<td><input name='outcome"+nextillness+"' type='text' id='outcome"+nextillness+"'></td>";
        maketr += "<td>";
        maketr += "<a href='#' class='add add-illness' ><i class='fa fa-fw fa-plus'></i></a>";
        maketr += "<a href='#' class='remove remove-illness' ><i class='fa fa-fw fa-times'></i></a>";
        maketr += "</td></tr>";
        $('#illness-list tbody').append(maketr);
        
        $('.dateillness').datepicker({
            autoclose: true
        });
        
        e.preventDefault();
    });
    
    $('#illness-list').on('click','.remove-illness',function(e){
        $(this).parent().parent().remove();
        e.preventDefault();
    });

    var idSurgery = $('#surgery-list tr:last').attr('id');
    var maxSurgery = idSurgery.replace(/[^0-9\.]+/g, '');

    $('#surgery-list').on('click','.add-surgery',function(e){
        $(".datesurgery").datepicker("destroy");
        var nextsurgery = parseInt(maxSurgery) + 1;
        maxSurgery = nextsurgery;
        var maketr = "<tr><td><input name='surgeryname1"+maxSurgery+"' type='text' id='surgeryname1"+maxSurgery+"'></td>";
        maketr += "<td><input class='datesurgery' name='datesurgery"+maxSurgery+"' type='text' id='datesurgery"+maxSurgery+"'></td>";
        maketr += "<td><input name='reason"+maxSurgery+"' type='text' id='reason"+maxSurgery+"'></td>";            
        maketr += "<td><input name='hospital"+maxSurgery+"' type='text' id='hospital"+maxSurgery+"'></td>";
        maketr += "<td>";
        maketr += "<a href='#' class='add add-surgery' ><i class='fa fa-fw fa-plus'></i></a>";
        maketr += "<a href='#' class='remove remove-surgery' ><i class='fa fa-fw fa-times'></i></a>";
        maketr += "</td></tr>";
        $('#surgery-list tbody').append(maketr);
        
        $('.datesurgery').datepicker({
            autoclose: true
        });
        
        e.preventDefault();
    });
    
    $('#surgery-list').on('click','.remove-surgery',function(e){
        $(this).parent().parent().remove();
        e.preventDefault();
    });

    var idhospitalization = $('#hospitalization-list tr:last').attr('id');
    var maxhospitalization = idhospitalization.replace(/[^0-9\.]+/g, '');

    $('#hospitalization-list').on('click','.add-hospitalization',function(e){
        $(".datehospitalization").datepicker("destroy");
        var nexthospitalization = parseInt(maxhospitalization) + 1;
        maxhospitalization = nexthospitalization;
        var maketr = "<tr>"
        maketr += "<td><input class='datehospitalization' name='datehospitalization"+maxhospitalization+"' type='text' id='datehospitalization"+maxhospitalization+"'></td>";
        maketr += "<td><input name='reasonhospitalization"+maxhospitalization+"' type='text' id='reasonhospitalization"+maxhospitalization+"'></td>";            
        maketr += "<td><input name='hospitalhospitalization"+maxhospitalization+"' type='text' id='hospitalhospitalization"+maxhospitalization+"'></td>";
        maketr += "<td>";
        maketr += "<a href='#' class='add add-hospitalization' ><i class='fa fa-fw fa-plus'></i></a>";
        maketr += "<a href='#' class='remove remove-hospitalization' ><i class='fa fa-fw fa-times'></i></a>";
        maketr += "</td></tr>";
        $('#hospitalization-list tbody').append(maketr);
        
        $('.datehospitalization').datepicker({
            autoclose: true
        });
        
        e.preventDefault();
    });
    
    $('#hospitalization-list').on('click','.remove-hospitalization',function(e){
        $(this).parent().parent().remove();
        e.preventDefault();
    });

    var idhistory = $('#family-list tr:last').attr('id');
    var maxfamily = idhistory.replace(/[^0-9\.]+/g, '');

    $('#family-list').on('click','.add-family',function(e){
        var nextFamily = parseInt(maxfamily) + 1;
        maxfamily = nextFamily;
        var maketr = "<tr>"
        maketr += "<td><input name='family_member"+maxfamily+"' type='text' id='family_member"+maxfamily+"'></td>";
        maketr += "<td><input name='familyage"+maxfamily+"' type='text' id='familyage"+maxfamily+"'></td>";
        maketr += "<td>";
        maketr += "<select name='deceased"+maxfamily+"' id='deceased"+maxfamily+"'>";
        maketr += "<option>Select</option>";
        maketr += "<option value='0'>No</option>";
        maketr += "<option value='1'>Yes</option>";
        maketr += "</select>";
        maketr += "</td>";
        maketr += "<td><input name='cause_of_death"+maxfamily+"' type='text' id='cause_of_death"+maxfamily+"'></td>";
        maketr += "<td>"
        maketr += "<select name='familycomplexion"+maxfamily+"' id='familycomplexion"+maxfamily+"'>";
        maketr += "<option>Select</option>";
        maketr += "<option value='1'>Thin</option>";
        maketr += "<option value='2'>Normal weight</option>";
        maketr += "<option value='3'>Slightly overweight</option>";
        maketr += "<option value='4'>Moderately overweight</option>";
        maketr += "</select>";
        maketr += "</td>";
        maketr += "<td><input name='familyhealth"+maxfamily+"' type='text' id='familyhealth"+maxfamily+"'></td>";
        maketr += "<td>";
        maketr += "<a href='#' class='add add-family' ><i class='fa fa-fw fa-plus'></i></a>";
        maketr += "<a href='#' class='remove remove-family' ><i class='fa fa-fw fa-times'></i></a>";
        maketr += "</td></tr>";
        $('#family-list tbody').append(maketr);
            
        e.preventDefault();
    });
    
    $('#family-list').on('click','.remove-family',function(e){
        $(this).parent().parent().remove();
        e.preventDefault();
    });

    var iddigestive = $('#digestive-list tr:last').attr('id');
    var maxdigestive = iddigestive.replace(/[^0-9\.]+/g, '');

    $('#digestive-list').on('click','.add-digestive',function(e){
        var nextdigestive = parseInt(maxdigestive) + 1;
        maxdigestive = nextdigestive;
        var maketr = "<tr>"
        maketr += "<td><input name='digestivefood"+maxdigestive+"' type='text' id='digestivefood"+maxdigestive+"'></td>";
        maketr += "<td><input name='digestiveresult"+maxdigestive+"' type='text' id='digestiveresult"+maxdigestive+"'></td>";            
        maketr += "<td>";
        maketr += "<a href='#' class='add add-digestive' ><i class='fa fa-fw fa-plus'></i></a>";
        maketr += "<a href='#' class='remove remove-digestive' ><i class='fa fa-fw fa-times'></i></a>";
        maketr += "</td></tr>";
        $('#digestive-list tbody').append(maketr);
            
        e.preventDefault();
    });
    
    $('#digestive-list').on('click','.remove-digestive',function(e){
        $(this).parent().parent().remove();
        e.preventDefault();
    });

    var bone = $('#dr-list tr:last').attr('id');
    var maxbone = bone.replace(/[^0-9\.]+/g, '');

    $('#dr-list').on('click','.add-bone',function(e){
        $(".bonedate").datepicker("destroy");
        var nextbone = parseInt(maxbone) + 1;
        maxbone = nextbone;
        var maketr = "<tr><td><input name='bonedoctor"+maxbone+"' type='text' id='bonedoctor"+maxbone+"'></td>";
        maketr += "<td><input class='bonedate' name='bonedate"+maxbone+"' type='text' id='bonedate"+maxbone+"'></td>";            
        maketr += "<td><input name='bonediagnosis"+maxbone+"' type='text' id='bonediagnosis"+maxbone+"'></td>";
        maketr += "<td>";
        maketr += "<a href='#' class='add add-bone' ><i class='fa fa-fw fa-plus'></i></a>";
        maketr += "<a href='#' class='remove remove-bone' ><i class='fa fa-fw fa-times'></i></a>";
        maketr += "</td></tr>";
        $('#dr-list tbody').append(maketr);
        
        $('.bonedate').datepicker({
            autoclose: true
        });
        
        e.preventDefault();
    });
    
    $('#dr-list').on('click','.remove-bone',function(e){
        $(this).parent().parent().remove();
        e.preventDefault();
    });

    $("#height").change(function(){
        if($("#height").val() != "" && $("#weight").val() != ""){
            var height =  parseFloat($("#height").val());
            var mass =  parseFloat($("#weight").val());
            var bmi = 703 * (mass/(height*height));
            $("#bmi").val(bmi.toFixed(2));
        }
    });

    $("#weight").change(function(){
        if($("#weight").val() != "" && $("#height").val() != ""){
            var height =  parseFloat($("#height").val());
            var mass =  parseFloat($("#weight").val());
            var bmi = 703 * (mass/(height*height));
            $("#bmi").val(bmi.toFixed(2));
        }
    });

});