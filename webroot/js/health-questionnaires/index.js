$( document ).ready(function() {     
    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#health-list tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $('#formsearch').submit(function () {
        return false; // return false to cancel form action
    });
});