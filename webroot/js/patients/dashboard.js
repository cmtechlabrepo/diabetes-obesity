/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$( document ).ready(function () {

    // 'use strict';


    $.ajax({
      type: "POST",
      url: 'getcheckups',
      headers: {
          'X-CSRF-Token': csrfToken
      },
      contentType: "application/json; charset=utf-8",
      // data: JSON.stringify({ notification_type: "email", status: status}),
      dataType: "json",
      success: function (data) {
          // $("#notifications-alert").fadeTo(2000, 500).slideUp(500, function(){
          //   $("#notifications-alert").slideUp(500);
          // });
          console.log(data);
          
          line.setData(data);
      },
      error: function (jqXHR) {
          console.log("Algo mal");                
          console.log(jqXHR.responseText);
      }
    });

    var line = new Morris.Line({
      element          : 'line-chart',
      resize           : true,
      // data             : [
      //   { y: '2011 Q1', item1: 2666 },
      //   { y: '2011 Q2', item1: 2778 },
      //   { y: '2011 Q3', item1: 4912 },
      //   { y: '2011 Q4', item1: 3767 },
      //   { y: '2012 Q1', item1: 6810 },
      //   { y: '2012 Q2', item1: 5670 },
      //   { y: '2012 Q3', item1: 4820 },
      //   { y: '2012 Q4', item1: 15073 },
      //   { y: '2013 Q1', item1: 10687 },
      //   { y: '2013 Q2', item1: 8432 }
      // ],
      xkey             : "y",
      ykeys            : ['Weight'],
      labels           : 'y',
      lineColors       : ['#3c8dbc'],
      lineWidth        : 2,
      hideHover        : "false",
      gridTextColor    : '#000',
      gridStrokeWidth  : 0.4,
      pointSize        : 3,
      pointStrokeColors: ['#222d32'],
      gridLineColor    : '#B1B1B1',
      gridTextFamily   : 'Open Sans',
      gridTextSize     : 10
    });

    $('#calendar').fullCalendar({
        header    : {
          left  : 'prev,next today',
          center: 'title',
          right : 'month,agendaWeek,agendaDay'
        },
        buttonText: {
          today: 'today',
          month: 'month',
          week : 'week',
          day  : 'day'
        },
        events: {
            url: '/medfile/appointments/getappointments',
            method: 'POST',
            headers: {
                'X-CSRF-Token': csrfToken
            },
            failure: function() {
              alert('there was an error while fetching events!');
            },
            color: 'yellow',   // a non-ajax option
            textColor: 'white' // a non-ajax option
        },
        editable  : false,
        droppable : false, // this allows things to be dropped onto the calendar !!!
      })

      $('#email').change(function() {
        var status = 0;
        if($(this).is(":checked")) {
            console.log("email: on");
            status = 1;
        }

        $.ajax({
          type: "POST",
          url: 'notificationstoggle',
          headers: {
              'X-CSRF-Token': csrfToken
          },
          contentType: "application/json; charset=utf-8",
          data: JSON.stringify({ notification_type: "email", status: status}),
          dataType: "json",
          success: function (data) {
              $("#notifications-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#notifications-alert").slideUp(500);
              });
          },
          error: function (jqXHR) {
              console.log("Algo mal");                
              console.log(jqXHR.responseText);
          }
        });

      });

      $('#sms').change(function() {
        var status = 0;
        if($(this).is(":checked")) {
            console.log("sms: on");
            status = 1;
        }

        $.ajax({
          type: "POST",
          url: 'notificationstoggle',
          headers: {
              'X-CSRF-Token': csrfToken
          },
          contentType: "application/json; charset=utf-8",
          data: JSON.stringify({ notification_type: "sms", status: status}),
          dataType: "json",
          success: function (data) {
              console.log("todo bien");
              console.log(data);
              $("#notifications-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#notifications-alert").slideUp(500);
              });
          },
          error: function (jqXHR) {
              console.log("Algo mal");                
              console.log(jqXHR.responseText);
          }
        }); 
      });

      $("#new-photo").change(function () {
          var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
          if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
              alert("Only formats are allowed : "+fileExtension.join(', '));
              $("#new-photo").val('');
          }
      });

      $('#upload-photo').click(function(){    
        //on change event  
        var formdata = new FormData();
        if($("#new-photo").prop('files').length > 0)
        {
            var file = $("#new-photo").prop('files')[0];
            formdata.append("photo", file);

            $.ajax({
              url: "uploadphoto",
              headers: {
                'X-CSRF-Token': csrfToken
              },
              type: "POST",
              data: formdata,
              processData: false,
              contentType: false,
              success: function (result) {
                   location.reload();
              }
          });
        }else{
          alert("Please select a valid image file");
        }
      });

  });
  