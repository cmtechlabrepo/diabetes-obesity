$( document ).ready(function() {     
    $('#fecha_cirugia,#pago').datepicker({
        autoclose: true
    });

    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#patients-list tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $('#formsearch').submit(function () {
        return false; // return false to cancel form action
    });

    var patientId;
    var surgery_date; 

    $("a[id^='patient']").click(function(){
        surgery_date = "";
        patientId = this.id.replace("patient-","");
        $.ajax({
            type: "POST",
            url: 'patients/get_patient',
            headers: {
                'X-CSRF-Token': csrfToken
            },
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ patientId: patientId}),
            dataType: "json",
            success: function (data) {
                // console.log("todo bien");
                // console.log(data);
                $("#llamada").val(data.primera_llamada);
                $("#pago").val(data.primer_pago);
                $("#comentarios").val(data.comentarios_llamada);
                
                if(data.fecha_cirugia_autorizada != null && data.fecha_cirugia_autorizada != ""){
                    var dateParts = data.fecha_cirugia_autorizada.substr(0,10).split("-");
                    var jsDate = dateParts[1]+"/"+dateParts[2]+"/"+dateParts[0];
                    surgery_date = jsDate;

                    var dateParts2 = data.primer_pago.substr(0,10).split("-");
                    var jsDate2 = dateParts2[1]+"/"+dateParts2[2]+"/"+dateParts2[0];
                    payment_date = jsDate2;

                    $("#pago").val(payment_date);
                    $("#fecha_cirugia").val(surgery_date);
                    // console.log("Fecha: Js "+jsDate);
                    // console.log("Fecha: "+surgery_date);
                    $('#fecha_cirugia').keyup();
                    $("#pago").keyup();
                }
            },
            error: function (jqXHR) {
                console.log("Algo mal");                
                console.log(jqXHR.responseText);
            }
        });
    });

    var csrfToken = $('[name="_csrfToken"]').val();

    $("#save").click(function(){
        console.log(patientId);
        var llamada = $("#llamada").val();
        var pago = $("#pago").val();
        var metodo_pago = $("#metodo_pago").val();
        var fecha = $("#fecha_cirugia").val();
        var comentarios = $("#comentarios").val();
        $.ajax({
            type: "POST",
            url: 'patients/save_admin',
            headers: {
                'X-CSRF-Token': csrfToken
            },
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ patientId: patientId, llamada: llamada , pago:pago, fecha: fecha, comentarios: comentarios,metodo_pago:metodo_pago}),
            dataType: "json",
            success: function (data) {
                console.log("todo bien");
                console.log(data);
                if(data.saved == 1){
                    $("#saved").show("fadeup");
                }
            },
            error: function (jqXHR) {
                console.log("Algo mal");                
                console.log(jqXHR.responseText);
            }
        });

        if(changeSurgery == 1){
            console.log("Si se va a cambiar");
            $.ajax({
                type: "POST",
                url: 'patients/change_date',
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ patientId: patientId, fecha_original: surgery_date, fecha_nueva:newDate}),
                dataType: "json",
                success: function (data) {
                    console.log("todo bien postpuesta");
                    console.log(data);
                    if(data.changed == 1){
                        $("#saved").show("fadeup");
                    }
                },
                error: function (jqXHR) {
                    console.log("Algo mal");                
                    console.log(jqXHR.responseText);
                }
            });
            // clearForm();
            // $('.modal').modal('toggle');
        }

        changeSurgery = 0;
        newDate = "";
        clearForm();

        //Refrescamos para que se vean los cambios en la tabla
        //location.reload(true);
    });

     function clearForm(){
        $("#llamada").val("");
        $("#pago").val("");
        $("#comentarios").val("");
        $("#fecha_cirugia").val("");
        console.log("se limpio el form");
     }

     var changeSurgery = 0;
     var newDate;

     $('#fecha_cirugia').change(function(){
        //changeSurgery = 1;
        newDate = $("#fecha_cirugia").val();
        console.log("Cambiando");
        if(surgery_date != "" && newDate != surgery_date){
            changeSurgery = 1;
            console.log("cambio la fecha:"+patientId);
        }
     });
     

     $('.modal').on('hidden.bs.modal', function (e) {
        console.log("Cerro modal");
        clearForm();
        surgery_date = "";
      })

});