$( document ).ready(function() {     

    var idreferrals = $('#referral-list tr:last').attr('id');
    var maxreferrals = idreferrals.replace(/[^0-9\.]+/g, '');

    $('#referral-list').on('click','.add-referral',function(e){
        var nextreferral = parseInt(maxreferrals) + 1;
        maxreferrals = nextreferral;
        var maketr = "<tr><td><input name='email"+nextreferral+"' type='text' id='email"+nextreferral+"'></td>";
        maketr += "<td><select name='type"+nextreferral+"' id='type"+nextreferral+"'>";
        maketr += "<option>Select</option>";
        maketr += "<option value=1>Friend</option>";
        maketr += "<option value=1>Family</option>";
        maketr == "</select></td>";
        maketr += "<td>";
        maketr += "<a href='#' class='add add-referral' ><i class='fa fa-fw fa-plus'></i></a>";
        maketr += "<a href='#' class='remove remove-referral' ><i class='fa fa-fw fa-times'></i></a>";
        maketr += "</td></tr>";
        $('#referral-list tbody').append(maketr);
        
        e.preventDefault();
    });
    
    $('#referral-list').on('click','.remove-referral',function(e){
        $(this).parent().parent().remove();
        e.preventDefault();
    });

});