$( document ).ready(function() {     
    /* initialize the calendar
     -----------------------------------------------------------------*/
    $('#calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week : 'week',
        day  : 'day'
      },
      events: {
          url: '/bake2/appointments/getappointments',
          method: 'POST',
          headers: {
              'X-CSRF-Token': csrfToken
          },
          failure: function() {
            alert('there was an error while fetching events!');
          },
          color: 'yellow',   // a non-ajax option
          textColor: 'white' // a non-ajax option
      },
      editable  : false,
      droppable : false, // this allows things to be dropped onto the calendar !!!
    //   eventDrop: function(info) {
    //       //alert(info.event.title + " was dropped on " + info.event.start.toISOString()) // this function is called when something is dropped

    //       var eventId = info.id;
    //       var fecha = info.start._i;

    //       console.log(info);
          
    //       var fechaString = fecha[0]+"-"+(fecha[1]+1)+"-"+fecha[2];
    //       var timeString = fecha[3]+":"+fecha[4];

    //       console.log(fechaString);
    //       console.log(timeString);

    //       $.ajax({
    //           type: "POST",
    //           url: '/bake2/appointments/change',
    //           headers: {
    //               'X-CSRF-Token': csrfToken
    //           },
    //           contentType: "application/json; charset=utf-8",
    //           data: JSON.stringify({ id: eventId, date:fechaString,time:timeString}),
    //           dataType: "json",
    //           success: function (data) {
    //               console.log("todo bien cambiado");
    //           },
    //           error: function (jqXHR) {
    //               console.log("Algo mal");                
    //               console.log(jqXHR.responseText);
    //           }
    //       });
    //   }
    })


});