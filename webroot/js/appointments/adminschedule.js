$( document ).ready(function() {

    $('#calendar').fullCalendar({
        header    : {
            left  : 'prev,next today',
            center: 'title',
            right : 'month,agendaWeek,agendaDay'
        },
        buttonText: {
            today: 'today',
            month: 'month',
            week : 'week',
            day  : 'day'
        },
        events: {
            url: '/medfile/appointments/getappointments',            
            method: 'POST',
            // data: {start_date:$("#start_date").val(),end_date:$("#date2").val(),patient_id:$("#patient").val(),doctor_id:$("#patient").val()},
            headers: {
                'X-CSRF-Token': csrfToken
            },
            failure: function() {
                alert('there was an error while fetching events!');
            },
            color: 'yellow',   // a non-ajax option
            textColor: 'white' // a non-ajax option
        },
        editable  : true,
        droppable : true, // this allows things to be dropped onto the calendar !!!
        eventDrop: function(info) {

            var eventId = info.id;
            var fecha = info.start._i;

            console.log(info);
            
            var fechaString = fecha[0]+"-"+(fecha[1]+1)+"-"+fecha[2];
            var timeString = fecha[3]+":"+fecha[4];

            console.log(fechaString);
            console.log(timeString);

            $.ajax({
                type: "POST",
                url: '/medfile/appointments/change',
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ id: eventId, date:fechaString,time:timeString}),
                dataType: "json",
                success: function (data) {
                    console.log("todo bien cambiado");
                },
                error: function (jqXHR) {
                    console.log("Algo mal");                
                    console.log(jqXHR.responseText);
                }
            });
        },
        eventRender: function(event, element) {
            element.bind('dblclick', function() {
               var eventId = event.id;
               if (confirm("You are about to delete an appointment!")) {
                $.ajax({
                    type: "POST",
                    url: '/medfile/appointments/delete',
                    headers: {
                        'X-CSRF-Token': csrfToken
                    },
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ id: eventId}),
                    dataType: "json",
                    success: function (data) {
                        console.log("todo bien borrado");
                        location.reload();
                    },
                    error: function (jqXHR) {
                        console.log("Algo mal");                
                        console.log(jqXHR.responseText);
                    }
                });
            }
          });          
         }
    });

    $('#date2,#start_date').datepicker(
        {autoclose: true}
    );

    $("#filter").click(function(e){
        $('#calendar').fullCalendar('removeEvents');
        $('#calendar').fullCalendar('addEventSource', {
            url: '/medfile/appointments/filterappointments',            
            method: 'POST',
            data: {start_date:$("#start_date").val(),end_date:$("#date2").val(),patient_id:$("#patient").val(),doctor_id:$("#doctor").val()},
            headers: {
                'X-CSRF-Token': csrfToken
            },
            failure: function() {
                alert('there was an error while fetching events!');
            },
            color: 'yellow',   // a non-ajax option
            textColor: 'white' // a non-ajax option
        })
        $('#calendar').fullCalendar('rerenderEvents');

        e.preventDefault();
    });
});