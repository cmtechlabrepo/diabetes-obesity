$( document ).ready(function() {   


    $("#changepwForm").submit(function () {
        if (($("#new-password").val() != "" && $("#confirm-password").val() != "" ) && ($("#new-password").val() == $("#confirm-password").val())) {
            return true;
        }else{
            $("#error-pws").show("fadeup");
            $("#new-password").css('border-color','red'); 
            $("#confirm-password").css('border-color','red');
            return false;
        }
    });

});