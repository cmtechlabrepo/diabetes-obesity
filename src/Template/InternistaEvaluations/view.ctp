<section class="content-header">
  <h1>
    Internista Evaluation
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $internistaEvaluation->has('patient') ? $this->Html->link($internistaEvaluation->patient->name, ['controller' => 'Patients', 'action' => 'view', $internistaEvaluation->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Doctor') ?></dt>
            <dd><?= $internistaEvaluation->has('doctor') ? $this->Html->link($internistaEvaluation->doctor->name, ['controller' => 'Doctors', 'action' => 'view', $internistaEvaluation->doctor->id]) : '' ?></dd>
            <dt scope="row"><?= __('Procedure') ?></dt>
            <dd><?= $internistaEvaluation->has('procedure') ? $this->Html->link($internistaEvaluation->procedure->name, ['controller' => 'Procedures', 'action' => 'view', $internistaEvaluation->procedure->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $internistaEvaluation->has('user') ? $this->Html->link($internistaEvaluation->user->name, ['controller' => 'Users', 'action' => 'view', $internistaEvaluation->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($internistaEvaluation->id) ?></dd>
            <dt scope="row"><?= __('Recomendaciones') ?></dt>
            <dd><?= $this->Number->format($internistaEvaluation->recomendaciones) ?></dd>
            <dt scope="row"><?= __('Procedure Date') ?></dt>
            <dd><?= h($internistaEvaluation->procedure_date) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($internistaEvaluation->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($internistaEvaluation->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($internistaEvaluation->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Medical History') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($internistaEvaluation->medical_history); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Anotaciones Fisicas') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($internistaEvaluation->anotaciones_fisicas); ?>
        </div>
      </div>
    </div>
  </div>
</section>
