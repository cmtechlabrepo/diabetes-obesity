<section class="content-header">
  <h1>
    Major Illness
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Illness') ?></dt>
            <dd><?= h($majorIllness->illness) ?></dd>
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $majorIllness->has('patient') ? $this->Html->link($majorIllness->patient->name, ['controller' => 'Patients', 'action' => 'view', $majorIllness->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $majorIllness->has('user') ? $this->Html->link($majorIllness->user->name, ['controller' => 'Users', 'action' => 'view', $majorIllness->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($majorIllness->id) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($majorIllness->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($majorIllness->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($majorIllness->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Treatment') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($majorIllness->treatment); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Outcome') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($majorIllness->outcome); ?>
        </div>
      </div>
    </div>
  </div>
</section>
