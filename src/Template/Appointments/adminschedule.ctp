<?php echo $this->Html->script('appointments/adminschedule.js'); ?>
<?php echo $this->Html->script('appointments/add.js'); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.custom-combobox {
    position: relative;
    display: inline-block;
}

.custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}

.custom-combobox-input {
    margin-left: 10px;
    padding: 5px 10px;
}
</style>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- /.col -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo __('Appointments'); ?></h3>
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li  class="active"><a href="#add" data-toggle="tab">Add</a></li>
                    <li><a href="#search" data-toggle="tab">Search</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="add">
                        <div class="box box-solid">
                            <!-- /.box-header -->
                            <!-- form start -->
                            <?php echo $this->Form->create($appointment, ['role' => 'form','id'=>'add-appointment']); ?>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <?php echo $this->Form->control('patient_id', ['options' => $patients, 'empty' => true]); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <?php echo $this->Form->control('doctor_id', ['options' => $doctors, 'empty' => true]); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <?php echo $this->Form->control('date', ['type'=>'text','empty' => true,'autocomplete'=>"off"]);?>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <?php echo $this->Form->control('time', ['type'=>'text','empty' => true,'autocomplete'=>"off"]);?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <?php echo $this->Form->control('comment');?>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.box-body -->
                            <?php echo $this->Form->submit(__('Save',['id'=>'save'])); ?>

                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                    <!--Cierra Add-->
                    <div class="tab-pane" id="search">
                        <div class="box-body">
                            <b>Search by patient, by doctor, by dates or combine.<b>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <?php echo $this->Form->control('patient', ['options' => $patients, 'empty' => true]); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <?php echo $this->Form->control('doctor', ['options' => $doctors, 'empty' => true]); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <?php echo $this->Form->control('date', ['label'=>"Start date","id"=>'start_date','type'=>'text','empty' => true,'autocomplete'=>"off"]);?>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <?php echo $this->Form->control('date2', ['label'=>"End date",'type'=>'text','empty' => true,'autocomplete'=>"off"]);?>
                                </div>
                            </div>
                        </div>
                        <input type="submit" id="filter" class="btn btn-success" value="Search">
                    </div>
                    <!--Cierra Search -->
                </div>
            </div>
        </div>


        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
        </div>
    </div>
</section>

<!-- fullCalendar -->
<?php echo $this->Html->css('AdminLTE./bower_components/fullcalendar/dist/fullcalendar.min', ['block' => 'css']); ?>
<?php echo $this->Html->css('AdminLTE./bower_components/fullcalendar/dist/fullcalendar.print.min', ['block' => 'css', 'media' => 'print']); ?>

<!-- jQuery UI 1.11.4 -->
<?php echo $this->Html->script('AdminLTE./bower_components/jquery-ui/jquery-ui.min', ['block' => 'script']); ?>
<!-- fullCalendar -->
<?php echo $this->Html->script('AdminLTE./bower_components/moment/moment', ['block' => 'script']); ?>
<?php echo $this->Html->script('AdminLTE./bower_components/fullcalendar/dist/fullcalendar.min', ['block' => 'script']); ?>

