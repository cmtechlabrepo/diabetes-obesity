<section class="content-header">
  <h1>
    Patients Lab File
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $patientsLabFile->has('patient') ? $this->Html->link($patientsLabFile->patient->name, ['controller' => 'Patients', 'action' => 'view', $patientsLabFile->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Doctor') ?></dt>
            <dd><?= $patientsLabFile->has('doctor') ? $this->Html->link($patientsLabFile->doctor->name, ['controller' => 'Doctors', 'action' => 'view', $patientsLabFile->doctor->id]) : '' ?></dd>
            <dt scope="row"><?= __('File') ?></dt>
            <dd><?= h($patientsLabFile->file) ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $patientsLabFile->has('user') ? $this->Html->link($patientsLabFile->user->name, ['controller' => 'Users', 'action' => 'view', $patientsLabFile->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($patientsLabFile->id) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($patientsLabFile->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($patientsLabFile->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($patientsLabFile->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
