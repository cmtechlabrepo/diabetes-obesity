<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Patients Lab Tests

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'addforpatient'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                  <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                  <th scope="col"><?= $this->Paginator->sort('patient_id') ?></th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('lab_test_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('value') ?></th> -->
                  <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('doctor') ?></th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('modified') ?></th> -->
                  <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($patientsLabTests as $patientsLabTest): ?>
                <tr>
                  <!-- <td><?= $this->Number->format($patientsLabTest->id) ?></td> -->
                  <td><?= $patientsLabTest->has('patient') ? $this->Html->link($patientsLabTest->patient->name, ['controller' => 'Patients', 'action' => 'view', $patientsLabTest->patient->id]) : '' ?></td>
                  <!-- <td><?= $patientsLabTest->has('lab_test') ? $this->Html->link($patientsLabTest->patient->id, ['controller' => 'LabTests', 'action' => 'view', $patientsLabTest->lab_test->id]) : '' ?></td>
                  <td><?= h($patientsLabTest->value) ?></td> -->
                  <td><?= h($patientsLabTest->date) ?></td>
                  <td><?= h($patientsLabTest->doctor_id) ?></td>
                  <!-- <td><?= h($patientsLabTest->created) ?></td>
                  <td><?= h($patientsLabTest->modified) ?></td> -->
                  <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['action' => 'viewlabtest',$patientsLabTest->patient->id,date("Y-m-d",strtotime($patientsLabTest->date))], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'editlabs', $patientsLabTest->patient->id,date("Y-m-d",strtotime($patientsLabTest->date))], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $patientsLabTest->patient->id,date("Y-m-d",strtotime($patientsLabTest->date))], ['confirm' => __('Are you sure you want to delete this?', $patientsLabTest->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->        

      </div>
      <!-- /.box -->
    </div>
  </div>
</section>