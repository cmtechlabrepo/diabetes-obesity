<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PatientsLabTest $patientsLabTest
 */
?>
<?php echo $this->Html->script('patients-lab-tests/addforpatient.js'); ?>
<!-- Content Header (Page header) -->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.custom-combobox {
    position: relative;
    display: inline-block;
}

.custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}

.custom-combobox-input {
    margin-left: 10px;
    padding: 5px 10px;
}
</style>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<section class="content-header">
    <h1>
        Laboratorios Clinicos
        <!-- <small><?php echo __('Add'); ?></small> -->
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i>
                <?php echo __('Home'); ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <!-- <h3 class="box-title"><?php echo __('Form'); ?></h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create("",['role' => 'form']); ?>
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <?php
                                echo $this->Form->control('patient_id', ['options' => $patients, 'empty' => true]);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <?php
                                echo $this->Form->control('doctor_id',['label'=>'Doctor','options'=>$doctors,'empty'=>true]);
                            ?>
                        </div>
                    </div>
                    <h3>Quimica Sanguinea de 4 elementos</h3>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="1">Glucosa</label>
                                <input type="text" name="1" class="form-control" id="1" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="2">Urea</label>
                                <input type="text" name="2" class="form-control" id="2" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="3">Nitrogeno Ureico</label>
                                <input type="text" name="3" class="form-control" id="3" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="4">Creatinina Serica</label>
                                <input type="text" name="4" class="form-control" id="4" />
                            </div>
                        </div>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="5">Acido Urico</label>
                                <input type="text" name="5" class="form-control" id="5" />
                            </div>
                        </div>
                    </div>
                    <h3>Electrolitos Sericos</h3>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="6">Calcio</label>
                                <input type="text" name="6" class="form-control" id="6" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="7">Fosforo</label>
                                <input type="text" name="7" class="form-control" id="7" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="8">Magnesio</label>
                                <input type="text" name="8" class="form-control" id="8" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="9">
                                    Eco2</label>
                                <input type="text" name="9" class="form-control" id="9" />
                            </div>
                        </div>

                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="10">
                                    Sodio</label>
                                <input type="text" name="10" class="form-control" id="10" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="11">
                                    Potasio</label>
                                <input type="text" name="11" class="form-control" id="11" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="12">
                                    Cloro</label>
                                <input type="text" name="12" class="form-control" id="12" />
                            </div>
                        </div>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="13">
                                    Anion Gapk</label>
                                <input type="text" name="13" class="form-control" id="13" />
                            </div>
                        </div>
                    </div>
                    <h3>Lipoproteinas</h3>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="14">
                                    Colesterol Total</label>
                                <input type="text" name="14" class="form-control" id="14" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="15">
                                    Trigliceridos</label>
                                <input type="text" name="15" class="form-control" id="15" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="16">
                                    Colesterol Hdl</label>
                                <input type="text" name="16" class="form-control" id="16" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="17">
                                    Colesterol Ldl</label>
                                <input type="text" name="17" class="form-control" id="17" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="18">
                                    Colesterol Muy Baja Densidad</label>
                                <input type="text" name="18" class="form-control" id="18" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="19">
                                    Indice Aterogenico</label>
                                <input type="text" name="19" class="form-control" id="19" />
                            </div>
                        </div>
                    </div>
                    <h3>Perfil de funcionamiento hepatico</h3>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="20">
                                    Bilirrubina Total</label>
                                <input type="text" name="20" class="form-control" id="20" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="21">
                                    Bilirrubina Directa</label>
                                <input type="text" name="21" class="form-control" id="21" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="22">
                                    Bilirrubina Indirecta</label>
                                <input type="text" name="22" class="form-control" id="22" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="23">
                                    Proteinas Totales</label>
                                <input type="text" name="23" class="form-control" id="23" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="24">
                                    Albumina</label>
                                <input type="text" name="24" class="form-control" id="24" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="25">
                                    Globulinas</label>
                                <input type="text" name="25" class="form-control" id="25" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="26">
                                    Relacion Ag</label>
                                <input type="text" name="26" class="form-control" id="26" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="27">
                                    Fosfatasa Alcalina</label>
                                <input type="text" name="27" class="form-control" id="27" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="28">
                                    Gamma Glutamil Transpeptidasa</label>
                                <input type="text" name="28" class="form-control" id="28" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="29">
                                    Alanino Amino Transferasa</label>
                                <input type="text" name="29" class="form-control" id="29" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="30">
                                    Asparto Amino Transferasa</label>
                                <input type="text" name="30" class="form-control" id="30" />
                            </div>
                        </div>

                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="31">
                                    Deshidrogenasa Lactica</label>
                                <input type="text" name="31" class="form-control" id="31" />
                            </div>
                        </div>
                    </div>
                    <h3>Perfil Pancreatico</h3>
                    <div class='row'>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="32">
                                    Amilasa</label>
                                <input type="text" name="32" class="form-control" id="32" />
                            </div>
                        </div>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="33">
                                    Lipasa</label>
                                <input type="text" name="33" class="form-control" id="33" />
                            </div>
                        </div>
                    </div>
                    <h3>Perfil Cardiaco</h3>
                    <div class='row'>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="34">
                                    Creatinfosfoquinasa</label>
                                <input type="text" name="34" class="form-control" id="34" />
                            </div>
                        </div>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="35">
                                    Creatincinasa Fraccion Mb</label>
                                <input type="text" name="35" class="form-control" id="35" />
                            </div>
                        </div>
                    </div>

                    <h2>Biometria Hematica C.</h2>
                    <h3>Formula Blanca</h3>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="36">
                                    Leucocitos</label>
                                <input type="text" name="36" class="form-control" id="36" />
                            </div>
                        </div>

                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="37">
                                    Neutrofilos %</label>
                                <input type="text" name="37" class="form-control" id="37" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="38">
                                    Linfocitos %</label>
                                <input type="text" name="38" class="form-control" id="38" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="39">
                                    Monocitos %</label>
                                <input type="text" name="39" class="form-control" id="39" />
                            </div>
                        </div>

                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="40">
                                    Eosinofilos %</label>
                                <input type="text" name="40" class="form-control" id="40" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="41">
                                    Basofilos %</label>
                                <input type="text" name="41" class="form-control" id="41" />
                            </div>
                        </div>
                    </div>
                    <h3>Valores Absolutos</h3>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="42">
                                    Neutrofilos</label>
                                <input type="text" name="42" class="form-control" id="42" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="43">
                                    Linfocitos</label>
                                <input type="text" name="43" class="form-control" id="43" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="44">
                                    Monocitos</label>
                                <input type="text" name="44" class="form-control" id="44" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="45">
                                    Eosinofilos</label>
                                <input type="text" name="45" class="form-control" id="45" />
                            </div>
                        </div>

                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="46">
                                    Basofilos</label>
                                <input type="text" name="46" class="form-control" id="46" />
                            </div>
                        </div>
                    </div>

                    <h3>Formula Roja</h3>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="47">
                                    Eritrocitos</label>
                                <input type="text" name="47" class="form-control" id="47" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="48">
                                    Hemoglobina</label>
                                <input type="text" name="48" class="form-control" id="48" />
                            </div>
                        </div>

                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="49">
                                    Hematocrito</label>
                                <input type="text" name="49" class="form-control" id="49" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="50">
                                    Mcv</label>
                                <input type="text" name="50" class="form-control" id="50" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="51">
                                    Mch</label>
                                <input type="text" name="51" class="form-control" id="51" />
                            </div>
                        </div>

                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="52">
                                    Mchc</label>
                                <input type="text" name="52" class="form-control" id="52" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="53">
                                    Rdw</label>
                                <input type="text" name="53" class="form-control" id="53" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="54">
                                    Plaquetas</label>
                                <input type="text" name="54" class="form-control" id="54" />
                            </div>
                        </div>

                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="55">
                                    Volumen Plaquetar Medio</label>
                                <input type="text" name="55" class="form-control" id="55" />
                            </div>
                        </div>
                    </div>

                    <h3>Grupo y RH</h3>
                    <div class='row'>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="56">
                                    Grupo Sanguineo</label>
                                <input type="text" name="56" class="form-control" id="56" />
                            </div>
                        </div>
                        <div class='col-md-6 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="57">
                                    Rh</label>
                                <input type="text" name="57" class="form-control" id="57" />
                            </div>
                        </div>
                    </div>

                    <h3>Perfil de coagulacion</h3>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="58">
                                    Tiempo De Promtombina</label>
                                <input type="text" name="58" class="form-control" id="58" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="59">
                                    Tiempo De Promtombina %</label>
                                <input type="text" name="59" class="form-control" id="59" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="60">
                                    Inr</label>
                                <input type="text" name="60" class="form-control" id="60" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="61">
                                    Tiempo De Tromboplastina Parcial Activada</label>
                                <input type="text" name="61" class="form-control" id="61" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="62">
                                    Testigo Tp</label>
                                <input type="text" name="62" class="form-control" id="62" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="63">
                                    Isi</label>
                                <input type="text" name="63" class="form-control" id="63" />
                            </div>
                        </div>
                    </div>

                    <h3>Perfil tiroideo</h3>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="64">
                                    Tsh</label>
                                <input type="text" name="64" class="form-control" id="64" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="65">
                                    T4</label>
                                <input type="text" name="65" class="form-control" id="65" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="66">
                                    T3</label>
                                <input type="text" name="66" class="form-control" id="66" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="67">
                                    T4 Libre</label>
                                <input type="text" name="67" class="form-control" id="67" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="68">
                                    T3 Libre</label>
                                <input type="text" name="68" class="form-control" id="68" />
                            </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <div class="form-group input text">
                                <label class="control-label" for="69">
                                    Captacion T3</label>
                                <input type="text" name="69" class="form-control" id="69" />
                            </div>
                        </div>
                    </div>

                    <?php
                // $numRow = 0;
                // $numCol = 0;
                // foreach($LabTestsData as $key => $value){
                //     $value = str_replace('_'," ", $value);
                //     $value = str_replace('pct',"%", $value);
                //     $value = ucwords($value);
                //     if($numRow == 0){
                //         echo "<div class='row'>";
                //     }
                //     echo "<div class='col-md-4 col-xs-12'>";
                //     echo $this->Form->control($key,["label"=>$value]);
                //     echo "</div>";
                //     if($numCol < 2){                        
                //         $numCol++;
                //         $numRow = 1;
                //     }else {                        
                //         $numCol = 0;
                //         $numRow = 0;
                //         echo "</div>";
                //     }
                // }
              ?>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>