<section class="content-header">
  <h1>
    Patients Lab Test
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $patientsLabTest->has('patient') ? $this->Html->link($patientsLabTest->patient->name, ['controller' => 'Patients', 'action' => 'view', $patientsLabTest->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Lab Test') ?></dt>
            <dd><?= $patientsLabTest->has('lab_test') ? $this->Html->link($patientsLabTest->lab_test->id, ['controller' => 'LabTests', 'action' => 'view', $patientsLabTest->lab_test->id]) : '' ?></dd>
            <dt scope="row"><?= __('Value') ?></dt>
            <dd><?= h($patientsLabTest->value) ?></dd>
            <dt scope="row"><?= __('Doctor') ?></dt>
            <dd><?= h($patientsLabTest->doctor) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($patientsLabTest->id) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($patientsLabTest->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($patientsLabTest->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($patientsLabTest->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
