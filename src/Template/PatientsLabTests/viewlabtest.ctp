<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th rowspan=2>Estudio</th>
                            <th rowspan=2>Valor</th>
                            <th rowspan=2>Unidades</th>
                            <th colspan=2>Valores de referencia</th>
                        </tr>
                        <tr>
                            <th>Limite Inferior</th>
                            <th>Limite Superior</th>
                        </tr>
                        </tr>
                        <?php foreach($LabTestsData as $l){  
                            $l['estudio'] = str_replace("_"," ",$l['estudio']);
                            $l['estudio'] = str_replace("pct","%",$l['estudio']);
                            ?>
                        <tr>
                            <td><?= ucwords($l['estudio']); ?></td>
                            <td><?= $values[$l['id']]; ?></td>
                            <td><?= ucwords($l['unidades']); ?></td>
                            <td><?= ucwords($l['limite_inferior']); ?></td>
                            <td><?= ucwords($l['limite_superior']); ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>