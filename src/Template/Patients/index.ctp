
<div id="saved" class="alert alert-success alert-dismissible collapse">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    Saved Successfully!
</div>

<?php echo $this->Html->script('patients/index.js'); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Patients

        <div class="pull-right">
            <?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('List'); ?></h3>
                    <div class="box-tools">
                        <form action="<?php echo $this->Url->build(); ?>" id='formsearch' method="POST">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" id='search' class="form-control pull-right"
                                    placeholder="<?php echo __('Search'); ?>">

                                <!-- <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div> -->
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <!-- <th scope="col"><?= $this->Paginator->sort('location') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('surgeon') ?></th> -->
                                <th scope="col"><?= $this->Paginator->sort('primera_llamada') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('primer_pago') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('fecha_cirugia_autorizada') ?></th>
                                
                                <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody id='patients-list'>
                            <?php 
                            $yesno = array(""=>"No",0=>'No',1=>'Yes');
                            foreach ($patients as $patient){ ?>
                            <tr>
                                <td><?= $this->Number->format($patient->id) ?></td>
                                <!-- <td><?= h($patient->location) ?></td>
                                <td><?= h($patient->doctor->name) ?></td> -->
                                <td><?= h($patient->name) ?></td>
                                <td><?= h($yesno[$patient->primera_llamada]) ?></td>
                                <td><?= h($patient->primer_pago) ?></td>
                                <td><?= h($patient->fecha_cirugia_autorizada) ?></td>
                                <td class="actions text-right">
                                <?php 
                                    $role = $this->request->session()->read('Auth.User')['role_id']; 
                                    if(in_array($role,array(1,2))){ ?>
                                        <a href="#" id='<?= "patient-".$patient->id; ?>' class="btn bg-maroon btn-xs"
                                            data-toggle="modal" data-target=".bd-example-modal-lg">Admin Actions
                                        </a>
                                    <?php 
                                    }
                                    //print_r($patient); echo "<br>";
                                    // echo $this->Html->link(__('Edit Health Questionnaire'), ['controller'=>'HealthQuestionnaires','action' => 'edit', $patient->healthquestionnaires->id], ['class'=>'btn btn-primary btn-xs']);
                                    if(isset($patient['health_questionnaires'][0]['id'])){ 
                                        echo $this->Html->link(__('Edit Health Questionnaire'), ['controller'=>'HealthQuestionnaires','action' => 'edit', isset($patient['health_questionnaires'][0]['id'])], ['class'=>'btn btn-primary btn-xs']);
                                    }
                                    else{ 
                                        echo $this->Html->link(__('Add Health Questionnaire'), ['controller'=>'HealthQuestionnaires','action' => 'addfrompatient',$patient->id], ['class'=>'btn btn-primary btn-xs']);
                                    } ?>
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $patient->id], ['class'=>'btn btn-info btn-xs']) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $patient->id], ['class'=>'btn btn-warning btn-xs']) ?>
                                    <?php if(in_array($role,array(1,2))){ ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $patient->id], ['confirm' => __('Are you sure you want to delete # {0}?', $patient->id), 'class'=>'btn btn-danger btn-xs']) ?>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <!-- Large modal -->
                <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button> -->
                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
                    aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content" style="padding:20px;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="modal-title">Acciones Administrativas</h3>
                            </div>
                            <div class="modal-body">
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <th>Llamada realizada</th>
                                                <td>
                                                    <select name="llamada" class="form-control" id="llamada">
                                                        <option value="">Select</option>
                                                        <option value="0">No</option>
                                                        <option value="1">Yes</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Comentarios de llamada</th>
                                                <td>
                                                    <textarea name="comentarios" class="form-control" id="comentarios"
                                                        rows="5"></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Fecha de pago</th>
                                                <td>
                                                    <input type="text" name="pago" id="pago"
                                                        class="form-control" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Metodo de pago</th>
                                                <td>
                                                    <select name="metodo_pago" class="form-control" id="metodo_pago">
                                                        <option value="">Select</option>
                                                        <option value="1">Wells Fargo</option>
                                                        <option value="2">Paypal</option>
                                                        <option value="3">Efectivo</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Fecha de cirugia</th>
                                                <td>
                                                    <input type="text" name="fecha_cirugia" id="fecha_cirugia"
                                                        class="form-control" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a href="#" id='save' class="btn btn-primary" data-toggle="modal"
                                    data-target=".bd-example-modal-lg">Save
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>