<!-- Content Header (Page header) -->
<style>
.profile-user-img{
  width:200px;
  height:200px;
}
</style>
<?php echo $this->Html->script('patients/dashboard.js'); ?>
<section class="content-header">
      <h1>
        My Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li><a href="#">Examples</a></li> -->
        <li class="active">My profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

              <?php 
              $image = $this->Url->build("/..".$picture);
              echo $this->Html->image($image, ['class' => 'profile-user-img img-responsive img-circle', 'alt' => 'User profile picture']); ?>

              <h3 class="profile-username text-center"><?php echo $this->request->getSession()->read('Auth.User')['name'];  ?></h3>

              <!-- <p class="text-muted text-center">Software Engineer</p> -->
              <a href="#" class="btn btn-primary btn-block"  data-toggle="modal" data-target="#exampleModal"><b>Upload new photo</b></a>
              <?php
                if($patient->fecha_cirugia_autorizada != "" && $patient->fecha_cirugia_autorizada != null){
                  $fecha = date("F j Y",strtotime($patient->fecha_cirugia_autorizada));
                }else{
                  $fecha = "N/A";
                }
              ?>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Date of surgery</b> <a class="pull-right"><?php echo $fecha; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Weight before surgery</b> <a class="pull-right"><?php echo $patient->weight.'lbs'; ?></a>
                </li>
                <li class="list-group-item">                  
                  <b>Current Weight</b> <a class="pull-right"><? echo $followupsData[(count($followupsData)-1)]['weight'].'lbs' ?></a>
                </li>
                <li class="list-group-item">
                  <b>Total Weight Loss</b> <a class="pull-right"><?php echo (($patient->weight) - $followupsData[(count($followupsData)-1)]['weight'])."lbs" ?></a>
                </li>
              </ul>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li  class="active"><a href="#activity" data-toggle="tab">My progress</a></li>
              <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
              <li><a href="#calendar" data-toggle="tab">Appointments</a></li>
              <li><a href="#settings" data-toggle="tab">Account Settings</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                  <!-- Post -->
                  <div class="post clearfix">
                    <!-- solid sales graph -->
                      <div class="box box-solid">
                          <div class="box-header">
                          <i class="fa fa-th"></i>

                          <h3 class="box-title">My progress</h3>

                          </div>
                          <div class="box-body border-radius-none">
                          <div class="chart" id="line-chart" style="height: 250px;"></div>
                          </div>
                          <!-- /.box-body -->
                          
                      </div>
                      <!-- /.box -->
                  </div>
                  <!-- /.post -->                
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                  <!-- The timeline -->
                  <ul class="timeline timeline-inverse">
                    <!-- timeline time label -->
                    <li class="time-label">
                          <span class="bg-green">
                            <?php echo date("F j, Y",strtotime($patient->fecha_cirugia_autorizada)); ?>
                          </span>
                    </li>                
                    <li>
                      <i class="fa  fa-plus-square bg-green"></i>
                      <div class="timeline-item">                      
                        <h3 class="timeline-header"><a href="#">You had your surgery!</a></h3>
                      </div>
                    </li>
                    <?php foreach($followupsData as $f) { ?>
                      <li class="time-label">
                          <span class="bg-blue">
                            <?php echo date("F j, Y",strtotime($f['created'])); ?>
                          </span>
                    </li>                
                    <li>
                      <i class="fa  fa-stethoscope bg-blue"></i>

                      <div class="timeline-item">                      
                        <h3 class="timeline-header no-border"><a href="#">Checkup</a> <?php echo "New weight: ".$f['weight']."lbs"; ?></h3>
                      </div>
                    </li>
                    <?php } ?>
                    <!-- END timeline item -->
                    <li>
                      <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                  </ul>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="calendar">
                  <div class="box box-primary">
                      <div class="box-body no-padding">
                          <!-- THE CALENDAR -->
                          <div id="thecalendar"></div>
                      </div>
                      <!-- /.box-body -->
                  </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="settings">
                    <h3>Notifications</h3>
                    <div class="alert alert-success collapse" id="notifications-alert">
                      <span>
                        <p id="alert-text">Setting Saved!</p>
                      </span>                      
                    </div>
                    <div class="checkbox">
                        <label>
                            <?php if($patient->email_notifications == 0){ ?>
                              <input type="checkbox" id='email' data-toggle="toggle">
                            <?php }else{ ?>
                              <input type="checkbox" id='email' data-toggle="toggle" checked>
                            <?php } ?>
                            <b>Email notifications</b>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <?php if($patient->sms_notifications == 0){ ?>
                              <input type="checkbox" id='sms' data-toggle="toggle">
                            <?php }else{ ?>
                              <input type="checkbox" id='sms' data-toggle="toggle" checked>
                            <?php } ?>
                            <b>SMS notifications</b>
                        </label>
                    </div>
                    <h3><a src="#">Change password</a></h3>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="exampleModalLabel">Upload new picture</h3>              
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="message-text" class="col-form-label">Choose picture:</label>
                <input type="file" class="form-control" name="new-photo" id="new-photo">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary" id="upload-photo" data-dismiss="modal">Upload!</button>
          </div>
        </div>
      </div>
    </div>

    </section>
    <!-- /.content -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- fullCalendar -->
    <?php echo $this->Html->css('AdminLTE./bower_components/fullcalendar/dist/fullcalendar.min', ['block' => 'css']); ?>
    <?php echo $this->Html->css('AdminLTE./bower_components/fullcalendar/dist/fullcalendar.print.min', ['block' => 'css', 'media' => 'print']); ?>
    <?php echo $this->Html->css('AdminLTE./bower_components/morris.js/morris', ['block' => 'css', 'media' => 'print']); ?>
    <!-- jQuery UI 1.11.4 -->
    <?php echo $this->Html->script('AdminLTE./bower_components/jquery-ui/jquery-ui.min', ['block' => 'script']); ?>
    <!-- fullCalendar -->
    <?php echo $this->Html->script('AdminLTE./bower_components/moment/moment', ['block' => 'script']); ?>
    <?php echo $this->Html->script('AdminLTE./bower_components/fullcalendar/dist/fullcalendar.min', ['block' => 'script']); ?>
    <?php echo $this->Html->script('AdminLTE./bower_components/raphael/raphael.min', ['block' => 'script']); ?>
    <?php echo $this->Html->script('AdminLTE./bower_components/morris.js/morris.min', ['block' => 'script']); ?>