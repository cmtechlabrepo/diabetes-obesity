<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Patient $patient
 */
?>
<?php echo $this->Html->script('patients/edit.js'); ?>
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Patient
      <small><?php echo __('Edit'); ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('The information you provide will help us to plan your treatment, please carefully fill it out.'); ?></h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <?php echo $this->Form->create($patient, ['role' => 'form']); ?>
          <div class="box-body">
            <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('location');
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('doctor_id',['label'=>'Doctor','options'=>$doctors,'empty' => true]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('name');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('address');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('city');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('state');
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('zipcode');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('home_phone');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('work_phone');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('cell_phone');
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('email_id', ['type'=>'hidden','value'=>$emailsData[0]['id']]);
                     echo $this->Form->control('old_email', ['type'=>'hidden','value'=>$emailsData[0]['email']]);
                     echo $this->Form->control('email', ['type'=>'text','value'=>$emailsData[0]['email']]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('date_of_birth', ['type'=>'text','id'=>'datepicker1']);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('gender_id');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('marital_status_id');
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('age');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('height');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('weight');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('bmi');
                     ?>
                  </div>
                </div>

                  <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Measurements'); ?></h3>
                  </div>
                  <div class="row">
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('neck');
                       ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('wrist');
                       ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('waist');
                       ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('hip');
                       ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-xs-12">
                      <?php
                       echo $this->Form->control('thigh');
                       ?>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <?php
                       echo $this->Form->control('shirt_size');
                       ?>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <?php
                       echo $this->Form->control('pants_size');
                       ?>
                    </div>
                  </div>
              </div>            
          </div>

            <!-- /.box-body -->

          <?php echo $this->Form->submit(__('Submit')); ?>

          <?php echo $this->Form->end(); ?>
        </div>
        <!-- /.box -->
      </div>
  </div>
  <!-- /.row -->
</section>
