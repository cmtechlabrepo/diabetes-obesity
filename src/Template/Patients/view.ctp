<section class="content-header">
  <h1>
    Patient
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Location') ?></dt>
            <dd><?= h($patient->location) ?></dd>
            <dt scope="row"><?= __('Surgeon') ?></dt>
            <dd><?= h($patient->surgeon) ?></dd>
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($patient->name) ?></dd>
            <dt scope="row"><?= __('Address') ?></dt>
            <dd><?= h($patient->address) ?></dd>
            <dt scope="row"><?= __('Zipcode') ?></dt>
            <dd><?= h($patient->zipcode) ?></dd>
            <dt scope="row"><?= __('Home Phone') ?></dt>
            <dd><?= h($patient->home_phone) ?></dd>
            <dt scope="row"><?= __('Work Phone') ?></dt>
            <dd><?= h($patient->work_phone) ?></dd>
            <dt scope="row"><?= __('Cell Phone') ?></dt>
            <dd><?= h($patient->cell_phone) ?></dd>
            <dt scope="row"><?= __('Height') ?></dt>
            <dd><?= h($patient->height) ?></dd>
            <dt scope="row"><?= __('Shirt Size') ?></dt>
            <dd><?= h($patient->shirt_size) ?></dd>
            <dt scope="row"><?= __('Pants Size') ?></dt>
            <dd><?= h($patient->pants_size) ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $patient->has('user') ? $this->Html->link($patient->user->name, ['controller' => 'Users', 'action' => 'view', $patient->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($patient->id) ?></dd>
            <dt scope="row"><?= __('City') ?></dt>
            <dd><?= $this->Number->format($patient->city) ?></dd>
            <dt scope="row"><?= __('State') ?></dt>
            <dd><?= $this->Number->format($patient->state) ?></dd>
            <dt scope="row"><?= __('Gender Id') ?></dt>
            <dd><?= $this->Number->format($patient->gender_id) ?></dd>
            <dt scope="row"><?= __('Marital Status Id') ?></dt>
            <dd><?= $this->Number->format($patient->marital_status_id) ?></dd>
            <dt scope="row"><?= __('Age') ?></dt>
            <dd><?= $this->Number->format($patient->age) ?></dd>
            <dt scope="row"><?= __('Weight') ?></dt>
            <dd><?= $this->Number->format($patient->weight) ?></dd>
            <dt scope="row"><?= __('Bmi') ?></dt>
            <dd><?= $this->Number->format($patient->bmi) ?></dd>
            <dt scope="row"><?= __('Neck') ?></dt>
            <dd><?= $this->Number->format($patient->neck) ?></dd>
            <dt scope="row"><?= __('Wrist') ?></dt>
            <dd><?= $this->Number->format($patient->wrist) ?></dd>
            <dt scope="row"><?= __('Waist') ?></dt>
            <dd><?= $this->Number->format($patient->waist) ?></dd>
            <dt scope="row"><?= __('Hip') ?></dt>
            <dd><?= $this->Number->format($patient->hip) ?></dd>
            <dt scope="row"><?= __('Thigh') ?></dt>
            <dd><?= $this->Number->format($patient->thigh) ?></dd>
            <dt scope="row"><?= __('Date Of Birth') ?></dt>
            <dd><?= h($patient->date_of_birth) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($patient->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($patient->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
