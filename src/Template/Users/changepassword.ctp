<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div id="error-pws" class="alert alert-danger alert-dismissible collapse">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-times"></i> Alert!</h4>
    Passwords do not match or are empty, please make sure they are not empty and they match before submitting.
</div>

<?php echo $this->Html->script('users/changepw.js'); ?>

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      User
      <small><?php echo __('Edit'); ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Form'); ?></h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <?php echo $this->Form->create(false, ["id"=>"changepwForm"]); ?>
            <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <label>User</label><br><?= $user->name ?>
                    <?= $this->Form->control('user_id',['type'=>'hidden','value'=>$user->id]) ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <label>Email</label><br><?= $user->email ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?= $this->Form->control('new_password',['type'=>'password']) ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?= $this->Form->control('confirm_password',['type'=>'password']); ?>
                  </div>
                </div>
            </div>
            <!-- /.box-body -->
          <?php echo $this->Form->submit(__('Change Password')); ?>
          <?php echo $this->Form->end(); ?>
        </div>
        <!-- /.box -->
      </div>
  </div>
  <!-- /.row -->
</section>
