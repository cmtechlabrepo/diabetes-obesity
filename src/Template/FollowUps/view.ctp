<section class="content-header">
  <h1>
    Follow Up
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $followUp->has('patient') ? $this->Html->link($followUp->patient->name, ['controller' => 'Patients', 'action' => 'view', $followUp->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Doctor') ?></dt>
            <dd><?= $followUp->has('doctor') ? $this->Html->link($followUp->doctor->name, ['controller' => 'Doctors', 'action' => 'view', $followUp->doctor->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($followUp->id) ?></dd>
            <dt scope="row"><?= __('Weight') ?></dt>
            <dd><?= $this->Number->format($followUp->weight) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($followUp->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($followUp->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($followUp->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
