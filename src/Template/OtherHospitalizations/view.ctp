<section class="content-header">
  <h1>
    Other Hospitalization
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Hospital') ?></dt>
            <dd><?= h($otherHospitalization->hospital) ?></dd>
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $otherHospitalization->has('patient') ? $this->Html->link($otherHospitalization->patient->name, ['controller' => 'Patients', 'action' => 'view', $otherHospitalization->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $otherHospitalization->has('user') ? $this->Html->link($otherHospitalization->user->name, ['controller' => 'Users', 'action' => 'view', $otherHospitalization->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($otherHospitalization->id) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($otherHospitalization->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($otherHospitalization->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($otherHospitalization->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Reason') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($otherHospitalization->reason); ?>
        </div>
      </div>
    </div>
  </div>
</section>
