<section class="content-header">
  <h1>
    Surgery Change
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $surgeryChange->has('user') ? $this->Html->link($surgeryChange->user->name, ['controller' => 'Users', 'action' => 'view', $surgeryChange->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $surgeryChange->has('patient') ? $this->Html->link($surgeryChange->patient->name, ['controller' => 'Patients', 'action' => 'view', $surgeryChange->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($surgeryChange->id) ?></dd>
            <dt scope="row"><?= __('Fecha Original') ?></dt>
            <dd><?= h($surgeryChange->fecha_original) ?></dd>
            <dt scope="row"><?= __('Nueva Fecha') ?></dt>
            <dd><?= h($surgeryChange->nueva_fecha) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($surgeryChange->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($surgeryChange->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Razon') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($surgeryChange->razon); ?>
        </div>
      </div>
    </div>
  </div>
</section>
