<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Surgery Changes

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                  <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('fecha_original') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('nueva_fecha') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('patient_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                  <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($surgeryChanges as $surgeryChange): ?>
                <tr>
                  <td><?= $this->Number->format($surgeryChange->id) ?></td>
                  <td><?= h($surgeryChange->fecha_original) ?></td>
                  <td><?= h($surgeryChange->nueva_fecha) ?></td>
                  <td><?= $surgeryChange->has('user') ? $this->Html->link($surgeryChange->user->name, ['controller' => 'Users', 'action' => 'view', $surgeryChange->user->id]) : '' ?></td>
                  <td><?= $surgeryChange->has('patient') ? $this->Html->link($surgeryChange->patient->name, ['controller' => 'Patients', 'action' => 'view', $surgeryChange->patient->id]) : '' ?></td>
                  <td><?= h($surgeryChange->created) ?></td>
                  <td><?= h($surgeryChange->modified) ?></td>
                  <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $surgeryChange->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $surgeryChange->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $surgeryChange->id], ['confirm' => __('Are you sure you want to delete # {0}?', $surgeryChange->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>