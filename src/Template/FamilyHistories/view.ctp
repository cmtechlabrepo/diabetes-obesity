<section class="content-header">
  <h1>
    Family History
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Family Member') ?></dt>
            <dd><?= h($familyHistory->family_member) ?></dd>
            <dt scope="row"><?= __('Age') ?></dt>
            <dd><?= h($familyHistory->age) ?></dd>
            <dt scope="row"><?= __('Cause Of Death') ?></dt>
            <dd><?= h($familyHistory->cause_of_death) ?></dd>
            <dt scope="row"><?= __('Health Problems') ?></dt>
            <dd><?= h($familyHistory->health_problems) ?></dd>
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $familyHistory->has('patient') ? $this->Html->link($familyHistory->patient->name, ['controller' => 'Patients', 'action' => 'view', $familyHistory->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $familyHistory->has('user') ? $this->Html->link($familyHistory->user->name, ['controller' => 'Users', 'action' => 'view', $familyHistory->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($familyHistory->id) ?></dd>
            <dt scope="row"><?= __('Complexion') ?></dt>
            <dd><?= $this->Number->format($familyHistory->complexion) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($familyHistory->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($familyHistory->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
