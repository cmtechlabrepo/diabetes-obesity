<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Family Histories

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                  <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('family_member') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('age') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('cause_of_death') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('complexion') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('health_problems') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('patient_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                  <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($familyHistories as $familyHistory): ?>
                <tr>
                  <td><?= $this->Number->format($familyHistory->id) ?></td>
                  <td><?= h($familyHistory->family_member) ?></td>
                  <td><?= h($familyHistory->age) ?></td>
                  <td><?= h($familyHistory->cause_of_death) ?></td>
                  <td><?= $this->Number->format($familyHistory->complexion) ?></td>
                  <td><?= h($familyHistory->health_problems) ?></td>
                  <td><?= $familyHistory->has('patient') ? $this->Html->link($familyHistory->patient->name, ['controller' => 'Patients', 'action' => 'view', $familyHistory->patient->id]) : '' ?></td>
                  <td><?= $familyHistory->has('user') ? $this->Html->link($familyHistory->user->name, ['controller' => 'Users', 'action' => 'view', $familyHistory->user->id]) : '' ?></td>
                  <td><?= h($familyHistory->created) ?></td>
                  <td><?= h($familyHistory->modified) ?></td>
                  <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $familyHistory->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $familyHistory->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $familyHistory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $familyHistory->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>