<section class="content-header">
  <h1>
    Psicologia Evaluation
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $psicologiaEvaluation->has('patient') ? $this->Html->link($psicologiaEvaluation->patient->name, ['controller' => 'Patients', 'action' => 'view', $psicologiaEvaluation->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Doctor') ?></dt>
            <dd><?= $psicologiaEvaluation->has('doctor') ? $this->Html->link($psicologiaEvaluation->doctor->name, ['controller' => 'Doctors', 'action' => 'view', $psicologiaEvaluation->doctor->id]) : '' ?></dd>
            <dt scope="row"><?= __('Procedure') ?></dt>
            <dd><?= $psicologiaEvaluation->has('procedure') ? $this->Html->link($psicologiaEvaluation->procedure->name, ['controller' => 'Procedures', 'action' => 'view', $psicologiaEvaluation->procedure->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $psicologiaEvaluation->has('user') ? $this->Html->link($psicologiaEvaluation->user->name, ['controller' => 'Users', 'action' => 'view', $psicologiaEvaluation->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($psicologiaEvaluation->id) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($psicologiaEvaluation->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($psicologiaEvaluation->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($psicologiaEvaluation->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Observaciones') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($psicologiaEvaluation->observaciones); ?>
        </div>
      </div>
    </div>
  </div>
</section>
