<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Doctor $doctor
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Doctor
        <small><?php echo __('Add'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i>
                <?php echo __('Home'); ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Form'); ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($doctor, ['role' => 'form']); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <?php
                              echo $this->Form->control('name');
                            ?>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <?php
                            echo $this->Form->control('telephone');
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <?php
                            echo $this->Form->control('mobile_phone');
                            ?>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <?php
                            echo $this->Form->control('address');
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <?php
                            echo $this->Form->control('email');
                            ?>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <?php
                            echo $this->Form->control('doctor_type_id', ['label'=>"Specialty",'options' => $doctorTypes, 'empty' => true]);
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>