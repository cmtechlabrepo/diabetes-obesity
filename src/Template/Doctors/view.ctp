<section class="content-header">
  <h1>
    Doctor
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($doctor->name) ?></dd>
            <dt scope="row"><?= __('Telephone') ?></dt>
            <dd><?= h($doctor->telephone) ?></dd>
            <dt scope="row"><?= __('Mobile Phone') ?></dt>
            <dd><?= h($doctor->mobile_phone) ?></dd>
            <dt scope="row"><?= __('Address') ?></dt>
            <dd><?= h($doctor->address) ?></dd>
            <dt scope="row"><?= __('Email') ?></dt>
            <dd><?= h($doctor->email) ?></dd>
            <dt scope="row"><?= __('Doctor Type') ?></dt>
            <dd><?= $doctor->has('doctor_type') ? $this->Html->link($doctor->doctor_type->name, ['controller' => 'DoctorTypes', 'action' => 'view', $doctor->doctor_type->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $doctor->has('user') ? $this->Html->link($doctor->user->name, ['controller' => 'Users', 'action' => 'view', $doctor->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($doctor->id) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($doctor->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($doctor->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
