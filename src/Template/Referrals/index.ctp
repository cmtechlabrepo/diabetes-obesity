<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Referrals

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                  <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('patient_id') ?></th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('survey_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('user_id') ?></th> -->
                  <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('modified') ?></th> -->
                  <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($referrals as $referral): ?>
                <tr>
                  <td><?= $this->Number->format($referral->id) ?></td>
                  <td><?= h($referral->email) ?></td>
                  <td><?= $referral->has('patient') ? $this->Html->link($referral->patient->name, ['controller' => 'Patients', 'action' => 'view', $referral->patient->id]) : '' ?></td>
                  <!-- <td><?= $referral->has('survey') ? $this->Html->link($referral->survey->id, ['controller' => 'Surveys', 'action' => 'view', $referral->survey->id]) : '' ?></td> -->
                  <!-- <td><?= $referral->has('user') ? $this->Html->link($referral->user->name, ['controller' => 'Users', 'action' => 'view', $referral->user->id]) : '' ?></td> -->
                  <td><?= h($referral->date) ?></td>
                  <!-- <td><?= h($referral->created) ?></td>
                  <td><?= h($referral->modified) ?></td> -->
                  <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $referral->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $referral->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $referral->id], ['confirm' => __('Are you sure you want to delete # {0}?', $referral->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>