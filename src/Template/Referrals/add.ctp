<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Referral $referral
 */
?>
<?php echo $this->Html->script('referrals/add.js'); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Referrals
        <small><?php echo __('Add'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i>
                <?php echo __('Home'); ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
            <label>Would you like to recommend our services to a friend or family? If you give us his /
                her email address he / she will be given a $50 USD bonus and the great opportunity to
                start a healthy life!</label>
            <?php
                // $referrals = array(1=>"Yes, a friend",2=>"Yes, a family",0=>"No, I don´t");                                
                // echo $this->Form->radio('give_referrals',$referrals);
            ?>
            </div>
          
    
    <?php echo $this->Form->create($referral, ['role' => 'form']); ?>
    
        <div class="col-md-12 col-xs-12">
            <div class="box-body table-responsive no-padding">
                <table id="referral-list" class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Email address</th>
                            <th scope="col">Friend / Family</th>
                            <th scope="col">Add / Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="referral1">
                            <td><input name='email1' type="text" id="email1"></td>
                            <td>
                                <select name="type1" id="type1">
                                    <option>Select</option>
                                    <option value=1>Friend</option>
                                    <option value=2>Family</option>
                                </select>
                            </td>
                            <td>
                                <a href="#" class="add add-referral"><i class="fa fa-fw fa-plus"></i></a>
                                <a href="#" class="remove remove-referral"><i class="fa fa-fw fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php echo $this->Form->submit(__('Submit')); ?>

        <?php echo $this->Form->end(); ?>
        </div>
        </div>
    </div>
    <!-- /.row -->
</section>