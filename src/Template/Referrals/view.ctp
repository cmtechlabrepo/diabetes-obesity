<section class="content-header">
  <h1>
    Referral
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Email') ?></dt>
            <dd><?= h($referral->email) ?></dd>
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $referral->has('patient') ? $this->Html->link($referral->patient->name, ['controller' => 'Patients', 'action' => 'view', $referral->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Survey') ?></dt>
            <dd><?= $referral->has('survey') ? $this->Html->link($referral->survey->id, ['controller' => 'Surveys', 'action' => 'view', $referral->survey->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $referral->has('user') ? $this->Html->link($referral->user->name, ['controller' => 'Users', 'action' => 'view', $referral->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($referral->id) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= $this->Number->format($referral->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($referral->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($referral->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
