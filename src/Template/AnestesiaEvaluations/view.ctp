<section class="content-header">
  <h1>
    Anestesia Evaluation
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $anestesiaEvaluation->has('patient') ? $this->Html->link($anestesiaEvaluation->patient->name, ['controller' => 'Patients', 'action' => 'view', $anestesiaEvaluation->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Procedure') ?></dt>
            <dd><?= $anestesiaEvaluation->has('procedure') ? $this->Html->link($anestesiaEvaluation->procedure->name, ['controller' => 'Procedures', 'action' => 'view', $anestesiaEvaluation->procedure->id]) : '' ?></dd>
            <dt scope="row"><?= __('Doctor') ?></dt>
            <dd><?= $anestesiaEvaluation->has('doctor') ? $this->Html->link($anestesiaEvaluation->doctor->name, ['controller' => 'Doctors', 'action' => 'view', $anestesiaEvaluation->doctor->id]) : '' ?></dd>
            <dt scope="row"><?= __('Tele Torax') ?></dt>
            <dd><?= h($anestesiaEvaluation->tele_torax) ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $anestesiaEvaluation->has('user') ? $this->Html->link($anestesiaEvaluation->user->name, ['controller' => 'Users', 'action' => 'view', $anestesiaEvaluation->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($anestesiaEvaluation->id) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= $this->Number->format($anestesiaEvaluation->modified) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($anestesiaEvaluation->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($anestesiaEvaluation->created) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Diagnosis') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($anestesiaEvaluation->diagnosis); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Medical History') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($anestesiaEvaluation->medical_history); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Asa') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($anestesiaEvaluation->asa); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Instructions') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($anestesiaEvaluation->instructions); ?>
        </div>
      </div>
    </div>
  </div>
</section>
