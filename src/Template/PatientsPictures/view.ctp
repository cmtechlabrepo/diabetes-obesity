<section class="content-header">
  <h1>
    Patients Picture
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Path') ?></dt>
            <dd><?= h($patientsPicture->path) ?></dd>
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $patientsPicture->has('patient') ? $this->Html->link($patientsPicture->patient->name, ['controller' => 'Patients', 'action' => 'view', $patientsPicture->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($patientsPicture->id) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($patientsPicture->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($patientsPicture->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
