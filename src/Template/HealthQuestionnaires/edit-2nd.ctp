<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HealthQuestionnaire $healthQuestionnaire
 */
?>
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Health Questionnaire
      <!-- <small><?php echo __('The information you provide will help us to plan your tratment, please carefully fill it out.'); ?></small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->

        <!-- Aqui -->
        <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">The information you provide will help us to plan your tratment, please carefully fill it out.</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
      <!-- Aqui -->
       <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                  <?php  echo __('PATIENT INFORMATION'); ?>
              </a>
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo $this->Form->create($healthQuestionnaire, ['role' => 'form']); ?>
             <div class="box-body">
         <div id="collapseOne" class="panel-collapse collapse in">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('location');
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('surgeon');
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('name');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('address');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('city');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('state');
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('zip_code');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('home_phone');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('work_phone');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('cell_phone');
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('date_of_birth', ['type'=>'text','id'=>'datepicker1']);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('gender',['type' => 'select', 'options' => [1 => __('Male') , 2 => __('Female')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('marital_status',['type' => 'select', 'options' => [1 => __('Single') , 2 => __('Married'), 3 => __('Divorced'), 4 => __('Separated'), 5 => __('Widow/Widower')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('age');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('height');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('weight');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('BMI');
                     ?>
                  </div>
                </div>

                  <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Measurements'); ?></h3>
                  </div>
                  <div class="row">
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('neck');
                       ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('wrist');
                       ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('waist');
                       ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('hip');
                       ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-xs-12">
                      <?php
                       echo $this->Form->control('thigh');
                       ?>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <?php
                       echo $this->Form->control('size_shirt');
                       ?>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <?php
                       echo $this->Form->control('size_pants');
                       ?>
                    </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                  <?php echo __('PROCEDURE'); ?>
                </a>
              </h3>
              </div>
            <div id="collapse2" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('procedure_id',['label'=>"Please indicate which procedure you are interested in"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('suggested_date', ['label'=>'Suggested date. Do you have any date on mind to have your procedure?','type'=>'text','id'=>'datepicker2']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                    echo $this->Form->control('details');
                    ?>
                  </div>
                </div>

              </div>
            </div>
          </div>


          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                  <?php echo __('REFERRAL INFORMATION'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                    echo $this->Form->control('referring_person');
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                    echo $this->Form->control('date_of_referral', ['type'=>'text','id'=>'datepicker3']);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                  <?php echo __('EMERGENCY CONTACT INFO'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('emergency_name');
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('emergency_phone');
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                  <?php echo __('PRIMARY HEALTH CARE PROVIDER'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('primary_health_provider');
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('time_treated',['label'=>"How long has he/she trated you?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('condition_treated',['label'=>'Condition treated?']);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                  <?php echo __('ALLERGY'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('allergic_medication',['label'=>"Are you allergic to any MEDICATION?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('allergic_medication',['label'=>"Are you allergic to any MEDICATION?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                    echo $this->Form->control('allergic_medication',['label'=>"Are you allergic to any MEDICATION?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('allergic_medication_list',['label'=>"Please list"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                    echo $this->Form->control('supplies_allergic',['label'=>"Are you allergic to any WOUND CARE SUPPLIES (tape, latex, isodine, alcohol, merthiolate, talcum powder, etc)?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('supplies_allergic_list',['label'=>"Please list"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('allergic_food',['label'=>"Are you allergic to any FOOD (fruits, vegetables, seeds, meat, fish, egg, dairy, etc)?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('allergic_food_list',['label'=>"Please list"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('blood_transfusion',['label'=>"Have you ever had a blood transfusion?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('transfusion_details',['label'=>'When and details']);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                  <?php echo __('FAMILY HISTORY'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse7" class="panel-collapse collapse">
              <div class="box-body">
                <label>What other family members have or have had (indicate mother´s / father´s side of your family):</label>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('breast_colo_prostate_cancer',['label'=>"Breast, Colon or Prostate Cancer",'type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('cancer_specific',['label'=>"Cancer (specify type)",'type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('diabetes',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('heart_attack',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('stroke',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('high_blood_pressure',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('arthritis',['label'=>'Arthritis or back trouble','type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('obesity',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                  <?php echo __('RESPIRATORY SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse8" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('shortness_breath',['label'=>"Do you experience shortness of breath with physical activity?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('shortness_breath_long',['label'=>'How long have you been aware of this?']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('excercise_regularly',['label'=>"Do you excercise regularly?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('asthma',['label'=>"Do you have or have you had asthma?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
                <?php echo __('BLOOD AND CIRCULATORY SYSTEM'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse9" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('swelling_ankles',['label'=>"Do you experience swelling of the ankles?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <?php
                echo $this->Form->control('decrese_swelling',['label','What do you do to decrease the swelling?']);
                echo $this->Form->control('decrease_pain',['label','What do you take to relieve the pain?']);
                ?>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
                  <?php echo __('ENDOCRINOLOGY SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse10" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('thyroid_problems',['label'=>"Thyroid problems?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('own_diabetes',['label'=>"Are you diabetic?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('medications_for_diabetes',['label'=>'What are you taking for your diabetes?']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('monitor_blood_sugar',['label'=>"Do you monitor your blood sugar?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('monitor_how_ofter',['label'=>"How often?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('pcos',['label'=>"Have you been diagnosed with PCOS?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">
                  <?php echo __('CARDIAC SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse11" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('own_high_blood_pressure',['label'=>"Do you have high blood pressure?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('medications_blood_pressure',['label'=>"What are you raking for your high blood pressure?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('chest_pain',['label'=>"Do you experience chest pain?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('chest_pain_often',['label'=>"How often"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('history_heart_disease',['label'=>"Do you have any history of heart disease?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('explain_heart_dicease',['label'=>"Explain"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('own_heart_attack',['label'=>"Have you had a heart attack?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">
                  <?php echo __('HEPATIC SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse12" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('fatty_liver',['label'=>"Have you diagnosed with fatty liver, cirrhosis, hepatitis or any liver disease?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('fatty_liver_details',['label'=>'Details']);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">
                  <?php echo __('AUTO IMMUNE SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse13" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('rheumatoid_arthritis',['label'=>"Have you been diagnosed with Rheumatoid Arthritis?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('nsaids',['label'=>"Are you taking or have you taken nonsteroidal anti-inflammatory drugs (NSAIDs) for joint pain?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('lupus',['label'=>"Have you been diagnosed for Lupus?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('hiv',['label'=>"Have you been diagnosed as HIV positive?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse14">
              <?php echo __('DIGESTION PROBLEMS'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse14" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('diverticulitis',['label'=>"Have you been diagnosed with diverticulitis?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('ulcers',['label'=>"Do you any history of ulcers?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('crohns_disease',['label'=>"Have you been disgnosed with Crohn´s disease or Ulcerative Colitis?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('heartburn',['label'=>"Do you have indigestion or heartburn?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('heartburn_long',['label'=>"If so, for how long"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('pain_abdomen',['label'=>"Do you ever have any type or pain in the abdomen?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('pain_details',['label'=>'Pain details (sharp, dull, hot, cold, aching, crushing, etc)']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('bowel_movements',['label'=>"Any changes in bowel movements?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('bloody_stools',['label'=>"Any bloody stools?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('hemorrhoids',['label'=>"History of hemorrhoids?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse15">
              <?php echo __('MENTAL HEALTH'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse15" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('eating_disorder',['label'=>"Have you ever been treated for an eating disorder?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('happy_life',['label'=>"Are generally happy with your life other than your weight?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('history_depression',['label'=>"Do you have a history of depression?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('are_you_depressed',['label'=>"Do you feel depressed?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('stress_problem',['label'=>"Is stress a major problem for you?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('panic_stressed',['label'=>"Do you panic when stressed?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('problems_eating',['label'=>"Do you have problems with eating or your appetite?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('cry_frecuently',['label'=>"Do you cry frequently?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('attempted_suicide',['label'=>"Have you ever attempted suicide?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('thought_hurting_self',['label'=>"Have you ever seriously thought about hurting yourself?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('trouble_sleeping',['label'=>"Do you have trouble sleeping?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('been_to_counselor',['Have you ever been to a counselor?'=>"",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse16">
              <?php echo __('ALCOHOL'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse16" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('drink_alcohol',['label'=>"Do you drink alcohol?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('kind_alcohol',['label'=>"If yes, what kind?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('drinks_per_week',['label'=>"How many drinks per week?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('concerned_drink',['label'=>"Are you concerned about the amount you drink?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('considered_stopping',['label'=>"Have you considered stopping?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('blackouts',['label'=>"Have you ever experienced blackouts?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse17">
                  <?php echo __('TOBACCO'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse17" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('use_tobacco',['label'=>"Do you use tobacco?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                      echo $this->Form->control('cigarret_packs_day',['label'=>"Cigarrettes - pks./day"]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                      echo $this->Form->control('chew_day',['label'=>"Chew - #/day"]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                      echo $this->Form->control('pipe_day',['label'=>"Pipe- #/day"]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                      echo $this->Form->control('cigars_day',['label'=>"Cigars - #/day"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-md-12">
                    <?php
                      echo $this->Form->control('number_years',['label'=>"# of years"]);
                     ?>
                  </div>
                  <div class="col-md-3 col-md-12">
                    <?php
                      echo $this->Form->control('years_quit',['label'=>"Or Year Quit"]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse18">
                <?php echo __('CAFFEINE'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse18" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('use_caffeine',['label'=>"Do you use caffeine (coffee, cola, chocolate, energetic beverages)",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('caffeine_form',['label'=>"If yes, in what form?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('caffeine_how_much',['label'=>"How much per day?"]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse19">
              <?php echo __('DRUGS'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse19" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('recreational_drugs',['label'=>"Do you currently use recreational or street drugs?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('drugs_details',['label'=>"If yes, please give details:"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('drugs_needle',['label'=>"Have you ever given yourself street drugs with a needle?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse20">
              <?php echo __('SEX'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse20" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('sexually_active',['label'=>"Are you sexually active?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('trying_pregnancy',['label'=>"If yes, are you trying for a pregnancy?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('contraceptive',['label'=>'If not, trying for a pregnancy list contraceptive or barrier mehod used']);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('intercourse_discomfort',['label'=>"Any discomfort with intercourse?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                    echo $this->Form->control('speak_about_hiv',['label'=>"Illnes related to the Human Immunodeficiency Virus (HIV), such as AIDS, has become a major public health problem. Risk factors for
                    this illmess include intravenous drug use and unprotected sexual intercourse. Would you like to speak with your provider about
                    your risk of this illmess?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>


                <!-- <div class="box-header with-border">
                  <h3 class="box-title"><?php echo __('BONE OR JOINT PROBLEM'); ?></h3>
                </div> -->
                <?php
                // echo $this->Form->control('ankles_swelling');
                // echo $this->Form->control('ankles_pain');
                // echo $this->Form->control('ankles_stiffness');
                // echo $this->Form->control('ankles_popping');
                // echo $this->Form->control('knees_swelling');
                // echo $this->Form->control('knees_pain');
                // echo $this->Form->control('knees_stiffness');
                // echo $this->Form->control('kness_popping');
                // echo $this->Form->control('hips_swelling');
                // echo $this->Form->control('hips_pain');
                // echo $this->Form->control('hips_stiffness');
                // echo $this->Form->control('hips_popping');
                // echo $this->Form->control('back_swelling');
                // echo $this->Form->control('back_pain');
                // echo $this->Form->control('back_stiffness');
                // echo $this->Form->control('back_popping');
                // echo $this->Form->control('others_swelling');
                // echo $this->Form->control('others_pain');
                // echo $this->Form->control('others_stiffing');
                // echo $this->Form->control('others_popping');
                // echo $this->Form->control('treatment1');
                // echo $this->Form->control('treatment2');
                // echo $this->Form->control('join_medications');
                // echo $this->Form->control('join_medications_details');
                // echo $this->Form->control('degenerative_changes');
                // echo $this->Form->control('degenerative_changes_details');
                ?>
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse21">
                  <?php echo __('DIET HISTORY'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse21" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('weight_loss_surgery',['label'=>"Have you ever had surgery to aid in weight loss?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('how_long_overweight',['label'=>"How long have you been overweight?"]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('diet_pills',['label'=>"Have you tried diet pills?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('how_much_weight_loss',['label'=>"How much weight did you lose with the diet program (s)?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('how_quickly_regain',['label'=>"How quickly did you regain the weight afterwards?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('max_weight',['label'=>"What has been your MAXIMUM WEIGHT?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('min_weight',['label'=>"What has been your MINIMUM WEIGHT?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <label>Enlist the food your like the most and the food you dislike the most</label>
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('foods_liked',['label'=>"LIKE:"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('foods_disliked',['label'=>"DISLIKE:"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('snaker',['label'=>"Are you snacker?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('volume_eater',['label'=>"Are you a volume eater?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('eats_sweets',['label'=>"Do you eat a lot of sweets?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('sweets_how_ofter',['label'=>"How often do you eat sweets?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('carbonated_beverages',['label'=>"Do you frequently eat fast food and/or do you drink carbonated beverages?"]);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse22">
                <?php echo __('EXERCISE'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse22" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                    echo $this->Form->control('type_lifestyle',['label'=>"Please select the option that best suits your lifestyle",'type' => 'select', 'options' => [1 => __('Sedentary (No excercise)') , 2 => __('Mild excercise (i.e., climb staris, walk 3 blocks, golf)'), 3 => __('Occasional vigorous exercise (i.e. work or recreation, less than 120 min/week)'), 4 => __('Regular vigorous exercise (i.e. work or recreation more than 120 min/week)')], 'empty' => __('Select')]);
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse23">
              <?php echo __('REVIEW OF SYSTEMS: Unless otherwise specified, mark the correct box and provide any information about your current status.'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse23" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('severe_fatigue',['label'=>"FREQUENT OR SEVERE FATIGUE",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('severe_fatigue_details',['label'=>'Details or comments']);
                     ?>
                  </div>
                </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
                  echo $this->Form->control('severe_weekness',['label'=>"FREQUENT OR SEVERE WEAKNESS",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
                    echo $this->Form->control('severe_weekness_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
                    echo $this->Form->control('night_sweats',['label'=>"FEVER, CHILLS OR NIGHT SWEATS",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
                    echo $this->Form->control('night_sweats_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
                    echo $this->Form->control('severe_headaches',['label'=>"FREQUENT OR SEVERE HEADACHES",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('severe_headaches_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('loss_of_conciousness',['label'=>"ANY HISTORY OF HEAD INJURY WITH LOSS OF CONSCIOUSNESS",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('loss_of_conciousness_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('hearing_problems',['label'=>"HEARING PROBLEMS",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('hearing_problems_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('ear_pain',['label'=>"EAR PAIN",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('ear_pain_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('nasal_congestion',['label'=>"NASAL CONGESTION",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('nasal_congestion_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('sinus_congestion',['label'=>"CHRONIC SINUS CONGESTION",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('sinus_congestion_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('bloody_nose',['label'=>"FREQUENT BLOODY NOSE",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('bloody_nose_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('dental_problems',['label'=>"DENTALS PROBLEMS",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('dental_problems_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('dentures',['label'=>"DENTURES",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('dentures_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('sores_in_mouth',['label'=>"SORES IN MOUTH",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('sores_in_mouth_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('wheezing',['label'=>"WHEEZING",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
  echo $this->Form->control('wheezing_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('coughing',['label'=>"COUGHING",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('coughing_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('breast_lump',['label'=>"BREAST LUMP, PAIN OR DISCHARGE",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('breast_lump_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('heart_murmur',['label'=>"HEART MURMUR",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('heart_murmur_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('high_blood_pressure2',['label'=>"HIGH BLOOD PRESSURE",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('high_blood_pressure2_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('chest_pain_excercise',['label'=>"CHEST PAIN WITH EXCERCISE OR ACTIVITY",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('chest_pain_excercise_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('std',['label'=>"ANY SEXUALLY TRANSMITTED DISEASE THAT WAS NOT TREATED",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('std_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('anemia',['label'=>"ANEMIA",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('anemia_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('bleeding_tendency',['label'=>"BLEEDING TENDENCY",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('bleeding_tendency_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('convulsions',['label'=>"CONVULSIONS, SEIZURES",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('convulsions_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('paralysis',['label'=>"PARALYSIS",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('paralysis_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('numbness',['label'=>"NUMBNESS OR TINGLING",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('numbness_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('memory_loss',['label'=>"MEMORY LOSS",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('memory_loss_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('depression',['label'=>"DEPRESSION",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('depression_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('anxiety',['label'=>"ANXIETY",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('anxiety_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('mood_swings',['label'=>"MOOD SWINGS",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('mood_swings_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('sleep_problems',['label'=>"SLEEP PROBLEMS",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('sleep_problems_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('drug_alcochol_abuse',['label'=>"DRUG OR ALCOHOL ABUSE",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('drug_alcochol_abuse_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
echo $this->Form->control('rash_hives',['label'=>"CHRONIC SKIN RASH OR HIVES",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('rash_hives_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('asthma2',['label'=>"ASTHMA",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
  echo $this->Form->control('asthma2_details',['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
  echo $this->Form->control('hay_fever',['label'=>"HAY FEVER",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('hay_fever_details',['label'=>'Details or comments'],['label'=>'Details or comments']);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
echo $this->Form->control('additional_information',['label'=>"Please list any additional information you beleive would assist in your health planning"]);
                   ?>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
            <!-- /.box-body -->
            <div class="row">
              <div class="col-md-12 col-xs-12" align="center">
                <?php echo $this->Form->submit(__('Submit')); ?>
              </div>
            </div>



          <?php echo $this->Form->end(); ?>
        </div>
        <!-- /.box -->
      </div>
  </div>
  <!-- /.row -->
</section>
