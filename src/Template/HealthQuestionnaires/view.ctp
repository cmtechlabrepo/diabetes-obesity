<section class="content-header">
  <h1>
    Health Questionnaire
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Location') ?></dt>
            <dd><?= h($healthQuestionnaire->location) ?></dd>
            <dt scope="row"><?= __('Surgeon') ?></dt>
            <dd><?= h($healthQuestionnaire->surgeon) ?></dd>
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($healthQuestionnaire->name) ?></dd>
            <dt scope="row"><?= __('City') ?></dt>
            <dd><?= h($healthQuestionnaire->city) ?></dd>
            <dt scope="row"><?= __('State') ?></dt>
            <dd><?= h($healthQuestionnaire->state) ?></dd>
            <dt scope="row"><?= __('Zip Code') ?></dt>
            <dd><?= h($healthQuestionnaire->zip_code) ?></dd>
            <dt scope="row"><?= __('Home Phone') ?></dt>
            <dd><?= h($healthQuestionnaire->home_phone) ?></dd>
            <dt scope="row"><?= __('Work Phone') ?></dt>
            <dd><?= h($healthQuestionnaire->work_phone) ?></dd>
            <dt scope="row"><?= __('Height') ?></dt>
            <dd><?= h($healthQuestionnaire->height) ?></dd>
            <dt scope="row"><?= __('Neck') ?></dt>
            <dd><?= h($healthQuestionnaire->neck) ?></dd>
            <dt scope="row"><?= __('Wrist') ?></dt>
            <dd><?= h($healthQuestionnaire->wrist) ?></dd>
            <dt scope="row"><?= __('Waist') ?></dt>
            <dd><?= h($healthQuestionnaire->waist) ?></dd>
            <dt scope="row"><?= __('Hip') ?></dt>
            <dd><?= h($healthQuestionnaire->hip) ?></dd>
            <dt scope="row"><?= __('Thigh') ?></dt>
            <dd><?= h($healthQuestionnaire->thigh) ?></dd>
            <dt scope="row"><?= __('Size Shirt') ?></dt>
            <dd><?= h($healthQuestionnaire->size_shirt) ?></dd>
            <dt scope="row"><?= __('Size Pants') ?></dt>
            <dd><?= h($healthQuestionnaire->size_pants) ?></dd>
            <dt scope="row"><?= __('Referring Person') ?></dt>
            <dd><?= h($healthQuestionnaire->referring_person) ?></dd>
            <dt scope="row"><?= __('Emergency Name') ?></dt>
            <dd><?= h($healthQuestionnaire->emergency_name) ?></dd>
            <dt scope="row"><?= __('Emergency Phone') ?></dt>
            <dd><?= h($healthQuestionnaire->emergency_phone) ?></dd>
            <dt scope="row"><?= __('Primary Health Provider') ?></dt>
            <dd><?= h($healthQuestionnaire->primary_health_provider) ?></dd>
            <dt scope="row"><?= __('Time Treated') ?></dt>
            <dd><?= h($healthQuestionnaire->time_treated) ?></dd>
            <dt scope="row"><?= __('Condition Treated') ?></dt>
            <dd><?= h($healthQuestionnaire->condition_treated) ?></dd>
            <dt scope="row"><?= __('Shortness Breath Long') ?></dt>
            <dd><?= h($healthQuestionnaire->shortness_breath_long) ?></dd>
            <dt scope="row"><?= __('Kind Alcohol') ?></dt>
            <dd><?= h($healthQuestionnaire->kind_alcohol) ?></dd>
            <dt scope="row"><?= __('Cigarret Packs Day') ?></dt>
            <dd><?= h($healthQuestionnaire->cigarret_packs_day) ?></dd>
            <dt scope="row"><?= __('Chew Day') ?></dt>
            <dd><?= h($healthQuestionnaire->chew_day) ?></dd>
            <dt scope="row"><?= __('Pipe Day') ?></dt>
            <dd><?= h($healthQuestionnaire->pipe_day) ?></dd>
            <dt scope="row"><?= __('Cigars Day') ?></dt>
            <dd><?= h($healthQuestionnaire->cigars_day) ?></dd>
            <dt scope="row"><?= __('Caffeine Form') ?></dt>
            <dd><?= h($healthQuestionnaire->caffeine_form) ?></dd>
            <dt scope="row"><?= __('Caffeine How Much') ?></dt>
            <dd><?= h($healthQuestionnaire->caffeine_how_much) ?></dd>
            <dt scope="row"><?= __('Contraceptive') ?></dt>
            <dd><?= h($healthQuestionnaire->contraceptive) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->id) ?></dd>
            <dt scope="row"><?= __('Cell Phone') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->cell_phone) ?></dd>
            <dt scope="row"><?= __('Gender') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->gender) ?></dd>
            <dt scope="row"><?= __('Marital Status') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->marital_status) ?></dd>
            <dt scope="row"><?= __('Age') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->age) ?></dd>
            <dt scope="row"><?= __('Weight') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->weight) ?></dd>
            <dt scope="row"><?= __('BMI') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->BMI) ?></dd>
            <dt scope="row"><?= __('Procedure Id') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->procedure_id) ?></dd>
            <dt scope="row"><?= __('Allergic Medication') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->allergic_medication) ?></dd>
            <dt scope="row"><?= __('Supplies Allergic') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->supplies_allergic) ?></dd>
            <dt scope="row"><?= __('Allergic Food') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->allergic_food) ?></dd>
            <dt scope="row"><?= __('Blood Transfusion') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->blood_transfusion) ?></dd>
            <dt scope="row"><?= __('Breast Colo Prostate Cancer') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->breast_colo_prostate_cancer) ?></dd>
            <dt scope="row"><?= __('Cancer Specific') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->cancer_specific) ?></dd>
            <dt scope="row"><?= __('Diabetes') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->diabetes) ?></dd>
            <dt scope="row"><?= __('Heart Attack') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->heart_attack) ?></dd>
            <dt scope="row"><?= __('Stroke') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->stroke) ?></dd>
            <dt scope="row"><?= __('High Blood Pressure') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->high_blood_pressure) ?></dd>
            <dt scope="row"><?= __('Arthritis') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->arthritis) ?></dd>
            <dt scope="row"><?= __('Obesity') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->obesity) ?></dd>
            <dt scope="row"><?= __('Shortness Breath') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->shortness_breath) ?></dd>
            <dt scope="row"><?= __('Excercise Regularly') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->excercise_regularly) ?></dd>
            <dt scope="row"><?= __('Asthma') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->asthma) ?></dd>
            <dt scope="row"><?= __('Swelling Ankles') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->swelling_ankles) ?></dd>
            <dt scope="row"><?= __('Thyroid Problems') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->thyroid_problems) ?></dd>
            <dt scope="row"><?= __('Own Diabetes') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->own_diabetes) ?></dd>
            <dt scope="row"><?= __('Monitor Blood Sugar') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->monitor_blood_sugar) ?></dd>
            <dt scope="row"><?= __('Pcos') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->pcos) ?></dd>
            <dt scope="row"><?= __('Own High Blood Pressure') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->own_high_blood_pressure) ?></dd>
            <dt scope="row"><?= __('Chest Pain') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->chest_pain) ?></dd>
            <dt scope="row"><?= __('History Heart Disease') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->history_heart_disease) ?></dd>
            <dt scope="row"><?= __('Own Heart Attack') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->own_heart_attack) ?></dd>
            <dt scope="row"><?= __('Fatty Liver') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->fatty_liver) ?></dd>
            <dt scope="row"><?= __('Rheumatoid Arthritis') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->rheumatoid_arthritis) ?></dd>
            <dt scope="row"><?= __('Nsaids') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->nsaids) ?></dd>
            <dt scope="row"><?= __('Lupus') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->lupus) ?></dd>
            <dt scope="row"><?= __('Hiv') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->hiv) ?></dd>
            <dt scope="row"><?= __('Diverticulitis') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->diverticulitis) ?></dd>
            <dt scope="row"><?= __('Ulcers') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->ulcers) ?></dd>
            <dt scope="row"><?= __('Crohns Disease') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->crohns_disease) ?></dd>
            <dt scope="row"><?= __('Heartburn') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->heartburn) ?></dd>
            <dt scope="row"><?= __('Pain Abdomen') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->pain_abdomen) ?></dd>
            <dt scope="row"><?= __('Bowel Movements') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->bowel_movements) ?></dd>
            <dt scope="row"><?= __('Bloody Stools') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->bloody_stools) ?></dd>
            <dt scope="row"><?= __('Hemorrhoids') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->hemorrhoids) ?></dd>
            <dt scope="row"><?= __('Eating Disorder') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->eating_disorder) ?></dd>
            <dt scope="row"><?= __('Happy Life') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->happy_life) ?></dd>
            <dt scope="row"><?= __('History Depression') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->history_depression) ?></dd>
            <dt scope="row"><?= __('Are You Depressed') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->are_you_depressed) ?></dd>
            <dt scope="row"><?= __('Stress Problem') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->stress_problem) ?></dd>
            <dt scope="row"><?= __('Panic Stressed') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->panic_stressed) ?></dd>
            <dt scope="row"><?= __('Problems Eating') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->problems_eating) ?></dd>
            <dt scope="row"><?= __('Cry Frecuently') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->cry_frecuently) ?></dd>
            <dt scope="row"><?= __('Attempted Suicide') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->attempted_suicide) ?></dd>
            <dt scope="row"><?= __('Thought Hurting Self') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->thought_hurting_self) ?></dd>
            <dt scope="row"><?= __('Trouble Sleeping') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->trouble_sleeping) ?></dd>
            <dt scope="row"><?= __('Been To Counselor') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->been_to_counselor) ?></dd>
            <dt scope="row"><?= __('Drink Alcohol') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->drink_alcohol) ?></dd>
            <dt scope="row"><?= __('Drinks Per Week') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->drinks_per_week) ?></dd>
            <dt scope="row"><?= __('Concerned Drink') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->concerned_drink) ?></dd>
            <dt scope="row"><?= __('Considered Stopping') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->considered_stopping) ?></dd>
            <dt scope="row"><?= __('Blackouts') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->blackouts) ?></dd>
            <dt scope="row"><?= __('Use Tobacco') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->use_tobacco) ?></dd>
            <dt scope="row"><?= __('Number Years') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->number_years) ?></dd>
            <dt scope="row"><?= __('Years Quit') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->years_quit) ?></dd>
            <dt scope="row"><?= __('Use Caffeine') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->use_caffeine) ?></dd>
            <dt scope="row"><?= __('Recreational Drugs') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->recreational_drugs) ?></dd>
            <dt scope="row"><?= __('Drugs Needle') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->drugs_needle) ?></dd>
            <dt scope="row"><?= __('Sexually Active') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->sexually_active) ?></dd>
            <dt scope="row"><?= __('Trying Pregnancy') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->trying_pregnancy) ?></dd>
            <dt scope="row"><?= __('Intercourse Discomfort') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->intercourse_discomfort) ?></dd>
            <dt scope="row"><?= __('Speak About Hiv') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->speak_about_hiv) ?></dd>
            <dt scope="row"><?= __('Ankles Swelling') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->ankles_swelling) ?></dd>
            <dt scope="row"><?= __('Ankles Pain') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->ankles_pain) ?></dd>
            <dt scope="row"><?= __('Ankles Stiffness') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->ankles_stiffness) ?></dd>
            <dt scope="row"><?= __('Ankles Popping') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->ankles_popping) ?></dd>
            <dt scope="row"><?= __('Knees Swelling') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->knees_swelling) ?></dd>
            <dt scope="row"><?= __('Knees Pain') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->knees_pain) ?></dd>
            <dt scope="row"><?= __('Knees Stiffness') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->knees_stiffness) ?></dd>
            <dt scope="row"><?= __('Kness Popping') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->kness_popping) ?></dd>
            <dt scope="row"><?= __('Hips Swelling') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->hips_swelling) ?></dd>
            <dt scope="row"><?= __('Hips Pain') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->hips_pain) ?></dd>
            <dt scope="row"><?= __('Hips Stiffness') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->hips_stiffness) ?></dd>
            <dt scope="row"><?= __('Hips Popping') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->hips_popping) ?></dd>
            <dt scope="row"><?= __('Back Swelling') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->back_swelling) ?></dd>
            <dt scope="row"><?= __('Back Pain') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->back_pain) ?></dd>
            <dt scope="row"><?= __('Back Stiffness') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->back_stiffness) ?></dd>
            <dt scope="row"><?= __('Back Popping') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->back_popping) ?></dd>
            <dt scope="row"><?= __('Others Swelling') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->others_swelling) ?></dd>
            <dt scope="row"><?= __('Others Pain') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->others_pain) ?></dd>
            <dt scope="row"><?= __('Others Stiffing') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->others_stiffing) ?></dd>
            <dt scope="row"><?= __('Others Popping') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->others_popping) ?></dd>
            <dt scope="row"><?= __('Join Medications') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->join_medications) ?></dd>
            <dt scope="row"><?= __('Degenerative Changes') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->degenerative_changes) ?></dd>
            <dt scope="row"><?= __('Weight Loss Surgery') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->weight_loss_surgery) ?></dd>
            <dt scope="row"><?= __('How Long Overweight') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->how_long_overweight) ?></dd>
            <dt scope="row"><?= __('Diet Pills') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->diet_pills) ?></dd>
            <dt scope="row"><?= __('How Much Weight Loss') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->how_much_weight_loss) ?></dd>
            <dt scope="row"><?= __('Max Weight') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->max_weight) ?></dd>
            <dt scope="row"><?= __('Min Weight') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->min_weight) ?></dd>
            <dt scope="row"><?= __('Snaker') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->snaker) ?></dd>
            <dt scope="row"><?= __('Volume Eater') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->volume_eater) ?></dd>
            <dt scope="row"><?= __('Eats Sweets') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->eats_sweets) ?></dd>
            <dt scope="row"><?= __('Carbonated Beverages') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->carbonated_beverages) ?></dd>
            <dt scope="row"><?= __('Type Lifestyle') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->type_lifestyle) ?></dd>
            <dt scope="row"><?= __('Severe Fatigue') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->severe_fatigue) ?></dd>
            <dt scope="row"><?= __('Severe Weekness') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->severe_weekness) ?></dd>
            <dt scope="row"><?= __('Night Sweats') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->night_sweats) ?></dd>
            <dt scope="row"><?= __('Severe Headaches') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->severe_headaches) ?></dd>
            <dt scope="row"><?= __('Loss Of Conciousness') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->loss_of_conciousness) ?></dd>
            <dt scope="row"><?= __('Hearing Problems') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->hearing_problems) ?></dd>
            <dt scope="row"><?= __('Ear Pain') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->ear_pain) ?></dd>
            <dt scope="row"><?= __('Nasal Congestion') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->nasal_congestion) ?></dd>
            <dt scope="row"><?= __('Sinus Congestion') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->sinus_congestion) ?></dd>
            <dt scope="row"><?= __('Bloody Nose') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->bloody_nose) ?></dd>
            <dt scope="row"><?= __('Dental Problems') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->dental_problems) ?></dd>
            <dt scope="row"><?= __('Dentures') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->dentures) ?></dd>
            <dt scope="row"><?= __('Sores In Mouth') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->sores_in_mouth) ?></dd>
            <dt scope="row"><?= __('Wheezing') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->wheezing) ?></dd>
            <dt scope="row"><?= __('Coughing') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->coughing) ?></dd>
            <dt scope="row"><?= __('Breast Lump') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->breast_lump) ?></dd>
            <dt scope="row"><?= __('Heart Murmur') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->heart_murmur) ?></dd>
            <dt scope="row"><?= __('High Blood Pressure2') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->high_blood_pressure2) ?></dd>
            <dt scope="row"><?= __('Chest Pain Excercise') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->chest_pain_excercise) ?></dd>
            <dt scope="row"><?= __('Std') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->std) ?></dd>
            <dt scope="row"><?= __('Anemia') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->anemia) ?></dd>
            <dt scope="row"><?= __('Bleeding Tendency') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->bleeding_tendency) ?></dd>
            <dt scope="row"><?= __('Convulsions') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->convulsions) ?></dd>
            <dt scope="row"><?= __('Paralysis') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->paralysis) ?></dd>
            <dt scope="row"><?= __('Numbness') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->numbness) ?></dd>
            <dt scope="row"><?= __('Memory Loss') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->memory_loss) ?></dd>
            <dt scope="row"><?= __('Depression') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->depression) ?></dd>
            <dt scope="row"><?= __('Anxiety') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->anxiety) ?></dd>
            <dt scope="row"><?= __('Mood Swings') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->mood_swings) ?></dd>
            <dt scope="row"><?= __('Sleep Problems') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->sleep_problems) ?></dd>
            <dt scope="row"><?= __('Drug Alcochol Abuse') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->drug_alcochol_abuse) ?></dd>
            <dt scope="row"><?= __('Rash Hives') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->rash_hives) ?></dd>
            <dt scope="row"><?= __('Asthma2') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->asthma2) ?></dd>
            <dt scope="row"><?= __('Hay Fever') ?></dt>
            <dd><?= $this->Number->format($healthQuestionnaire->hay_fever) ?></dd>
            <dt scope="row"><?= __('Date Of Birth') ?></dt>
            <dd><?= h($healthQuestionnaire->date_of_birth) ?></dd>
            <dt scope="row"><?= __('Suggested Date') ?></dt>
            <dd><?= h($healthQuestionnaire->suggested_date) ?></dd>
            <dt scope="row"><?= __('Date Of Referral') ?></dt>
            <dd><?= h($healthQuestionnaire->date_of_referral) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($healthQuestionnaire->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($healthQuestionnaire->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Address') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->address); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Allergic Medication List') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->allergic_medication_list); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Supplies Allergic List') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->supplies_allergic_list); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Allergic Food List') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->allergic_food_list); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Transfusion Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->transfusion_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Decrese Swelling') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->decrese_swelling); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Decrease Pain') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->decrease_pain); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Medications For Diabetes') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->medications_for_diabetes); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Monitor How Ofter') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->monitor_how_ofter); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Medications Blood Pressure') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->medications_blood_pressure); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Chest Pain Often') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->chest_pain_often); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Explain Heart Dicease') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->explain_heart_dicease); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Fatty Liver Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->fatty_liver_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Heartburn Long') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->heartburn_long); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Pain Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->pain_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Drugs Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->drugs_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Treatment1') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->treatment1); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Treatment2') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->treatment2); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Join Medications Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->join_medications_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Degenerative Changes Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->degenerative_changes_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('How Quickly Regain') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->how_quickly_regain); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Foods Liked') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->foods_liked); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Foods Disliked') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->foods_disliked); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Sweets How Ofter') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->sweets_how_ofter); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Severe Fatigue Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->severe_fatigue_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Severe Weekness Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->severe_weekness_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Night Sweats Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->night_sweats_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Severe Headaches Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->severe_headaches_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Loss Of Conciousness Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->loss_of_conciousness_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Hearing Problems Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->hearing_problems_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Ear Pain Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->ear_pain_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Nasal Congestion Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->nasal_congestion_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Sinus Congestion Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->sinus_congestion_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Bloody Nose Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->bloody_nose_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Dental Problems Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->dental_problems_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Dentures Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->dentures_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Sores In Mouth Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->sores_in_mouth_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Wheezing Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->wheezing_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Coughing Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->coughing_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Breast Lump Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->breast_lump_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Heart Murmur Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->heart_murmur_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('High Blood Pressure2 Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->high_blood_pressure2_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Chest Pain Excercise Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->chest_pain_excercise_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Std Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->std_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Anemia Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->anemia_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Bleeding Tendency Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->bleeding_tendency_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Convulsions Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->convulsions_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Paralysis Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->paralysis_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Numbness Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->numbness_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Memory Loss Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->memory_loss_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Depression Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->depression_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Anxiety Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->anxiety_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Mood Swings Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->mood_swings_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Sleep Problems Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->sleep_problems_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Drug Alcochol Abuse Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->drug_alcochol_abuse_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Rash Hives Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->rash_hives_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Asthma2 Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->asthma2_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Hay Fever Details') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->hay_fever_details); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Additional Information') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($healthQuestionnaire->additional_information); ?>
        </div>
      </div>
    </div>
  </div>
</section>
