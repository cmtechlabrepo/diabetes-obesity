<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HealthQuestionnaire $healthQuestionnaire
 */
?>
<?php echo $this->Html->script('health-questionnaires/add.js'); ?>



<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Health Questionnaire
      <!-- <small><?php echo __('The information you provide will help us to plan your treatment, please carefully fill it out.'); ?></small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->

        <!-- Aqui -->
        <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">The information you provide will help us to plan your treatment, please carefully fill it out.</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
      <!-- Aqui -->
       <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                  <?php  echo __('PATIENT INFORMATION'); ?>
              </a>
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo $this->Form->create($healthQuestionnaire, ['role' => 'form']); ?>
             <div class="box-body">
         <div id="collapseOne" class="panel-collapse collapse in">
              <div class="box-body">
                <!-- <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('location',['required' => true]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('doctor_id',['label'=>'Surgeon','options'=>$doctors,'empty' => true]);
                     ?>
                  </div>
                </div> -->
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('name',['required' => true]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('address',['required' => true]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('city',['required' => true]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('state',['required' => true]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('zip_code',['required' => true]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('home_phone',['required' => true]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('work_phone');
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('cell_phone');
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('email', ['type'=>'text']);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('date_of_birth', ['type'=>'text','id'=>'datepicker1']);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('gender',['type' => 'select', 'options' => [1 => __('Male') , 2 => __('Female')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('marital_status',['type' => 'select', 'options' => [1 => __('Single') , 2 => __('Married'), 3 => __('Divorced'), 4 => __('Separated'), 5 => __('Widow/Widower')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('age',['label'=>'Age (years)']);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('height',['label'=>'Height (inches)']);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('weight',['label'=>'Weight (pounds)']);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('BMI');
                     ?>
                  </div>
                </div>

                  <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Measurements'); ?></h3>
                  </div>
                  <div class="row">
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('neck',['label'=>'Neck (inches)']);
                       ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('wrist',['label'=>'Wrist (inches)']);
                       ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('waist',['label'=>'Waist (inches)']);
                       ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                      <?php
                       echo $this->Form->control('hip',['label'=>'Hip (inches)']);
                       ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-xs-12">
                      <?php
                       echo $this->Form->control('thigh',['label'=>'Thigh (inches)']);
                       ?>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <?php
                       echo $this->Form->control('size_shirt');
                       ?>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <?php
                       echo $this->Form->control('size_pants');
                       ?>
                    </div>
                  </div>
              </div>
            </div>
          </div>

          
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                  <?php echo __('PROCEDURE'); ?>
                </a>
              </h3>
              </div>
            <div id="collapse2" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('procedure_id',['label'=>"Please indicate which procedure you are interested in"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('suggested_date', ['label'=>'Suggested date. Do you have any date on mind to have your procedure?','type'=>'text','id'=>'datepicker2']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                    echo $this->Form->control('details');
                    ?>
                  </div>
                </div>

              </div>
            </div>
          </div>


          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                  <?php echo __('REFERRAL INFORMATION'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                    echo $this->Form->control('referring_person');
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                    echo $this->Form->control('date_of_referral', ['type'=>'text','id'=>'datepicker3']);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                  <?php echo __('EMERGENCY CONTACT INFO'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('emergency_name');
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('emergency_phone');
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                  <?php echo __('PRIMARY HEALTH CARE PROVIDER'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('primary_health_provider');
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('time_treated',['label'=>"How long has he/she trated you?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('condition_treated',['label'=>'Condition treated?']);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                  <?php echo __('ALLERGY'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('allergic_medication',['label'=>"Are you allergic to any MEDICATION?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('allergic_medication_list',['label'=>"Please list"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                    echo $this->Form->control('supplies_allergic',['label'=>"Are you allergic to any WOUND CARE SUPPLIES (tape, latex, isodine, alcohol, merthiolate, talcum powder, etc)?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('supplies_allergic_list',['label'=>"Please list"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('allergic_food',['label'=>"Are you allergic to any FOOD (fruits, vegetables, seeds, meat, fish, egg, dairy, etc)?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('allergic_food_list',['label'=>"Please list"]);
                     ?>
                  </div>
                </div>
                
              </div>
            </div>
          </div>

          <!-- REGRESAR AQUI -->
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse24">
                  <?php echo __('CURRENT MEDICATION (Include vitamins, over the counter medication, etc)'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse24" class="panel-collapse collapse">
              <div class="box-body">
              <input type="checkbox" name="no-medication" value=1><b>Check this if you are not taking any medications or please list each one below</b><br><br>

                <div class="box-body table-responsive no-padding">
                  <table id="medication-list" class="table table-hover">
                    <thead>
                      <tr>
                          <th scope="col">Name of medication</th>
                          <th scope="col">Dose</th>
                          <th scope="col">How often taken</th>                  
                          <th scope="col">Purpose</th>
                          <th scope="col">When use started</th>
                          <th scope="col">Brand</th>
                          <th scope="col">Add / Remove</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr id="medication1">
                            <td><input name='medicationname1' type="text" id="medicationname1"></td>
                            <td><input name="dose1" type="text" id="dose1"></td>
                            <td><input name="oftentaken1" type="text" id="oftentaken1"></td>                  
                            <td><input name="purpose1" type="text" id="purpose1"></td>
                            <td><input name="started1" class="started" type="text" id="started1"></td>
                            <td><input name="medication_brand1" type="text" id="medication_brand1"></td>
                            <td>                    
                              <a href="#" class="add add-medication" ><i class="fa fa-fw fa-plus"></i></a>
                              <a href="#" class="remove remove-medication" ><i class="fa fa-fw fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse25">
                  <?php echo __('LIST OF ANY MAJOR ILLNESSES'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse25" class="panel-collapse collapse">
              <div class="box-body">
                <input type="checkbox" name="no-ilness" value=1><b>Check this if you dont have any ilnesses or please list each one below</b><br><br>
                <div class="box-body table-responsive no-padding">
                  <table id="illness-list" class="table table-hover">
                    <thead>
                      <tr>
                          <th scope="col">Date</th>
                          <th scope="col">Illness</th>
                          <th scope="col">Treatment</th>                  
                          <th scope="col">Outcome</th>
                          <th scope="col">Add / Remove</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr id="illness1">
                            <td><input class="dateillness" name='dateillness1' type="text" id="dateillness1"></td>
                            <td><input name="illness1" type="text" id="illness1"></td>
                            <td><input name="treatment1" type="text" id="treatment1"></td>                  
                            <td><input name="outcome1" type="text" id="outcome1"></td>
                            <td>                    
                              <a href="#" class="add add-illness" ><i class="fa fa-fw fa-plus"></i></a>
                              <a href="#" class="remove remove-illness" ><i class="fa fa-fw fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse26">
                  <?php echo __('LIST ANY SURGERIES'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse26" class="panel-collapse collapse">
              <div class="box-body">
              <input type="checkbox" name="no-surgery" value=1><b>Check this if you have not had any surgeries or please list each one below</b><br><br>
                <div class="box-body table-responsive no-padding">
                  <table id="surgery-list" class="table table-hover">
                    <thead>
                      <tr>
                          <th scope="col">Surgery</th>
                          <th scope="col">Date</th>
                          <th scope="col">Reason</th>                  
                          <th scope="col">Hospital</th>
                          <th scope="col">Add / Remove</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr id="surgery1">
                            <td><input name='surgeryname1' type="text" id="surgeryname1"></td>
                            <td><input class="datesurgery" name="datesurgery1" type="text" id="datesurgery1"></td>
                            <td><input name="reason1" type="text" id="reason1"></td>                  
                            <td><input name="hospital1" type="text" id="hospital1"></td>
                            <td>                    
                              <a href="#" class="add add-surgery" ><i class="fa fa-fw fa-plus"></i></a>
                              <a href="#" class="remove remove-surgery" ><i class="fa fa-fw fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('blood_transfusion',['label'=>"Have you ever had a blood transfusion?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('transfusion_details',['label'=>'When and details']);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse27">
                  <?php echo __('OTHER HOSPITALIZATIONS'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse27" class="panel-collapse collapse">
              <div class="box-body">
              <input type="checkbox" name="no-hospitalizations" value=1><b>Check this if you have not had any hospitalizations or please list each one below</b><br><br>
                <div class="box-body table-responsive no-padding">
                  <table id="hospitalization-list" class="table table-hover">
                    <thead>
                      <tr>
                          <th scope="col">Date</th>
                          <th scope="col">Reason</th>                  
                          <th scope="col">Hospital</th>
                          <th scope="col">Add / Remove</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr id="hospitalization1">
                            <td><input class="datehospitalization" name="datehospitalization1" type="text" id="datehospitalization1"></td>
                            <td><input name="reasonhospitalization1" type="text" id="reasonhospitalization1"></td>                  
                            <td><input name="hospitalhospitalization1" type="text" id="hospitalhospitalization1"></td>
                            <td>                    
                              <a href="#" class="add add-hospitalization" ><i class="fa fa-fw fa-plus"></i></a>
                              <a href="#" class="remove remove-hospitalization" ><i class="fa fa-fw fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                  <?php echo __('FAMILY HEALTH HISTORY  (Immediate family only)'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse7" class="panel-collapse collapse">
              <div class="box-body">
              <input type="checkbox" name="no-family" value=1><b>Check this if none of your immediate family has any relevant medical problems or please list each one below</b><br><br>
                <div class="box-body table-responsive no-padding">
                  <table id="family-list" class="table table-hover">
                    <thead>
                      <tr>
                          <th scope="col">Family member</th>
                          <th scope="col">Age</th>
                          <th scope="col">Deceased?</th>
                          <th scope="col">Cause of death</th>
                          <th scope="col">Complexion</th>
                          <th scope="col">Health Problems</th>
                          <th scope="col">Add / Remove</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr id="family1">
                            <td><input name='family_member1' type="text" id="family_member1"></td>
                            <td><input name="familyage1" type="text" id="familyage1"></td>
                            <td>
                              <select name="deceased1" id="deceased1">
                                <option>Select</option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>                                
                              </select>
                            </td>
                            <td><input name="cause_of_death1" type="text" id="cause_of_death1"></td>                            
                            <td>
                              <select name="familycomplexion1" id="familycomplexion1">
                                <option>Select</option>
                                <option value="1">Thin</option>
                                <option value="2">Normal weight</option>
                                <option value="3">Slightly overweight</option>
                                <option value="4">Moderately overweight</option>
                              </select>
                            </td>
                            <td><input name="familyhealth1" type="text" id="familyhealth1"></td>
                            <td>                    
                              <a href="#" class="add add-family" ><i class="fa fa-fw fa-plus"></i></a>
                              <a href="#" class="remove remove-family" ><i class="fa fa-fw fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>

                <label>What other family members have or have had (indicate mother´s / father´s side of your family):</label>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('breast_colo_prostate_cancer',['label'=>"Breast, Colon or Prostate Cancer",'type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side'), 3 => __('Both sides')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('cancer_specific',['label'=>"Cancer (specify type)",'type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side'), 3 => __('Both sides')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('diabetes',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side'), 3 => __('Both sides')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('heart_attack',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side'), 3 => __('Both sides')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('stroke',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side'), 3 => __('Both sides')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('high_blood_pressure',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side'), 3 => __('Both sides')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('arthritis',['label'=>'Arthritis or back trouble','type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side'), 3 => __('Both sides')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                     echo $this->Form->control('obesity',['type' => 'select', 'options' => [1 => __('Mother side') , 2 => __('Father side'), 3 => __('Both sides')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                  <?php echo __('RESPIRATORY SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse8" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('shortness_breath',['label'=>"Do you experience shortness of breath with physical activity?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('shortness_breath_long',['label'=>'How long have you been aware of this?']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('excercise_regularly',['label'=>"Do you excercise regularly?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('asthma',['label'=>"Do you have or have you had asthma?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse29">
                <?php echo __('SLEEP EVALUATION'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse29" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('snore',['label'=>"Do you snore?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('average_sleep',['label'=>"On average, how many hours you sleep every day?",'type' => 'text']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                     <?php
                     echo $this->Form->control('wakeup_tired',['label'=>"When you wake up, do you feel tired?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('tired_sleepy',['label'=>"During the day, are you tired or sleepy?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                     <?php
                     echo $this->Form->control('cpap',['label'=>"Do you use CPAP?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
                <?php echo __('BLOOD AND CIRCULATORY SYSTEM'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse9" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('swelling_ankles',['label'=>"Do you experience swelling of the ankles?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <?php
                echo $this->Form->control('decrese_swelling',['label','What do you do to decrease the swelling?']);
                echo $this->Form->control('decrease_pain',['label','What do you take to relieve the pain?']);
                ?>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
                  <?php echo __('ENDOCRINOLOGY SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse10" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('thyroid_problems',['label'=>"Thyroid problems?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('own_diabetes',['label'=>"Are you diabetic?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('medications_for_diabetes',['label'=>'What are you taking for your diabetes?']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('monitor_blood_sugar',['label'=>"Do you monitor your blood sugar?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('monitor_how_ofter',['label'=>"How often?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('pcos',['label'=>"Have you been diagnosed with PCOS?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">
                  <?php echo __('CARDIAC SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse11" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('own_high_blood_pressure',['label'=>"Do you have high blood pressure?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('medications_blood_pressure',['label'=>"What are you raking for your high blood pressure?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('chest_pain',['label'=>"Do you experience chest pain?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('chest_pain_often',['label'=>"How often"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('history_heart_disease',['label'=>"Do you have any history of heart disease?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('explain_heart_dicease',['label'=>"Explain"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('own_heart_attack',['label'=>"Have you had a heart attack?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">
                  <?php echo __('HEPATIC SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse12" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('fatty_liver',['label'=>"Have you diagnosed with fatty liver, cirrhosis, hepatitis or any liver disease?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('fatty_liver_details',['label'=>'Details']);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">
                  <?php echo __('AUTO IMMUNE SYSTEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse13" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('rheumatoid_arthritis',['label'=>"Have you been diagnosed with Rheumatoid Arthritis?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('nsaids',['label'=>"Are you taking or have you taken nonsteroidal anti-inflammatory drugs (NSAIDs) for joint pain?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('lupus',['label'=>"Have you been diagnosed for Lupus?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('hiv',['label'=>"Have you been diagnosed as HIV positive?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('other_autoinmmune',['label'=>"ANY OTHER (Multiple Sclerosis, Fibromyalgia, ETC)",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse14">
              <?php echo __('DIGESTION PROBLEMS'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse14" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('constipation',['label'=>"Do you regularly have constipation?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('diverticulitis',['label'=>"Have you been diagnosed with diverticulitis?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('ulcers',['label'=>"Do you any history of ulcers?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('crohns_disease',['label'=>"Have you been disgnosed with Crohn´s disease or Ulcerative Colitis?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('heartburn',['label'=>"Do you have indigestion or heartburn?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('heartburn_long',['label'=>"If so, for how long"]);
                     ?>
                  </div>
                </div>

                <label>What foods or drinks cause digestive problems for you?</label>
                <br><input type="checkbox" name="no-digestive" value=1><b>Check this if there isn't a food or drink that causes digestive problems for you</b><br><br>
                <div class="box-body table-responsive no-padding">
                  <table id="digestive-list" class="table table-hover">
                    <thead>
                      <tr>
                          <th scope="col">Food / Drink</th>
                          <th scope="col">Result of eating / drinking</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr id="digestive1">
                            <td><input name='digestivefood1' type="text" id="digestivefood1"></td>
                            <td><input name="digestiveresult1" type="text" id="digestiveresult1"></td>
                            <td>                    
                              <a href="#" class="add add-digestive" ><i class="fa fa-fw fa-plus"></i></a>
                              <a href="#" class="remove remove-digestive" ><i class="fa fa-fw fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>

                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('pain_abdomen',['label'=>"Do you ever have any type or pain in the abdomen?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('pain_details',['label'=>'Pain details (sharp, dull, hot, cold, aching, crushing, etc)']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('bowel_movements',['label'=>"Any changes in bowel movements?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('bloody_stools',['label'=>"Any bloody stools?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('hemorrhoids',['label'=>"History of hemorrhoids?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse15">
              <?php echo __('MENTAL HEALTH'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse15" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('eating_disorder',['label'=>"Have you ever been treated for an eating disorder?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('happy_life',['label'=>"Are generally happy with your life other than your weight?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('history_depression',['label'=>"Do you have a history of depression?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('are_you_depressed',['label'=>"Do you feel depressed?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('stress_problem',['label'=>"Is stress a major problem for you?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('panic_stressed',['label'=>"Do you panic when stressed?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('problems_eating',['label'=>"Do you have problems with eating or your appetite?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('cry_frecuently',['label'=>"Do you cry frequently?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('attempted_suicide',['label'=>"Have you ever attempted suicide?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('thought_hurting_self',['label'=>"Have you ever seriously thought about hurting yourself?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('trouble_sleeping',['label'=>"Do you have trouble sleeping?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('been_to_counselor',['label'=>"Have you ever been to a counselor?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse16">
              <?php echo __('ALCOHOL'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse16" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('drink_alcohol',['label'=>"Do you drink alcohol?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('kind_alcohol',['label'=>"If yes, what kind?"]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                     echo $this->Form->control('frecuency',['label'=>"Frecuency?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('how_drink_it',['label'=>"How do you drink it?",'type' => 'select', 'options' => [1 => __('Plain') , 2 => __('Mixed')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('mixed_preference',['label'=>"When is mixed, what do you prefer?",'type' => 'select', 'options' => [1 => __('Soda') , 2 => __('Juice'), 3 => __('carbonated water')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('eat_drinking',['label'=>"Do you eat something when you are drinking?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('eat_what',['label'=>"What do you usually eat?",'type' => 'text']);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('drinks_per_week',['label'=>"How many drinks per week?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('concerned_drink',['label'=>"Are you concerned about the amount you drink?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('considered_stopping',['label'=>"Have you considered stopping?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('blackouts',['label'=>"Have you ever experienced blackouts?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse17">
                  <?php echo __('TOBACCO'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse17" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('use_tobacco',['label'=>"Do you use tobacco?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-xs-12">
                    <?php
                      echo $this->Form->control('cigarret_packs_day',['label'=>"Cigarrettes - pks./day"]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                      echo $this->Form->control('chew_day',['label'=>"Chew - #/day"]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                      echo $this->Form->control('pipe_day',['label'=>"Pipe- #/day"]);
                     ?>
                  </div>
                  <div class="col-md-3 col-xs-12">
                    <?php
                      echo $this->Form->control('cigars_day',['label'=>"Cigars - #/day"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-md-12">
                    <?php
                      echo $this->Form->control('number_years',['label'=>"# of years"]);
                     ?>
                  </div>
                  <div class="col-md-3 col-md-12">
                    <?php
                      echo $this->Form->control('years_quit',['label'=>"Or Year Quit"]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse18">
                <?php echo __('CAFFEINE'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse18" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('use_caffeine',['label'=>"Do you use caffeine (coffee, cola, chocolate, energetic beverages)",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('caffeine_form',['label'=>"If yes, in what form?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('caffeine_how_much',['label'=>"How much per day?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <label>In one day, what is the volume of liquid that you are drinking of?</label>
                  <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                      <tr>
                        <th></th>
                        <th>How many</th>
                        <th>Brand</th>
                      </tr>
                      <tr>
                        <th>Carbonated</th>
                        <td>
                          <?php
                          echo $this->Form->control('carbonated_quantity',['label'=>false]);
                          ?>
                        </td>
                        <td>
                          <?php
                          echo $this->Form->control('carbonated_brand',['label'=>false]);
                          ?>
                        </td>
                      </tr>
                      <tr>
                        <th>Sugary</th>
                        <td>
                          <?php
                          echo $this->Form->control('sugary_quantity',['label'=>false]);
                          ?>
                        </td>
                        <td>
                          <?php
                          echo $this->Form->control('sugary_brand',['label'=>false]);
                          ?>
                        </td>
                      </tr>
                      <tr>
                        <th>Natural</th>
                        <td>
                          <?php
                          echo $this->Form->control('natural_quantity',['label'=>false]);
                          ?>
                        </td>
                        <td>
                          <?php
                          echo $this->Form->control('natural_brand',['label'=>false,'type'=>'text']);
                          ?>
                        </td>
                      </tr>                      
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse19">
              <?php echo __('DRUGS'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse19" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('recreational_drugs',['label'=>"Do you currently use recreational or street drugs?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('drugs_details',['label'=>"If yes, please give details:"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('drugs_needle',['label'=>"Have you ever given yourself street drugs with a needle?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse20">
              <?php echo __('SEX'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse20" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('sexually_active',['label'=>"Are you sexually active?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('trying_pregnancy',['label'=>"If yes, are you trying for a pregnancy?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('contraceptive',['label'=>'If not, trying for a pregnancy list contraceptive or barrier mehod used']);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('intercourse_discomfort',['label'=>"Any discomfort with intercourse?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                    echo $this->Form->control('speak_about_hiv',['label'=>"Illnes related to the Human Immunodeficiency Virus (HIV), such as AIDS, has become a major public health problem. Risk factors for
                    this illmess include intravenous drug use and unprotected sexual intercourse. Would you like to speak with your provider about
                    your risk of this illmess?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                </div>
            </div>
          </div>
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse28">
                  <?php echo __('BONE OR JOINT PROBLEM'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse28" class="panel-collapse collapse">
              <div class="box-body table-responsive no-padding">
                <table id="bone-list" class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">Location</th>
                      <th scope="col">Swelling</th>
                      <th scope="col">Pain</th>
                      <th scope="col">Stiffness</th>
                      <th scope="col">Popping Crackling</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Ankles</td>
                      <td><?php echo $this->Form->checkbox('ankles_swelling', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('ankles_pain', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('ankles_stiffness', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('ankles_popping', ['label'=>false,'value' => 1]);?></td>
                    </tr>
                    <tr>
                      <td>Knees</td>
                      <td><?php echo $this->Form->checkbox('knees_swelling', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('knees_pain', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('knees_stiffness', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('kness_popping', ['label'=>false,'value' => 1]);?></td>
                    </tr>
                    <tr>
                      <td>Hips</td>
                      <td><?php echo $this->Form->checkbox('hips_swelling', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('hips_pain', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('hips_stiffness', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('hips_popping', ['label'=>false,'value' => 1]);?></td>
                    </tr>
                    <tr>
                      <td>Back</td>
                      <td><?php echo $this->Form->checkbox('back_swelling', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('back_pain', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('back_stiffness', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('back_popping', ['label'=>false,'value' => 1]);?></td>
                    </tr>
                    <tr>
                      <td>Others</td>
                      <td><?php echo $this->Form->checkbox('others_swelling', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('others_pain', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('others_stiffness', ['label'=>false,'value' => 1]);?></td>
                      <td><?php echo $this->Form->checkbox('others_popping', ['label'=>false,'value' => 1]);?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <label>Have you ever sought treatment for bone or joint problems or injuries? Give details below (include physical therapy and chiropractic)</label>
              <br><input type="checkbox" name="no-bone" value=1><b>Check this if you've never sought treatment for bone or joint problems or injuries </b><br><br>
              <div class="box-body table-responsive no-padding">
                <table id="dr-list" class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">Doctor</th>
                      <th scope="col">Date of treament</th>
                      <th scope="col">Diagnosis / Treatment</th>
                      <th scope="col">Add / Remove</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr id="bone1">
                      <td><input name='bonedoctor1' type="text" id="bonedoctor1"></td>
                      <td><input class="bonedate" name="bonedate1" type="text" id="bonedate1"></td>
                      <td><input name="bonediagnosis1" type="text" id="bonediagnosis1"></td>
                      <td>
                        <a href="#" class="add add-bone" ><i class="fa fa-fw fa-plus"></i></a>
                        <a href="#" class="remove remove-bone" ><i class="fa fa-fw fa-times"></i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse21">
                  <?php echo __('DIET HISTORY'); ?>
                </a>
              </h3>
            </div>
            <div id="collapse21" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('weight_loss_surgery',['label'=>"Have you ever had surgery to aid in weight loss?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('how_long_overweight',['label'=>"How long (years) have you been overweight?"]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('diet_pills',['label'=>"Have you tried diet pills?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <label>Which diet programs have you tried? Check the following diet programs that apply to you:</label>
                <?php
                $checkboxarray= array(
                  "Diet medication",
                  "Medifast",
                  "Rader Institute",
                  "Prikin diet",
                  "Living Well Lady",
                  "Hypnosis",
                  "Slim Fast",
                  "Topfast",
                  "Optifast",
                  "Low-Calorie- Diet",
                  "Jenny Craig",
                  "air force Diet",
                  "Subliminal tapes",
                  "Overeaters anonymus",
                  "Diet Center",
                  "Physician Supervised Diet",
                  "Numerous Book Diets",
                  "Self-imposed fasts",
                  "High- Protein Diet",
                  "Virginia Mason Clinic",
                  "Cabrini Eating disorder",
                  "Weight Watchers",
                  "Mayo Clinic",
                  "Other",
                );
                echo $this->Form->select('diet_programs',$checkboxarray, array('multiple'=>'checkbox'));
                ?>

                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('how_much_weight_loss',['label'=>"How much weight (pounds) did you lose with the diet program (s)?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                      echo $this->Form->control('how_quickly_regain',['label'=>"How quickly did you regain the weight afterwards?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('max_weight',['label'=>"What has been your MAXIMUM WEIGHT (pounds)?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('how_long_max',['label'=>"How long ago was that (years)?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('min_weight',['label'=>"What has been your MINIMUM WEIGHT (pounds)?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('how_long_min',['label'=>"How long ago was that (years)?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('expected_loss',['label'=>"How much weight are you expecting to lose (pounds)?"]);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('expected_time',['label'=>"In how much time?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <label>Enlist the food your like the most and the food you dislike the most</label>
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('foods_liked',['label'=>"LIKE:"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                     echo $this->Form->control('foods_disliked',['label'=>"DISLIKE:"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('snaker',['label'=>"Are you snacker?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('volume_eater',['label'=>"Are you a volume eater?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                  <div class="col-md-4 col-xs-12">
                    <?php
                      echo $this->Form->control('eats_sweets',['label'=>"Do you eat a lot of sweets?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('sweets_how_ofter',['label'=>"How often do you eat sweets?"]);
                     ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                      echo $this->Form->control('carbonated_beverages',['label'=>"Do you frequently eat fast food and/or do you drink carbonated beverages?",'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                     ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse22">
                <?php echo __('EXERCISE'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse22" class="panel-collapse collapse">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <?php
                    echo $this->Form->control('type_lifestyle',['label'=>"Please select the option that best suits your lifestyle",'type' => 'select', 'options' => [1 => __('Sedentary (No excercise)') , 2 => __('Mild excercise (i.e., daily activity, domestic work, go to the market, walk the dog, climb stairs, walk 3 blocks, golf)'), 3 => __('Occasional vigorous exercise (i.e. work or recreation, less than 120 min/week)'), 4 => __('Regular vigorous exercise (i.e. work or recreation more than 120 min/week)')], 'empty' => __('Select')]);
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse23">
              <?php echo __('REVIEW OF SYSTEMS: Unless otherwise specified, mark the correct box and provide any information about your current status.'); ?>
              </a>
              </h3>
            </div>
            <div id="collapse23" class="panel-collapse collapse">
              <div class="box-body">
              <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                          <th scope="col">Add / Remove</th>
                          <th scope="col">Yes/No</th>
                          <th scope="col">Details or comments</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>FREQUENT OR SEVERE FATIGUE</td>
                            <td>
                              <?php
                              echo $this->Form->control('severe_fatigue',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                              ?>
                            </td>
                            <td>
                              <?php
                                echo $this->Form->control('severe_fatigue_details',['label'=>false]);
                              ?>
                            </td>                  
                        </tr>
                        <tr>
                          <td>FREQUENT OR SEVERE WEAKNESS</td>
                          <td>
                            <?php
                              echo $this->Form->control('severe_weekness',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                               echo $this->Form->control('severe_weekness_details',['label'=>false]);
                            ?>
                          </td>                  
                        </tr>
                        <tr>
                          <td>FEVER, CHILLS OR NIGHT SWEATS</td>
                          <td>
                            <?php
                              echo $this->Form->control('night_sweats',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                               echo $this->Form->control('night_sweats_details',['label'=>false]);
                            ?>
                          </td>                  
                        </tr>                        
                        <tr>
                          <td>FREQUENT OR SEVERE HEADACHES</td>
                          <td>
                            <?php
                              echo $this->Form->control('severe_headaches',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                               echo $this->Form->control('severe_headaches_details',['label'=>false]);
                            ?>
                          </td>                  
                        </tr>
                        <tr>
                          <td>ANY HISTORY OF HEAD INJURY WITH LOSS OF CONSCIOUSNESS</td>
                          <td>
                            <?php
                              echo $this->Form->control('loss_of_conciousness',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                               echo $this->Form->control('loss_of_conciousness_details',['label'=>false]);
                            ?>
                          </td>                  
                        </tr>
                        <tr>
                          <td>HEARING PROBLEMS</td>
                          <td>
                            <?php
                              echo $this->Form->control('hearing_problems',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                               echo $this->Form->control('hearing_problems_details',['label'=>false]);
                            ?>
                          </td>                  
                        </tr>
                        <tr>
                          <td>EAR PAIN</td>
                          <td>
                            <?php
                              echo $this->Form->control('ear_pain',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                               echo $this->Form->control('ear_pain_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>NASAL CONGESTION</td>
                          <td>
                            <?php
                              echo $this->Form->control('nasal_congestion',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                               echo $this->Form->control('nasal_congestion_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>CHRONIC SINUS CONGESTION</td>
                          <td>
                            <?php
                              echo $this->Form->control('sinus_congestion',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                               echo $this->Form->control('sinus_congestion_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>FREQUENT BLOODY NOSE</td>
                          <td>
                            <?php
                              echo $this->Form->control('bloody_nose',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('bloody_nose_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>DENTALS PROBLEMS</td>
                          <td>
                            <?php
                              echo $this->Form->control('dental_problems',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('dental_problems_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>DENTURES</td>
                          <td>
                            <?php
                              echo $this->Form->control('dentures',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('dentures_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>SORES IN MOUTH</td>
                          <td>
                            <?php
                              echo $this->Form->control('sores_in_mouth',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('sores_in_mouth_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>WHEEZING</td>
                          <td>
                            <?php
                              echo $this->Form->control('wheezing',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $this->Form->control('wheezing_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>COUGHING</td>
                          <td>
                            <?php
                              echo $this->Form->control('coughing',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('coughing_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>BREAST LUMP, PAIN OR DISCHARGE</td>
                          <td>
                            <?php
                              echo $this->Form->control('breast_lump',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('breast_lump_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>HEART MURMUR</td>
                          <td>
                            <?php
                              echo $this->Form->control('heart_murmur',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('heart_murmur_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>HIGH BLOOD PRESSURE</td>
                          <td>
                            <?php
                              echo $this->Form->control('high_blood_pressure2',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('high_blood_pressure2_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>CHEST PAIN WITH EXCERCISE OR ACTIVITY</td>
                          <td>
                            <?php
                              echo $this->Form->control('chest_pain_excercise',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('chest_pain_excercise_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>ANY SEXUALLY TRANSMITTED DISEASE THAT WAS NOT TREATED</td>
                          <td>
                            <?php
                              echo $this->Form->control('std',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('std_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>ANEMIA</td>
                          <td>
                            <?php
                              echo $this->Form->control('anemia',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('anemia_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>BLEEDING TENDENCY</td>
                          <td>
                            <?php
                              echo $this->Form->control('bleeding_tendency',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('bleeding_tendency_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>CONVULSIONS, SEIZURES</td>
                          <td>
                            <?php
                              echo $this->Form->control('convulsions',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('convulsions_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>PARALYSIS</td>
                          <td>
                            <?php
                              echo $this->Form->control('paralysis',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('paralysis_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>NUMBNESS OR TINGLING</td>
                          <td>
                            <?php
                              echo $this->Form->control('numbness',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('numbness_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>MEMORY LOSS</td>
                          <td>
                            <?php
                              echo $this->Form->control('memory_loss',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('memory_loss_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>DEPRESSION</td>
                          <td>
                            <?php
                              echo $this->Form->control('depression',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('depression_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>ANXIETY</td>
                          <td>
                            <?php
                              echo $this->Form->control('anxiety',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('anxiety_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>MOOD SWINGS</td>
                          <td>
                            <?php
                              echo $this->Form->control('mood_swings',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('mood_swings_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>SLEEP PROBLEMS</td>
                          <td>
                            <?php
                              echo $this->Form->control('sleep_problems',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('sleep_problems_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>DRUG OR ALCOHOL ABUSE</td>
                          <td>
                            <?php
                              echo $this->Form->control('drug_alcochol_abuse',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('drug_alcochol_abuse_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>CHRONIC SKIN RASH OR HIVES</td>
                          <td>
                            <?php
                              echo $this->Form->control('rash_hives',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('rash_hives_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>ASTHMA</td>
                          <td>
                            <?php
                              echo $this->Form->control('asthma2',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('asthma2_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>
                        <tr>
                          <td>HAY FEVER</td>
                          <td>
                            <?php
                              echo $this->Form->control('hay_fever',['label'=>false,'type' => 'select', 'options' => [0 => __('No') , 1 => __('Yes')], 'empty' => __('Select')]);
                            ?>
                          </td>
                          <td>
                            <?php
                              echo $this->Form->control('hay_fever_details',['label'=>false]);
                            ?>
                          </td>
                        </tr>                        
                    </tbody>
                  </table>
                </div>

                
              </div>
              </div> 
            </div>

            <?php
                    echo $this->Form->control('additional_information',['label'=>"Please list any additional information you believe would assist in your health planning"]);
                ?>
                
                <label>I understand that full disclosure is necessary to my medical safety, I have filled out this medical history to the
                  best of my knowledge and I have answered these questions with complete honesty to insure my health and safety.</label>
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('signature_initials',['label'=>'Patient Initials']);
                     ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <?php
                     echo $this->Form->control('signature_date',['label'=>'Date','type'=>'text','id'=>'signature_date']);
                     ?>
                  </div>
                </div>
          </div>
        </div>
            <!-- /.box-body -->
            <div class="row">
              <div class="col-md-12 col-xs-12" align="center">
                <?php echo $this->Form->submit(__('Submit')); ?>
              </div>
            </div>

          <?php echo $this->Form->end(); ?>
        </div>
        <!-- /.box -->
      </div>
  </div>
  <!-- /.row -->
</section>
