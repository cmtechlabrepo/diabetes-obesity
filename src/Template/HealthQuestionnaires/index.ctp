<?php echo $this->Html->script('health-questionnaires/index.js'); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Health Questionnaires

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" id='formsearch' method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" id='search' class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <!-- <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div> -->
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                  <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('location') ?></th> -->
                  <th scope="col"><?= $this->Paginator->sort('surgeon') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('city') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('state') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('zip_code') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('home_phone') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('work_phone') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('cell_phone') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('date_of_birth') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('gender') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('marital_status') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('age') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('height') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('weight') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('BMI') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('neck') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('wrist') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('waist') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('hip') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('thigh') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('size_shirt') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('size_pants') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('procedure_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('suggested_date') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('referring_person') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('date_of_referral') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('emergency_name') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('emergency_phone') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('primary_health_provider') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('time_treated') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('condition_treated') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('allergic_medication') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('supplies_allergic') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('allergic_food') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('blood_transfusion') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('breast_colo_prostate_cancer') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('cancer_specific') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('diabetes') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('heart_attack') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('stroke') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('high_blood_pressure') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('arthritis') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('obesity') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('shortness_breath') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('shortness_breath_long') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('excercise_regularly') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('asthma') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('swelling_ankles') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('thyroid_problems') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('own_diabetes') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('monitor_blood_sugar') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('pcos') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('own_high_blood_pressure') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('chest_pain') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('history_heart_disease') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('own_heart_attack') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('fatty_liver') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('rheumatoid_arthritis') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('nsaids') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('lupus') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('hiv') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('diverticulitis') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('ulcers') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('crohns_disease') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('heartburn') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('pain_abdomen') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('bowel_movements') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('bloody_stools') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('hemorrhoids') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('eating_disorder') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('happy_life') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('history_depression') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('are_you_depressed') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('stress_problem') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('panic_stressed') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('problems_eating') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('cry_frecuently') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('attempted_suicide') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('thought_hurting_self') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('trouble_sleeping') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('been_to_counselor') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('drink_alcohol') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('kind_alcohol') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('drinks_per_week') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('concerned_drink') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('considered_stopping') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('blackouts') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('use_tobacco') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('cigarret_packs_day') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('chew_day') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('pipe_day') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('cigars_day') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('number_years') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('years_quit') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('use_caffeine') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('caffeine_form') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('caffeine_how_much') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('recreational_drugs') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('drugs_needle') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('sexually_active') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('trying_pregnancy') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('contraceptive') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('intercourse_discomfort') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('speak_about_hiv') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('ankles_swelling') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('ankles_pain') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('ankles_stiffness') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('ankles_popping') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('knees_swelling') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('knees_pain') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('knees_stiffness') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('kness_popping') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('hips_swelling') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('hips_pain') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('hips_stiffness') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('hips_popping') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('back_swelling') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('back_pain') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('back_stiffness') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('back_popping') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('others_swelling') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('others_pain') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('others_stiffing') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('others_popping') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('join_medications') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('degenerative_changes') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('weight_loss_surgery') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('how_long_overweight') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('diet_pills') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('how_much_weight_loss') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('max_weight') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('min_weight') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('snaker') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('volume_eater') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('eats_sweets') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('carbonated_beverages') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('type_lifestyle') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('severe_fatigue') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('severe_weekness') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('night_sweats') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('severe_headaches') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('loss_of_conciousness') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('hearing_problems') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('ear_pain') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('nasal_congestion') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('sinus_congestion') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('bloody_nose') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('dental_problems') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('dentures') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('sores_in_mouth') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('wheezing') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('coughing') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('breast_lump') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('heart_murmur') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('high_blood_pressure2') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('chest_pain_excercise') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('std') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('anemia') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('bleeding_tendency') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('convulsions') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('paralysis') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('numbness') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('memory_loss') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('depression') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('anxiety') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('mood_swings') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('sleep_problems') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('drug_alcochol_abuse') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('rash_hives') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('asthma2') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('hay_fever') ?></th> -->
                  <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('modified') ?></th> -->
                  <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody id='health-list'>
              <?php foreach ($healthQuestionnaires as $healthQuestionnaire): ?>
                <tr>
                  <td><?= $this->Number->format($healthQuestionnaire->id) ?>
                  <!--<td><?= h($healthQuestionnaire->location) ?></td> -->
                  <td><?= h($healthQuestionnaire->doctor->name) ?></td>
                  <td><?= h($healthQuestionnaire->name) ?></td>
                  <!-- <td><?= h($healthQuestionnaire->city) ?></td>
                  <td><?= h($healthQuestionnaire->state) ?></td>
                  <td><?= h($healthQuestionnaire->zip_code) ?></td>
                  <td><?= h($healthQuestionnaire->home_phone) ?></td>
                  <td><?= h($healthQuestionnaire->work_phone) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->cell_phone) ?></td>
                  <td><?= h($healthQuestionnaire->date_of_birth) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->gender) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->marital_status) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->age) ?></td>
                  <td><?= h($healthQuestionnaire->height) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->weight) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->BMI) ?></td>
                  <td><?= h($healthQuestionnaire->neck) ?></td>
                  <td><?= h($healthQuestionnaire->wrist) ?></td>
                  <td><?= h($healthQuestionnaire->waist) ?></td>
                  <td><?= h($healthQuestionnaire->hip) ?></td>
                  <td><?= h($healthQuestionnaire->thigh) ?></td>
                  <td><?= h($healthQuestionnaire->size_shirt) ?></td>
                  <td><?= h($healthQuestionnaire->size_pants) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->procedure_id) ?></td>
                  <td><?= h($healthQuestionnaire->suggested_date) ?></td>
                  <td><?= h($healthQuestionnaire->referring_person) ?></td>
                  <td><?= h($healthQuestionnaire->date_of_referral) ?></td>
                  <td><?= h($healthQuestionnaire->emergency_name) ?></td>
                  <td><?= h($healthQuestionnaire->emergency_phone) ?></td>
                  <td><?= h($healthQuestionnaire->primary_health_provider) ?></td>
                  <td><?= h($healthQuestionnaire->time_treated) ?></td>
                  <td><?= h($healthQuestionnaire->condition_treated) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->allergic_medication) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->supplies_allergic) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->allergic_food) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->blood_transfusion) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->breast_colo_prostate_cancer) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->cancer_specific) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->diabetes) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->heart_attack) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->stroke) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->high_blood_pressure) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->arthritis) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->obesity) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->shortness_breath) ?></td>
                  <td><?= h($healthQuestionnaire->shortness_breath_long) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->excercise_regularly) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->asthma) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->swelling_ankles) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->thyroid_problems) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->own_diabetes) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->monitor_blood_sugar) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->pcos) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->own_high_blood_pressure) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->chest_pain) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->history_heart_disease) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->own_heart_attack) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->fatty_liver) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->rheumatoid_arthritis) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->nsaids) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->lupus) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->hiv) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->diverticulitis) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->ulcers) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->crohns_disease) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->heartburn) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->pain_abdomen) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->bowel_movements) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->bloody_stools) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->hemorrhoids) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->eating_disorder) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->happy_life) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->history_depression) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->are_you_depressed) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->stress_problem) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->panic_stressed) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->problems_eating) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->cry_frecuently) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->attempted_suicide) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->thought_hurting_self) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->trouble_sleeping) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->been_to_counselor) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->drink_alcohol) ?></td>
                  <td><?= h($healthQuestionnaire->kind_alcohol) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->drinks_per_week) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->concerned_drink) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->considered_stopping) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->blackouts) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->use_tobacco) ?></td>
                  <td><?= h($healthQuestionnaire->cigarret_packs_day) ?></td>
                  <td><?= h($healthQuestionnaire->chew_day) ?></td>
                  <td><?= h($healthQuestionnaire->pipe_day) ?></td>
                  <td><?= h($healthQuestionnaire->cigars_day) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->number_years) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->years_quit) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->use_caffeine) ?></td>
                  <td><?= h($healthQuestionnaire->caffeine_form) ?></td>
                  <td><?= h($healthQuestionnaire->caffeine_how_much) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->recreational_drugs) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->drugs_needle) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->sexually_active) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->trying_pregnancy) ?></td>
                  <td><?= h($healthQuestionnaire->contraceptive) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->intercourse_discomfort) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->speak_about_hiv) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->ankles_swelling) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->ankles_pain) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->ankles_stiffness) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->ankles_popping) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->knees_swelling) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->knees_pain) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->knees_stiffness) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->kness_popping) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->hips_swelling) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->hips_pain) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->hips_stiffness) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->hips_popping) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->back_swelling) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->back_pain) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->back_stiffness) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->back_popping) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->others_swelling) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->others_pain) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->others_stiffing) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->others_popping) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->join_medications) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->degenerative_changes) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->weight_loss_surgery) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->how_long_overweight) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->diet_pills) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->how_much_weight_loss) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->max_weight) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->min_weight) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->snaker) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->volume_eater) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->eats_sweets) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->carbonated_beverages) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->type_lifestyle) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->severe_fatigue) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->severe_weekness) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->night_sweats) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->severe_headaches) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->loss_of_conciousness) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->hearing_problems) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->ear_pain) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->nasal_congestion) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->sinus_congestion) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->bloody_nose) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->dental_problems) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->dentures) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->sores_in_mouth) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->wheezing) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->coughing) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->breast_lump) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->heart_murmur) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->high_blood_pressure2) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->chest_pain_excercise) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->std) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->anemia) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->bleeding_tendency) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->convulsions) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->paralysis) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->numbness) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->memory_loss) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->depression) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->anxiety) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->mood_swings) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->sleep_problems) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->drug_alcochol_abuse) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->rash_hives) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->asthma2) ?></td>
                  <td><?= $this->Number->format($healthQuestionnaire->hay_fever) ?></td> -->
                  <td><?= h($healthQuestionnaire->created) ?></td>
                  <!-- <td><?= h($healthQuestionnaire->modified) ?></td> -->
                  <td class="actions text-right">
                      <?php 
                      $role = $this->request->getSession()->read('Auth.User')['role_id']; 
                      if(in_array($role,array(1,2))){ ?>
                      <?= $this->Html->link(__('Information for call'), ['action' => 'callinfo', $healthQuestionnaire->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?php } ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $healthQuestionnaire->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?php if(in_array($role,array(1,2))){ ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $healthQuestionnaire->id], ['confirm' => __('Are you sure you want to delete # {0}?', $healthQuestionnaire->id), 'class'=>'btn btn-danger btn-xs']) ?>
                      <?php } ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>