<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HealthQuestionnaire $healthQuestionnaire
 */
?>
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Health Questionnaire
      <small><?php echo __('Edit'); ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Form'); ?></h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <?php echo $this->Form->create($healthQuestionnaire, ['role' => 'form']); ?>
            <div class="box-body">
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <?php
                   echo $this->Form->control('location');
                   ?>
                </div>
                <div class="col-md-6 col-xs-12">
                  <?php
                   echo $this->Form->control('surgeon');
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('name');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('address');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('city');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('state');
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('zip_code');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('home_phone');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('work_phone');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('cell_phone');
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
                  echo $this->Form->control('date_of_birth', ['empty' => true]);
                   ?>
                </div>
                <div class="col-md-4 col-xs-12">
                  <?php
                  echo $this->Form->control('gender');
                   ?>
                </div>
                <div class="col-md-4 col-xs-12">
                  <?php
                  echo $this->Form->control('marital_status');
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('age');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('height');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('weight');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                   echo $this->Form->control('BMI');
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3 col-xs-12">
                  <?php
                  echo $this->Form->control('neck');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                  echo $this->Form->control('wrist');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                  echo $this->Form->control('waist');
                   ?>
                </div>
                <div class="col-md-3 col-xs-12">
                  <?php
                  echo $this->Form->control('hip');
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <?php
                   echo $this->Form->control('thigh');
                   ?>
                </div>
                <div class="col-md-4 col-xs-12">
                  <?php
                   echo $this->Form->control('size_shirt');
                   ?>
                </div>
                <div class="col-md-4 col-xs-12">
                  <?php
                   echo $this->Form->control('size_pants');
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <?php
                    echo $this->Form->control('procedure_id');
                   ?>
                </div>
                <div class="col-md-6 col-xs-12">
                  <?php
                    echo $this->Form->control('suggested_date', ['empty' => true]);
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <?php
                    echo $this->Form->control('details');
                   ?>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <?php
echo $this->Form->control('referring_person');
                   ?>
                </div>
                <div class="col-md-6 col-xs-12">
                  <?php
echo $this->Form->control('date_of_referral', ['empty' => true]);
                   ?>
                </div>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('emergency_name');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('emergency_phone');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('primary_health_provider');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('time_treated');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('condition_treated');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('allergic_medication');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('allergic_medication');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                echo $this->Form->control('allergic_medication');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('allergic_medication_list');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                echo $this->Form->control('supplies_allergic');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('supplies_allergic_list');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('allergic_food');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('allergic_food_list');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('blood_transfusion');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('transfusion_details');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-xs-12">
                <?php
                 echo $this->Form->control('breast_colo_prostate_cancer');
                 ?>
              </div>
              <div class="col-md-3 col-xs-12">
                <?php
                 echo $this->Form->control('cancer_specific');
                 ?>
              </div>
              <div class="col-md-3 col-xs-12">
                <?php
                 echo $this->Form->control('diabetes');
                 ?>
              </div>
              <div class="col-md-3 col-xs-12">
                <?php
                 echo $this->Form->control('heart_attack');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-xs-12">
                <?php
                 echo $this->Form->control('stroke');
                 ?>
              </div>
              <div class="col-md-3 col-xs-12">
                <?php
                 echo $this->Form->control('high_blood_pressure');
                 ?>
              </div>
              <div class="col-md-3 col-xs-12">
                <?php
                 echo $this->Form->control('arthritis');
                 ?>
              </div>
              <div class="col-md-3 col-xs-12">
                <?php
                 echo $this->Form->control('obesity');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('shortness_breath');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('shortness_breath_long');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('excercise_regularly');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('asthma');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('swelling_ankles');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                echo $this->Form->control('decrese_swelling');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                 echo $this->Form->control('decrease_pain');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('thyroid_problems');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('own_diabetes');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                 echo $this->Form->control('medications_for_diabetes');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('monitor_blood_sugar');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('monitor_how_ofter');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('pcos');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('own_high_blood_pressure');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                 echo $this->Form->control('medications_blood_pressure');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('chest_pain');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('chest_pain_often');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('history_heart_disease');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                 echo $this->Form->control('explain_heart_dicease');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('own_heart_attack');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('fatty_liver');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('fatty_liver_details');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('rheumatoid_arthritis');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('nsaids');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('lupus');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('hiv');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('diverticulitis');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('ulcers');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('crohns_disease');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('heartburn');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('heartburn_long');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('pain_abdomen');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                 echo $this->Form->control('pain_details');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('bowel_movements');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('bloody_stools');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('hemorrhoids');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('eating_disorder');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('happy_life');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('history_depression');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('are_you_depressed');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('stress_problem');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('panic_stressed');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('problems_eating');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('cry_frecuently');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('attempted_suicide');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('thought_hurting_self');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('trouble_sleeping');
                 ?>
              </div>
              <div class="col-md-4 col-xs-12">
                <?php
                 echo $this->Form->control('been_to_counselor');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('drink_alcohol');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('kind_alcohol');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('drinks_per_week');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('concerned_drink');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('considered_stopping');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('blackouts');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('use_tobacco');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-xs-12">
                <?php
                  echo $this->Form->control('cigarret_packs_day');
                 ?>
              </div>
              <div class="col-md-3 col-xs-12">
                <?php
                  echo $this->Form->control('chew_day');
                 ?>
              </div>
              <div class="col-md-3 col-xs-12">
                <?php
                  echo $this->Form->control('pipe_day');
                 ?>
              </div>
              <div class="col-md-3 col-xs-12">
                <?php
                  echo $this->Form->control('cigars_day');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-md-12">
                <?php
                  echo $this->Form->control('number_years');
                 ?>
              </div>
              <div class="col-md-3 col-md-12">
                <?php
                  echo $this->Form->control('years_quit');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('use_caffeine');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('caffeine_form');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('caffeine_how_much');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('recreational_drugs');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('drugs_details');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <?php
                  echo $this->Form->control('drugs_needle');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('sexually_active');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('trying_pregnancy');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('contraceptive');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('intercourse_discomfort');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                echo $this->Form->control('speak_about_hiv');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('ankles_swelling');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('ankles_pain');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('ankles_stiffness');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('ankles_popping');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
               echo $this->Form->control('knees_swelling');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
               echo $this->Form->control('knees_pain');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
              echo $this->Form->control('knees_stiffness');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
             echo $this->Form->control('kness_popping');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
              echo $this->Form->control('hips_swelling');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('hips_pain');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('hips_stiffness');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('hips_popping');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('back_swelling');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('back_pain');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('back_stiffness');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('back_popping');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('others_swelling');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('others_pain');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('others_stiffing');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('others_popping');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('treatment1');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('treatment2');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('join_medications');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('join_medications_details');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                   echo $this->Form->control('degenerative_changes');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('degenerative_changes_details');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('weight_loss_surgery');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('how_long_overweight');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('diet_pills');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('how_much_weight_loss');
                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('how_quickly_regain');
                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('max_weight');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('min_weight');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('foods_liked');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('foods_disliked');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('snaker');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                  echo $this->Form->control('volume_eater');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('eats_sweets');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('sweets_how_ofter');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('carbonated_beverages');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('type_lifestyle');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('severe_fatigue');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('severe_fatigue_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('severe_weekness');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('severe_weekness_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('night_sweats');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('night_sweats_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('severe_headaches');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('severe_headaches_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('loss_of_conciousness');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('loss_of_conciousness_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('hearing_problems');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('hearing_problems_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('ear_pain');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('ear_pain_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('nasal_congestion');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('nasal_congestion_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('sinus_congestion');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('sinus_congestion_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('bloody_nose');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('bloody_nose_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('dental_problems');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('dental_problems_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('dentures');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('dentures_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('sores_in_mouth');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('sores_in_mouth_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('wheezing');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('wheezing_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('coughing');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('coughing_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('breast_lump');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('breast_lump_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('heart_murmur');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('heart_murmur_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('high_blood_pressure2');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('high_blood_pressure2_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('chest_pain_excercise');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('chest_pain_excercise_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('std');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('std_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('anemia');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('anemia_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('bleeding_tendency');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('bleeding_tendency_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('convulsions');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('convulsions_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('paralysis');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('paralysis_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('numbness');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('numbness_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('memory_loss');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('memory_loss_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('depression');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('depression_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                   echo $this->Form->control('anxiety');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('anxiety_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('mood_swings');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('mood_swings_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('sleep_problems');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('sleep_problems_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('drug_alcochol_abuse');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('drug_alcochol_abuse_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('rash_hives');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('rash_hives_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('asthma2');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('asthma2_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <?php
                echo $this->Form->control('hay_fever');

                 ?>
              </div>
              <div class="col-md-6 col-xs-12">
                <?php
                 echo $this->Form->control('hay_fever_details');

                 ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php
                  echo $this->Form->control('additional_information');
                 ?>
              </div>
            </div>
            </div>
            <!-- /.box-body -->
             <div class="row">
               <div class="col-md-12 col-xs-12" align="center">
                 <?php echo $this->Form->submit(__('Submit')); ?>
               </div>
             </div>


          <?php echo $this->Form->end(); ?>
        </div>
        <!-- /.box -->
      </div>
  </div>
  <!-- /.row -->
</section>
