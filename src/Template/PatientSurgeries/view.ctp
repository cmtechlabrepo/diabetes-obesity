<section class="content-header">
  <h1>
    Patient Surgery
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Surgery') ?></dt>
            <dd><?= h($patientSurgery->surgery) ?></dd>
            <dt scope="row"><?= __('Hospital') ?></dt>
            <dd><?= h($patientSurgery->hospital) ?></dd>
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $patientSurgery->has('patient') ? $this->Html->link($patientSurgery->patient->name, ['controller' => 'Patients', 'action' => 'view', $patientSurgery->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $patientSurgery->has('user') ? $this->Html->link($patientSurgery->user->name, ['controller' => 'Users', 'action' => 'view', $patientSurgery->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($patientSurgery->id) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($patientSurgery->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($patientSurgery->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($patientSurgery->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Reason') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($patientSurgery->reason); ?>
        </div>
      </div>
    </div>
  </div>
</section>
