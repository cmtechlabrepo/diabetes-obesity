<?php
use Cake\Core\Configure; 

$this->layout = 'loginLayout';

?>

<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo $this->Url->build(); ?>"><?php echo Configure::read('Theme.logo.large') ?></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Write your email we will send you an email to verify.</p>

    <?php echo $this->Form->create(); ?>
      <div class="box-body">
        <?php
          echo $this->Form->control('email');
        ?>
      </div>
            <!-- /.box-body -->
            <?php echo $this->Form->submit(__('Submit')); ?>

          <?php echo $this->Form->end(); ?>
    <!-- <a href="#">I forgot my password</a><br> -->
    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->