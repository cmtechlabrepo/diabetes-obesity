<?php
use Cake\Core\Configure; 

$this->layout = 'loginLayout';

?>
<div class="login-box" style="width: 50%;">
  <div class="login-logo">
    <a href="<?php echo $this->Url->build(); ?>"><?php echo Configure::read('Theme.logo.normal') ?></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" style="text-align:center;">
    <!-- <p class="login-box-msg">Write your email we will send you an email to verify.</p> -->

    <h2>Thank you, we will send you an email with the link to fill the questionnaire, please check your inbox and spam folders.</h2>
    
    <!-- <a href="#">I forgot my password</a><br> -->
    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->