<section class="content-header">
  <h1>
    Procedure
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($procedure->name) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($procedure->id) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($procedure->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($procedure->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Health Questionnaires') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($procedure->health_questionnaires)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Location') ?></th>
                    <th scope="col"><?= __('Surgeon') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Address') ?></th>
                    <th scope="col"><?= __('City') ?></th>
                    <th scope="col"><?= __('State') ?></th>
                    <th scope="col"><?= __('Zip Code') ?></th>
                    <th scope="col"><?= __('Home Phone') ?></th>
                    <th scope="col"><?= __('Work Phone') ?></th>
                    <th scope="col"><?= __('Cell Phone') ?></th>
                    <th scope="col"><?= __('Date Of Birth') ?></th>
                    <th scope="col"><?= __('Gender') ?></th>
                    <th scope="col"><?= __('Marital Status') ?></th>
                    <th scope="col"><?= __('Age') ?></th>
                    <th scope="col"><?= __('Height') ?></th>
                    <th scope="col"><?= __('Weight') ?></th>
                    <th scope="col"><?= __('BMI') ?></th>
                    <th scope="col"><?= __('Neck') ?></th>
                    <th scope="col"><?= __('Wrist') ?></th>
                    <th scope="col"><?= __('Waist') ?></th>
                    <th scope="col"><?= __('Hip') ?></th>
                    <th scope="col"><?= __('Thigh') ?></th>
                    <th scope="col"><?= __('Size Shirt') ?></th>
                    <th scope="col"><?= __('Size Pants') ?></th>
                    <th scope="col"><?= __('Procedure Id') ?></th>
                    <th scope="col"><?= __('Suggested Date') ?></th>
                    <th scope="col"><?= __('Details') ?></th>
                    <th scope="col"><?= __('Referring Person') ?></th>
                    <th scope="col"><?= __('Date Of Referral') ?></th>
                    <th scope="col"><?= __('Emergency Name') ?></th>
                    <th scope="col"><?= __('Emergency Phone') ?></th>
                    <th scope="col"><?= __('Primary Health Provider') ?></th>
                    <th scope="col"><?= __('Time Treated') ?></th>
                    <th scope="col"><?= __('Condition Treated') ?></th>
                    <th scope="col"><?= __('Allergic Medication') ?></th>
                    <th scope="col"><?= __('Allergic Medication List') ?></th>
                    <th scope="col"><?= __('Supplies Allergic') ?></th>
                    <th scope="col"><?= __('Supplies Allergic List') ?></th>
                    <th scope="col"><?= __('Allergic Food') ?></th>
                    <th scope="col"><?= __('Allergic Food List') ?></th>
                    <th scope="col"><?= __('Blood Transfusion') ?></th>
                    <th scope="col"><?= __('Transfusion Details') ?></th>
                    <th scope="col"><?= __('Breast Colo Prostate Cancer') ?></th>
                    <th scope="col"><?= __('Cancer Specific') ?></th>
                    <th scope="col"><?= __('Diabetes') ?></th>
                    <th scope="col"><?= __('Heart Attack') ?></th>
                    <th scope="col"><?= __('Stroke') ?></th>
                    <th scope="col"><?= __('High Blood Pressure') ?></th>
                    <th scope="col"><?= __('Arthritis') ?></th>
                    <th scope="col"><?= __('Obesity') ?></th>
                    <th scope="col"><?= __('Shortness Breath') ?></th>
                    <th scope="col"><?= __('Shortness Breath Long') ?></th>
                    <th scope="col"><?= __('Excercise Regularly') ?></th>
                    <th scope="col"><?= __('Asthma') ?></th>
                    <th scope="col"><?= __('Swelling Ankles') ?></th>
                    <th scope="col"><?= __('Decrese Swelling') ?></th>
                    <th scope="col"><?= __('Decrease Pain') ?></th>
                    <th scope="col"><?= __('Thyroid Problems') ?></th>
                    <th scope="col"><?= __('Own Diabetes') ?></th>
                    <th scope="col"><?= __('Medications For Diabetes') ?></th>
                    <th scope="col"><?= __('Monitor Blood Sugar') ?></th>
                    <th scope="col"><?= __('Monitor How Ofter') ?></th>
                    <th scope="col"><?= __('Pcos') ?></th>
                    <th scope="col"><?= __('Own High Blood Pressure') ?></th>
                    <th scope="col"><?= __('Medications Blood Pressure') ?></th>
                    <th scope="col"><?= __('Chest Pain') ?></th>
                    <th scope="col"><?= __('Chest Pain Often') ?></th>
                    <th scope="col"><?= __('History Heart Disease') ?></th>
                    <th scope="col"><?= __('Explain Heart Dicease') ?></th>
                    <th scope="col"><?= __('Own Heart Attack') ?></th>
                    <th scope="col"><?= __('Fatty Liver') ?></th>
                    <th scope="col"><?= __('Fatty Liver Details') ?></th>
                    <th scope="col"><?= __('Rheumatoid Arthritis') ?></th>
                    <th scope="col"><?= __('Nsaids') ?></th>
                    <th scope="col"><?= __('Lupus') ?></th>
                    <th scope="col"><?= __('Hiv') ?></th>
                    <th scope="col"><?= __('Diverticulitis') ?></th>
                    <th scope="col"><?= __('Ulcers') ?></th>
                    <th scope="col"><?= __('Crohns Disease') ?></th>
                    <th scope="col"><?= __('Heartburn') ?></th>
                    <th scope="col"><?= __('Heartburn Long') ?></th>
                    <th scope="col"><?= __('Pain Abdomen') ?></th>
                    <th scope="col"><?= __('Pain Details') ?></th>
                    <th scope="col"><?= __('Bowel Movements') ?></th>
                    <th scope="col"><?= __('Bloody Stools') ?></th>
                    <th scope="col"><?= __('Hemorrhoids') ?></th>
                    <th scope="col"><?= __('Eating Disorder') ?></th>
                    <th scope="col"><?= __('Happy Life') ?></th>
                    <th scope="col"><?= __('History Depression') ?></th>
                    <th scope="col"><?= __('Are You Depressed') ?></th>
                    <th scope="col"><?= __('Stress Problem') ?></th>
                    <th scope="col"><?= __('Panic Stressed') ?></th>
                    <th scope="col"><?= __('Problems Eating') ?></th>
                    <th scope="col"><?= __('Cry Frecuently') ?></th>
                    <th scope="col"><?= __('Attempted Suicide') ?></th>
                    <th scope="col"><?= __('Thought Hurting Self') ?></th>
                    <th scope="col"><?= __('Trouble Sleeping') ?></th>
                    <th scope="col"><?= __('Been To Counselor') ?></th>
                    <th scope="col"><?= __('Drink Alcohol') ?></th>
                    <th scope="col"><?= __('Kind Alcohol') ?></th>
                    <th scope="col"><?= __('Drinks Per Week') ?></th>
                    <th scope="col"><?= __('Concerned Drink') ?></th>
                    <th scope="col"><?= __('Considered Stopping') ?></th>
                    <th scope="col"><?= __('Blackouts') ?></th>
                    <th scope="col"><?= __('Use Tobacco') ?></th>
                    <th scope="col"><?= __('Cigarret Packs Day') ?></th>
                    <th scope="col"><?= __('Chew Day') ?></th>
                    <th scope="col"><?= __('Pipe Day') ?></th>
                    <th scope="col"><?= __('Cigars Day') ?></th>
                    <th scope="col"><?= __('Number Years') ?></th>
                    <th scope="col"><?= __('Years Quit') ?></th>
                    <th scope="col"><?= __('Use Caffeine') ?></th>
                    <th scope="col"><?= __('Caffeine Form') ?></th>
                    <th scope="col"><?= __('Caffeine How Much') ?></th>
                    <th scope="col"><?= __('Recreational Drugs') ?></th>
                    <th scope="col"><?= __('Drugs Details') ?></th>
                    <th scope="col"><?= __('Drugs Needle') ?></th>
                    <th scope="col"><?= __('Sexually Active') ?></th>
                    <th scope="col"><?= __('Trying Pregnancy') ?></th>
                    <th scope="col"><?= __('Contraceptive') ?></th>
                    <th scope="col"><?= __('Intercourse Discomfort') ?></th>
                    <th scope="col"><?= __('Speak About Hiv') ?></th>
                    <th scope="col"><?= __('Ankles Swelling') ?></th>
                    <th scope="col"><?= __('Ankles Pain') ?></th>
                    <th scope="col"><?= __('Ankles Stiffness') ?></th>
                    <th scope="col"><?= __('Ankles Popping') ?></th>
                    <th scope="col"><?= __('Knees Swelling') ?></th>
                    <th scope="col"><?= __('Knees Pain') ?></th>
                    <th scope="col"><?= __('Knees Stiffness') ?></th>
                    <th scope="col"><?= __('Kness Popping') ?></th>
                    <th scope="col"><?= __('Hips Swelling') ?></th>
                    <th scope="col"><?= __('Hips Pain') ?></th>
                    <th scope="col"><?= __('Hips Stiffness') ?></th>
                    <th scope="col"><?= __('Hips Popping') ?></th>
                    <th scope="col"><?= __('Back Swelling') ?></th>
                    <th scope="col"><?= __('Back Pain') ?></th>
                    <th scope="col"><?= __('Back Stiffness') ?></th>
                    <th scope="col"><?= __('Back Popping') ?></th>
                    <th scope="col"><?= __('Others Swelling') ?></th>
                    <th scope="col"><?= __('Others Pain') ?></th>
                    <th scope="col"><?= __('Others Stiffing') ?></th>
                    <th scope="col"><?= __('Others Popping') ?></th>
                    <th scope="col"><?= __('Treatment1') ?></th>
                    <th scope="col"><?= __('Treatment2') ?></th>
                    <th scope="col"><?= __('Join Medications') ?></th>
                    <th scope="col"><?= __('Join Medications Details') ?></th>
                    <th scope="col"><?= __('Degenerative Changes') ?></th>
                    <th scope="col"><?= __('Degenerative Changes Details') ?></th>
                    <th scope="col"><?= __('Weight Loss Surgery') ?></th>
                    <th scope="col"><?= __('How Long Overweight') ?></th>
                    <th scope="col"><?= __('Diet Pills') ?></th>
                    <th scope="col"><?= __('How Much Weight Loss') ?></th>
                    <th scope="col"><?= __('How Quickly Regain') ?></th>
                    <th scope="col"><?= __('Max Weight') ?></th>
                    <th scope="col"><?= __('Min Weight') ?></th>
                    <th scope="col"><?= __('Foods Liked') ?></th>
                    <th scope="col"><?= __('Foods Disliked') ?></th>
                    <th scope="col"><?= __('Snaker') ?></th>
                    <th scope="col"><?= __('Volume Eater') ?></th>
                    <th scope="col"><?= __('Eats Sweets') ?></th>
                    <th scope="col"><?= __('Sweets How Ofter') ?></th>
                    <th scope="col"><?= __('Carbonated Beverages') ?></th>
                    <th scope="col"><?= __('Type Lifestyle') ?></th>
                    <th scope="col"><?= __('Severe Fatigue') ?></th>
                    <th scope="col"><?= __('Severe Fatigue Details') ?></th>
                    <th scope="col"><?= __('Severe Weekness') ?></th>
                    <th scope="col"><?= __('Severe Weekness Details') ?></th>
                    <th scope="col"><?= __('Night Sweats') ?></th>
                    <th scope="col"><?= __('Night Sweats Details') ?></th>
                    <th scope="col"><?= __('Severe Headaches') ?></th>
                    <th scope="col"><?= __('Severe Headaches Details') ?></th>
                    <th scope="col"><?= __('Loss Of Conciousness') ?></th>
                    <th scope="col"><?= __('Loss Of Conciousness Details') ?></th>
                    <th scope="col"><?= __('Hearing Problems') ?></th>
                    <th scope="col"><?= __('Hearing Problems Details') ?></th>
                    <th scope="col"><?= __('Ear Pain') ?></th>
                    <th scope="col"><?= __('Ear Pain Details') ?></th>
                    <th scope="col"><?= __('Nasal Congestion') ?></th>
                    <th scope="col"><?= __('Nasal Congestion Details') ?></th>
                    <th scope="col"><?= __('Sinus Congestion') ?></th>
                    <th scope="col"><?= __('Sinus Congestion Details') ?></th>
                    <th scope="col"><?= __('Bloody Nose') ?></th>
                    <th scope="col"><?= __('Bloody Nose Details') ?></th>
                    <th scope="col"><?= __('Dental Problems') ?></th>
                    <th scope="col"><?= __('Dental Problems Details') ?></th>
                    <th scope="col"><?= __('Dentures') ?></th>
                    <th scope="col"><?= __('Dentures Details') ?></th>
                    <th scope="col"><?= __('Sores In Mouth') ?></th>
                    <th scope="col"><?= __('Sores In Mouth Details') ?></th>
                    <th scope="col"><?= __('Wheezing') ?></th>
                    <th scope="col"><?= __('Wheezing Details') ?></th>
                    <th scope="col"><?= __('Coughing') ?></th>
                    <th scope="col"><?= __('Coughing Details') ?></th>
                    <th scope="col"><?= __('Breast Lump') ?></th>
                    <th scope="col"><?= __('Breast Lump Details') ?></th>
                    <th scope="col"><?= __('Heart Murmur') ?></th>
                    <th scope="col"><?= __('Heart Murmur Details') ?></th>
                    <th scope="col"><?= __('High Blood Pressure2') ?></th>
                    <th scope="col"><?= __('High Blood Pressure2 Details') ?></th>
                    <th scope="col"><?= __('Chest Pain Excercise') ?></th>
                    <th scope="col"><?= __('Chest Pain Excercise Details') ?></th>
                    <th scope="col"><?= __('Std') ?></th>
                    <th scope="col"><?= __('Std Details') ?></th>
                    <th scope="col"><?= __('Anemia') ?></th>
                    <th scope="col"><?= __('Anemia Details') ?></th>
                    <th scope="col"><?= __('Bleeding Tendency') ?></th>
                    <th scope="col"><?= __('Bleeding Tendency Details') ?></th>
                    <th scope="col"><?= __('Convulsions') ?></th>
                    <th scope="col"><?= __('Convulsions Details') ?></th>
                    <th scope="col"><?= __('Paralysis') ?></th>
                    <th scope="col"><?= __('Paralysis Details') ?></th>
                    <th scope="col"><?= __('Numbness') ?></th>
                    <th scope="col"><?= __('Numbness Details') ?></th>
                    <th scope="col"><?= __('Memory Loss') ?></th>
                    <th scope="col"><?= __('Memory Loss Details') ?></th>
                    <th scope="col"><?= __('Depression') ?></th>
                    <th scope="col"><?= __('Depression Details') ?></th>
                    <th scope="col"><?= __('Anxiety') ?></th>
                    <th scope="col"><?= __('Anxiety Details') ?></th>
                    <th scope="col"><?= __('Mood Swings') ?></th>
                    <th scope="col"><?= __('Mood Swings Details') ?></th>
                    <th scope="col"><?= __('Sleep Problems') ?></th>
                    <th scope="col"><?= __('Sleep Problems Details') ?></th>
                    <th scope="col"><?= __('Drug Alcochol Abuse') ?></th>
                    <th scope="col"><?= __('Drug Alcochol Abuse Details') ?></th>
                    <th scope="col"><?= __('Rash Hives') ?></th>
                    <th scope="col"><?= __('Rash Hives Details') ?></th>
                    <th scope="col"><?= __('Asthma2') ?></th>
                    <th scope="col"><?= __('Asthma2 Details') ?></th>
                    <th scope="col"><?= __('Hay Fever') ?></th>
                    <th scope="col"><?= __('Hay Fever Details') ?></th>
                    <th scope="col"><?= __('Additional Information') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($procedure->health_questionnaires as $healthQuestionnaires): ?>
              <tr>
                    <td><?= h($healthQuestionnaires->id) ?></td>
                    <td><?= h($healthQuestionnaires->location) ?></td>
                    <td><?= h($healthQuestionnaires->surgeon) ?></td>
                    <td><?= h($healthQuestionnaires->name) ?></td>
                    <td><?= h($healthQuestionnaires->address) ?></td>
                    <td><?= h($healthQuestionnaires->city) ?></td>
                    <td><?= h($healthQuestionnaires->state) ?></td>
                    <td><?= h($healthQuestionnaires->zip_code) ?></td>
                    <td><?= h($healthQuestionnaires->home_phone) ?></td>
                    <td><?= h($healthQuestionnaires->work_phone) ?></td>
                    <td><?= h($healthQuestionnaires->cell_phone) ?></td>
                    <td><?= h($healthQuestionnaires->date_of_birth) ?></td>
                    <td><?= h($healthQuestionnaires->gender) ?></td>
                    <td><?= h($healthQuestionnaires->marital_status) ?></td>
                    <td><?= h($healthQuestionnaires->age) ?></td>
                    <td><?= h($healthQuestionnaires->height) ?></td>
                    <td><?= h($healthQuestionnaires->weight) ?></td>
                    <td><?= h($healthQuestionnaires->BMI) ?></td>
                    <td><?= h($healthQuestionnaires->neck) ?></td>
                    <td><?= h($healthQuestionnaires->wrist) ?></td>
                    <td><?= h($healthQuestionnaires->waist) ?></td>
                    <td><?= h($healthQuestionnaires->hip) ?></td>
                    <td><?= h($healthQuestionnaires->thigh) ?></td>
                    <td><?= h($healthQuestionnaires->size_shirt) ?></td>
                    <td><?= h($healthQuestionnaires->size_pants) ?></td>
                    <td><?= h($healthQuestionnaires->procedure_id) ?></td>
                    <td><?= h($healthQuestionnaires->suggested_date) ?></td>
                    <td><?= h($healthQuestionnaires->details) ?></td>
                    <td><?= h($healthQuestionnaires->referring_person) ?></td>
                    <td><?= h($healthQuestionnaires->date_of_referral) ?></td>
                    <td><?= h($healthQuestionnaires->emergency_name) ?></td>
                    <td><?= h($healthQuestionnaires->emergency_phone) ?></td>
                    <td><?= h($healthQuestionnaires->primary_health_provider) ?></td>
                    <td><?= h($healthQuestionnaires->time_treated) ?></td>
                    <td><?= h($healthQuestionnaires->condition_treated) ?></td>
                    <td><?= h($healthQuestionnaires->allergic_medication) ?></td>
                    <td><?= h($healthQuestionnaires->allergic_medication_list) ?></td>
                    <td><?= h($healthQuestionnaires->supplies_allergic) ?></td>
                    <td><?= h($healthQuestionnaires->supplies_allergic_list) ?></td>
                    <td><?= h($healthQuestionnaires->allergic_food) ?></td>
                    <td><?= h($healthQuestionnaires->allergic_food_list) ?></td>
                    <td><?= h($healthQuestionnaires->blood_transfusion) ?></td>
                    <td><?= h($healthQuestionnaires->transfusion_details) ?></td>
                    <td><?= h($healthQuestionnaires->breast_colo_prostate_cancer) ?></td>
                    <td><?= h($healthQuestionnaires->cancer_specific) ?></td>
                    <td><?= h($healthQuestionnaires->diabetes) ?></td>
                    <td><?= h($healthQuestionnaires->heart_attack) ?></td>
                    <td><?= h($healthQuestionnaires->stroke) ?></td>
                    <td><?= h($healthQuestionnaires->high_blood_pressure) ?></td>
                    <td><?= h($healthQuestionnaires->arthritis) ?></td>
                    <td><?= h($healthQuestionnaires->obesity) ?></td>
                    <td><?= h($healthQuestionnaires->shortness_breath) ?></td>
                    <td><?= h($healthQuestionnaires->shortness_breath_long) ?></td>
                    <td><?= h($healthQuestionnaires->excercise_regularly) ?></td>
                    <td><?= h($healthQuestionnaires->asthma) ?></td>
                    <td><?= h($healthQuestionnaires->swelling_ankles) ?></td>
                    <td><?= h($healthQuestionnaires->decrese_swelling) ?></td>
                    <td><?= h($healthQuestionnaires->decrease_pain) ?></td>
                    <td><?= h($healthQuestionnaires->thyroid_problems) ?></td>
                    <td><?= h($healthQuestionnaires->own_diabetes) ?></td>
                    <td><?= h($healthQuestionnaires->medications_for_diabetes) ?></td>
                    <td><?= h($healthQuestionnaires->monitor_blood_sugar) ?></td>
                    <td><?= h($healthQuestionnaires->monitor_how_ofter) ?></td>
                    <td><?= h($healthQuestionnaires->pcos) ?></td>
                    <td><?= h($healthQuestionnaires->own_high_blood_pressure) ?></td>
                    <td><?= h($healthQuestionnaires->medications_blood_pressure) ?></td>
                    <td><?= h($healthQuestionnaires->chest_pain) ?></td>
                    <td><?= h($healthQuestionnaires->chest_pain_often) ?></td>
                    <td><?= h($healthQuestionnaires->history_heart_disease) ?></td>
                    <td><?= h($healthQuestionnaires->explain_heart_dicease) ?></td>
                    <td><?= h($healthQuestionnaires->own_heart_attack) ?></td>
                    <td><?= h($healthQuestionnaires->fatty_liver) ?></td>
                    <td><?= h($healthQuestionnaires->fatty_liver_details) ?></td>
                    <td><?= h($healthQuestionnaires->rheumatoid_arthritis) ?></td>
                    <td><?= h($healthQuestionnaires->nsaids) ?></td>
                    <td><?= h($healthQuestionnaires->lupus) ?></td>
                    <td><?= h($healthQuestionnaires->hiv) ?></td>
                    <td><?= h($healthQuestionnaires->diverticulitis) ?></td>
                    <td><?= h($healthQuestionnaires->ulcers) ?></td>
                    <td><?= h($healthQuestionnaires->crohns_disease) ?></td>
                    <td><?= h($healthQuestionnaires->heartburn) ?></td>
                    <td><?= h($healthQuestionnaires->heartburn_long) ?></td>
                    <td><?= h($healthQuestionnaires->pain_abdomen) ?></td>
                    <td><?= h($healthQuestionnaires->pain_details) ?></td>
                    <td><?= h($healthQuestionnaires->bowel_movements) ?></td>
                    <td><?= h($healthQuestionnaires->bloody_stools) ?></td>
                    <td><?= h($healthQuestionnaires->hemorrhoids) ?></td>
                    <td><?= h($healthQuestionnaires->eating_disorder) ?></td>
                    <td><?= h($healthQuestionnaires->happy_life) ?></td>
                    <td><?= h($healthQuestionnaires->history_depression) ?></td>
                    <td><?= h($healthQuestionnaires->are_you_depressed) ?></td>
                    <td><?= h($healthQuestionnaires->stress_problem) ?></td>
                    <td><?= h($healthQuestionnaires->panic_stressed) ?></td>
                    <td><?= h($healthQuestionnaires->problems_eating) ?></td>
                    <td><?= h($healthQuestionnaires->cry_frecuently) ?></td>
                    <td><?= h($healthQuestionnaires->attempted_suicide) ?></td>
                    <td><?= h($healthQuestionnaires->thought_hurting_self) ?></td>
                    <td><?= h($healthQuestionnaires->trouble_sleeping) ?></td>
                    <td><?= h($healthQuestionnaires->been_to_counselor) ?></td>
                    <td><?= h($healthQuestionnaires->drink_alcohol) ?></td>
                    <td><?= h($healthQuestionnaires->kind_alcohol) ?></td>
                    <td><?= h($healthQuestionnaires->drinks_per_week) ?></td>
                    <td><?= h($healthQuestionnaires->concerned_drink) ?></td>
                    <td><?= h($healthQuestionnaires->considered_stopping) ?></td>
                    <td><?= h($healthQuestionnaires->blackouts) ?></td>
                    <td><?= h($healthQuestionnaires->use_tobacco) ?></td>
                    <td><?= h($healthQuestionnaires->cigarret_packs_day) ?></td>
                    <td><?= h($healthQuestionnaires->chew_day) ?></td>
                    <td><?= h($healthQuestionnaires->pipe_day) ?></td>
                    <td><?= h($healthQuestionnaires->cigars_day) ?></td>
                    <td><?= h($healthQuestionnaires->number_years) ?></td>
                    <td><?= h($healthQuestionnaires->years_quit) ?></td>
                    <td><?= h($healthQuestionnaires->use_caffeine) ?></td>
                    <td><?= h($healthQuestionnaires->caffeine_form) ?></td>
                    <td><?= h($healthQuestionnaires->caffeine_how_much) ?></td>
                    <td><?= h($healthQuestionnaires->recreational_drugs) ?></td>
                    <td><?= h($healthQuestionnaires->drugs_details) ?></td>
                    <td><?= h($healthQuestionnaires->drugs_needle) ?></td>
                    <td><?= h($healthQuestionnaires->sexually_active) ?></td>
                    <td><?= h($healthQuestionnaires->trying_pregnancy) ?></td>
                    <td><?= h($healthQuestionnaires->contraceptive) ?></td>
                    <td><?= h($healthQuestionnaires->intercourse_discomfort) ?></td>
                    <td><?= h($healthQuestionnaires->speak_about_hiv) ?></td>
                    <td><?= h($healthQuestionnaires->ankles_swelling) ?></td>
                    <td><?= h($healthQuestionnaires->ankles_pain) ?></td>
                    <td><?= h($healthQuestionnaires->ankles_stiffness) ?></td>
                    <td><?= h($healthQuestionnaires->ankles_popping) ?></td>
                    <td><?= h($healthQuestionnaires->knees_swelling) ?></td>
                    <td><?= h($healthQuestionnaires->knees_pain) ?></td>
                    <td><?= h($healthQuestionnaires->knees_stiffness) ?></td>
                    <td><?= h($healthQuestionnaires->kness_popping) ?></td>
                    <td><?= h($healthQuestionnaires->hips_swelling) ?></td>
                    <td><?= h($healthQuestionnaires->hips_pain) ?></td>
                    <td><?= h($healthQuestionnaires->hips_stiffness) ?></td>
                    <td><?= h($healthQuestionnaires->hips_popping) ?></td>
                    <td><?= h($healthQuestionnaires->back_swelling) ?></td>
                    <td><?= h($healthQuestionnaires->back_pain) ?></td>
                    <td><?= h($healthQuestionnaires->back_stiffness) ?></td>
                    <td><?= h($healthQuestionnaires->back_popping) ?></td>
                    <td><?= h($healthQuestionnaires->others_swelling) ?></td>
                    <td><?= h($healthQuestionnaires->others_pain) ?></td>
                    <td><?= h($healthQuestionnaires->others_stiffing) ?></td>
                    <td><?= h($healthQuestionnaires->others_popping) ?></td>
                    <td><?= h($healthQuestionnaires->treatment1) ?></td>
                    <td><?= h($healthQuestionnaires->treatment2) ?></td>
                    <td><?= h($healthQuestionnaires->join_medications) ?></td>
                    <td><?= h($healthQuestionnaires->join_medications_details) ?></td>
                    <td><?= h($healthQuestionnaires->degenerative_changes) ?></td>
                    <td><?= h($healthQuestionnaires->degenerative_changes_details) ?></td>
                    <td><?= h($healthQuestionnaires->weight_loss_surgery) ?></td>
                    <td><?= h($healthQuestionnaires->how_long_overweight) ?></td>
                    <td><?= h($healthQuestionnaires->diet_pills) ?></td>
                    <td><?= h($healthQuestionnaires->how_much_weight_loss) ?></td>
                    <td><?= h($healthQuestionnaires->how_quickly_regain) ?></td>
                    <td><?= h($healthQuestionnaires->max_weight) ?></td>
                    <td><?= h($healthQuestionnaires->min_weight) ?></td>
                    <td><?= h($healthQuestionnaires->foods_liked) ?></td>
                    <td><?= h($healthQuestionnaires->foods_disliked) ?></td>
                    <td><?= h($healthQuestionnaires->snaker) ?></td>
                    <td><?= h($healthQuestionnaires->volume_eater) ?></td>
                    <td><?= h($healthQuestionnaires->eats_sweets) ?></td>
                    <td><?= h($healthQuestionnaires->sweets_how_ofter) ?></td>
                    <td><?= h($healthQuestionnaires->carbonated_beverages) ?></td>
                    <td><?= h($healthQuestionnaires->type_lifestyle) ?></td>
                    <td><?= h($healthQuestionnaires->severe_fatigue) ?></td>
                    <td><?= h($healthQuestionnaires->severe_fatigue_details) ?></td>
                    <td><?= h($healthQuestionnaires->severe_weekness) ?></td>
                    <td><?= h($healthQuestionnaires->severe_weekness_details) ?></td>
                    <td><?= h($healthQuestionnaires->night_sweats) ?></td>
                    <td><?= h($healthQuestionnaires->night_sweats_details) ?></td>
                    <td><?= h($healthQuestionnaires->severe_headaches) ?></td>
                    <td><?= h($healthQuestionnaires->severe_headaches_details) ?></td>
                    <td><?= h($healthQuestionnaires->loss_of_conciousness) ?></td>
                    <td><?= h($healthQuestionnaires->loss_of_conciousness_details) ?></td>
                    <td><?= h($healthQuestionnaires->hearing_problems) ?></td>
                    <td><?= h($healthQuestionnaires->hearing_problems_details) ?></td>
                    <td><?= h($healthQuestionnaires->ear_pain) ?></td>
                    <td><?= h($healthQuestionnaires->ear_pain_details) ?></td>
                    <td><?= h($healthQuestionnaires->nasal_congestion) ?></td>
                    <td><?= h($healthQuestionnaires->nasal_congestion_details) ?></td>
                    <td><?= h($healthQuestionnaires->sinus_congestion) ?></td>
                    <td><?= h($healthQuestionnaires->sinus_congestion_details) ?></td>
                    <td><?= h($healthQuestionnaires->bloody_nose) ?></td>
                    <td><?= h($healthQuestionnaires->bloody_nose_details) ?></td>
                    <td><?= h($healthQuestionnaires->dental_problems) ?></td>
                    <td><?= h($healthQuestionnaires->dental_problems_details) ?></td>
                    <td><?= h($healthQuestionnaires->dentures) ?></td>
                    <td><?= h($healthQuestionnaires->dentures_details) ?></td>
                    <td><?= h($healthQuestionnaires->sores_in_mouth) ?></td>
                    <td><?= h($healthQuestionnaires->sores_in_mouth_details) ?></td>
                    <td><?= h($healthQuestionnaires->wheezing) ?></td>
                    <td><?= h($healthQuestionnaires->wheezing_details) ?></td>
                    <td><?= h($healthQuestionnaires->coughing) ?></td>
                    <td><?= h($healthQuestionnaires->coughing_details) ?></td>
                    <td><?= h($healthQuestionnaires->breast_lump) ?></td>
                    <td><?= h($healthQuestionnaires->breast_lump_details) ?></td>
                    <td><?= h($healthQuestionnaires->heart_murmur) ?></td>
                    <td><?= h($healthQuestionnaires->heart_murmur_details) ?></td>
                    <td><?= h($healthQuestionnaires->high_blood_pressure2) ?></td>
                    <td><?= h($healthQuestionnaires->high_blood_pressure2_details) ?></td>
                    <td><?= h($healthQuestionnaires->chest_pain_excercise) ?></td>
                    <td><?= h($healthQuestionnaires->chest_pain_excercise_details) ?></td>
                    <td><?= h($healthQuestionnaires->std) ?></td>
                    <td><?= h($healthQuestionnaires->std_details) ?></td>
                    <td><?= h($healthQuestionnaires->anemia) ?></td>
                    <td><?= h($healthQuestionnaires->anemia_details) ?></td>
                    <td><?= h($healthQuestionnaires->bleeding_tendency) ?></td>
                    <td><?= h($healthQuestionnaires->bleeding_tendency_details) ?></td>
                    <td><?= h($healthQuestionnaires->convulsions) ?></td>
                    <td><?= h($healthQuestionnaires->convulsions_details) ?></td>
                    <td><?= h($healthQuestionnaires->paralysis) ?></td>
                    <td><?= h($healthQuestionnaires->paralysis_details) ?></td>
                    <td><?= h($healthQuestionnaires->numbness) ?></td>
                    <td><?= h($healthQuestionnaires->numbness_details) ?></td>
                    <td><?= h($healthQuestionnaires->memory_loss) ?></td>
                    <td><?= h($healthQuestionnaires->memory_loss_details) ?></td>
                    <td><?= h($healthQuestionnaires->depression) ?></td>
                    <td><?= h($healthQuestionnaires->depression_details) ?></td>
                    <td><?= h($healthQuestionnaires->anxiety) ?></td>
                    <td><?= h($healthQuestionnaires->anxiety_details) ?></td>
                    <td><?= h($healthQuestionnaires->mood_swings) ?></td>
                    <td><?= h($healthQuestionnaires->mood_swings_details) ?></td>
                    <td><?= h($healthQuestionnaires->sleep_problems) ?></td>
                    <td><?= h($healthQuestionnaires->sleep_problems_details) ?></td>
                    <td><?= h($healthQuestionnaires->drug_alcochol_abuse) ?></td>
                    <td><?= h($healthQuestionnaires->drug_alcochol_abuse_details) ?></td>
                    <td><?= h($healthQuestionnaires->rash_hives) ?></td>
                    <td><?= h($healthQuestionnaires->rash_hives_details) ?></td>
                    <td><?= h($healthQuestionnaires->asthma2) ?></td>
                    <td><?= h($healthQuestionnaires->asthma2_details) ?></td>
                    <td><?= h($healthQuestionnaires->hay_fever) ?></td>
                    <td><?= h($healthQuestionnaires->hay_fever_details) ?></td>
                    <td><?= h($healthQuestionnaires->additional_information) ?></td>
                    <td><?= h($healthQuestionnaires->created) ?></td>
                    <td><?= h($healthQuestionnaires->modified) ?></td>
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'HealthQuestionnaires', 'action' => 'view', $healthQuestionnaires->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'HealthQuestionnaires', 'action' => 'edit', $healthQuestionnaires->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'HealthQuestionnaires', 'action' => 'delete', $healthQuestionnaires->id], ['confirm' => __('Are you sure you want to delete # {0}?', $healthQuestionnaires->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
