<section class="content-header">
  <h1>
    Bone Problem
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Doctor') ?></dt>
            <dd><?= h($boneProblem->doctor) ?></dd>
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $boneProblem->has('patient') ? $this->Html->link($boneProblem->patient->name, ['controller' => 'Patients', 'action' => 'view', $boneProblem->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $boneProblem->has('user') ? $this->Html->link($boneProblem->user->name, ['controller' => 'Users', 'action' => 'view', $boneProblem->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($boneProblem->id) ?></dd>
            <dt scope="row"><?= __('Date Treatment') ?></dt>
            <dd><?= h($boneProblem->date_treatment) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($boneProblem->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($boneProblem->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Diagnosis') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($boneProblem->diagnosis); ?>
        </div>
      </div>
    </div>
  </div>
</section>
