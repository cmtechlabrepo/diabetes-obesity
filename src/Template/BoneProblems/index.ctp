<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Bone Problems

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                  <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('doctor') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('date_treatment') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('patient_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                  <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($boneProblems as $boneProblem): ?>
                <tr>
                  <td><?= $this->Number->format($boneProblem->id) ?></td>
                  <td><?= h($boneProblem->doctor) ?></td>
                  <td><?= h($boneProblem->date_treatment) ?></td>
                  <td><?= $boneProblem->has('patient') ? $this->Html->link($boneProblem->patient->name, ['controller' => 'Patients', 'action' => 'view', $boneProblem->patient->id]) : '' ?></td>
                  <td><?= $boneProblem->has('user') ? $this->Html->link($boneProblem->user->name, ['controller' => 'Users', 'action' => 'view', $boneProblem->user->id]) : '' ?></td>
                  <td><?= h($boneProblem->created) ?></td>
                  <td><?= h($boneProblem->modified) ?></td>
                  <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $boneProblem->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $boneProblem->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $boneProblem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $boneProblem->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>