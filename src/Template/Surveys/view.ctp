<section class="content-header">
  <h1>
    Survey
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Ratings'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <?php 
              $referrals = array(1=>"Yes, a friend",2=>"Yes, a family",0=>"No, I don´t");
              $rating = array(1=>"Very bad",2=>'Bad',3=>"Regular",4=>'Good',5=>"Very good");
              $yesno = array(0=>"No",1=>"Yes");
            ?>
            <!-- <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $survey->has('patient') ? $this->Html->link($survey->patient->name, ['controller' => 'Patients', 'action' => 'view', $survey->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $survey->has('user') ? $this->Html->link($survey->user->name, ['controller' => 'Users', 'action' => 'view', $survey->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($survey->id) ?></dd> -->
            <dt scope="row"><?= __('Service Rating') ?></dt>
            <dd><?= $rating[$this->Number->format($survey->service_rating)] ?></dd>
            <dt scope="row"><?= __('Guidance Performance') ?></dt>
            <dd><?= $rating[$this->Number->format($survey->guidance_performance)] ?></dd>
            <dt scope="row"><?= __('Medical Procedure Explanation') ?></dt>
            <dd><?= $rating[$this->Number->format($survey->medical_procedure_explanation)] ?></dd>
            <dt scope="row"><?= __('Care Before After') ?></dt>
            <dd><?= $rating[$this->Number->format($survey->care_before_after)] ?></dd>
            <dt scope="row"><?= __('Nutritional Information') ?></dt>
            <dd><?= $rating[$this->Number->format($survey->nutritional_information)] ?></dd>
            <dt scope="row"><?= __('Transportation Punctuality') ?></dt>
            <dd><?= $rating[$this->Number->format($survey->transportation_punctuality)] ?></dd>
            <dt scope="row"><?= __('Transportation Cleaning') ?></dt>
            <dd><?= $rating[$this->Number->format($survey->transportation_cleaning)] ?></dd>
            <dt scope="row"><?= __('Transportation Driving') ?></dt>
            <dd><?= $rating[$this->Number->format($survey->transportation_driving)] ?></dd>
            <dt scope="row"><?= __('Recommend Us') ?></dt>
            <dd><?= $yesno[$this->Number->format($survey->recommend_us)] ?></dd>
            <dt scope="row"><?= __('Give Referrals') ?></dt>
            <dd><?= $referrals[$this->Number->format($survey->give_referrals)] ?></dd>
            <!-- <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= $this->Number->format($survey->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($survey->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($survey->modified) ?></dd> -->
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Do you want to tell us something about the hospital?') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($survey->about_hospital); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Do you want to tell us something about the hotel?') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($survey->about_hotel); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Why would you recommend us?') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($survey->why_recommend); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Referrals') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($survey->referrals)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Email') ?></th>
                    <!-- <th scope="col"><?= __('Patient Id') ?></th> -->
                    <!-- <th scope="col"><?= __('Survey Id') ?></th>
                    <th scope="col"><?= __('User Id') ?></th>
                    <th scope="col"><?= __('Date') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th> -->
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($survey->referrals as $referrals): ?>
              <tr>
                    <td><?= h($referrals->id) ?></td>
                    <td><?= h($referrals->email) ?></td>
                    <!-- <td><?= h($referrals->patient_id) ?></td> -->
                    <!-- <td><?= h($referrals->survey_id) ?></td>
                    <td><?= h($referrals->user_id) ?></td>
                    <td><?= h($referrals->date) ?></td>
                    <td><?= h($referrals->created) ?></td>
                    <td><?= h($referrals->modified) ?></td> -->
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'Referrals', 'action' => 'view', $referrals->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'Referrals', 'action' => 'edit', $referrals->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'Referrals', 'action' => 'delete', $referrals->id], ['confirm' => __('Are you sure you want to delete # {0}?', $referrals->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
