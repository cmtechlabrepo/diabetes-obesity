<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Survey $survey
 */
?>
<?php echo $this->Html->script('surveys/add.js'); ?>
<style>
/* body div {
  font-size: 15px;
  margin-top: 15px;
  text-align: center;
} */

div.smileys input[type="radio"] {
  -webkit-appearance: none;
  width: 50px;
  height: 50px;
  border: none;
  cursor: pointer;
  transition: border .2s ease;
  -webkit-filter: grayscale(100%);
          filter: grayscale(100%);
  margin: 0 5px;
  transition: all .2s ease;
}
div.smileys input[type="radio"]:hover, div.smileys input[type="radio"]:checked {
  -webkit-filter: grayscale(0);
          filter: grayscale(0);
}
div.smileys input[type="radio"]:focus {
  outline: 0;
}
div.smileys input[type="radio"].happy {
  background: url("https://res.cloudinary.com/turdlife/image/upload/v1492864443/happy_ampvnc.svg") center;
  background-size: cover;
}
div.smileys input[type="radio"].neutral {
  background: url("https://res.cloudinary.com/turdlife/image/upload/v1492864443/neutral_t3q8hz.svg") center;
  background-size: cover;
}
div.smileys input[type="radio"].sad {
  background: url("https://res.cloudinary.com/turdlife/image/upload/v1492864443/sad_bj1tuj.svg") center;
  background-size: cover;
}

.meaning {
    font-size: 20px;
    font-weight: bold;
}

td {
    min-width: 300px;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        We want to improve for you</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i>
                <?php echo __('Home'); ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Help us to improve.<br><br>
The information you provide will be used to improve our service, your answers will be treated confidentially and will not be used for any
purpose other than the research carried out by Diabetes Obesity Clinic'); ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($survey, ['role' => 'form']); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label>1. How do you rate our service in general?</label><br>
                            <div id="smileys1" class="smileys">
                                <input type="radio" name="service_rating" value="0" title="Bad" class="sad">
                                <input type="radio" name="service_rating" value="0.5" title="Regular" class="neutral">
                                <input type="radio" name="service_rating" value="1" title="Good" class="happy">                                
                            </div>
                            <div class="meaning" id="meaning"></div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label>2. How do you rate the performance of...</label>
                            <div class="box-body table-responsive">
                                <table class="table-striped">
                                    <tr>
                                        <td>Guidance of Patient Coordinator</td>
                                        <td>
                                            <div id="smileys2" class="smileys">
                                                <input type="radio" name="guidance_performance" value="0" title="Bad" class="sad">
                                                <input type="radio" name="guidance_performance" value="0.5" title="Regular" class="neutral">
                                                <input type="radio" name="guidance_performance" value="1" title="Good" class="happy">                                
                                            </div>
                                            <div class="meaning" id="meaning2"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Explanation about the Medical Procedure (Doctors)</td>
                                        <td>
                                            <div id="smileys3" class="smileys">
                                                <input type="radio" name="medical_procedure_explanation" value="0" title="Bad" class="sad">
                                                <input type="radio" name="medical_procedure_explanation" value="0.5" title="Regular" class="neutral">
                                                <input type="radio" name="medical_procedure_explanation" value="1" title="Good" class="happy">                                
                                            </div>
                                            <div class="meaning" id="meaning3"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Care before and after surgery (Nurses)</td>
                                        <td>
                                            <div id="smileys4" class="smileys">
                                                <input type="radio" name="care_before_after" value="0" title="Bad" class="sad">
                                                <input type="radio" name="care_before_after" value="0.5" title="Regular" class="neutral">
                                                <input type="radio" name="care_before_after" value="1" title="Good" class="happy">                                
                                            </div>
                                            <div class="meaning" id="meaning4"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nutritional information (Nutritionist)</td>
                                        <td>
                                            <div id="smileys5" class="smileys">
                                                <input type="radio" name="nutritional_information" value="0" title="Bad" class="sad">
                                                <input type="radio" name="nutritional_information" value="0.5" title="Regular" class="neutral">
                                                <input type="radio" name="nutritional_information" value="1" title="Good" class="happy">                                
                                            </div>
                                            <div class="meaning" id="meaning5"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label>3. How do you rate transportation in...</label>
                            <div class="box-body table-responsive">
                                <table class="table-striped">
                                    <tr>
                                        <td>Puntuality</td>
                                        <td>
                                            <div id="smileys6" class="smileys">
                                                <input type="radio" name="transportation_punctuality" value="0" title="Bad" class="sad">
                                                <input type="radio" name="transportation_punctuality" value="0.5" title="Regular" class="neutral">
                                                <input type="radio" name="transportation_punctuality" value="1" title="Good" class="happy">                                
                                            </div>
                                            <div class="meaning" id="meaning6"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cleaning</td>
                                        <td>
                                            <div id="smileys7" class="smileys">
                                                <input type="radio" name="transportation_cleaning" value="0" title="Bad" class="sad">
                                                <input type="radio" name="transportation_cleaning" value="0.5" title="Regular" class="neutral">
                                                <input type="radio" name="transportation_cleaning" value="1" title="Good" class="happy">                                
                                            </div>
                                            <div class="meaning" id="meaning7"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Way to drive the car</td>
                                        <td>
                                            <div id="smileys8" class="smileys">
                                                <input type="radio" name="transportation_driving" value="0" title="Bad" class="sad">
                                                <input type="radio" name="transportation_driving" value="0.5" title="Regular" class="neutral">
                                                <input type="radio" name="transportation_driving" value="1" title="Good" class="happy">                                
                                            </div>
                                            <div class="meaning" id="meaning8"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                echo $this->Form->control('about_hospital',['label'=>'4. Do you want to tell us something about the hospital?']);
                ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                echo $this->Form->control('about_hotel',['label'=>"5. Do you want to tell us something about the hotel?"]);
                ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <?php
                echo $this->Form->control('recommend_us',["label"=>"6. Would you recommended us?",'options'=>[""=>"Select",0=>"No",2=>"Yes"]]);
                ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                echo $this->Form->control('why_recommend',['label'=>'Why?']);
                ?>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label>Would you like to recommend our services to a friend or family? If you give us his /
                                her email address he / she will be given a $50 USD bonus and the great opportunity to
                                start a healthy life!</label>
                            <?php
                                // $referrals = array(1=>"Yes, a friend",2=>"Yes, a family",0=>"No, I don´t");                                
                                // echo $this->Form->radio('give_referrals',$referrals);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="box-body table-responsive no-padding">
                                <table id="referral-list" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">Email address</th>
                                            <th scope="col">Friend / Family</th>
                                            <th scope="col">Add / Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="referral1">
                                            <td><input name='email1' type="text" id="email1"></td>
                                            <td>
                                                <select name="type1" id="type1">
                                                    <option>Select</option>
                                                    <option value=1>Friend</option>
                                                    <option value=2>Family</option>
                                                </select>
                                            </td>
                                            <td>
                                                <a href="#" class="add add-referral"><i class="fa fa-fw fa-plus"></i></a>
                                                <a href="#" class="remove remove-referral"><i class="fa fa-fw fa-times"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> -->
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
</section>