<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Survey $survey
 */
?>
<?php echo $this->Html->script('surveys/add.js'); ?>
<style>
.rating {
    display: inline-block;
    position: relative;
    height: 50px;
    line-height: 50px;
    font-size: 50px;
}

.rating label {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    cursor: pointer;
}

.rating label:last-child {
    position: static;
}

.rating label:nth-child(1) {
    z-index: 5;
}

.rating label:nth-child(2) {
    z-index: 4;
}

.rating label:nth-child(3) {
    z-index: 3;
}

.rating label:nth-child(4) {
    z-index: 2;
}

.rating label:nth-child(5) {
    z-index: 1;
}

.rating label input {
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
}

.rating label .icon {
    float: left;
    color: transparent;
}

.rating label:last-child .icon {
    color: #000;
}

.rating:not(:hover) label input:checked~.icon,
.rating:hover label:hover input~.icon {
    color: #09f;
}

.rating label input:focus:not(:checked)~.icon:last-child {
    color: #000;
    text-shadow: 0 0 5px #09f;
}

.radio label {
    position: relative;
    display: inline-block;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: 400;
    vertical-align: middle;
    cursor: pointer;
}

.meaning {
    font-size: 20px;
    font-weight: bold;
}

td {
    min-width: 300px;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        We want to improve for you</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i>
                <?php echo __('Home'); ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Help us to improve.<br><br>
The information you provide will be used to improve our service, your answers will be treated confidentially and will not be used for any
purpose other than the research carried out by Diabetes Obesity Clinic'); ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($survey, ['role' => 'form']); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label>1. How do you rate our service in general?</label><br>
                            <div class="rating">
                                <label class='star-rating'>
                                    <input type="radio" name="service_rating" title="Very bad" value="1" />
                                    <span class="icon">★</span>
                                </label>
                                <label class='star-rating'>
                                    <input type="radio" name="service_rating" title="Bad" value="2" />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                                <label class='star-rating'>
                                    <input type="radio" name="service_rating" title="Regular" value="3" />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                                <label class='star-rating'>
                                    <input type="radio" name="service_rating" title="Good" value="4" />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                                <label class='star-rating'>
                                    <input type="radio" name="service_rating" title="Very good" value="5" />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                            </div>
                            <div class="meaning" id="meaning"></div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label>2. How do you rate the performance of...</label>
                            <div class="box-body table-responsive">
                                <table class="table-striped">
                                    <tr>
                                        <td>Guidance of Patient Coordinator</td>
                                        <td>
                                            <div class="rating">
                                                <label class='star-rating'>
                                                    <input type="radio" name="guidance_performance" title="Very bad"
                                                        value="1" />
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="guidance_performance" title="Bad"
                                                        value="2" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="guidance_performance" title="Regular"
                                                        value="3" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="guidance_performance" title="Good"
                                                        value="4" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="guidance_performance" title="Very good"
                                                        value="5" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                            </div>
                                            <div class="meaning" id="meaning2"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Explanation about the Medical Procedure (Doctors)</td>
                                        <td>
                                            <div class="rating">
                                                <label class='star-rating'>
                                                    <input type="radio" name="medical_procedure_explanation"
                                                        title="Very bad" value="1" />
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="medical_procedure_explanation" title="Bad"
                                                        value="2" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="medical_procedure_explanation"
                                                        title="Regular" value="3" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="medical_procedure_explanation"
                                                        title="Good" value="4" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="medical_procedure_explanation"
                                                        title="Very good" value="5" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                            </div>
                                            <div class="meaning" id="meaning3"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Care before and after surgery (Nurses)</td>
                                        <td>
                                            <div class="rating">
                                                <label class='star-rating'>
                                                    <input type="radio" name="care_before_after" title="Very bad"
                                                        value="1" />
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="care_before_after" title="Bad"
                                                        value="2" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="care_before_after" title="Regular"
                                                        value="3" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="care_before_after" title="Good"
                                                        value="4" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="care_before_after" title="Very good"
                                                        value="5" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                            </div>
                                            <div class="meaning" id="meaning4"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nutritional information (Nutritionist)</td>
                                        <td>
                                            <div class="rating">
                                                <label class='star-rating'>
                                                    <input type="radio" name="nutritional_information" title="Very bad"
                                                        value="1" />
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="nutritional_information" title="Bad"
                                                        value="2" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="nutritional_information" title="Regular"
                                                        value="3" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="nutritional_information" title="Good"
                                                        value="4" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="nutritional_information" title="Very good"
                                                        value="5" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                            </div>
                                            <div class="meaning" id="meaning5"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label>3. How do you rate transportation in...</label>
                            <div class="box-body table-responsive">
                                <table class="table-striped">
                                    <tr>
                                        <td>Puntuality</td>
                                        <td>
                                            <div class="rating">
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_punctuality"
                                                        title="Very bad" value="1" />
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_punctuality" title="Bad"
                                                        value="2" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_punctuality"
                                                        title="Regular" value="3" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_punctuality" title="Good"
                                                        value="4" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_punctuality"
                                                        title="Very good" value="5" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                            </div>
                                            <div class="meaning" id="meaning6"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cleaning</td>
                                        <td>
                                            <div class="rating">
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_cleaning" title="Very bad"
                                                        value="1" />
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_cleaning" title="Bad"
                                                        value="2" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_cleaning" title="Regular"
                                                        value="3" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_cleaning" title="Good"
                                                        value="4" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_cleaning" title="Very good"
                                                        value="5" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                            </div>
                                            <div class="meaning" id="meaning7"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Way to drive the car</td>
                                        <td>
                                            <div class="rating">
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_driving" title="Very bad"
                                                        value="1" />
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_driving" title="Bad"
                                                        value="2" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_driving" title="Regular"
                                                        value="3" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_driving" title="Good"
                                                        value="4" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                                <label class='star-rating'>
                                                    <input type="radio" name="transportation_driving" title="Very good"
                                                        value="5" />
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                    <span class="icon">★</span>
                                                </label>
                                            </div>
                                            <div class="meaning" id="meaning8"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                echo $this->Form->control('about_hospital',['label'=>'4. Do you want to tell us something about the hospital?']);
                ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                echo $this->Form->control('about_hotel',['label'=>"5. Do you want to tell us something about the hotel?"]);
                ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <?php
                echo $this->Form->control('recommend_us',["label"=>"6. Would you recommended us?",'options'=>[""=>"Select",0=>"No",1=>"Yes"]]);
                ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                echo $this->Form->control('why_recommend',['label'=>'Why?']);
                ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label>Would you like to recommend our services to a friend or family? If you give us his /
                                her email address he / she will be given a $50 USD bonus and the great opportunity to
                                start a healthy life!</label>
                            <?php
                                $referrals = array(1=>"Yes, a friend",2=>"Yes, a family",0=>"No, I don´t");                                
                                echo $this->Form->radio('give_referrals',$referrals);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="box-body table-responsive no-padding">
                                <table id="referral-list" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">Email address</th>
                                            <th scope="col">Friend / Family</th>
                                            <th scope="col">Add / Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="referral1">
                                            <td><input name='email1' type="text" id="email1"></td>
                                            <td>
                                                <select name="type1" id="type1">
                                                    <option>Select</option>
                                                    <option value=1>Friend</option>
                                                    <option value=2>Family</option>
                                                </select>
                                            </td>
                                            <td>
                                                <a href="#" class="add add-referral"><i class="fa fa-fw fa-plus"></i></a>
                                                <a href="#" class="remove remove-referral"><i class="fa fa-fw fa-times"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
</section>