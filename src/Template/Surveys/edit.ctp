<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Survey $survey
 */
?>
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Survey
      <small><?php echo __('Edit'); ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Form'); ?></h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <?php echo $this->Form->create($survey, ['role' => 'form']); ?>
            <div class="box-body">
              <?php
                echo $this->Form->control('service_rating');
                echo $this->Form->control('guidance_performance');
                echo $this->Form->control('medical_procedure_explanation');
                echo $this->Form->control('care_before_after');
                echo $this->Form->control('nutritional_information');
                echo $this->Form->control('transportation_punctuality');
                echo $this->Form->control('transportation_cleaning');
                echo $this->Form->control('transportation_driving');
                echo $this->Form->control('about_hospital');
                echo $this->Form->control('about_hotel');
                echo $this->Form->control('recommend_us');
                echo $this->Form->control('why_recommend');
                echo $this->Form->control('give_referrals');
                echo $this->Form->control('date');
                echo $this->Form->control('patient_id', ['options' => $patients, 'empty' => true]);
                echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
              ?>
            </div>
            <!-- /.box-body -->

          <?php echo $this->Form->submit(__('Submit')); ?>

          <?php echo $this->Form->end(); ?>
        </div>
        <!-- /.box -->
      </div>
  </div>
  <!-- /.row -->
</section>
