<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Surveys

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                  <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                  <th scope="col">Total Score</th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('guidance_performance') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('medical_procedure_explanation') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('care_before_after') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('nutritional_information') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('transportation_punctuality') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('transportation_cleaning') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('transportation_driving') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('recommend_us') ?></th> -->
                  <!-- <th scope="col"><?= $this->Paginator->sort('give_referrals') ?></th> -->
                  <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('patient_id') ?></th> -->
                  <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                  <!-- <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('modified') ?></th> -->
                  <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $referrals = array(1=>"Yes, a friend",2=>"Yes, a family",0=>"No, I don´t");
                $rating = array(0=>'Bad',0.5=>"Regular",1=>'Good');
                foreach ($surveys as $survey): 
                $score = 0;
                $score += $survey->service_rating;
                $score += $survey->guidance_performance;
                $score += $survey->medical_procedure_explanation;
                $score += $survey->care_before_after;
                $score += $survey->nutritional_information;
                $score += $survey->transportation_punctuality;
                $score += $survey->transportation_cleaning;
                $score += $survey->transportation_driving;
                $score += $survey->recommend_us;
                ?>
                <tr>
                  <td><?= $this->Number->format($survey->id) ?></td>
                  <td><?= $score ?></td>
                  <!-- <td><?= $this->Number->format($survey->guidance_performance) ?></td>
                  <td><?= $this->Number->format($survey->medical_procedure_explanation) ?></td>
                  <td><?= $this->Number->format($survey->care_before_after) ?></td>
                  <td><?= $this->Number->format($survey->nutritional_information) ?></td>
                  <td><?= $this->Number->format($survey->transportation_punctuality) ?></td>
                  <td><?= $this->Number->format($survey->transportation_cleaning) ?></td>
                  <td><?= $this->Number->format($survey->transportation_driving) ?></td>
                  <td><?= $this->Number->format($survey->recommend_us) ?></td> -->
                  <!-- <td><?= $referrals[$survey->give_referrals] ?></td> -->
                  <td><?= h($survey->date) ?></td>
                  <!-- <td><?= $survey->has('patient') ? $this->Html->link($survey->patient->name, ['controller' => 'Patients', 'action' => 'view', $survey->patient->id]) : '' ?></td> -->
                  <td><?= $survey->has('user') ? $this->Html->link($survey->user->name, ['controller' => 'Users', 'action' => 'view', $survey->user->id]) : '' ?></td>
                  <!-- <td><?= h($survey->created) ?></td>
                  <td><?= h($survey->modified) ?></td> -->
                  <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $survey->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <!-- <?= $this->Html->link(__('Edit'), ['action' => 'edit', $survey->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $survey->id], ['confirm' => __('Are you sure you want to delete # {0}?', $survey->id), 'class'=>'btn btn-danger btn-xs']) ?> -->
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>