<section class="content-header">
  <h1>
    Digestive Problem
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Food Drink') ?></dt>
            <dd><?= h($digestiveProblem->food_drink) ?></dd>
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $digestiveProblem->has('patient') ? $this->Html->link($digestiveProblem->patient->name, ['controller' => 'Patients', 'action' => 'view', $digestiveProblem->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $digestiveProblem->has('user') ? $this->Html->link($digestiveProblem->user->name, ['controller' => 'Users', 'action' => 'view', $digestiveProblem->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($digestiveProblem->id) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($digestiveProblem->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($digestiveProblem->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Result Of Eating') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($digestiveProblem->result_of_eating); ?>
        </div>
      </div>
    </div>
  </div>
</section>
