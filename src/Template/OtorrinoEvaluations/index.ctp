<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Otorrino Evaluations

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                  <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('patient_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('doctor_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('procedure_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                  <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($otorrinoEvaluations as $otorrinoEvaluation): ?>
                <tr>
                  <td><?= $this->Number->format($otorrinoEvaluation->id) ?></td>
                  <td><?= $otorrinoEvaluation->has('patient') ? $this->Html->link($otorrinoEvaluation->patient->name, ['controller' => 'Patients', 'action' => 'view', $otorrinoEvaluation->patient->id]) : '' ?></td>
                  <td><?= $otorrinoEvaluation->has('doctor') ? $this->Html->link($otorrinoEvaluation->doctor->name, ['controller' => 'Doctors', 'action' => 'view', $otorrinoEvaluation->doctor->id]) : '' ?></td>
                  <td><?= $otorrinoEvaluation->has('procedure') ? $this->Html->link($otorrinoEvaluation->procedure->name, ['controller' => 'Procedures', 'action' => 'view', $otorrinoEvaluation->procedure->id]) : '' ?></td>
                  <td><?= h($otorrinoEvaluation->date) ?></td>
                  <td><?= $otorrinoEvaluation->has('user') ? $this->Html->link($otorrinoEvaluation->user->name, ['controller' => 'Users', 'action' => 'view', $otorrinoEvaluation->user->id]) : '' ?></td>
                  <td><?= h($otorrinoEvaluation->created) ?></td>
                  <td><?= h($otorrinoEvaluation->modified) ?></td>
                  <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $otorrinoEvaluation->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $otorrinoEvaluation->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $otorrinoEvaluation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $otorrinoEvaluation->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>