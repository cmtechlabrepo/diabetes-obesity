<section class="content-header">
  <h1>
    Otorrino Evaluation
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $otorrinoEvaluation->has('patient') ? $this->Html->link($otorrinoEvaluation->patient->name, ['controller' => 'Patients', 'action' => 'view', $otorrinoEvaluation->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Doctor') ?></dt>
            <dd><?= $otorrinoEvaluation->has('doctor') ? $this->Html->link($otorrinoEvaluation->doctor->name, ['controller' => 'Doctors', 'action' => 'view', $otorrinoEvaluation->doctor->id]) : '' ?></dd>
            <dt scope="row"><?= __('Procedure') ?></dt>
            <dd><?= $otorrinoEvaluation->has('procedure') ? $this->Html->link($otorrinoEvaluation->procedure->name, ['controller' => 'Procedures', 'action' => 'view', $otorrinoEvaluation->procedure->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $otorrinoEvaluation->has('user') ? $this->Html->link($otorrinoEvaluation->user->name, ['controller' => 'Users', 'action' => 'view', $otorrinoEvaluation->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($otorrinoEvaluation->id) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($otorrinoEvaluation->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($otorrinoEvaluation->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($otorrinoEvaluation->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Observaciones') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($otorrinoEvaluation->observaciones); ?>
        </div>
      </div>
    </div>
  </div>
</section>
