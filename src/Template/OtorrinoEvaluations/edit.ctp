<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OtorrinoEvaluations $otorrinoEvaluation
 */
?>
<?php echo $this->Html->script('otorrino-evaluations/add.js'); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.custom-combobox {
    position: relative;
    display: inline-block;
}

.custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
}

.custom-combobox-input {
    margin-left: 10px;
    padding: 5px 10px;
}
</style>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Valoracion otorrino
        <small><?php echo __('Add'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i>
                <?php echo __('Home'); ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Form'); ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($otorrinoEvaluation, ['role' => 'form']); ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                      echo $this->Form->control('patient_id', ['options' => $patients, 'empty' => true]);
                      ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <?php
                        echo $this->Form->control('doctor_id', ['options' => $doctors, 'empty' => true]);
                        ?>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <?php
                        echo $this->Form->control('procedure_id', ['options' => $procedures, 'empty' => true]);
                        ?>
                        </div>
                        <!-- <div class="col-md-4 col-xs-12">
                            <?php
                        echo $this->Form->control('procedure_date', ['type'=>'text','id'=>'date']);
                        // echo $this->Form->control('date', ['empty' => true]);
                        ?>
                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                        echo $this->Form->control('observaciones');
                        ?>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                        echo $this->Form->control('anotaciones_fisicas');
                        ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php
                        echo $this->Form->control('recomendaciones');
                        // echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
                        ?>
                        </div>
                    </div> -->
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>