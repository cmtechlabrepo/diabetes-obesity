<section class="content-header">
  <h1>
    Nutricional Evaluation
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Patient') ?></dt>
            <dd><?= $nutricionalEvaluation->has('patient') ? $this->Html->link($nutricionalEvaluation->patient->name, ['controller' => 'Patients', 'action' => 'view', $nutricionalEvaluation->patient->id]) : '' ?></dd>
            <dt scope="row"><?= __('Doctor') ?></dt>
            <dd><?= $nutricionalEvaluation->has('doctor') ? $this->Html->link($nutricionalEvaluation->doctor->name, ['controller' => 'Doctors', 'action' => 'view', $nutricionalEvaluation->doctor->id]) : '' ?></dd>
            <dt scope="row"><?= __('Procedure') ?></dt>
            <dd><?= $nutricionalEvaluation->has('procedure') ? $this->Html->link($nutricionalEvaluation->procedure->name, ['controller' => 'Procedures', 'action' => 'view', $nutricionalEvaluation->procedure->id]) : '' ?></dd>
            <dt scope="row"><?= __('User') ?></dt>
            <dd><?= $nutricionalEvaluation->has('user') ? $this->Html->link($nutricionalEvaluation->user->name, ['controller' => 'Users', 'action' => 'view', $nutricionalEvaluation->user->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($nutricionalEvaluation->id) ?></dd>
            <dt scope="row"><?= __('Date') ?></dt>
            <dd><?= h($nutricionalEvaluation->date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($nutricionalEvaluation->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($nutricionalEvaluation->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Observaciones') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($nutricionalEvaluation->observaciones); ?>
        </div>
      </div>
    </div>
  </div>
</section>
