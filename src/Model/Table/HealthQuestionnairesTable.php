<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HealthQuestionnaires Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Doctors
 * @property \App\Model\Table\ProceduresTable|\Cake\ORM\Association\BelongsTo $Procedures
 * @property \App\Model\Table\PatientsTable|\Cake\ORM\Association\BelongsTo $Patients
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\HealthQuestionnaire get($primaryKey, $options = [])
 * @method \App\Model\Entity\HealthQuestionnaire newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HealthQuestionnaire[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HealthQuestionnaire|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HealthQuestionnaire|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HealthQuestionnaire patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HealthQuestionnaire[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HealthQuestionnaire findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class HealthQuestionnairesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('health_questionnaires');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Doctors', [
            'foreignKey' => 'doctor_id'
        ]);
        $this->belongsTo('Procedures', [
            'foreignKey' => 'procedure_id'
        ]);
        $this->belongsTo('Patients', [
            'foreignKey' => 'patient_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('location')
            ->maxLength('location', 200)
            ->allowEmptyString('location');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->scalar('address')
            ->allowEmptyString('address');

        $validator
            ->scalar('city')
            ->maxLength('city', 200)
            ->allowEmptyString('city');

        $validator
            ->scalar('state')
            ->maxLength('state', 200)
            ->allowEmptyString('state');

        $validator
            ->scalar('zip_code')
            ->maxLength('zip_code', 20)
            ->allowEmptyString('zip_code');

        $validator
            ->scalar('home_phone')
            ->maxLength('home_phone', 20)
            ->allowEmptyString('home_phone');

        $validator
            ->scalar('work_phone')
            ->maxLength('work_phone', 20)
            ->allowEmptyString('work_phone');

        $validator
            ->scalar('cell_phone')
            ->maxLength('cell_phone', 20)
            ->allowEmptyString('cell_phone');

        $validator
            ->date('date_of_birth')
            ->allowEmptyDate('date_of_birth');

        $validator
            ->allowEmptyString('gender');

        $validator
            ->allowEmptyString('marital_status');

        $validator
            ->integer('age')
            ->allowEmptyString('age');

        $validator
            ->scalar('height')
            ->maxLength('height', 20)
            ->allowEmptyString('height');

        $validator
            ->numeric('weight')
            ->allowEmptyString('weight');

        $validator
            ->numeric('BMI')
            ->allowEmptyString('BMI');

        $validator
            ->scalar('neck')
            ->maxLength('neck', 20)
            ->allowEmptyString('neck');

        $validator
            ->scalar('wrist')
            ->maxLength('wrist', 20)
            ->allowEmptyString('wrist');

        $validator
            ->scalar('waist')
            ->maxLength('waist', 20)
            ->allowEmptyString('waist');

        $validator
            ->scalar('hip')
            ->maxLength('hip', 20)
            ->allowEmptyString('hip');

        $validator
            ->scalar('thigh')
            ->maxLength('thigh', 20)
            ->allowEmptyString('thigh');

        $validator
            ->scalar('size_shirt')
            ->maxLength('size_shirt', 20)
            ->allowEmptyString('size_shirt');

        $validator
            ->scalar('size_pants')
            ->maxLength('size_pants', 20)
            ->allowEmptyString('size_pants');

        $validator
            ->date('suggested_date')
            ->allowEmptyDate('suggested_date');

        $validator
            ->scalar('details')
            ->allowEmptyString('details');

        $validator
            ->scalar('referring_person')
            ->maxLength('referring_person', 255)
            ->allowEmptyString('referring_person');

        $validator
            ->date('date_of_referral')
            ->allowEmptyDate('date_of_referral');

        $validator
            ->scalar('emergency_name')
            ->maxLength('emergency_name', 255)
            ->allowEmptyString('emergency_name');

        $validator
            ->scalar('emergency_phone')
            ->maxLength('emergency_phone', 20)
            ->allowEmptyString('emergency_phone');

        $validator
            ->scalar('primary_health_provider')
            ->maxLength('primary_health_provider', 255)
            ->allowEmptyString('primary_health_provider');

        $validator
            ->scalar('time_treated')
            ->maxLength('time_treated', 100)
            ->allowEmptyString('time_treated');

        $validator
            ->scalar('condition_treated')
            ->maxLength('condition_treated', 500)
            ->allowEmptyString('condition_treated');

        $validator
            ->allowEmptyString('allergic_medication');

        $validator
            ->scalar('allergic_medication_list')
            ->allowEmptyString('allergic_medication_list');

        $validator
            ->allowEmptyString('supplies_allergic');

        $validator
            ->scalar('supplies_allergic_list')
            ->allowEmptyString('supplies_allergic_list');

        $validator
            ->allowEmptyString('allergic_food');

        $validator
            ->scalar('allergic_food_list')
            ->allowEmptyString('allergic_food_list');

        $validator
            ->allowEmptyString('blood_transfusion');

        $validator
            ->scalar('transfusion_details')
            ->allowEmptyString('transfusion_details');

        $validator
            ->allowEmptyString('breast_colo_prostate_cancer');

        $validator
            ->allowEmptyString('cancer_specific');

        $validator
            ->allowEmptyString('diabetes');

        $validator
            ->allowEmptyString('heart_attack');

        $validator
            ->allowEmptyString('stroke');

        $validator
            ->allowEmptyString('high_blood_pressure');

        $validator
            ->allowEmptyString('arthritis');

        $validator
            ->allowEmptyString('obesity');

        $validator
            ->allowEmptyString('shortness_breath');

        $validator
            ->scalar('shortness_breath_long')
            ->maxLength('shortness_breath_long', 100)
            ->allowEmptyString('shortness_breath_long');

        $validator
            ->allowEmptyString('excercise_regularly');

        $validator
            ->allowEmptyString('asthma');

        $validator
            ->integer('snore')
            ->allowEmptyString('snore');

        $validator
            ->scalar('average_sleep')
            ->maxLength('average_sleep', 200)
            ->allowEmptyString('average_sleep');

        $validator
            ->integer('wakeup_tired')
            ->allowEmptyString('wakeup_tired');

        $validator
            ->integer('tired_sleepy')
            ->allowEmptyString('tired_sleepy');

        $validator
            ->integer('cpap')
            ->allowEmptyString('cpap');

        $validator
            ->allowEmptyString('swelling_ankles');

        $validator
            ->scalar('decrese_swelling')
            ->allowEmptyString('decrese_swelling');

        $validator
            ->scalar('decrease_pain')
            ->allowEmptyString('decrease_pain');

        $validator
            ->allowEmptyString('thyroid_problems');

        $validator
            ->allowEmptyString('own_diabetes');

        $validator
            ->scalar('medications_for_diabetes')
            ->allowEmptyString('medications_for_diabetes');

        $validator
            ->allowEmptyString('monitor_blood_sugar');

        $validator
            ->scalar('monitor_how_ofter')
            ->allowEmptyString('monitor_how_ofter');

        $validator
            ->allowEmptyString('pcos');

        $validator
            ->allowEmptyString('own_high_blood_pressure');

        $validator
            ->scalar('medications_blood_pressure')
            ->allowEmptyString('medications_blood_pressure');

        $validator
            ->allowEmptyString('chest_pain');

        $validator
            ->scalar('chest_pain_often')
            ->allowEmptyString('chest_pain_often');

        $validator
            ->allowEmptyString('history_heart_disease');

        $validator
            ->scalar('explain_heart_dicease')
            ->allowEmptyString('explain_heart_dicease');

        $validator
            ->allowEmptyString('own_heart_attack');

        $validator
            ->allowEmptyString('fatty_liver');

        $validator
            ->scalar('fatty_liver_details')
            ->allowEmptyString('fatty_liver_details');

        $validator
            ->allowEmptyString('rheumatoid_arthritis');

        $validator
            ->allowEmptyString('nsaids');

        $validator
            ->allowEmptyString('lupus');

        $validator
            ->allowEmptyString('hiv');

        $validator
            ->integer('other_autoinmmune')
            ->allowEmptyString('other_autoinmmune');

        $validator
            ->integer('constipation')
            ->allowEmptyString('constipation');

        $validator
            ->allowEmptyString('diverticulitis');

        $validator
            ->allowEmptyString('ulcers');

        $validator
            ->allowEmptyString('crohns_disease');

        $validator
            ->allowEmptyString('heartburn');

        $validator
            ->scalar('heartburn_long')
            ->allowEmptyString('heartburn_long');

        $validator
            ->allowEmptyString('pain_abdomen');

        $validator
            ->scalar('pain_details')
            ->allowEmptyString('pain_details');

        $validator
            ->allowEmptyString('bowel_movements');

        $validator
            ->allowEmptyString('bloody_stools');

        $validator
            ->allowEmptyString('hemorrhoids');

        $validator
            ->allowEmptyString('eating_disorder');

        $validator
            ->allowEmptyString('happy_life');

        $validator
            ->allowEmptyString('history_depression');

        $validator
            ->allowEmptyString('are_you_depressed');

        $validator
            ->allowEmptyString('stress_problem');

        $validator
            ->allowEmptyString('panic_stressed');

        $validator
            ->allowEmptyString('problems_eating');

        $validator
            ->allowEmptyString('cry_frecuently');

        $validator
            ->allowEmptyString('attempted_suicide');

        $validator
            ->allowEmptyString('thought_hurting_self');

        $validator
            ->allowEmptyString('trouble_sleeping');

        $validator
            ->allowEmptyString('been_to_counselor');

        $validator
            ->allowEmptyString('drink_alcohol');

        $validator
            ->scalar('kind_alcohol')
            ->maxLength('kind_alcohol', 200)
            ->allowEmptyString('kind_alcohol');

        $validator
            ->numeric('drinks_per_week')
            ->allowEmptyString('drinks_per_week');

        $validator
            ->allowEmptyString('concerned_drink');

        $validator
            ->allowEmptyString('considered_stopping');

        $validator
            ->allowEmptyString('blackouts');

        $validator
            ->scalar('frecuency')
            ->maxLength('frecuency', 200)
            ->allowEmptyString('frecuency');

        $validator
            ->integer('how_drink_it')
            ->allowEmptyString('how_drink_it');

        $validator
            ->integer('mixed_preference')
            ->allowEmptyString('mixed_preference');

        $validator
            ->integer('eat_drinking')
            ->allowEmptyString('eat_drinking');

        $validator
            ->scalar('eat_what')
            ->maxLength('eat_what', 200)
            ->allowEmptyString('eat_what');

        $validator
            ->allowEmptyString('use_tobacco');

        $validator
            ->scalar('cigarret_packs_day')
            ->maxLength('cigarret_packs_day', 100)
            ->allowEmptyString('cigarret_packs_day');

        $validator
            ->scalar('chew_day')
            ->maxLength('chew_day', 100)
            ->allowEmptyString('chew_day');

        $validator
            ->scalar('pipe_day')
            ->maxLength('pipe_day', 100)
            ->allowEmptyString('pipe_day');

        $validator
            ->scalar('cigars_day')
            ->maxLength('cigars_day', 100)
            ->allowEmptyString('cigars_day');

        $validator
            ->numeric('number_years')
            ->allowEmptyString('number_years');

        $validator
            ->numeric('years_quit')
            ->allowEmptyString('years_quit');

        $validator
            ->allowEmptyString('use_caffeine');

        $validator
            ->scalar('caffeine_form')
            ->maxLength('caffeine_form', 100)
            ->allowEmptyString('caffeine_form');

        $validator
            ->scalar('caffeine_how_much')
            ->maxLength('caffeine_how_much', 100)
            ->allowEmptyString('caffeine_how_much');

        $validator
            ->numeric('carbonated_quantity')
            ->allowEmptyString('carbonated_quantity');

        $validator
            ->scalar('carbonated_brand')
            ->maxLength('carbonated_brand', 200)
            ->allowEmptyString('carbonated_brand');

        $validator
            ->numeric('sugary_quantity')
            ->allowEmptyString('sugary_quantity');

        $validator
            ->scalar('sugary_brand')
            ->maxLength('sugary_brand', 200)
            ->allowEmptyString('sugary_brand');

        $validator
            ->numeric('natural_quantity')
            ->allowEmptyString('natural_quantity');

        $validator
            ->integer('natural_brand')
            ->allowEmptyString('natural_brand');

        $validator
            ->allowEmptyString('recreational_drugs');

        $validator
            ->scalar('drugs_details')
            ->allowEmptyString('drugs_details');

        $validator
            ->allowEmptyString('drugs_needle');

        $validator
            ->allowEmptyString('sexually_active');

        $validator
            ->allowEmptyString('trying_pregnancy');

        $validator
            ->scalar('contraceptive')
            ->maxLength('contraceptive', 200)
            ->allowEmptyString('contraceptive');

        $validator
            ->integer('intercourse_discomfort')
            ->allowEmptyString('intercourse_discomfort');

        $validator
            ->allowEmptyString('speak_about_hiv');

        $validator
            ->allowEmptyString('ankles_swelling');

        $validator
            ->allowEmptyString('ankles_pain');

        $validator
            ->allowEmptyString('ankles_stiffness');

        $validator
            ->allowEmptyString('ankles_popping');

        $validator
            ->allowEmptyString('knees_swelling');

        $validator
            ->allowEmptyString('knees_pain');

        $validator
            ->allowEmptyString('knees_stiffness');

        $validator
            ->allowEmptyString('kness_popping');

        $validator
            ->allowEmptyString('hips_swelling');

        $validator
            ->allowEmptyString('hips_pain');

        $validator
            ->allowEmptyString('hips_stiffness');

        $validator
            ->allowEmptyString('hips_popping');

        $validator
            ->allowEmptyString('back_swelling');

        $validator
            ->allowEmptyString('back_pain');

        $validator
            ->allowEmptyString('back_stiffness');

        $validator
            ->allowEmptyString('back_popping');

        $validator
            ->allowEmptyString('others_swelling');

        $validator
            ->allowEmptyString('others_pain');

        $validator
            ->allowEmptyString('others_stiffing');

        $validator
            ->allowEmptyString('others_popping');

        $validator
            ->scalar('treatment1')
            ->allowEmptyString('treatment1');

        $validator
            ->scalar('treatment2')
            ->allowEmptyString('treatment2');

        $validator
            ->allowEmptyString('join_medications');

        $validator
            ->scalar('join_medications_details')
            ->allowEmptyString('join_medications_details');

        $validator
            ->allowEmptyString('degenerative_changes');

        $validator
            ->scalar('degenerative_changes_details')
            ->allowEmptyString('degenerative_changes_details');

        $validator
            ->allowEmptyString('weight_loss_surgery');

        $validator
            ->allowEmptyString('how_long_overweight');

        $validator
            ->allowEmptyString('diet_pills');

        $validator
            ->numeric('how_much_weight_loss')
            ->allowEmptyString('how_much_weight_loss');

        $validator
            ->scalar('how_quickly_regain')
            ->allowEmptyString('how_quickly_regain');

        $validator
            ->numeric('max_weight')
            ->allowEmptyString('max_weight');

        $validator
            ->numeric('min_weight')
            ->allowEmptyString('min_weight');

        $validator
            ->scalar('how_long_max')
            ->maxLength('how_long_max', 200)
            ->allowEmptyString('how_long_max');

        $validator
            ->scalar('how_long_min')
            ->maxLength('how_long_min', 200)
            ->allowEmptyString('how_long_min');

        $validator
            ->scalar('expected_loss')
            ->maxLength('expected_loss', 200)
            ->allowEmptyString('expected_loss');

        $validator
            ->scalar('expected_time')
            ->maxLength('expected_time', 200)
            ->allowEmptyString('expected_time');

        $validator
            ->scalar('foods_liked')
            ->allowEmptyString('foods_liked');

        $validator
            ->scalar('foods_disliked')
            ->allowEmptyString('foods_disliked');

        $validator
            ->allowEmptyString('snaker');

        $validator
            ->allowEmptyString('volume_eater');

        $validator
            ->allowEmptyString('eats_sweets');

        $validator
            ->scalar('sweets_how_ofter')
            ->allowEmptyString('sweets_how_ofter');

        $validator
            ->allowEmptyString('carbonated_beverages');

        $validator
            ->allowEmptyString('type_lifestyle');

        $validator
            ->allowEmptyString('severe_fatigue');

        $validator
            ->scalar('severe_fatigue_details')
            ->allowEmptyString('severe_fatigue_details');

        $validator
            ->allowEmptyString('severe_weekness');

        $validator
            ->scalar('severe_weekness_details')
            ->allowEmptyString('severe_weekness_details');

        $validator
            ->allowEmptyString('night_sweats');

        $validator
            ->scalar('night_sweats_details')
            ->allowEmptyString('night_sweats_details');

        $validator
            ->allowEmptyString('severe_headaches');

        $validator
            ->scalar('severe_headaches_details')
            ->allowEmptyString('severe_headaches_details');

        $validator
            ->allowEmptyString('loss_of_conciousness');

        $validator
            ->scalar('loss_of_conciousness_details')
            ->allowEmptyString('loss_of_conciousness_details');

        $validator
            ->allowEmptyString('hearing_problems');

        $validator
            ->scalar('hearing_problems_details')
            ->allowEmptyString('hearing_problems_details');

        $validator
            ->allowEmptyString('ear_pain');

        $validator
            ->scalar('ear_pain_details')
            ->allowEmptyString('ear_pain_details');

        $validator
            ->allowEmptyString('nasal_congestion');

        $validator
            ->scalar('nasal_congestion_details')
            ->allowEmptyString('nasal_congestion_details');

        $validator
            ->allowEmptyString('sinus_congestion');

        $validator
            ->scalar('sinus_congestion_details')
            ->allowEmptyString('sinus_congestion_details');

        $validator
            ->allowEmptyString('bloody_nose');

        $validator
            ->scalar('bloody_nose_details')
            ->allowEmptyString('bloody_nose_details');

        $validator
            ->allowEmptyString('dental_problems');

        $validator
            ->scalar('dental_problems_details')
            ->allowEmptyString('dental_problems_details');

        $validator
            ->allowEmptyString('dentures');

        $validator
            ->scalar('dentures_details')
            ->allowEmptyString('dentures_details');

        $validator
            ->allowEmptyString('sores_in_mouth');

        $validator
            ->scalar('sores_in_mouth_details')
            ->allowEmptyString('sores_in_mouth_details');

        $validator
            ->allowEmptyString('wheezing');

        $validator
            ->scalar('wheezing_details')
            ->allowEmptyString('wheezing_details');

        $validator
            ->allowEmptyString('coughing');

        $validator
            ->scalar('coughing_details')
            ->allowEmptyString('coughing_details');

        $validator
            ->allowEmptyString('breast_lump');

        $validator
            ->scalar('breast_lump_details')
            ->allowEmptyString('breast_lump_details');

        $validator
            ->allowEmptyString('heart_murmur');

        $validator
            ->scalar('heart_murmur_details')
            ->allowEmptyString('heart_murmur_details');

        $validator
            ->allowEmptyString('high_blood_pressure2');

        $validator
            ->scalar('high_blood_pressure2_details')
            ->allowEmptyString('high_blood_pressure2_details');

        $validator
            ->allowEmptyString('chest_pain_excercise');

        $validator
            ->scalar('chest_pain_excercise_details')
            ->allowEmptyString('chest_pain_excercise_details');

        $validator
            ->allowEmptyString('std');

        $validator
            ->scalar('std_details')
            ->allowEmptyString('std_details');

        $validator
            ->allowEmptyString('anemia');

        $validator
            ->scalar('anemia_details')
            ->allowEmptyString('anemia_details');

        $validator
            ->allowEmptyString('bleeding_tendency');

        $validator
            ->scalar('bleeding_tendency_details')
            ->allowEmptyString('bleeding_tendency_details');

        $validator
            ->allowEmptyString('convulsions');

        $validator
            ->scalar('convulsions_details')
            ->allowEmptyString('convulsions_details');

        $validator
            ->allowEmptyString('paralysis');

        $validator
            ->scalar('paralysis_details')
            ->allowEmptyString('paralysis_details');

        $validator
            ->allowEmptyString('numbness');

        $validator
            ->scalar('numbness_details')
            ->allowEmptyString('numbness_details');

        $validator
            ->allowEmptyString('memory_loss');

        $validator
            ->scalar('memory_loss_details')
            ->allowEmptyString('memory_loss_details');

        $validator
            ->allowEmptyString('depression');

        $validator
            ->scalar('depression_details')
            ->allowEmptyString('depression_details');

        $validator
            ->allowEmptyString('anxiety');

        $validator
            ->scalar('anxiety_details')
            ->allowEmptyString('anxiety_details');

        $validator
            ->allowEmptyString('mood_swings');

        $validator
            ->scalar('mood_swings_details')
            ->allowEmptyString('mood_swings_details');

        $validator
            ->allowEmptyString('sleep_problems');

        $validator
            ->scalar('sleep_problems_details')
            ->allowEmptyString('sleep_problems_details');

        $validator
            ->allowEmptyString('drug_alcochol_abuse');

        $validator
            ->scalar('drug_alcochol_abuse_details')
            ->allowEmptyString('drug_alcochol_abuse_details');

        $validator
            ->allowEmptyString('rash_hives');

        $validator
            ->scalar('rash_hives_details')
            ->allowEmptyString('rash_hives_details');

        $validator
            ->allowEmptyString('asthma2');

        $validator
            ->scalar('asthma2_details')
            ->allowEmptyString('asthma2_details');

        $validator
            ->allowEmptyString('hay_fever');

        $validator
            ->scalar('hay_fever_details')
            ->allowEmptyString('hay_fever_details');

        $validator
            ->scalar('additional_information')
            ->allowEmptyString('additional_information');

        $validator
            ->scalar('signature_initials')
            ->maxLength('signature_initials', 10)
            ->allowEmptyString('signature_initials');

        $validator
            ->date('signature_date')
            ->allowEmptyDate('signature_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['doctor_id'], 'Doctors'));
        $rules->add($rules->existsIn(['procedure_id'], 'Procedures'));
        $rules->add($rules->existsIn(['patient_id'], 'Patients'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
