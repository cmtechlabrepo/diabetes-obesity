<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LabTests Model
 *
 * @method \App\Model\Entity\LabTest get($primaryKey, $options = [])
 * @method \App\Model\Entity\LabTest newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LabTest[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LabTest|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LabTest|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LabTest patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LabTest[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LabTest findOrCreate($search, callable $callback = null, $options = [])
 */
class LabTestsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('lab_tests');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('estudio')
            ->maxLength('estudio', 200)
            ->allowEmptyString('estudio');

        $validator
            ->scalar('unidades')
            ->maxLength('unidades', 50)
            ->allowEmptyString('unidades');

        $validator
            ->numeric('limite_inferior')
            ->allowEmptyString('limite_inferior');

        $validator
            ->numeric('limite_superior')
            ->allowEmptyString('limite_superior');

        return $validator;
    }
}
