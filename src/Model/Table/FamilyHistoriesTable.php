<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FamilyHistories Model
 *
 * @property \App\Model\Table\PatientsTable|\Cake\ORM\Association\BelongsTo $Patients
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\FamilyHistory get($primaryKey, $options = [])
 * @method \App\Model\Entity\FamilyHistory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FamilyHistory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FamilyHistory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FamilyHistory|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FamilyHistory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FamilyHistory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FamilyHistory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FamilyHistoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('family_histories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Patients', [
            'foreignKey' => 'patient_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('family_member')
            ->maxLength('family_member', 300)
            ->allowEmptyString('family_member');

        $validator
            ->scalar('age')
            ->maxLength('age', 100)
            ->allowEmptyString('age');

        $validator
            ->scalar('deceased')
            ->maxLength('deceased', 4)
            ->allowEmptyString('deceased');

        $validator
            ->scalar('cause_of_death')
            ->maxLength('cause_of_death', 300)
            ->allowEmptyString('cause_of_death');

        $validator
            ->allowEmptyString('complexion');

        $validator
            ->scalar('health_problems')
            ->maxLength('health_problems', 500)
            ->allowEmptyString('health_problems');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['patient_id'], 'Patients'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
