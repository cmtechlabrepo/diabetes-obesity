<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 * @property \App\Model\Table\AnestesiaEvaluationsTable|\Cake\ORM\Association\HasMany $AnestesiaEvaluations
 * @property \App\Model\Table\BoneProblemsTable|\Cake\ORM\Association\HasMany $BoneProblems
 * @property \App\Model\Table\DigestiveProblemsTable|\Cake\ORM\Association\HasMany $DigestiveProblems
 * @property \App\Model\Table\DoctorsTable|\Cake\ORM\Association\HasMany $Doctors
 * @property \App\Model\Table\FamilyHistoriesTable|\Cake\ORM\Association\HasMany $FamilyHistories
 * @property \App\Model\Table\HealthQuestionnairesTable|\Cake\ORM\Association\HasMany $HealthQuestionnaires
 * @property \App\Model\Table\InfoMedicationsTable|\Cake\ORM\Association\HasMany $InfoMedications
 * @property \App\Model\Table\InternistaEvaluationsTable|\Cake\ORM\Association\HasMany $InternistaEvaluations
 * @property \App\Model\Table\MajorIllnessesTable|\Cake\ORM\Association\HasMany $MajorIllnesses
 * @property \App\Model\Table\OtherHospitalizationsTable|\Cake\ORM\Association\HasMany $OtherHospitalizations
 * @property \App\Model\Table\PatientSurgeriesTable|\Cake\ORM\Association\HasMany $PatientSurgeries
 * @property \App\Model\Table\PatientsTable|\Cake\ORM\Association\HasMany $Patients
 * @property \App\Model\Table\PatientsLabTestsTable|\Cake\ORM\Association\HasMany $PatientsLabTests
 * @property \App\Model\Table\ReferralsTable|\Cake\ORM\Association\HasMany $Referrals
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\HasMany $Roles
 * @property \App\Model\Table\SurgeryChangesTable|\Cake\ORM\Association\HasMany $SurgeryChanges
 * @property \App\Model\Table\SurveysTable|\Cake\ORM\Association\HasMany $Surveys
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AnestesiaEvaluations', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('BoneProblems', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('DigestiveProblems', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Doctors', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('FamilyHistories', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('HealthQuestionnaires', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('InfoMedications', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('InternistaEvaluations', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('MajorIllnesses', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('OtherHospitalizations', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PatientSurgeries', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Patients', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PatientsLabTests', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Referrals', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Roles', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('SurgeryChanges', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Surveys', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->allowEmptyString('email', false);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', false);

        $validator
            ->integer('closed_modal')
            ->allowEmptyString('closed_modal');

        $validator
            ->integer('filled_survey')
            ->allowEmptyString('filled_survey');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }
}
