<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Patients Model
 *
 * @property \App\Model\Table\DoctorsTable|\Cake\ORM\Association\BelongsTo $Doctors
 * @property \App\Model\Table\GendersTable|\Cake\ORM\Association\BelongsTo $Genders
 * @property \App\Model\Table\MaritalStatusesTable|\Cake\ORM\Association\BelongsTo $MaritalStatuses
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\AnestesiaEvaluationsTable|\Cake\ORM\Association\HasMany $AnestesiaEvaluations
 * @property \App\Model\Table\AppointmentsTable|\Cake\ORM\Association\HasMany $Appointments
 * @property \App\Model\Table\BoneProblemsTable|\Cake\ORM\Association\HasMany $BoneProblems
 * @property \App\Model\Table\DigestiveProblemsTable|\Cake\ORM\Association\HasMany $DigestiveProblems
 * @property \App\Model\Table\EmailsTable|\Cake\ORM\Association\HasMany $Emails
 * @property \App\Model\Table\FamilyHistoriesTable|\Cake\ORM\Association\HasMany $FamilyHistories
 * @property \App\Model\Table\HealthQuestionnairesTable|\Cake\ORM\Association\HasMany $HealthQuestionnaires
 * @property \App\Model\Table\InfoMedicationsTable|\Cake\ORM\Association\HasMany $InfoMedications
 * @property \App\Model\Table\InternistaEvaluationsTable|\Cake\ORM\Association\HasMany $InternistaEvaluations
 * @property \App\Model\Table\MajorIllnessesTable|\Cake\ORM\Association\HasMany $MajorIllnesses
 * @property \App\Model\Table\NutricionalEvaluationsTable|\Cake\ORM\Association\HasMany $NutricionalEvaluations
 * @property \App\Model\Table\OtherHospitalizationsTable|\Cake\ORM\Association\HasMany $OtherHospitalizations
 * @property \App\Model\Table\OtorrinoEvaluationsTable|\Cake\ORM\Association\HasMany $OtorrinoEvaluations
 * @property \App\Model\Table\PatientSurgeriesTable|\Cake\ORM\Association\HasMany $PatientSurgeries
 * @property \App\Model\Table\PsicologiaEvaluationsTable|\Cake\ORM\Association\HasMany $PsicologiaEvaluations
 * @property \App\Model\Table\ReferralsTable|\Cake\ORM\Association\HasMany $Referrals
 * @property \App\Model\Table\SurgeryChangesTable|\Cake\ORM\Association\HasMany $SurgeryChanges
 * @property \App\Model\Table\SurveysTable|\Cake\ORM\Association\HasMany $Surveys
 * @property \App\Model\Table\LabTestsTable|\Cake\ORM\Association\BelongsToMany $LabTests
 *
 * @method \App\Model\Entity\Patient get($primaryKey, $options = [])
 * @method \App\Model\Entity\Patient newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Patient[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Patient|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Patient|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Patient patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Patient[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Patient findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PatientsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('patients');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Doctors', [
            'foreignKey' => 'doctor_id'
        ]);
        $this->belongsTo('Genders', [
            'foreignKey' => 'gender_id'
        ]);
        $this->belongsTo('MaritalStatuses', [
            'foreignKey' => 'marital_status_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('AnestesiaEvaluations', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('Appointments', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('BoneProblems', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('DigestiveProblems', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('Emails', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('FamilyHistories', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('HealthQuestionnaires', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('InfoMedications', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('InternistaEvaluations', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('MajorIllnesses', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('NutricionalEvaluations', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('OtherHospitalizations', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('OtorrinoEvaluations', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('PatientSurgeries', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('PsicologiaEvaluations', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('Referrals', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('SurgeryChanges', [
            'foreignKey' => 'patient_id'
        ]);
        $this->hasMany('Surveys', [
            'foreignKey' => 'patient_id'
        ]);
        $this->belongsToMany('LabTests', [
            'foreignKey' => 'patient_id',
            'targetForeignKey' => 'lab_test_id',
            'joinTable' => 'patients_lab_tests'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('location')
            ->maxLength('location', 200)
            ->allowEmptyString('location');

        $validator
            ->scalar('name')
            ->maxLength('name', 500)
            ->allowEmptyString('name');

        $validator
            ->scalar('address')
            ->maxLength('address', 500)
            ->allowEmptyString('address');

        $validator
            ->scalar('city')
            ->maxLength('city', 200)
            ->allowEmptyString('city');

        $validator
            ->scalar('state')
            ->maxLength('state', 200)
            ->allowEmptyString('state');

        $validator
            ->scalar('zipcode')
            ->maxLength('zipcode', 50)
            ->allowEmptyString('zipcode');

        $validator
            ->scalar('home_phone')
            ->maxLength('home_phone', 50)
            ->allowEmptyString('home_phone');

        $validator
            ->scalar('work_phone')
            ->maxLength('work_phone', 50)
            ->allowEmptyString('work_phone');

        $validator
            ->scalar('cell_phone')
            ->maxLength('cell_phone', 50)
            ->allowEmptyString('cell_phone');

        $validator
            ->date('date_of_birth')
            ->allowEmptyDate('date_of_birth');

        $validator
            ->integer('age')
            ->allowEmptyString('age');

        $validator
            ->scalar('height')
            ->maxLength('height', 20)
            ->allowEmptyString('height');

        $validator
            ->numeric('weight')
            ->allowEmptyString('weight');

        $validator
            ->numeric('bmi')
            ->allowEmptyString('bmi');

        $validator
            ->numeric('neck')
            ->allowEmptyString('neck');

        $validator
            ->numeric('wrist')
            ->allowEmptyString('wrist');

        $validator
            ->numeric('waist')
            ->allowEmptyString('waist');

        $validator
            ->numeric('hip')
            ->allowEmptyString('hip');

        $validator
            ->numeric('thigh')
            ->allowEmptyString('thigh');

        $validator
            ->scalar('shirt_size')
            ->maxLength('shirt_size', 20)
            ->allowEmptyString('shirt_size');

        $validator
            ->scalar('pants_size')
            ->maxLength('pants_size', 20)
            ->allowEmptyString('pants_size');

        $validator
            ->integer('primera_llamada')
            ->allowEmptyString('primera_llamada');

        $validator
            ->scalar('comentarios_llamada')
            ->allowEmptyString('comentarios_llamada');

        $validator
            ->date('primer_pago')
            ->allowEmptyDate('primer_pago');

        $validator
            ->integer('metodo_pago')
            ->allowEmptyString('metodo_pago');

        $validator
            ->date('fecha_cirugia_autorizada')
            ->allowEmptyDate('fecha_cirugia_autorizada');

        $validator
            ->integer('sms_notifications')
            ->allowEmptyString('sms_notifications');

        $validator
            ->integer('email_notifications')
            ->allowEmptyString('email_notifications');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['doctor_id'], 'Doctors'));
        $rules->add($rules->existsIn(['gender_id'], 'Genders'));
        $rules->add($rules->existsIn(['marital_status_id'], 'MaritalStatuses'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
