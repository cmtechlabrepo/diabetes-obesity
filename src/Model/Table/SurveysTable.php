<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Surveys Model
 *
 * @property \App\Model\Table\PatientsTable|\Cake\ORM\Association\BelongsTo $Patients
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ReferralsTable|\Cake\ORM\Association\HasMany $Referrals
 *
 * @method \App\Model\Entity\Survey get($primaryKey, $options = [])
 * @method \App\Model\Entity\Survey newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Survey[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Survey|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Survey|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Survey patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Survey[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Survey findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SurveysTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('surveys');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Patients', [
            'foreignKey' => 'patient_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Referrals', [
            'foreignKey' => 'survey_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->numeric('service_rating')
            ->allowEmptyString('service_rating');

        $validator
            ->numeric('guidance_performance')
            ->allowEmptyString('guidance_performance');

        $validator
            ->numeric('medical_procedure_explanation')
            ->allowEmptyString('medical_procedure_explanation');

        $validator
            ->numeric('care_before_after')
            ->allowEmptyString('care_before_after');

        $validator
            ->numeric('nutritional_information')
            ->allowEmptyString('nutritional_information');

        $validator
            ->numeric('transportation_punctuality')
            ->allowEmptyString('transportation_punctuality');

        $validator
            ->numeric('transportation_cleaning')
            ->allowEmptyString('transportation_cleaning');

        $validator
            ->numeric('transportation_driving')
            ->allowEmptyString('transportation_driving');

        $validator
            ->scalar('about_hospital')
            ->allowEmptyString('about_hospital');

        $validator
            ->scalar('about_hotel')
            ->allowEmptyString('about_hotel');

        $validator
            ->integer('recommend_us')
            ->allowEmptyString('recommend_us');

        $validator
            ->scalar('why_recommend')
            ->allowEmptyString('why_recommend');

        $validator
            ->integer('give_referrals')
            ->allowEmptyString('give_referrals');

        $validator
            ->date('date')
            ->allowEmptyDate('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['patient_id'], 'Patients'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
