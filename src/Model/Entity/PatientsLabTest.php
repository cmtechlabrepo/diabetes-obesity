<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PatientsLabTest Entity
 *
 * @property int $id
 * @property int|null $patient_id
 * @property int|null $lab_test_id
 * @property string|null $value
 * @property \Cake\I18n\FrozenDate|null $date
 * @property string|null $doctor_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\LabTest $lab_test
 * @property \App\Model\Entity\Doctor $doctor
 */
class PatientsLabTest extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'patient_id' => true,
        'lab_test_id' => true,
        'value' => true,
        'date' => true,
        'doctor_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'lab_test' => true,
        'doctor' => true
    ];
}
