<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PatientsPicture Entity
 *
 * @property int $id
 * @property string $path
 * @property int $patient_id
 * @property int|null $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Patient $patient
 */
class PatientsPicture extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'path' => true,
        'patient_id' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'patient' => true
    ];
}
