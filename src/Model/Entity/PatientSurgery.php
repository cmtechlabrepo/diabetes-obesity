<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PatientSurgery Entity
 *
 * @property int $id
 * @property string|null $surgery
 * @property \Cake\I18n\FrozenDate|null $date
 * @property string|null $reason
 * @property string|null $hospital
 * @property int|null $patient_id
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\User $user
 */
class PatientSurgery extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'surgery' => true,
        'date' => true,
        'reason' => true,
        'hospital' => true,
        'patient_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'user' => true
    ];
}
