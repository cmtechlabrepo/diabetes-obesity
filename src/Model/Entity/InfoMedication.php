<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InfoMedication Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $dose
 * @property string|null $often_taken
 * @property string|null $purpose
 * @property \Cake\I18n\FrozenDate|null $started_use
 * @property string|null $brand
 * @property int $patient_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\User $user
 */
class InfoMedication extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'dose' => true,
        'often_taken' => true,
        'purpose' => true,
        'started_use' => true,
        'brand' => true,
        'patient_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'user' => true
    ];
}
