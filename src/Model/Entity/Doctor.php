<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Doctor Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $telephone
 * @property string|null $mobile_phone
 * @property string|null $address
 * @property string|null $email
 * @property int|null $doctor_type_id
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\DoctorType $doctor_type
 * @property \App\Model\Entity\User $user
 */
class Doctor extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'telephone' => true,
        'mobile_phone' => true,
        'address' => true,
        'email' => true,
        'doctor_type_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'doctor_type' => true,
        'user' => true
    ];
}
