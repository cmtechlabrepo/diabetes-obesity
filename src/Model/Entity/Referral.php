<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Referral Entity
 *
 * @property int $id
 * @property string|null $email
 * @property int|null $type
 * @property int|null $patient_id
 * @property int|null $survey_id
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenDate|null $date
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\Survey $survey
 * @property \App\Model\Entity\User $user
 */
class Referral extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'type' => true,
        'patient_id' => true,
        'survey_id' => true,
        'user_id' => true,
        'date' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'survey' => true,
        'user' => true
    ];
}
