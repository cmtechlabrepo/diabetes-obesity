<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LabTest Entity
 *
 * @property int $id
 * @property string|null $estudio
 * @property string|null $unidades
 * @property float|null $limite_inferior
 * @property float|null $limite_superior
 */
class LabTest extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'estudio' => true,
        'unidades' => true,
        'limite_inferior' => true,
        'limite_superior' => true
    ];
}
