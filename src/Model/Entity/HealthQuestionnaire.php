<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HealthQuestionnaire Entity
 *
 * @property int $id
 * @property string|null $location
 * @property int|null $doctor_id
 * @property string $name
 * @property string|null $address
 * @property string|null $city
 * @property string|null $state
 * @property string|null $zip_code
 * @property string|null $home_phone
 * @property string|null $work_phone
 * @property string|null $cell_phone
 * @property \Cake\I18n\FrozenDate|null $date_of_birth
 * @property int|null $gender
 * @property int|null $marital_status
 * @property int|null $age
 * @property string|null $height
 * @property float|null $weight
 * @property float|null $BMI
 * @property string|null $neck
 * @property string|null $wrist
 * @property string|null $waist
 * @property string|null $hip
 * @property string|null $thigh
 * @property string|null $size_shirt
 * @property string|null $size_pants
 * @property int|null $procedure_id
 * @property \Cake\I18n\FrozenDate|null $suggested_date
 * @property string|null $details
 * @property string|null $referring_person
 * @property \Cake\I18n\FrozenDate|null $date_of_referral
 * @property string|null $emergency_name
 * @property string|null $emergency_phone
 * @property string|null $primary_health_provider
 * @property string|null $time_treated
 * @property string|null $condition_treated
 * @property int|null $allergic_medication
 * @property string|null $allergic_medication_list
 * @property int|null $supplies_allergic
 * @property string|null $supplies_allergic_list
 * @property int|null $allergic_food
 * @property string|null $allergic_food_list
 * @property int|null $blood_transfusion
 * @property string|null $transfusion_details
 * @property int|null $breast_colo_prostate_cancer
 * @property int|null $cancer_specific
 * @property int|null $diabetes
 * @property int|null $heart_attack
 * @property int|null $stroke
 * @property int|null $high_blood_pressure
 * @property int|null $arthritis
 * @property int|null $obesity
 * @property int|null $shortness_breath
 * @property string|null $shortness_breath_long
 * @property int|null $excercise_regularly
 * @property int|null $asthma
 * @property int|null $snore
 * @property string|null $average_sleep
 * @property int|null $wakeup_tired
 * @property int|null $tired_sleepy
 * @property int|null $cpap
 * @property int|null $swelling_ankles
 * @property string|null $decrese_swelling
 * @property string|null $decrease_pain
 * @property int|null $thyroid_problems
 * @property int|null $own_diabetes
 * @property string|null $medications_for_diabetes
 * @property int|null $monitor_blood_sugar
 * @property string|null $monitor_how_ofter
 * @property int|null $pcos
 * @property int|null $own_high_blood_pressure
 * @property string|null $medications_blood_pressure
 * @property int|null $chest_pain
 * @property string|null $chest_pain_often
 * @property int|null $history_heart_disease
 * @property string|null $explain_heart_dicease
 * @property int|null $own_heart_attack
 * @property int|null $fatty_liver
 * @property string|null $fatty_liver_details
 * @property int|null $rheumatoid_arthritis
 * @property int|null $nsaids
 * @property int|null $lupus
 * @property int|null $hiv
 * @property int|null $other_autoinmmune
 * @property int|null $constipation
 * @property int|null $diverticulitis
 * @property int|null $ulcers
 * @property int|null $crohns_disease
 * @property int|null $heartburn
 * @property string|null $heartburn_long
 * @property int|null $pain_abdomen
 * @property string|null $pain_details
 * @property int|null $bowel_movements
 * @property int|null $bloody_stools
 * @property int|null $hemorrhoids
 * @property int|null $eating_disorder
 * @property int|null $happy_life
 * @property int|null $history_depression
 * @property int|null $are_you_depressed
 * @property int|null $stress_problem
 * @property int|null $panic_stressed
 * @property int|null $problems_eating
 * @property int|null $cry_frecuently
 * @property int|null $attempted_suicide
 * @property int|null $thought_hurting_self
 * @property int|null $trouble_sleeping
 * @property int|null $been_to_counselor
 * @property int|null $drink_alcohol
 * @property string|null $kind_alcohol
 * @property float|null $drinks_per_week
 * @property int|null $concerned_drink
 * @property int|null $considered_stopping
 * @property int|null $blackouts
 * @property string|null $frecuency
 * @property int|null $how_drink_it
 * @property int|null $mixed_preference
 * @property int|null $eat_drinking
 * @property string|null $eat_what
 * @property int|null $use_tobacco
 * @property string|null $cigarret_packs_day
 * @property string|null $chew_day
 * @property string|null $pipe_day
 * @property string|null $cigars_day
 * @property float|null $number_years
 * @property float|null $years_quit
 * @property int|null $use_caffeine
 * @property string|null $caffeine_form
 * @property string|null $caffeine_how_much
 * @property float|null $carbonated_quantity
 * @property string|null $carbonated_brand
 * @property float|null $sugary_quantity
 * @property string|null $sugary_brand
 * @property float|null $natural_quantity
 * @property int|null $natural_brand
 * @property int|null $recreational_drugs
 * @property string|null $drugs_details
 * @property int|null $drugs_needle
 * @property int|null $sexually_active
 * @property int|null $trying_pregnancy
 * @property string|null $contraceptive
 * @property int|null $intercourse_discomfort
 * @property int|null $speak_about_hiv
 * @property int|null $ankles_swelling
 * @property int|null $ankles_pain
 * @property int|null $ankles_stiffness
 * @property int|null $ankles_popping
 * @property int|null $knees_swelling
 * @property int|null $knees_pain
 * @property int|null $knees_stiffness
 * @property int|null $kness_popping
 * @property int|null $hips_swelling
 * @property int|null $hips_pain
 * @property int|null $hips_stiffness
 * @property int|null $hips_popping
 * @property int|null $back_swelling
 * @property int|null $back_pain
 * @property int|null $back_stiffness
 * @property int|null $back_popping
 * @property int|null $others_swelling
 * @property int|null $others_pain
 * @property int|null $others_stiffing
 * @property int|null $others_popping
 * @property string|null $treatment1
 * @property string|null $treatment2
 * @property int|null $join_medications
 * @property string|null $join_medications_details
 * @property int|null $degenerative_changes
 * @property string|null $degenerative_changes_details
 * @property int|null $weight_loss_surgery
 * @property int|null $how_long_overweight
 * @property int|null $diet_pills
 * @property float|null $how_much_weight_loss
 * @property string|null $how_quickly_regain
 * @property float|null $max_weight
 * @property float|null $min_weight
 * @property string|null $how_long_max
 * @property string|null $how_long_min
 * @property string|null $expected_loss
 * @property string|null $expected_time
 * @property string|null $foods_liked
 * @property string|null $foods_disliked
 * @property int|null $snaker
 * @property int|null $volume_eater
 * @property int|null $eats_sweets
 * @property string|null $sweets_how_ofter
 * @property int|null $carbonated_beverages
 * @property int|null $type_lifestyle
 * @property int|null $severe_fatigue
 * @property string|null $severe_fatigue_details
 * @property int|null $severe_weekness
 * @property string|null $severe_weekness_details
 * @property int|null $night_sweats
 * @property string|null $night_sweats_details
 * @property int|null $severe_headaches
 * @property string|null $severe_headaches_details
 * @property int|null $loss_of_conciousness
 * @property string|null $loss_of_conciousness_details
 * @property int|null $hearing_problems
 * @property string|null $hearing_problems_details
 * @property int|null $ear_pain
 * @property string|null $ear_pain_details
 * @property int|null $nasal_congestion
 * @property string|null $nasal_congestion_details
 * @property int|null $sinus_congestion
 * @property string|null $sinus_congestion_details
 * @property int|null $bloody_nose
 * @property string|null $bloody_nose_details
 * @property int|null $dental_problems
 * @property string|null $dental_problems_details
 * @property int|null $dentures
 * @property string|null $dentures_details
 * @property int|null $sores_in_mouth
 * @property string|null $sores_in_mouth_details
 * @property int|null $wheezing
 * @property string|null $wheezing_details
 * @property int|null $coughing
 * @property string|null $coughing_details
 * @property int|null $breast_lump
 * @property string|null $breast_lump_details
 * @property int|null $heart_murmur
 * @property string|null $heart_murmur_details
 * @property int|null $high_blood_pressure2
 * @property string|null $high_blood_pressure2_details
 * @property int|null $chest_pain_excercise
 * @property string|null $chest_pain_excercise_details
 * @property int|null $std
 * @property string|null $std_details
 * @property int|null $anemia
 * @property string|null $anemia_details
 * @property int|null $bleeding_tendency
 * @property string|null $bleeding_tendency_details
 * @property int|null $convulsions
 * @property string|null $convulsions_details
 * @property int|null $paralysis
 * @property string|null $paralysis_details
 * @property int|null $numbness
 * @property string|null $numbness_details
 * @property int|null $memory_loss
 * @property string|null $memory_loss_details
 * @property int|null $depression
 * @property string|null $depression_details
 * @property int|null $anxiety
 * @property string|null $anxiety_details
 * @property int|null $mood_swings
 * @property string|null $mood_swings_details
 * @property int|null $sleep_problems
 * @property string|null $sleep_problems_details
 * @property int|null $drug_alcochol_abuse
 * @property string|null $drug_alcochol_abuse_details
 * @property int|null $rash_hives
 * @property string|null $rash_hives_details
 * @property int|null $asthma2
 * @property string|null $asthma2_details
 * @property int|null $hay_fever
 * @property string|null $hay_fever_details
 * @property string|null $additional_information
 * @property string|null $signature_initials
 * @property \Cake\I18n\FrozenDate|null $signature_date
 * @property int $patient_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Procedure $procedure
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\User $user
 */
class HealthQuestionnaire extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'location' => true,
        'doctor_id' => true,
        'name' => true,
        'address' => true,
        'city' => true,
        'state' => true,
        'zip_code' => true,
        'home_phone' => true,
        'work_phone' => true,
        'cell_phone' => true,
        'date_of_birth' => true,
        'gender' => true,
        'marital_status' => true,
        'age' => true,
        'height' => true,
        'weight' => true,
        'BMI' => true,
        'neck' => true,
        'wrist' => true,
        'waist' => true,
        'hip' => true,
        'thigh' => true,
        'size_shirt' => true,
        'size_pants' => true,
        'procedure_id' => true,
        'suggested_date' => true,
        'details' => true,
        'referring_person' => true,
        'date_of_referral' => true,
        'emergency_name' => true,
        'emergency_phone' => true,
        'primary_health_provider' => true,
        'time_treated' => true,
        'condition_treated' => true,
        'allergic_medication' => true,
        'allergic_medication_list' => true,
        'supplies_allergic' => true,
        'supplies_allergic_list' => true,
        'allergic_food' => true,
        'allergic_food_list' => true,
        'blood_transfusion' => true,
        'transfusion_details' => true,
        'breast_colo_prostate_cancer' => true,
        'cancer_specific' => true,
        'diabetes' => true,
        'heart_attack' => true,
        'stroke' => true,
        'high_blood_pressure' => true,
        'arthritis' => true,
        'obesity' => true,
        'shortness_breath' => true,
        'shortness_breath_long' => true,
        'excercise_regularly' => true,
        'asthma' => true,
        'snore' => true,
        'average_sleep' => true,
        'wakeup_tired' => true,
        'tired_sleepy' => true,
        'cpap' => true,
        'swelling_ankles' => true,
        'decrese_swelling' => true,
        'decrease_pain' => true,
        'thyroid_problems' => true,
        'own_diabetes' => true,
        'medications_for_diabetes' => true,
        'monitor_blood_sugar' => true,
        'monitor_how_ofter' => true,
        'pcos' => true,
        'own_high_blood_pressure' => true,
        'medications_blood_pressure' => true,
        'chest_pain' => true,
        'chest_pain_often' => true,
        'history_heart_disease' => true,
        'explain_heart_dicease' => true,
        'own_heart_attack' => true,
        'fatty_liver' => true,
        'fatty_liver_details' => true,
        'rheumatoid_arthritis' => true,
        'nsaids' => true,
        'lupus' => true,
        'hiv' => true,
        'other_autoinmmune' => true,
        'constipation' => true,
        'diverticulitis' => true,
        'ulcers' => true,
        'crohns_disease' => true,
        'heartburn' => true,
        'heartburn_long' => true,
        'pain_abdomen' => true,
        'pain_details' => true,
        'bowel_movements' => true,
        'bloody_stools' => true,
        'hemorrhoids' => true,
        'eating_disorder' => true,
        'happy_life' => true,
        'history_depression' => true,
        'are_you_depressed' => true,
        'stress_problem' => true,
        'panic_stressed' => true,
        'problems_eating' => true,
        'cry_frecuently' => true,
        'attempted_suicide' => true,
        'thought_hurting_self' => true,
        'trouble_sleeping' => true,
        'been_to_counselor' => true,
        'drink_alcohol' => true,
        'kind_alcohol' => true,
        'drinks_per_week' => true,
        'concerned_drink' => true,
        'considered_stopping' => true,
        'blackouts' => true,
        'frecuency' => true,
        'how_drink_it' => true,
        'mixed_preference' => true,
        'eat_drinking' => true,
        'eat_what' => true,
        'use_tobacco' => true,
        'cigarret_packs_day' => true,
        'chew_day' => true,
        'pipe_day' => true,
        'cigars_day' => true,
        'number_years' => true,
        'years_quit' => true,
        'use_caffeine' => true,
        'caffeine_form' => true,
        'caffeine_how_much' => true,
        'carbonated_quantity' => true,
        'carbonated_brand' => true,
        'sugary_quantity' => true,
        'sugary_brand' => true,
        'natural_quantity' => true,
        'natural_brand' => true,
        'recreational_drugs' => true,
        'drugs_details' => true,
        'drugs_needle' => true,
        'sexually_active' => true,
        'trying_pregnancy' => true,
        'contraceptive' => true,
        'intercourse_discomfort' => true,
        'speak_about_hiv' => true,
        'ankles_swelling' => true,
        'ankles_pain' => true,
        'ankles_stiffness' => true,
        'ankles_popping' => true,
        'knees_swelling' => true,
        'knees_pain' => true,
        'knees_stiffness' => true,
        'kness_popping' => true,
        'hips_swelling' => true,
        'hips_pain' => true,
        'hips_stiffness' => true,
        'hips_popping' => true,
        'back_swelling' => true,
        'back_pain' => true,
        'back_stiffness' => true,
        'back_popping' => true,
        'others_swelling' => true,
        'others_pain' => true,
        'others_stiffing' => true,
        'others_popping' => true,
        'treatment1' => true,
        'treatment2' => true,
        'join_medications' => true,
        'join_medications_details' => true,
        'degenerative_changes' => true,
        'degenerative_changes_details' => true,
        'weight_loss_surgery' => true,
        'how_long_overweight' => true,
        'diet_pills' => true,
        'how_much_weight_loss' => true,
        'how_quickly_regain' => true,
        'max_weight' => true,
        'min_weight' => true,
        'how_long_max' => true,
        'how_long_min' => true,
        'expected_loss' => true,
        'expected_time' => true,
        'foods_liked' => true,
        'foods_disliked' => true,
        'snaker' => true,
        'volume_eater' => true,
        'eats_sweets' => true,
        'sweets_how_ofter' => true,
        'carbonated_beverages' => true,
        'type_lifestyle' => true,
        'severe_fatigue' => true,
        'severe_fatigue_details' => true,
        'severe_weekness' => true,
        'severe_weekness_details' => true,
        'night_sweats' => true,
        'night_sweats_details' => true,
        'severe_headaches' => true,
        'severe_headaches_details' => true,
        'loss_of_conciousness' => true,
        'loss_of_conciousness_details' => true,
        'hearing_problems' => true,
        'hearing_problems_details' => true,
        'ear_pain' => true,
        'ear_pain_details' => true,
        'nasal_congestion' => true,
        'nasal_congestion_details' => true,
        'sinus_congestion' => true,
        'sinus_congestion_details' => true,
        'bloody_nose' => true,
        'bloody_nose_details' => true,
        'dental_problems' => true,
        'dental_problems_details' => true,
        'dentures' => true,
        'dentures_details' => true,
        'sores_in_mouth' => true,
        'sores_in_mouth_details' => true,
        'wheezing' => true,
        'wheezing_details' => true,
        'coughing' => true,
        'coughing_details' => true,
        'breast_lump' => true,
        'breast_lump_details' => true,
        'heart_murmur' => true,
        'heart_murmur_details' => true,
        'high_blood_pressure2' => true,
        'high_blood_pressure2_details' => true,
        'chest_pain_excercise' => true,
        'chest_pain_excercise_details' => true,
        'std' => true,
        'std_details' => true,
        'anemia' => true,
        'anemia_details' => true,
        'bleeding_tendency' => true,
        'bleeding_tendency_details' => true,
        'convulsions' => true,
        'convulsions_details' => true,
        'paralysis' => true,
        'paralysis_details' => true,
        'numbness' => true,
        'numbness_details' => true,
        'memory_loss' => true,
        'memory_loss_details' => true,
        'depression' => true,
        'depression_details' => true,
        'anxiety' => true,
        'anxiety_details' => true,
        'mood_swings' => true,
        'mood_swings_details' => true,
        'sleep_problems' => true,
        'sleep_problems_details' => true,
        'drug_alcochol_abuse' => true,
        'drug_alcochol_abuse_details' => true,
        'rash_hives' => true,
        'rash_hives_details' => true,
        'asthma2' => true,
        'asthma2_details' => true,
        'hay_fever' => true,
        'hay_fever_details' => true,
        'additional_information' => true,
        'signature_initials' => true,
        'signature_date' => true,
        'patient_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'procedure' => true,
        'patient' => true,
        'user' => true
    ];
}
