<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SurgeryChange Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate|null $fecha_original
 * @property \Cake\I18n\FrozenDate|null $nueva_fecha
 * @property string|null $razon
 * @property int|null $user_id
 * @property int|null $patient_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Patient $patient
 */
class SurgeryChange extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fecha_original' => true,
        'nueva_fecha' => true,
        'razon' => true,
        'user_id' => true,
        'patient_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'patient' => true
    ];
}
