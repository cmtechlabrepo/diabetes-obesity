<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MajorIllness Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate|null $date
 * @property string|null $illness
 * @property string|null $treatment
 * @property string|null $outcome
 * @property int|null $patient_id
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\User $user
 */
class MajorIllness extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'date' => true,
        'illness' => true,
        'treatment' => true,
        'outcome' => true,
        'patient_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'user' => true
    ];
}
