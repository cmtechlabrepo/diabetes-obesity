<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FamilyHistory Entity
 *
 * @property int $id
 * @property string|null $family_member
 * @property string|null $age
 * @property string|null $deceased
 * @property string|null $cause_of_death
 * @property int|null $complexion
 * @property string|null $health_problems
 * @property int|null $patient_id
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\User $user
 */
class FamilyHistory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'family_member' => true,
        'age' => true,
        'deceased' => true,
        'cause_of_death' => true,
        'complexion' => true,
        'health_problems' => true,
        'patient_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'user' => true
    ];
}
