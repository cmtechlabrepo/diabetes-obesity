<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AnestesiaEvaluation Entity
 *
 * @property int $id
 * @property int|null $patient_id
 * @property int|null $procedure_id
 * @property int|null $doctor_id
 * @property string|null $diagnosis
 * @property string|null $medical_history
 * @property string|null $tele_torax
 * @property string|null $asa
 * @property string|null $instructions
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenDate|null $date
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\Procedure $procedure
 * @property \App\Model\Entity\Doctor $doctor
 * @property \App\Model\Entity\User $user
 */
class AnestesiaEvaluation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'patient_id' => true,
        'procedure_id' => true,
        'doctor_id' => true,
        'diagnosis' => true,
        'medical_history' => true,
        'tele_torax' => true,
        'asa' => true,
        'instructions' => true,
        'user_id' => true,
        'date' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'procedure' => true,
        'doctor' => true,
        'user' => true
    ];
}
