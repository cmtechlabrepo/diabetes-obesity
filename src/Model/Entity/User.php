<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property int $role_id
 * @property int|null $closed_modal
 * @property int|null $filled_survey
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Role[] $roles
 * @property \App\Model\Entity\AnestesiaEvaluation[] $anestesia_evaluations
 * @property \App\Model\Entity\BoneProblem[] $bone_problems
 * @property \App\Model\Entity\DigestiveProblem[] $digestive_problems
 * @property \App\Model\Entity\Doctor[] $doctors
 * @property \App\Model\Entity\FamilyHistory[] $family_histories
 * @property \App\Model\Entity\HealthQuestionnaire[] $health_questionnaires
 * @property \App\Model\Entity\InfoMedication[] $info_medications
 * @property \App\Model\Entity\InternistaEvaluation[] $internista_evaluations
 * @property \App\Model\Entity\MajorIllness[] $major_illnesses
 * @property \App\Model\Entity\OtherHospitalization[] $other_hospitalizations
 * @property \App\Model\Entity\PatientSurgery[] $patient_surgeries
 * @property \App\Model\Entity\Patient[] $patients
 * @property \App\Model\Entity\PatientsLabTest[] $patients_lab_tests
 * @property \App\Model\Entity\Referral[] $referrals
 * @property \App\Model\Entity\SurgeryChange[] $surgery_changes
 * @property \App\Model\Entity\Survey[] $surveys
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'email' => true,
        'password' => true,
        'role_id' => true,
        'closed_modal' => true,
        'filled_survey' => true,
        'created' => true,
        'modified' => true,
        'roles' => true,
        'anestesia_evaluations' => true,
        'bone_problems' => true,
        'digestive_problems' => true,
        'doctors' => true,
        'family_histories' => true,
        'health_questionnaires' => true,
        'info_medications' => true,
        'internista_evaluations' => true,
        'major_illnesses' => true,
        'other_hospitalizations' => true,
        'patient_surgeries' => true,
        'patients' => true,
        'patients_lab_tests' => true,
        'referrals' => true,
        'surgery_changes' => true,
        'surveys' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($value){
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }
}
