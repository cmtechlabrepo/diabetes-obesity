<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PatientsLabFile Entity
 *
 * @property int $id
 * @property int|null $patient_id
 * @property int|null $doctor_id
 * @property string|null $file
 * @property \Cake\I18n\FrozenDate|null $date
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\Doctor $doctor
 * @property \App\Model\Entity\User $user
 */
class PatientsLabFile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'patient_id' => true,
        'doctor_id' => true,
        'file' => true,
        'date' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'doctor' => true,
        'user' => true
    ];
}
