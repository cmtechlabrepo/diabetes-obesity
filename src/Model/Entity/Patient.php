<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Patient Entity
 *
 * @property int $id
 * @property string|null $location
 * @property int|null $doctor_id
 * @property string|null $name
 * @property string|null $address
 * @property string|null $city
 * @property string|null $state
 * @property string|null $zipcode
 * @property string|null $home_phone
 * @property string|null $work_phone
 * @property string|null $cell_phone
 * @property \Cake\I18n\FrozenDate|null $date_of_birth
 * @property int|null $gender_id
 * @property int|null $marital_status_id
 * @property int|null $age
 * @property string|null $height
 * @property float|null $weight
 * @property float|null $bmi
 * @property float|null $neck
 * @property float|null $wrist
 * @property float|null $waist
 * @property float|null $hip
 * @property float|null $thigh
 * @property string|null $shirt_size
 * @property string|null $pants_size
 * @property int|null $primera_llamada
 * @property string|null $comentarios_llamada
 * @property \Cake\I18n\FrozenDate|null $primer_pago
 * @property int|null $metodo_pago
 * @property \Cake\I18n\FrozenDate|null $fecha_cirugia_autorizada
 * @property int|null $sms_notifications
 * @property int|null $email_notifications
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Doctor $doctor
 * @property \App\Model\Entity\Gender $gender
 * @property \App\Model\Entity\MaritalStatus $marital_status
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\AnestesiaEvaluation[] $anestesia_evaluations
 * @property \App\Model\Entity\Appointment[] $appointments
 * @property \App\Model\Entity\BoneProblem[] $bone_problems
 * @property \App\Model\Entity\DigestiveProblem[] $digestive_problems
 * @property \App\Model\Entity\Email[] $emails
 * @property \App\Model\Entity\FamilyHistory[] $family_histories
 * @property \App\Model\Entity\HealthQuestionnaire[] $health_questionnaires
 * @property \App\Model\Entity\InfoMedication[] $info_medications
 * @property \App\Model\Entity\InternistaEvaluation[] $internista_evaluations
 * @property \App\Model\Entity\MajorIllness[] $major_illnesses
 * @property \App\Model\Entity\NutricionalEvaluation[] $nutricional_evaluations
 * @property \App\Model\Entity\OtherHospitalization[] $other_hospitalizations
 * @property \App\Model\Entity\OtorrinoEvaluation[] $otorrino_evaluations
 * @property \App\Model\Entity\PatientSurgery[] $patient_surgeries
 * @property \App\Model\Entity\PsicologiaEvaluation[] $psicologia_evaluations
 * @property \App\Model\Entity\Referral[] $referrals
 * @property \App\Model\Entity\SurgeryChange[] $surgery_changes
 * @property \App\Model\Entity\Survey[] $surveys
 * @property \App\Model\Entity\LabTest[] $lab_tests
 */
class Patient extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'location' => true,
        'doctor_id' => true,
        'name' => true,
        'address' => true,
        'city' => true,
        'state' => true,
        'zipcode' => true,
        'home_phone' => true,
        'work_phone' => true,
        'cell_phone' => true,
        'date_of_birth' => true,
        'gender_id' => true,
        'marital_status_id' => true,
        'age' => true,
        'height' => true,
        'weight' => true,
        'bmi' => true,
        'neck' => true,
        'wrist' => true,
        'waist' => true,
        'hip' => true,
        'thigh' => true,
        'shirt_size' => true,
        'pants_size' => true,
        'primera_llamada' => true,
        'comentarios_llamada' => true,
        'primer_pago' => true,
        'metodo_pago' => true,
        'fecha_cirugia_autorizada' => true,
        'sms_notifications' => true,
        'email_notifications' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'doctor' => true,
        'gender' => true,
        'marital_status' => true,
        'user' => true,
        'anestesia_evaluations' => true,
        'appointments' => true,
        'bone_problems' => true,
        'digestive_problems' => true,
        'emails' => true,
        'family_histories' => true,
        'health_questionnaires' => true,
        'info_medications' => true,
        'internista_evaluations' => true,
        'major_illnesses' => true,
        'nutricional_evaluations' => true,
        'other_hospitalizations' => true,
        'otorrino_evaluations' => true,
        'patient_surgeries' => true,
        'psicologia_evaluations' => true,
        'referrals' => true,
        'surgery_changes' => true,
        'surveys' => true,
        'lab_tests' => true
    ];
}
