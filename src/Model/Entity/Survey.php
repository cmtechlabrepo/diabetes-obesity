<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Survey Entity
 *
 * @property int $id
 * @property float|null $service_rating
 * @property float|null $guidance_performance
 * @property float|null $medical_procedure_explanation
 * @property float|null $care_before_after
 * @property float|null $nutritional_information
 * @property float|null $transportation_punctuality
 * @property float|null $transportation_cleaning
 * @property float|null $transportation_driving
 * @property string|null $about_hospital
 * @property string|null $about_hotel
 * @property int|null $recommend_us
 * @property string|null $why_recommend
 * @property int|null $give_referrals
 * @property \Cake\I18n\FrozenDate|null $date
 * @property int|null $patient_id
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Referral[] $referrals
 */
class Survey extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'service_rating' => true,
        'guidance_performance' => true,
        'medical_procedure_explanation' => true,
        'care_before_after' => true,
        'nutritional_information' => true,
        'transportation_punctuality' => true,
        'transportation_cleaning' => true,
        'transportation_driving' => true,
        'about_hospital' => true,
        'about_hotel' => true,
        'recommend_us' => true,
        'why_recommend' => true,
        'give_referrals' => true,
        'date' => true,
        'patient_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'user' => true,
        'referrals' => true
    ];
}
