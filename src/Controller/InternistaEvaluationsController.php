<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * InternistaEvaluations Controller
 *
 * @property \App\Model\Table\InternistaEvaluationsTable $InternistaEvaluations
 *
 * @method \App\Model\Entity\InternistaEvaluation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InternistaEvaluationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Doctors', 'Procedures', 'Users']
        ];
        $internistaEvaluations = $this->paginate($this->InternistaEvaluations);

        $this->set(compact('internistaEvaluations'));
    }

    /**
     * View method
     *
     * @param string|null $id Internista Evaluation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $internistaEvaluation = $this->InternistaEvaluations->get($id, [
            'contain' => ['Patients', 'Doctors', 'Procedures', 'Users']
        ]);

        $this->set('internistaEvaluation', $internistaEvaluation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $internistaEvaluation = $this->InternistaEvaluations->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['procedure_date'] = date('Y-m-d',strtotime($this->request->data['procedure_date']));
            $this->request->data['date'] = date('Y-m-d');
            $this->request->data['user_id'] =$this->Auth->user("id");

            $internistaEvaluation = $this->InternistaEvaluations->patchEntity($internistaEvaluation, $this->request->data());
            if ($this->InternistaEvaluations->save($internistaEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Internista Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Internista Evaluation'));
        }
        $patients = $this->InternistaEvaluations->Patients->find('list', ['limit' => 200]);
        $doctors = $this->InternistaEvaluations->Doctors->find('list', ['limit' => 200]);
        $procedures = $this->InternistaEvaluations->Procedures->find('list', ['limit' => 200]);
        $users = $this->InternistaEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('internistaEvaluation', 'patients', 'doctors', 'procedures', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Internista Evaluation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $internistaEvaluation = $this->InternistaEvaluations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->request->data['procedure_date'] = date('Y-m-d',strtotime($this->request->data['procedure_date']));
            
            $internistaEvaluation = $this->InternistaEvaluations->patchEntity($internistaEvaluation, $this->request->data());
            if ($this->InternistaEvaluations->save($internistaEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Internista Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Internista Evaluation'));
        }
        $patients = $this->InternistaEvaluations->Patients->find('list', ['limit' => 200]);
        $doctors = $this->InternistaEvaluations->Doctors->find('list', ['limit' => 200]);
        $procedures = $this->InternistaEvaluations->Procedures->find('list', ['limit' => 200]);
        $users = $this->InternistaEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('internistaEvaluation', 'patients', 'doctors', 'procedures', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Internista Evaluation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $internistaEvaluation = $this->InternistaEvaluations->get($id);
        if ($this->InternistaEvaluations->delete($internistaEvaluation)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Internista Evaluation'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Internista Evaluation'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
