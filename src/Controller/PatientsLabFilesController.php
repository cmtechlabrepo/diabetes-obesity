<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PatientsLabFiles Controller
 *
 * @property \App\Model\Table\PatientsLabFilesTable $PatientsLabFiles
 *
 * @method \App\Model\Entity\PatientsLabFile[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PatientsLabFilesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Doctors', 'Users']
        ];
        $patientsLabFiles = $this->paginate($this->PatientsLabFiles);

        $this->set(compact('patientsLabFiles'));
    }

    /**
     * View method
     *
     * @param string|null $id Patients Lab File id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $patientsLabFile = $this->PatientsLabFiles->get($id, [
            'contain' => ['Patients', 'Doctors', 'Users']
        ]);

        $this->set('patientsLabFile', $patientsLabFile);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $patientsLabFile = $this->PatientsLabFiles->newEntity();
        if ($this->request->is('post')) {
            if($_FILES["file"]["error"] == UPLOAD_ERR_OK){
                //echo "SImon<br>";
                $tmpfile = $_FILES["file"]["tmp_name"];
                $file = date("ymdhis").$_FILES["file"]["name"];

                //echo $tmpfile." - ".$file;

                $ruta = WWW_ROOT . "labtests/" . $file;
                if(move_uploaded_file($tmpfile, $ruta)){
                    //Ya se subio el archivo, hay que guardar el registro
                
                    $this->request->data['file'] = $ruta;
                    $this->request->data['date'] = date("Y-m-d",strtotime($this->request->data['date']));
                    $this->request->data['user_id'] = $this->Auth->user("id");
                }
            }

            $patientsLabFile = $this->PatientsLabFiles->patchEntity($patientsLabFile, $this->request->getData());
            if ($this->PatientsLabFiles->save($patientsLabFile)) {
                $this->Flash->success(__('The {0} has been saved.', 'Patients Lab File'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patients Lab File'));
        }
        $patients = $this->PatientsLabFiles->Patients->find('list', ['limit' => 200]);
        $doctors = $this->PatientsLabFiles->Doctors->find('list', ['limit' => 200]);
        $users = $this->PatientsLabFiles->Users->find('list', ['limit' => 200]);
        $this->set(compact('patientsLabFile', 'patients', 'doctors', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Patients Lab File id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $patientsLabFile = $this->PatientsLabFiles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $patientsLabFile = $this->PatientsLabFiles->patchEntity($patientsLabFile, $this->request->getData());
            if ($this->PatientsLabFiles->save($patientsLabFile)) {
                $this->Flash->success(__('The {0} has been saved.', 'Patients Lab File'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patients Lab File'));
        }
        $patients = $this->PatientsLabFiles->Patients->find('list', ['limit' => 200]);
        $doctors = $this->PatientsLabFiles->Doctors->find('list', ['limit' => 200]);
        $users = $this->PatientsLabFiles->Users->find('list', ['limit' => 200]);
        $this->set(compact('patientsLabFile', 'patients', 'doctors', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Patients Lab File id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $patientsLabFile = $this->PatientsLabFiles->get($id);
        if ($this->PatientsLabFiles->delete($patientsLabFile)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Patients Lab File'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Patients Lab File'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
