<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DoctorTypes Controller
 *
 * @property \App\Model\Table\DoctorTypesTable $DoctorTypes
 *
 * @method \App\Model\Entity\DoctorType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DoctorTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $doctorTypes = $this->paginate($this->DoctorTypes);

        $this->set(compact('doctorTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Doctor Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $doctorType = $this->DoctorTypes->get($id, [
            'contain' => ['Doctors']
        ]);

        $this->set('doctorType', $doctorType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $doctorType = $this->DoctorTypes->newEntity();
        if ($this->request->is('post')) {
            $doctorType = $this->DoctorTypes->patchEntity($doctorType, $this->request->data());
            if ($this->DoctorTypes->save($doctorType)) {
                $this->Flash->success(__('The {0} has been saved.', 'Doctor Type'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Doctor Type'));
        }
        $this->set(compact('doctorType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Doctor Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $doctorType = $this->DoctorTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $doctorType = $this->DoctorTypes->patchEntity($doctorType, $this->request->data());
            if ($this->DoctorTypes->save($doctorType)) {
                $this->Flash->success(__('The {0} has been saved.', 'Doctor Type'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Doctor Type'));
        }
        $this->set(compact('doctorType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Doctor Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $doctorType = $this->DoctorTypes->get($id);
        if ($this->DoctorTypes->delete($doctorType)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Doctor Type'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Doctor Type'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
