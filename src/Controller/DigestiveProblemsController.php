<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DigestiveProblems Controller
 *
 * @property \App\Model\Table\DigestiveProblemsTable $DigestiveProblems
 *
 * @method \App\Model\Entity\DigestiveProblem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DigestiveProblemsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Users']
        ];
        $digestiveProblems = $this->paginate($this->DigestiveProblems);

        $this->set(compact('digestiveProblems'));
    }

    /**
     * View method
     *
     * @param string|null $id Digestive Problem id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $digestiveProblem = $this->DigestiveProblems->get($id, [
            'contain' => ['Patients', 'Users']
        ]);

        $this->set('digestiveProblem', $digestiveProblem);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $digestiveProblem = $this->DigestiveProblems->newEntity();
        if ($this->request->is('post')) {
            $digestiveProblem = $this->DigestiveProblems->patchEntity($digestiveProblem, $this->request->data());
            if ($this->DigestiveProblems->save($digestiveProblem)) {
                $this->Flash->success(__('The {0} has been saved.', 'Digestive Problem'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Digestive Problem'));
        }
        $patients = $this->DigestiveProblems->Patients->find('list', ['limit' => 200]);
        $users = $this->DigestiveProblems->Users->find('list', ['limit' => 200]);
        $this->set(compact('digestiveProblem', 'patients', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Digestive Problem id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $digestiveProblem = $this->DigestiveProblems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $digestiveProblem = $this->DigestiveProblems->patchEntity($digestiveProblem, $this->request->data());
            if ($this->DigestiveProblems->save($digestiveProblem)) {
                $this->Flash->success(__('The {0} has been saved.', 'Digestive Problem'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Digestive Problem'));
        }
        $patients = $this->DigestiveProblems->Patients->find('list', ['limit' => 200]);
        $users = $this->DigestiveProblems->Users->find('list', ['limit' => 200]);
        $this->set(compact('digestiveProblem', 'patients', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Digestive Problem id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $digestiveProblem = $this->DigestiveProblems->get($id);
        if ($this->DigestiveProblems->delete($digestiveProblem)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Digestive Problem'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Digestive Problem'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
