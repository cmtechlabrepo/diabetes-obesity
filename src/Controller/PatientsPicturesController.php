<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PatientsPictures Controller
 *
 * @property \App\Model\Table\PatientsPicturesTable $PatientsPictures
 *
 * @method \App\Model\Entity\PatientsPicture[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PatientsPicturesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients']
        ];
        $patientsPictures = $this->paginate($this->PatientsPictures);

        $this->set(compact('patientsPictures'));
    }

    /**
     * View method
     *
     * @param string|null $id Patients Picture id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $patientsPicture = $this->PatientsPictures->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('patientsPicture', $patientsPicture);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $patientsPicture = $this->PatientsPictures->newEntity();
        if ($this->request->is('post')) {
            $patientsPicture = $this->PatientsPictures->patchEntity($patientsPicture, $this->request->data());
            if ($this->PatientsPictures->save($patientsPicture)) {
                $this->Flash->success(__('The {0} has been saved.', 'Patients Picture'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patients Picture'));
        }
        $patients = $this->PatientsPictures->Patients->find('list', ['limit' => 200]);
        $this->set(compact('patientsPicture', 'patients'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Patients Picture id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $patientsPicture = $this->PatientsPictures->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $patientsPicture = $this->PatientsPictures->patchEntity($patientsPicture, $this->request->data());
            if ($this->PatientsPictures->save($patientsPicture)) {
                $this->Flash->success(__('The {0} has been saved.', 'Patients Picture'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patients Picture'));
        }
        $patients = $this->PatientsPictures->Patients->find('list', ['limit' => 200]);
        $this->set(compact('patientsPicture', 'patients'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Patients Picture id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $patientsPicture = $this->PatientsPictures->get($id);
        if ($this->PatientsPictures->delete($patientsPicture)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Patients Picture'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Patients Picture'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
