<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OtherHospitalizations Controller
 *
 * @property \App\Model\Table\OtherHospitalizationsTable $OtherHospitalizations
 *
 * @method \App\Model\Entity\OtherHospitalization[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OtherHospitalizationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Users']
        ];
        $otherHospitalizations = $this->paginate($this->OtherHospitalizations);

        $this->set(compact('otherHospitalizations'));
    }

    /**
     * View method
     *
     * @param string|null $id Other Hospitalization id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $otherHospitalization = $this->OtherHospitalizations->get($id, [
            'contain' => ['Patients', 'Users']
        ]);

        $this->set('otherHospitalization', $otherHospitalization);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $otherHospitalization = $this->OtherHospitalizations->newEntity();
        if ($this->request->is('post')) {
            $otherHospitalization = $this->OtherHospitalizations->patchEntity($otherHospitalization, $this->request->data());
            if ($this->OtherHospitalizations->save($otherHospitalization)) {
                $this->Flash->success(__('The {0} has been saved.', 'Other Hospitalization'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Other Hospitalization'));
        }
        $patients = $this->OtherHospitalizations->Patients->find('list', ['limit' => 200]);
        $users = $this->OtherHospitalizations->Users->find('list', ['limit' => 200]);
        $this->set(compact('otherHospitalization', 'patients', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Other Hospitalization id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $otherHospitalization = $this->OtherHospitalizations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $otherHospitalization = $this->OtherHospitalizations->patchEntity($otherHospitalization, $this->request->data());
            if ($this->OtherHospitalizations->save($otherHospitalization)) {
                $this->Flash->success(__('The {0} has been saved.', 'Other Hospitalization'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Other Hospitalization'));
        }
        $patients = $this->OtherHospitalizations->Patients->find('list', ['limit' => 200]);
        $users = $this->OtherHospitalizations->Users->find('list', ['limit' => 200]);
        $this->set(compact('otherHospitalization', 'patients', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Other Hospitalization id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $otherHospitalization = $this->OtherHospitalizations->get($id);
        if ($this->OtherHospitalizations->delete($otherHospitalization)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Other Hospitalization'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Other Hospitalization'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
