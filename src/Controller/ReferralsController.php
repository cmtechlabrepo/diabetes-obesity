<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Referrals Controller
 *
 * @property \App\Model\Table\ReferralsTable $Referrals
 *
 * @method \App\Model\Entity\Referral[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReferralsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->Auth->user("role_id") == 12){
            $this->paginate = [
                 'contain' => ['Patients', 'Surveys', 'Users'],
                'conditions'=>[ "Referrals.user_id"=> $this->Auth->user("id")]
            ];
        }else{
            $this->paginate = [
                'contain' => ['Patients', 'Surveys', 'Users']
            ];
        }

        $referrals = $this->paginate($this->Referrals);
        
        //print_r($referrals);
        
        $this->set(compact('referrals'));
        
    }

    /**
     * View method
     *
     * @param string|null $id Referral id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $referral = $this->Referrals->get($id, [
            'contain' => ['Patients', 'Surveys', 'Users']
        ]);

        $this->set('referral', $referral);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $referral = $this->Referrals->newEntity();
        if ($this->request->is('post')) {
            $this->Emails = TableRegistry::get('Emails');
            $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "email"=> $this->Auth->user("email")]]);

            $emailsData = $EmailsQuery->toArray();

            $patient_id = $emailsData[0]['patient_id'];

            // if(in_array($this->request->data["give_referrals"],array(1,2))){
                $referrals = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"email")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $referrals[$key]['email'] = $v;
                        $referrals[$key]['user_id'] = $this->Auth->user("id");
                        $referrals[$key]['date'] = date("Y-m-d");
                        //$referrals[$key]['survey_id'] = $result->id;
                        $referrals[$key]['patient_id'] = $patient_id;
                    }
                    if(strpos($k,"type")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $referrals[$key]['type'] = $v;
                    }                
                }

                //Sacar entities vacios
                $emptyreferrals = array();
                foreach($referrals as $key => $value){
                    if($value['email'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyreferrals,$key);
                    }
                }
                
                foreach($emptyreferrals as $v){
                    //Quitamos los vacios                    
                    unset($referrals[$v]);
                }

                //Save referrals
                $this->Referrals = TableRegistry::get('Referrals');
                $referralEntities = $this->Referrals->newEntities($referrals);
                $resultreferrals = $this->Referrals->saveMany($referralEntities);
            // }

            if (count($resultreferrals) > 0) {
                $this->Flash->success(__('The {0} have been saved.', 'Referrals'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Referrals'));
        }

        $this->set(compact('referral'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Referral id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $referral = $this->Referrals->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $referral = $this->Referrals->patchEntity($referral, $this->request->data());
            if ($this->Referrals->save($referral)) {
                $this->Flash->success(__('The {0} has been saved.', 'Referral'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Referral'));
        }
        $patients = $this->Referrals->Patients->find('list', ['limit' => 200]);
        $surveys = $this->Referrals->Surveys->find('list', ['limit' => 200]);
        $users = $this->Referrals->Users->find('list', ['limit' => 200]);
        $this->set(compact('referral', 'patients', 'surveys', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Referral id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $referral = $this->Referrals->get($id);
        if ($this->Referrals->delete($referral)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Referral'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Referral'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
