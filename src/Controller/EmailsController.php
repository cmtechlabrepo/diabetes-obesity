<?php
namespace App\Controller;

use Cake\Mailer\Email;
use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Emails Controller
 *
 * @property \App\Model\Table\EmailsTable $Emails
 *
 * @method \App\Model\Entity\Email[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmailsController extends AppController
{

    public function beforeFilter(Event $event)
    {
       // allow all action
        $this->Auth->allow(['add','thanks']);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients']
        ];
        $emails = $this->paginate($this->Emails);

        $this->set(compact('emails'));
    }

    /**
     * View method
     *
     * @param string|null $id Email id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $email = $this->Emails->get($id, [
            'contain' => ['Patients']
        ]);

        $this->set('email', $email);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $email = $this->Emails->newEntity();
        if ($this->request->is('post')) {
            $email = $this->Emails->patchEntity($email, $this->request->data());
            $savedEmail = $this->Emails->save($email);
            if ($savedEmail->id){
                
                //$this->Flash->success(__('The {0} has been saved.', 'Email'));
                
                $message = "Welcome and thank you for registering your email at DiabetesObesityClinic.com!<br><br>";
                $message .= "You can now fill up a questionnaire to let us know how can we help you, to do so, please click the following link: ";
                $message .= "<a href='https://sandbox.cmtechlab.com/medfile/health-questionnaires/addfromemail/".$savedEmail->id."'>Click here</a>";
                $message .= "<br><br>";
                $message .= "Thanks.<br>";
                $message .= "DiabetesObesityClinic.com";

                $email = new Email('default');
                $email->from(['info@diabetesobesityclinic.com' => 'Diabetes Obesity Clinic'])
                    ->emailFormat('html')
                    //->to('carlosmontoya.it@gmail.com')
                    ->to($savedEmail->email)
                    ->subject('Diabetes Obesity Clinic: Thanks for registering!')
                    ->send($message);

                return $this->redirect(['action' => 'thanks']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Email'));
        }
        $patients = $this->Emails->Patients->find('list', ['limit' => 200]);
        $this->set(compact('email', 'patients'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Email id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $email = $this->Emails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $email = $this->Emails->patchEntity($email, $this->request->data());
            if ($this->Emails->save($email)) {
                $this->Flash->success(__('The {0} has been saved.', 'Email'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Email'));
        }
        $patients = $this->Emails->Patients->find('list', ['limit' => 200]);
        $this->set(compact('email', 'patients'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Email id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $email = $this->Emails->get($id);
        if ($this->Emails->delete($email)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Email'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Email'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function thanks(){

    }
}
