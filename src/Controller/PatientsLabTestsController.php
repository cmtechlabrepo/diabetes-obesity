<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * PatientsLabTests Controller
 *
 * @property \App\Model\Table\PatientsLabTestsTable $PatientsLabTests
 *
 * @method \App\Model\Entity\PatientsLabTest[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PatientsLabTestsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // $this->paginate = [
        //     'contain' => ['Patients', 'LabTests']
        // ];
        $patientsLabTests = $this->PatientsLabTests;
        
        $query = $this->PatientsLabTests->find();
        $query->select()
        ->distinct(['patient_id, date'])
        ->contain('Patients', 'LabTests');

        $patientsLabTests = $query->toArray();

        //print_r($patientsLabTests);

        $this->set(compact('patientsLabTests'));
    }

    /**
     * View method
     *
     * @param string|null $id Patients Lab Test id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $patientsLabTest = $this->PatientsLabTests->get($id, [
            'contain' => ['Patients', 'LabTests']
        ]);

        $this->set('patientsLabTest', $patientsLabTest);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $patientsLabTest = $this->PatientsLabTests->newEntity();
        if ($this->request->is('post')) {
            $patientsLabTest = $this->PatientsLabTests->patchEntity($patientsLabTest, $this->request->data());
            if ($this->PatientsLabTests->save($patientsLabTest)) {
                $this->Flash->success(__('The {0} has been saved.', 'Patients Lab Test'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patients Lab Test'));
        }
        $doctors = $this->PatientsLabTests->Doctors->find('list', ['limit' => 200]);
        $patients = $this->PatientsLabTests->Patients->find('list', ['limit' => 200]);
        $labTests = $this->PatientsLabTests->LabTests->find('list', ['limit' => 200]);
        $this->set(compact('patientsLabTest', 'patients', 'labTests','doctors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Patients Lab Test id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $patientsLabTest = $this->PatientsLabTests->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $patientsLabTest = $this->PatientsLabTests->patchEntity($patientsLabTest, $this->request->data());
            if ($this->PatientsLabTests->save($patientsLabTest)) {
                $this->Flash->success(__('The {0} has been saved.', 'Patients Lab Test'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patients Lab Test'));
        }
        $patients = $this->PatientsLabTests->Patients->find('list', ['limit' => 200]);
        $labTests = $this->PatientsLabTests->LabTests->find('list', ['limit' => 200]);
        $this->set(compact('patientsLabTest', 'patients', 'labTests'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Patients Lab Test id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null,$date=null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //$patientsLabTest = $this->PatientsLabTests->get($id);
        if ($this->PatientsLabTests->deleteAll(['patient_id' => $id,'date'=>$date])) {
            $this->Flash->success(__('The {0} has been deleted.', 'Patients Lab Test'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Patients Lab Test'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function addforpatient(){

        $patientsLabTest = $this->PatientsLabTests->newEntity();

        $this->LabTests = TableRegistry::get('LabTests');
        $LabTestsQuery = $this->LabTests->find('list',[
            'keyField' => 'id',
            'valueField' => 'estudio'
            ]);

        $LabTestsData = $LabTestsQuery->toArray();

        $patients = $this->PatientsLabTests->Patients->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ]);

        $doctors = $this->PatientsLabTests->Doctors->find('list', ['limit' => 200]);

        $this->set(compact("LabTestsData",'patients','doctors'));

        if($this->request->is("post")){
            
            // print_r($this->request->data);

            $patient = $this->request->data['patient_id'];
            $doctor = $this->request->data['doctor_id'];
            $user =  $this->Auth->user("id");

            unset($this->request->data['patient_id']);
            unset($this->request->data['doctor_id']);

            $toSave = array();
            $c = 0;
            foreach($this->request->data as $idlab => $value){                
                $toSave[$c]['user_id'] = $user;
                $toSave[$c]['date'] =  date("Y-m-d");
                $toSave[$c]['doctor_id'] =  $doctor;
                $toSave[$c]['patient_id'] =  $patient;
                $toSave[$c]['lab_test_id'] =  $idlab;
                $toSave[$c]['value'] =  $value;
                $toSave[$c]['user_id'] =  $this->Auth->user("id");;
                $c++;          
            }

            $labEntities = $this->PatientsLabTests->newEntities($toSave);
            if($this->PatientsLabTests->saveMany($labEntities)){
                $this->Flash->success(__('The {0} has been saved.', 'Patients Lab Test'));

                return $this->redirect(['action' => 'index']);
            }
        }        
    }

    public function editlabs($patient = null,$date=null){

        $this->LabTests = TableRegistry::get('LabTests');
        $LabTestsQuery = $this->LabTests->find('list',[
            'keyField' => 'id',
            'valueField' => 'estudio'
            ]);

        $LabTestsData = $LabTestsQuery->toArray();

        $LabValuesQuery = $this->PatientsLabTests->find('all',[
            'conditions'=>[
                'patient_id'=>$patient,
                'date'=>$date
        ]]);

        $LabValuesData = $LabValuesQuery->toArray();
        
        $values = array();
        $c = 0;

        foreach($LabValuesData as $v){
            $values[$v['lab_test_id']] = $v['value'];            
        }

        $this->set(compact('LabTestsData','patient','date','values'));

        if($this->request->is('post')){
            $patient = $this->request->data['patient'];
            $date = $this->request->data['date'];

            unset($this->request->data['patient']);
            unset($this->request->data['date']);

            foreach($this->request->data as $idlab => $value){ 
                $query = $this->PatientsLabTests->find('all', [
                    'conditions'=>[
                        'patient_id'=>intval($patient),
                        'date'=>$date,
                        'lab_test_id'=>intval($idlab)
                ]]);
                $row = $query->first();

                if($row != null){
                    $row->value = $value;
                }

                if($this->PatientsLabTests->save($row)){
                    $this->Flash->success(__('The {0} has been saved.', 'Patients Lab Test'));

                    return $this->redirect(['action' => 'index']);
                }
            }
        }
    }

    /**
     * View method
     *
     * @param string|null $id Patients Lab Test id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewlabtest($patientid = null,$date=null){

        $this->Patients = TableRegistry::get('Patients');

        $patient = $this->Patients->get($patientid);

        $this->LabTests = TableRegistry::get('LabTests');
        $LabTestsQuery = $this->LabTests->find('all');

        $LabTestsData = $LabTestsQuery->toArray();

        $LabValuesQuery = $this->PatientsLabTests->find('all',[
            'conditions'=>[
                'patient_id'=>$patientid,
                'date'=>$date
        ]]);

        $LabValuesData = $LabValuesQuery->toArray();
        
        $values = array();
        $c = 0;

        foreach($LabValuesData as $v){
            $values[$v['lab_test_id']] = $v['value'];            
        }

        $this->set(compact('values','LabTestsData','patient'));
    }


}
