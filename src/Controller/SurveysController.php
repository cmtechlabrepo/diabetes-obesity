<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Surveys Controller
 *
 * @property \App\Model\Table\SurveysTable $Surveys
 *
 * @method \App\Model\Entity\Survey[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SurveysController extends AppController
{

    public function isAuthorized($user)
    {
        $role = $user['role_id'];

        $canAdd = array(12);
        $canView = array(1,2);
        $canEdit = array(1,2);
        $canDelete = array(1);
        $action = $this->request->getParam('action');

        if (in_array($action, ['index'])) {
            return true;
        }
        
        if ($action === 'addwithsmiley' && in_array($role,$canAdd)) {
            return true;
        }

        if ($action === 'edit' && in_array($role,$canEdit)) {
            return true;
        }

        if ($action === 'view' && in_array($role,$canView)) {
            return true;
        }

        if ($action === 'delete' && in_array($role,$canDelete)) {
            return true;
        }       
        
        if ($action === 'clickedmodal' && in_array($role,$canAdd)) {
            return true;
        }   

        return false;
        //return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Users']
        ];
        $surveys = $this->paginate($this->Surveys);

        $this->set(compact('surveys'));
    }

    /**
     * View method
     *
     * @param string|null $id Survey id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $survey = $this->Surveys->get($id, [
            'contain' => ['Patients', 'Users', 'Referrals']
        ]);

        $this->set('survey', $survey);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // if($this->Auth->user("role_id") != 12){
        //     return $this->redirect(['action' => 'index']);
        // }
        $survey = $this->Surveys->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['user_id'] = $this->Auth->user("id");
            $this->request->data['date'] = date("Y-m-d");
            $survey = $this->Surveys->patchEntity($survey, $this->request->data());

            $this->Users = TableRegistry::get('Users');
            $user = $this->Users->get($this->Auth->user("id"));

            //Reescribir la sesion para que no muestre el modal
            $this->request->session()->write('current', $user);

            ///Actualizar campo closed_modal y filled_survey
            $user->closed_modal = 1;
            $user->filled_survey = 1;
            $this->Users->save($user);

            $result = $this->Surveys->save($survey);

            if ($result->id > 0) {
                //get referrals
                if(in_array($this->request->data["give_referrals"],array(1,2))){
                    // echo "<br><br>Si entro";
                    $referrals = array();
                    foreach($this->request->data as $k => $v){
                        if(strpos($k,"email")>-1){
                            $key = preg_replace('/[^0-9]/', '', $k);
                            $referrals[$key]['email'] = $v;
                            $referrals[$key]['user_id'] = $this->Auth->user("id");
                            $referrals[$key]['date'] = date("Y-m-d");
                            $referrals[$key]['survey_id'] = $result->id;
                            //$referrals[$key]['patient_id'] = $resultPatients->id;
                        }
                        if(strpos($k,"type")>-1){
                            $key = preg_replace('/[^0-9]/', '', $k);
                            $referrals[$key]['type'] = $v;
                        }                
                    }

                    //Sacar entities vacios
                    $emptyreferrals = array();
                    foreach($referrals as $key => $value){
                        if($value['email'] == ""){
                                //Esta vacio, agregamos el key a un arreglo
                                array_push($emptyreferrals,$key);
                        }
                    }
                    
                    foreach($emptyreferrals as $v){
                        //Quitamos los vacios                    
                        unset($referrals[$v]);
                    }
                    
                    // echo "<br><br>";
                    // print_r($referrals);

                    //Save referrals
                    $this->Referrals = TableRegistry::get('Referrals');
                    $referralEntities = $this->Referrals->newEntities($referrals);
                    $resultreferrals = $this->Referrals->saveMany($referralEntities);

                    // echo "<br><br>";
                    // print_r($resultreferrals);
                }
            
                $this->Flash->success(__('The {0} has been saved.', 'Survey'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Survey'));
        }
        $this->set(compact('survey', 'patients', 'users'));
    }

    public function addwithsmiley(){
        $survey = $this->Surveys->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['user_id'] = $this->Auth->user("id");
            $this->request->data['date'] = date("Y-m-d");
            $survey = $this->Surveys->patchEntity($survey, $this->request->data());

            $this->Users = TableRegistry::get('Users');
            $user = $this->Users->get($this->Auth->user("id"));

            //Reescribir la sesion para que no muestre el modal
            $this->request->session()->write('current', $user);

            ///Actualizar campo closed_modal y filled_survey
            $user->closed_modal = 1;
            $user->filled_survey = 1;
            $this->Users->save($user);

            if ($this->Surveys->save($survey)) {            
            
                $this->Flash->success(__('The {0} has been saved.', 'Survey'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Survey'));
        }
        $patients = $this->Surveys->Patients->find('list', ['limit' => 200]);
        $users = $this->Surveys->Users->find('list', ['limit' => 200]);
        $this->set(compact('survey', 'patients', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Survey id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $survey = $this->Surveys->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $survey = $this->Surveys->patchEntity($survey, $this->request->data());
            if ($this->Surveys->save($survey)) {
                $this->Flash->success(__('The {0} has been saved.', 'Survey'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Survey'));
        }
        $patients = $this->Surveys->Patients->find('list', ['limit' => 200]);
        $users = $this->Surveys->Users->find('list', ['limit' => 200]);
        $this->set(compact('survey', 'patients', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Survey id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $survey = $this->Surveys->get($id);
        if ($this->Surveys->delete($survey)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Survey'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Survey'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function clickedmodal(){
        $this->Users = TableRegistry::get('Users');
        
        $user = $this->Users->get($this->Auth->user("id"));

        ///Actualizar campo closed_modal
        $user->closed_modal = 1;

        //Reescribir la sesion para que no muestre el modal
        $this->request->session()->write('current', $user);

        //guardar y regresar una respuesta al ajax
        header('Content-Type: application/json');
        echo ($this->Users->save($user)) ? json_encode(array('changed' => 1 )) : json_encode(array('changed' => 0 ));
        exit;
    }
}
