<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FollowUps Controller
 *
 * @property \App\Model\Table\FollowUpsTable $FollowUps
 *
 * @method \App\Model\Entity\FollowUp[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowUpsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Doctors']
        ];
        $followUps = $this->paginate($this->FollowUps);

        $this->set(compact('followUps'));
    }

    /**
     * View method
     *
     * @param string|null $id Follow Up id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $followUp = $this->FollowUps->get($id, [
            'contain' => ['Patients', 'Doctors']
        ]);

        $this->set('followUp', $followUp);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $followUp = $this->FollowUps->newEntity();
        if ($this->request->is('post')) {
            
            $this->request->data['date'] = date("Y-m-d");
            $this->request->data['user_id'] = $this->Auth->user("id");

            $followUp = $this->FollowUps->patchEntity($followUp, $this->request->getData());

            if ($this->FollowUps->save($followUp)) {
                $this->Flash->success(__('The {0} has been saved.', 'Follow Up'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Follow Up'));
        }
        $patients = $this->FollowUps->Patients->find('list', ['limit' => 200]);
        $doctors = $this->FollowUps->Doctors->find('list', ['limit' => 200]);
        $this->set(compact('followUp', 'patients', 'doctors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Follow Up id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $followUp = $this->FollowUps->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $followUp = $this->FollowUps->patchEntity($followUp, $this->request->getData());
            if ($this->FollowUps->save($followUp)) {
                $this->Flash->success(__('The {0} has been saved.', 'Follow Up'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Follow Up'));
        }
        $patients = $this->FollowUps->Patients->find('list', ['limit' => 200]);
        $doctors = $this->FollowUps->Doctors->find('list', ['limit' => 200]);
        $this->set(compact('followUp', 'patients', 'doctors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Follow Up id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $followUp = $this->FollowUps->get($id);
        if ($this->FollowUps->delete($followUp)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Follow Up'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Follow Up'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
