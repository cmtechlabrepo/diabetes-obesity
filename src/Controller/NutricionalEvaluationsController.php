<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NutricionalEvaluations Controller
 *
 * @property \App\Model\Table\NutricionalEvaluationsTable $NutricionalEvaluations
 *
 * @method \App\Model\Entity\NutricionalEvaluation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NutricionalEvaluationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Doctors', 'Procedures', 'Users']
        ];
        $nutricionalEvaluations = $this->paginate($this->NutricionalEvaluations);

        $this->set(compact('nutricionalEvaluations'));
    }

    /**
     * View method
     *
     * @param string|null $id Nutricional Evaluation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nutricionalEvaluation = $this->NutricionalEvaluations->get($id, [
            'contain' => ['Patients', 'Doctors', 'Procedures', 'Users']
        ]);

        $this->set('nutricionalEvaluation', $nutricionalEvaluation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $nutricionalEvaluation = $this->NutricionalEvaluations->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['date'] = date('Y-m-d');
            $this->request->data['user_id'] =$this->Auth->user("id");

            $nutricionalEvaluation = $this->NutricionalEvaluations->patchEntity($nutricionalEvaluation, $this->request->data());
            if ($this->NutricionalEvaluations->save($nutricionalEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Nutricional Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Nutricional Evaluation'));
        }
        $patients = $this->NutricionalEvaluations->Patients->find('list', ['limit' => 200]);
        $doctors = $this->NutricionalEvaluations->Doctors->find('list', ['limit' => 200]);
        $procedures = $this->NutricionalEvaluations->Procedures->find('list', ['limit' => 200]);
        $users = $this->NutricionalEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('nutricionalEvaluation', 'patients', 'doctors', 'procedures', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Nutricional Evaluation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $nutricionalEvaluation = $this->NutricionalEvaluations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nutricionalEvaluation = $this->NutricionalEvaluations->patchEntity($nutricionalEvaluation, $this->request->data());
            if ($this->NutricionalEvaluations->save($nutricionalEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Nutricional Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Nutricional Evaluation'));
        }
        $patients = $this->NutricionalEvaluations->Patients->find('list', ['limit' => 200]);
        $doctors = $this->NutricionalEvaluations->Doctors->find('list', ['limit' => 200]);
        $procedures = $this->NutricionalEvaluations->Procedures->find('list', ['limit' => 200]);
        $users = $this->NutricionalEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('nutricionalEvaluation', 'patients', 'doctors', 'procedures', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Nutricional Evaluation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $nutricionalEvaluation = $this->NutricionalEvaluations->get($id);
        if ($this->NutricionalEvaluations->delete($nutricionalEvaluation)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Nutricional Evaluation'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Nutricional Evaluation'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
