<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * InfoMedications Controller
 *
 * @property \App\Model\Table\InfoMedicationsTable $InfoMedications
 *
 * @method \App\Model\Entity\InfoMedication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InfoMedicationsController extends AppController
{

    public function isAuthorized($user)
    {
        return true;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $infoMedications = $this->paginate($this->InfoMedications);

        $this->set(compact('infoMedications'));
    }

    /**
     * View method
     *
     * @param string|null $id Info Medication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $infoMedication = $this->InfoMedications->get($id, [
            'contain' => []
        ]);

        $this->set('infoMedication', $infoMedication);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $infoMedication = $this->InfoMedications->newEntity();
        if ($this->request->is('post')) {
            $infoMedication = $this->InfoMedications->patchEntity($infoMedication, $this->request->data());
            if ($this->InfoMedications->save($infoMedication)) {
                $this->Flash->success(__('The {0} has been saved.', 'Info Medication'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Info Medication'));
        }
        $this->set(compact('infoMedication'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Info Medication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $infoMedication = $this->InfoMedications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $infoMedication = $this->InfoMedications->patchEntity($infoMedication, $this->request->data());
            if ($this->InfoMedications->save($infoMedication)) {
                $this->Flash->success(__('The {0} has been saved.', 'Info Medication'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Info Medication'));
        }
        $this->set(compact('infoMedication'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Info Medication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $infoMedication = $this->InfoMedications->get($id);
        if ($this->InfoMedications->delete($infoMedication)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Info Medication'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Info Medication'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
