<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Patients Controller
 *
 * @property \App\Model\Table\PatientsTable $Patients
 *
 * @method \App\Model\Entity\Patient[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PatientsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        if($this->Auth->user("role_id") == 12){
            
            $this->Emails = TableRegistry::get('Emails');
            $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "email"=> $this->Auth->user("email")]]);

            $emailsData = $EmailsQuery->toArray();

            $this->paginate = [
                'conditions'=>[ "Patients.id"=> $emailsData[0]['patient_id']],
            ];

        }else{

            $this->paginate = [
                'contain' => ['Genders', 'MaritalStatuses', 'Users','HealthQuestionnaires','Doctors']
            ]; 
        }

        $patients = $this->paginate($this->Patients);

        $this->set(compact('patients'));
        if($this->request->is('post')){
            //print_r($this->request->data);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Patient id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $patient = $this->Patients->get($id, [
            'contain' => ['Genders', 'MaritalStatuses', 'Users']
        ]);

        $this->set('patient', $patient);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $patient = $this->Patients->newEntity();
        if ($this->request->is('post')) {
            
            $emailAddress = $this->request->data["email"];

            unset($this->request->data["email"]);

            $this->request->data["date_of_birth"] = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $this->request->data['user_id'] = $this->Auth->user("id");

            $patient = $this->Patients->patchEntity($patient, $this->request->data());

            $savedPatient = $this->Patients->save($patient);

            if ($savedPatient->id > 0) {

                $newEmail = array();

                $newEmail["email"] = $emailAddress;
                $newEmail["patient_id"] = $savedPatient->id;         
                
                $this->Emails = TableRegistry::get('Emails');
                $email = $this->Emails->newEntity();
                $email = $this->Emails->patchEntity($email, $newEmail);

                $this->Emails->save($email);

                $this->Flash->success(__('The {0} has been saved.', 'Patient'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patient'));
        }
        $genders = $this->Patients->Genders->find('list', ['limit' => 200]);
        $maritalStatuses = $this->Patients->MaritalStatuses->find('list', ['limit' => 200]);
        $users = $this->Patients->Users->find('list', ['limit' => 200]);
        $doctors = $this->Patients->Doctors->find('list', [
            'conditions'=>['doctor_type_id'=>1],
            'limit' => 200]);
        $this->set(compact('patient', 'genders', 'maritalStatuses', 'users','doctors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Patient id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $patient = $this->Patients->get($id, [
            'contain' => []
        ]);

        $this->Emails = TableRegistry::get('Emails');

        $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "patient_id"=> $id]]);

        $emailsData = $EmailsQuery->toArray();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data["date_of_birth"] = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $this->request->data['user_id'] = $this->Auth->user("id");

            $emailID = $this->request->data['email_id'];
            $oldEmail = $this->request->data['old_email'];
            $newEmail = $this->request->data['email'];

            unset($this->request->data['email_id']);
            unset($this->request->data['old_email']);
            unset($this->request->data['email']);

            if($newEmail != "" && $newEmail != $oldEmail){

                $this->Emails = TableRegistry::get('Emails');

                $emailRecord = $this->Emails->get($emailID);

                $emailRecord->email = $newEmail;

                $this->Emails->save($emailRecord);
            }

            $patient = $this->Patients->patchEntity($patient, $this->request->data());
            if ($this->Patients->save($patient)) {
                $this->Flash->success(__('The {0} has been saved.', 'Patient'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patient'));
        }
        $genders = $this->Patients->Genders->find('list', ['limit' => 200]);
        $maritalStatuses = $this->Patients->MaritalStatuses->find('list', ['limit' => 200]);
        $users = $this->Patients->Users->find('list', ['limit' => 200]);
        $doctors = $this->Patients->Doctors->find('list', [
            'conditions'=>['doctor_type_id'=>1],
            'limit' => 200]);
        $this->set(compact('patient', 'genders', 'maritalStatuses', 'users','doctors','emailsData'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Patient id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $patient = $this->Patients->get($id);
        if ($this->Patients->delete($patient)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Patient'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Patient'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function saveAdmin(){
        $patient_id = intval($this->request->data['patientId']);
        $pago = ($this->request->data['pago']) != "" ? date("Y-m-d",strtotime($this->request->data['fecha'])) : null;
        $metodo_pago = intval($this->request->data['metodo_pago']);
        $llamada = ($this->request->data['llamada']) == "1" ? 1 : 0;
        $fecha = ($this->request->data['fecha']) != "" ? date("Y-m-d",strtotime($this->request->data['fecha'])) : null;
        $comentarios = $this->request->data['comentarios'];

        ///echo "La fecha: ".$fecha;

        $patient = $this->Patients->get($patient_id, [
            'contain' => []
        ]);        
        
        $patient->primera_llamada = $llamada;
        $patient->comentarios_llamada = $comentarios;
        $patient->primer_pago = $pago;
        $patient->metodo_pago = $metodo_pago;

        // echo gettype($llamada);echo "<br>";
        // echo gettype($comentarios);echo "<br>";
        // echo gettype($pago);echo "<br>";
        // echo gettype($metodo_pago);echo "<br>";
        // echo "<br>";echo "<br>";echo "<br>";
        // echo gettype($patient->primera_llamada);echo "<br>";
        // echo gettype($patient->comentarios_llamada);echo "<br>";
        // echo gettype($patient->primer_pago);echo "<br>";
        // echo gettype($patient->metodo_pago);echo "<br>";
        //echo $pago.' - '.$llamada;
        //Revisar si ya hizo primer pago y llamada para crear un usuario y pw (Enviar correo a admin y al usuario)
        if($pago != null && $llamada == 1){
            //$patient = $this->Patients->get($patient_id);

            $this->Emails = TableRegistry::get('Emails');
            $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "patient_id"=> $patient_id]]);

            $EmailsData = $EmailsQuery->toArray();

            //print_r($EmailsData);

            $this->Users = TableRegistry::get('Users');
            $userQuery = $this->Users->find('all',['conditions'=>[ "email"=> $EmailsData[0]["email"]]]);

            $userData = $userQuery->toArray();
            
            //echo $userData[0]["email"];

            if(!isset($userData[0]["email"])){
                //echo "Entro";
                $password = $this->getPassword();

                $newUser = array();

                $newUser["name"] = $patient->name;
                $newUser["email"] = $EmailsData[0]['email'];
                $newUser["password"] = $password;
                $newUser["role_id"] = 12;                
                
                //print_r($EmailsData[0]["email"]);

                $user = $this->Users->newEntity();
                $user = $this->Users->patchEntity($user, $newUser);
                if ($this->Users->save($user)) {
                    //echo "se guardo user";
                    $message = "Thank you for continuing your journey with DiabetesObesityClinic.com!<br><br>";
                    $message .= "You can now login to the system here: ";
                    $message .= "<a href='https://sandbox.cmtechlab.com/medfile/users/login/'>Click here to go to the site</a>";
                    $message .= "<br><br>";
                    $message .= "Your login information for the site is:<br>";
                    $message .= "Username: ".$EmailsData[0]["email"]."<br>";
                    $message .= "Password: ".$password."<br>";
                    $message .= "<br><br>";
                    $message .= "Thanks.";
                    $message .= "DiabetesObesityClinic.com";

                    // $email = new Email('default');
                    // $email->from(['info@diabetesobesityclinic.com' => 'Diabetes Obesity Clinic'])
                    //     ->emailFormat('html')
                    //     //->to('carlosmontoya.it@gmail.com')
                    //     ->to($EmailsData[0]['email'])
                    //     ->subject('Diabetes Obesity Clinic: Your site credentials')
                    //     ->send($message);
                }
                
            }
            if($fecha != null){
                //guardar fecha de Cirugia                
                $patient->fecha_cirugia_autorizada = $fecha;
                $lastDate = $fecha;
                //Generar citas automaticamente
                $citas = array();
                for($x=0;$x<15;$x++){
                    $citas[$x]['date'] = ($x<4) ? date('Y-m-d', strtotime($lastDate. ' +7 days')) : date('Y-m-d', strtotime($lastDate. ' +30 days'));
                    $lastDate = $citas[$x]['date'];
                    $citas[$x]['time'] = date('H:i:s',strtotime("08:00"));
                    $citas[$x]['user_id'] = $this->Auth->user("id");
                    $citas[$x]['comment'] = "Follow up number ".($x+1);
                    $citas[$x]['patient_id'] = $patient_id;
                    $citas[$x]['doctor_id'] = 1;
                }
                //Save Appointments
                $this->Appointments = TableRegistry::get('Appointments');
                $AppointmentsEntities = $this->Appointments->newEntities($citas);
                $resultAppointments = $this->Appointments->saveMany($AppointmentsEntities);
    
                //Revisar si la fecha de la cirugia es dentro de 8 dias?
                $plu8days = date('Y-m-d', strtotime('+8 days'));
                if($fecha <= $plu8days){
                    //echo "Si esta dentro de los 8 dias";
                    //Enviar correo recordatorio.
                    $message = "Your surgery date is approaching!<br><br>";
                    $message .= "Remember to follow your diet.... ";
                    // $message .= "<a href='https://sandbox.cmtechlab.com/medfile/users/login/'>Click here to go to the site</a>";
                    // $message .= "<br><br>";
                    // $message .= "Your login information for the site is:<br>";
                    // $message .= "Username:".$EmailsData[0]["email"]." <br>";
                    // $message .= "Password:".$password." <br>";
                    // $message .= "<br><br>";
                    // $message .= "Thanks.";
                    // $message .= "DiabetesObesityClinic.com";

                    // $email = new Email('default');
                    // $email->from(['info@diabetesobesityclinic.com' => 'Diabetes Obesity Clinic'])
                    //     ->emailFormat('html')
                    //     //->to('carlosmontoya.it@gmail.com')
                    //     ->to($EmailsData[0]['email'])
                    //     ->subject('Diabetes Obesity Clinic: Your surgery date is approaching!')
                    //     ->send($message);
                }
            }

        }
        //regresar una respuesta al ajax
        header('Content-Type: application/json');
        echo ($this->Patients->save($patient)) ? json_encode(array('saved' => 1 )) : json_encode(array('saved' => 0 ));
        exit;
    }

    public function getPassword() { 
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
      
        for ($i = 0; $i < 10; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
      
        return $randomString; 
    }

    public function getPatient(){
        $patient_id = $this->request->data['patientId'];
        $patient = $this->Patients->get($patient_id, [
            //'fields'=>'primera_llamada,comentarios_llamada,primer_pago,fecha_cirugia_autorizada',
            'contain' => []
        ]);
        //regresar una respuesta al ajax
        header('Content-Type: application/json');
        echo json_encode($patient);
        exit;
    }


    public function changeDate(){
        $patient_id = $this->request->data['patientId'];
        $fecha_original = ($this->request->data['fecha_original']) != "" ? date("Y-m-d",strtotime($this->request->data['fecha_original'])) : null;
        $fecha_nueva = ($this->request->data['fecha_nueva']) != "" ? date("Y-m-d",strtotime($this->request->data['fecha_nueva'])) : null;
        

        $patient = $this->Patients->get($patient_id, [
            'contain' => []
        ]);        
        
        //Cambiar Fecha original en tabla Patients
        $patient->fecha_cirugia_autorizada = $fecha_nueva;
        $this->Patients->save($patient);

        //Borrar citas automaticas y generarlas nuevamente en appointments
        $this->Appointments = TableRegistry::get('Appointments');
        $this->Appointments->deleteAll(['patient_id' => $patient_id]);

        $lastDate = $fecha_nueva;
        //Generar citas automaticamente
        $citas = array();
        for($x=0;$x<15;$x++){
            $citas[$x]['date'] = ($x<4) ? date('Y-m-d', strtotime($lastDate. ' +7 days')) : date('Y-m-d', strtotime($lastDate. ' +30 days'));
            $lastDate = $citas[$x]['date'];
            $citas[$x]['time'] = date('H:i:s',strtotime("08:00"));
            $citas[$x]['user_id'] = $this->Auth->user("id");
            $citas[$x]['comment'] = "Follow up number ".($x+1);
            $citas[$x]['patient_id'] = $patient_id;
            $citas[$x]['doctor_id'] = 1;
        }
        //Save Appointments
        
        $AppointmentsEntities = $this->Appointments->newEntities($citas);
        $resultAppointments = $this->Appointments->saveMany($AppointmentsEntities);

        //guardar registro del cambio en tabla SurgeryChanges
        $surgeryChanges = array();

        $surgeryChanges['fecha_original'] = $fecha_original;
        $surgeryChanges['nueva_fecha'] = $fecha_nueva;
        $surgeryChanges['patient_id'] = $patient_id;
        $surgeryChanges['user_id'] = $this->Auth->user("id");

        $this->SurgeryChanges = TableRegistry::get('SurgeryChanges');
        $change = $this->SurgeryChanges->newEntity();
        $change = $this->SurgeryChanges->patchEntity($change, $surgeryChanges);
        
        //regresar una respuesta al ajax
        header('Content-Type: application/json');
        echo ($this->SurgeryChanges->save($change)) ? json_encode(array('changed' => 1 )) : json_encode(array('changed' => 0 ));
        exit;
    }

    public function dashboard(){
        $this->Emails = TableRegistry::get('Emails');
        $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "email"=> $this->Auth->user("email")]]);

        $emailsData = $EmailsQuery->toArray();

        $patient_id = $emailsData[0]['patient_id'];

        $this->PatientsPictures = TableRegistry::get('PatientsPictures');

        $pictureQuery = $this->PatientsPictures->find('all',['conditions'=>[ 
            "patient_id"=> $patient_id,
            "active"=>1
            ]]);

        $pictureData = $pictureQuery->first();

        if($pictureData != null){
            //Si el paciente ya ha subido fotos
            $picture = str_replace("/home1/chtechla/cmtechlab.com/sandbox/medfile","",$pictureData->path);
        }else{
            //Si no tiene fotos ponemos un placeholder
            $picture = "/img/user.png";
        }

        $this->FollowUps = TableRegistry::get('FollowUps');

        $followupsQuery = $this->FollowUps->find('all',['conditions'=>[ 
            "patient_id"=> $patient_id],
            "order"=>"created asc"
        ]);

        $followupsData = $followupsQuery->toArray();

        $patient = $this->Patients->get($patient_id, [
            'contain' => []
        ]);

        $this->set(compact('patient','picture',"followupsData"));    
    }

    public function notificationstoggle(){
        $notification_type = $this->request->data['notification_type'];
        $status = $this->request->data['status'];

        $this->Emails = TableRegistry::get('Emails');
        $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "email"=> $this->Auth->user("email")]]);

        $emailsData = $EmailsQuery->toArray();

        $patient_id = $emailsData[0]['patient_id'];

        $patient = $this->Patients->get($patient_id, [
            'contain' => []
        ]);
    
        if($notification_type == 'sms'){
            $patient->sms_notifications = $status;
        }else{
            $patient->email_notifications = $status;
        }

        //regresar una respuesta al ajax
        header('Content-Type: application/json');
        echo ($this->Patients->save($patient)) ? json_encode(array('saved' => 1 )) : json_encode(array('saved' => 0 ));
        exit;
    }

    public function uploadphoto(){
        if ($_FILES["photo"]["error"] == UPLOAD_ERR_OK)
        {
            $tmpfile = $_FILES["photo"]["tmp_name"];
            $file = date("ymdhis").$_FILES["photo"]["name"];

            $ruta = WWW_ROOT . "uploads/" . $file;
            
            if(move_uploaded_file($tmpfile, $ruta)){
                $this->Emails = TableRegistry::get('Emails');
                $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "email"=> $this->Auth->user("email")]]);
                $emailsData = $EmailsQuery->toArray();

                $patient_id = $emailsData[0]['patient_id'];

                $this->PatientsPictures = TableRegistry::get('PatientsPictures');
                $pictureQuery = $this->PatientsPictures->find('all',['conditions'=>[ 
                    "patient_id"=> $patient_id,
                    "Active" => 1
                    ]]);
        
                $pictureData = $pictureQuery->first();

                if($pictureData != null){
                    $pictureData->active = 0;
                    $this->PatientsPictures->save($pictureData);
                }

                $newPicture = array();
                $newPicture["path"] = $ruta;
                $newPicture["patient_id"] = $patient_id;
                $newPicture["active"] = 1;
                $picture = $this->PatientsPictures->newEntity();
                $picture = $this->PatientsPictures->patchEntity($picture, $newPicture);

                header('Content-Type: application/json');
                echo ($this->PatientsPictures->save($picture)) ? json_encode(array('saved' => 1 )) : json_encode(array('saved' => 0 ));
                exit;
            }
        }
    }

    public function getcheckups(){

        $this->Emails = TableRegistry::get('Emails');
        $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "email"=> $this->Auth->user("email")]]);

        $emailsData = $EmailsQuery->toArray();

        $patient_id = $emailsData[0]['patient_id'];

        $this->FollowUps = TableRegistry::get('FollowUps');

        $followupsQuery = $this->FollowUps->find('all',[
            'conditions'=>[ "patient_id"=> $patient_id],
            "order"=>"created asc"
        ]);

        $followupsData = $followupsQuery->toArray();

        $checkups =  array();

        $counter = 0;
        foreach($followupsData as $f){
            $checkups[$counter]['y'] = date("Y-m-d",strtotime($f['created']));
            $checkups[$counter]['Weight'] = $f['weight'];
            $counter++;
            //$checkups[date("Y/m/d",strtotime($f['created']))] =  $f['weight'];
            //$checkups[$counter]['Weight'] = $f['weight'];
        }


        header('Content-Type: application/json');
        echo json_encode($checkups);
        exit;
    }
}
