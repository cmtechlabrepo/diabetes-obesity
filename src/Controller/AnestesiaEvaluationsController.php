<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AnestesiaEvaluations Controller
 *
 * @property \App\Model\Table\AnestesiaEvaluationsTable $AnestesiaEvaluations
 *
 * @method \App\Model\Entity\AnestesiaEvaluation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AnestesiaEvaluationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Procedures', 'Doctors', 'Users']
        ];
        $anestesiaEvaluations = $this->paginate($this->AnestesiaEvaluations);

        $this->set(compact('anestesiaEvaluations'));
    }

    /**
     * View method
     *
     * @param string|null $id Anestesia Evaluation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $anestesiaEvaluation = $this->AnestesiaEvaluations->get($id, [
            'contain' => ['Patients', 'Procedures', 'Doctors', 'Users']
        ]);

        $this->set('anestesiaEvaluation', $anestesiaEvaluation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $anestesiaEvaluation = $this->AnestesiaEvaluations->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user("id");
            $this->request->data['date'] = date("Y-m-d");
            $anestesiaEvaluation = $this->AnestesiaEvaluations->patchEntity($anestesiaEvaluation, $this->request->data());
            if ($this->AnestesiaEvaluations->save($anestesiaEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Anestesia Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Anestesia Evaluation'));
        }
        $patients = $this->AnestesiaEvaluations->Patients->find('list', ['limit' => 200]);
        $procedures = $this->AnestesiaEvaluations->Procedures->find('list', ['limit' => 200]);
        $doctors = $this->AnestesiaEvaluations->Doctors->find('list', ['limit' => 200]);
        $users = $this->AnestesiaEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('anestesiaEvaluation', 'patients', 'procedures', 'doctors', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Anestesia Evaluation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $anestesiaEvaluation = $this->AnestesiaEvaluations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anestesiaEvaluation = $this->AnestesiaEvaluations->patchEntity($anestesiaEvaluation, $this->request->data());
            if ($this->AnestesiaEvaluations->save($anestesiaEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Anestesia Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Anestesia Evaluation'));
        }
        $patients = $this->AnestesiaEvaluations->Patients->find('list', ['limit' => 200]);
        $procedures = $this->AnestesiaEvaluations->Procedures->find('list', ['limit' => 200]);
        $doctors = $this->AnestesiaEvaluations->Doctors->find('list', ['limit' => 200]);
        $users = $this->AnestesiaEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('anestesiaEvaluation', 'patients', 'procedures', 'doctors', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Anestesia Evaluation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $anestesiaEvaluation = $this->AnestesiaEvaluations->get($id);
        if ($this->AnestesiaEvaluations->delete($anestesiaEvaluation)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Anestesia Evaluation'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Anestesia Evaluation'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
