<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OtorrinoEvaluations Controller
 *
 * @property \App\Model\Table\OtorrinoEvaluationsTable $OtorrinoEvaluations
 *
 * @method \App\Model\Entity\OtorrinoEvaluation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OtorrinoEvaluationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Doctors', 'Procedures', 'Users']
        ];
        $otorrinoEvaluations = $this->paginate($this->OtorrinoEvaluations);

        $this->set(compact('otorrinoEvaluations'));
    }

    /**
     * View method
     *
     * @param string|null $id Otorrino Evaluation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $otorrinoEvaluation = $this->OtorrinoEvaluations->get($id, [
            'contain' => ['Patients', 'Doctors', 'Procedures', 'Users']
        ]);

        $this->set('otorrinoEvaluation', $otorrinoEvaluation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $otorrinoEvaluation = $this->OtorrinoEvaluations->newEntity();
        
        if ($this->request->is('post')) {

            $this->request->data['date'] = date('Y-m-d');
            $this->request->data['user_id'] =$this->Auth->user("id");

            $otorrinoEvaluation = $this->OtorrinoEvaluations->patchEntity($otorrinoEvaluation, $this->request->data());
            if ($this->OtorrinoEvaluations->save($otorrinoEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Otorrino Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Otorrino Evaluation'));
        }
        $patients = $this->OtorrinoEvaluations->Patients->find('list', ['limit' => 200]);
        $doctors = $this->OtorrinoEvaluations->Doctors->find('list', ['limit' => 200]);
        $procedures = $this->OtorrinoEvaluations->Procedures->find('list', ['limit' => 200]);
        $users = $this->OtorrinoEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('otorrinoEvaluation', 'patients', 'doctors', 'procedures', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Otorrino Evaluation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $otorrinoEvaluation = $this->OtorrinoEvaluations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $otorrinoEvaluation = $this->OtorrinoEvaluations->patchEntity($otorrinoEvaluation, $this->request->data());
            if ($this->OtorrinoEvaluations->save($otorrinoEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Otorrino Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Otorrino Evaluation'));
        }
        $patients = $this->OtorrinoEvaluations->Patients->find('list', ['limit' => 200]);
        $doctors = $this->OtorrinoEvaluations->Doctors->find('list', ['limit' => 200]);
        $procedures = $this->OtorrinoEvaluations->Procedures->find('list', ['limit' => 200]);
        $users = $this->OtorrinoEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('otorrinoEvaluation', 'patients', 'doctors', 'procedures', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Otorrino Evaluation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $otorrinoEvaluation = $this->OtorrinoEvaluations->get($id);
        if ($this->OtorrinoEvaluations->delete($otorrinoEvaluation)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Otorrino Evaluation'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Otorrino Evaluation'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
