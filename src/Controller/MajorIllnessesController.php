<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MajorIllnesses Controller
 *
 * @property \App\Model\Table\MajorIllnessesTable $MajorIllnesses
 *
 * @method \App\Model\Entity\MajorIllness[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MajorIllnessesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Users']
        ];
        $majorIllnesses = $this->paginate($this->MajorIllnesses);

        $this->set(compact('majorIllnesses'));
    }

    /**
     * View method
     *
     * @param string|null $id Major Illness id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $majorIllness = $this->MajorIllnesses->get($id, [
            'contain' => ['Patients', 'Users']
        ]);

        $this->set('majorIllness', $majorIllness);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $majorIllness = $this->MajorIllnesses->newEntity();
        if ($this->request->is('post')) {
            $majorIllness = $this->MajorIllnesses->patchEntity($majorIllness, $this->request->data());
            if ($this->MajorIllnesses->save($majorIllness)) {
                $this->Flash->success(__('The {0} has been saved.', 'Major Illness'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Major Illness'));
        }
        $patients = $this->MajorIllnesses->Patients->find('list', ['limit' => 200]);
        $users = $this->MajorIllnesses->Users->find('list', ['limit' => 200]);
        $this->set(compact('majorIllness', 'patients', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Major Illness id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $majorIllness = $this->MajorIllnesses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $majorIllness = $this->MajorIllnesses->patchEntity($majorIllness, $this->request->data());
            if ($this->MajorIllnesses->save($majorIllness)) {
                $this->Flash->success(__('The {0} has been saved.', 'Major Illness'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Major Illness'));
        }
        $patients = $this->MajorIllnesses->Patients->find('list', ['limit' => 200]);
        $users = $this->MajorIllnesses->Users->find('list', ['limit' => 200]);
        $this->set(compact('majorIllness', 'patients', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Major Illness id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $majorIllness = $this->MajorIllnesses->get($id);
        if ($this->MajorIllnesses->delete($majorIllness)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Major Illness'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Major Illness'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
