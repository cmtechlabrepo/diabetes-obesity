<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LabTests Controller
 *
 * @property \App\Model\Table\LabTestsTable $LabTests
 *
 * @method \App\Model\Entity\LabTest[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LabTestsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $labTests = $this->paginate($this->LabTests);

        $this->set(compact('labTests'));
    }

    /**
     * View method
     *
     * @param string|null $id Lab Test id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $labTest = $this->LabTests->get($id, [
            'contain' => []
        ]);

        $this->set('labTest', $labTest);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $labTest = $this->LabTests->newEntity();
        if ($this->request->is('post')) {
            $labTest = $this->LabTests->patchEntity($labTest, $this->request->data());
            if ($this->LabTests->save($labTest)) {
                $this->Flash->success(__('The {0} has been saved.', 'Lab Test'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Lab Test'));
        }
        $this->set(compact('labTest'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Lab Test id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $labTest = $this->LabTests->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $labTest = $this->LabTests->patchEntity($labTest, $this->request->data());
            if ($this->LabTests->save($labTest)) {
                $this->Flash->success(__('The {0} has been saved.', 'Lab Test'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Lab Test'));
        }
        $this->set(compact('labTest'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Lab Test id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $labTest = $this->LabTests->get($id);
        if ($this->LabTests->delete($labTest)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Lab Test'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Lab Test'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
