<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * HealthQuestionnaires Controller
 *
 * @property \App\Model\Table\HealthQuestionnairesTable $HealthQuestionnaires
 *
 * @method \App\Model\Entity\HealthQuestionnaire[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HealthQuestionnairesController extends AppController
{
    public function beforeFilter(Event $event)
    {
       // allow all action
        $this->Auth->allow(['addfromemail','thanks']);
    }

    public function isAuthorized($user)
    {
        $role = $user['role_id'];

        $canAdd = array(1,2,3,4,5,6,7,8,12);
        $canView = array(1,2,3,4,5,6,7,8,12);
        $canEdit = array(1,2,3,4,5,6,7,8);
        $canDelete = array(1);
        $action = $this->request->getParam('action');

        if (in_array($action, ['index'])) {
            return true;
        }
        
        if ($action === 'add' && in_array($role,$canAdd)) {
            return true;
        }

        if ($action === 'edit' && in_array($role,$canEdit)) {
            return true;
        }

        if ($action === 'view' && in_array($role,$canView)) {
            return true;
        }

        if ($action === 'delete' && in_array($role,$canDelete)) {
            return true;
        }        

        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->Auth->user("role_id") == 12){
            $this->Emails = TableRegistry::get('Emails');
            $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "email"=> $this->Auth->user("email")]]);

            $emailsData = $EmailsQuery->toArray();

            $this->paginate = [
                'conditions'=>[ "HealthQuestionnaires.patient_id"=> $emailsData[0]['patient_id']],
                'contain' => ['Procedures','Doctors']
            ];
        }else{
            $this->paginate = [
                // 'conditions'=>[ "HealthQuestionnaires.user_id"=> $this->Auth->user("id")],
                'contain' => ['Procedures','Doctors']
            ];
        }
        $healthQuestionnaires = $this->paginate($this->HealthQuestionnaires);

        $this->set(compact('healthQuestionnaires'));
    }

    /**
     * View method
     *
     * @param string|null $id Health Questionnaire id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $healthQuestionnaire = $this->HealthQuestionnaires->get($id, [
            'contain' => ['Procedures']
        ]);

        $this->set('healthQuestionnaire', $healthQuestionnaire);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $healthQuestionnaire = $this->HealthQuestionnaires->newEntity();

        if ($this->request->is('post')) {
            
            $patient = array();

            $patient["location"] = $this->request->data["location"];
            $patient["doctor_id"] = $this->request->data["doctor_id"];
            $patient["name"] = $this->request->data["name"];
            $patient["address"] = $this->request->data["address"];
            $patient["city"] = $this->request->data["city"];
            $patient["state"] = $this->request->data["state"];
            $patient["zipcode"] = $this->request->data["zip_code"];
            $patient["home_phone"] = $this->request->data["home_phone"];
            $patient["work_phone"] = $this->request->data["work_phone"];
            $patient["cell_phone"] = $this->request->data["cell_phone"];
            $patient["date_of_birth"] = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $patient["gender_id"] = $this->request->data["gender"];
            $patient["marital_status_id"] = $this->request->data["marital_status"];
            $patient["age"] = $this->request->data["age"];
            $patient["height"] = $this->request->data["height"];
            $patient["weight"] = $this->request->data["weight"];
            $patient["bmi"] = $this->request->data["BMI"];
            $patient["neck"] = $this->request->data["neck"];
            $patient["wrist"] = $this->request->data["wrist"];
            $patient["waist"] = $this->request->data["waist"];
            $patient["hip"] = $this->request->data["hip"];
            $patient["thigh"] = $this->request->data["thigh"];
            $patient["shirt_size"] = $this->request->data["size_shirt"];
            $patient["pants_size"] = $this->request->data["size_pants"];
            $patient['user_id'] = $this->Auth->user("id");

            $this->Patients = TableRegistry::get('Patients');
            $patientEntity = $this->Patients->newEntity();
            $patientEntity = $this->Patients->patchEntity($patientEntity, $patient);

            if ($patientEntity->errors()) {
                print_r($patientEntity->errors());
            }

            $resultPatients = $this->Patients->save($patientEntity);

            $emailAddress = $this->request->data["email"];
            unset($this->request->data["email"]);

            if($resultPatients->id > 0 && $emailAddress != ""){
                //Salvar Email
                $newEmail = array();
                $newEmail["email"] = $emailAddress;
                $newEmail["patient_id"] = $resultPatients->id;         
                
                $this->Emails = TableRegistry::get('Emails');
                $email = $this->Emails->newEntity();
                $email = $this->Emails->patchEntity($email, $newEmail);

                $this->Emails->save($email);
            }

            //get medications
            if(!isset($this->request->data["no-medication"])){
                $medications = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"medicationname")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['name'] = $v;
                        $medications[$key]['user_id'] = $this->Auth->user("id");
                        $medications[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"dose")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['dose'] = $v;
                    }
                    if(strpos($k,"oftentaken")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['often_taken'] = $v;
                    }
                    if(strpos($k,"purpose")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['purpose'] = $v;
                    }
                    if(strpos($k,"medication_brand")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['brand'] = $v;
                    }
                    if(strpos($k,"started")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['started_use'] = date('Y-m-d', strtotime($v));
                    }                    
                }

                //Sacar entities vacios
                $emptyMedications = array();
                foreach($medications as $key => $value){
                    if($value['name'] == "" && $value['dose'] == "" && 
                        $value['often_taken'] == "" && $value['purpose'] == "" &&
                        $value['brand'] == "" && $value['started_use'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyMedications,$key);
                    }
                }
                
                foreach($emptyMedications as $v){
                    //Quitamos los vacios                    
                    unset($medications[$v]);
                }

                //Save Medications
                $this->InfoMedications = TableRegistry::get('InfoMedications');
                $MedicationEntities = $this->InfoMedications->newEntities($medications);
                $resultMedications = $this->InfoMedications->saveMany($MedicationEntities);
            }

            if(!isset($this->request->data["no-ilness"])){
                //get illnesses
                $ilnesses = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"dateillness")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['date'] =  date('Y-m-d', strtotime($v));
                        $ilnesses[$key]['user_id'] = $this->Auth->user("id");
                        $ilnesses[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"illness")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['illness'] = $v;
                    }
                    if(strpos($k,"treatment")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['treatment'] = $v;
                    }
                    if(strpos($k,"outcome")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['outcome'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyilnesses = array();
                foreach($ilnesses as $key => $value){
                    if($value['date'] == "" && $value['illness'] == "" && 
                        $value['treatment'] == "" && $value['outcome'] == "" ){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyilnesses,$key);
                    }
                }
                
                foreach($emptyilnesses as $v){
                    //Quitamos los vacios                    
                    unset($ilnesses[$v]);
                }

                //Save illnesses
                $this->MajorIllnesses = TableRegistry::get('MajorIllnesses');
                $MajorIllnessesEntities = $this->MajorIllnesses->newEntities($ilnesses);
                $resultMajorIllnesses = $this->MajorIllnesses->saveMany($MajorIllnessesEntities);
            }

            if(!isset($this->request->data["no-surgery"])){
                //get Surgeries
                $surgeries = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"surgeryname")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['surgery'] = $v;
                        $surgeries[$key]['user_id'] = $this->Auth->user("id");
                        $surgeries[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"datesurgery")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['date'] =  date('Y-m-d', strtotime($v));                    
                    }                
                    if(strpos($k,"reason")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['reason'] = $v;
                    }
                    if(strpos($k,"hospital")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['hospital'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptysurgeries = array();
                foreach($surgeries as $key => $value){
                    if($value['surgery'] == "" && $value['date'] == "" && 
                        $value['reason'] == "" && $value['hospital'] == "" ){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptysurgeries,$key);
                    }
                }
                
                foreach($emptysurgeries as $v){
                    //Quitamos los vacios                    
                    unset($surgeries[$v]);
                }

                //Save Surgeries
                $this->PatientSurgeries = TableRegistry::get('PatientSurgeries');
                $surgeriesEntities = $this->PatientSurgeries->newEntities($surgeries);
                $resultSurgeries = $this->PatientSurgeries->saveMany($surgeriesEntities);
            }

            if(!isset($this->request->data["no-hospitalizations"])){
                //get Hospitalizations
                $hospitalizations = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"datehospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['date'] =  date('Y-m-d', strtotime($v));
                        $hospitalizations[$key]['user_id'] = $this->Auth->user("id");
                        $hospitalizations[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"reasonhospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['reason'] = $v;
                    }
                    if(strpos($k,"hospitalhospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['hospital'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyhospitalizations = array();
                foreach($hospitalizations as $key => $value){
                    if($value['date'] == "" && $value['reason'] == "" && $value['hospital'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyhospitalizations,$key);
                    }
                }
                
                foreach($emptyhospitalizations as $v){
                    //Quitamos los vacios                    
                    unset($hospitalizations[$v]);
                }

                //Save Hospitalizations
                $this->OtherHospitalizations = TableRegistry::get('OtherHospitalizations');
                $hospitalizationsEntities = $this->OtherHospitalizations->newEntities($hospitalizations);
                $resulthospitalizations = $this->OtherHospitalizations->saveMany($hospitalizationsEntities);
            }

            if(!isset($this->request->data["no-family"])){
                //get Family History
                $famHistory = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"family_member")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['family_member'] =  $v;
                        $famHistory[$key]['user_id'] = $this->Auth->user("id");
                        $famHistory[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"familyage")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['age'] = $v;
                    }
                    if(strpos($k,"deceased")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['deceased'] = $v;
                    }
                    if(strpos($k,"cause_of_death")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['cause_of_death'] = $v;
                    }
                    if(strpos($k,"familycomplexion")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['complexion'] = $v;
                    }
                    if(strpos($k,"familyhealth")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['health_problems'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyfamHistory = array();
                foreach($famHistory as $key => $value){
                    if($value['family_member'] == "" && $value['age'] == "" && $value['deceased'] == "" &&
                    $value['cause_of_death'] == "" && $value['complexion'] == "" && $value['health_problems'] == ""
                    ){
                        //Esta vacio, agregamos el key a un arreglo
                        array_push($emptyfamHistory,$key);
                    }
                }
                
                foreach($emptyfamHistory as $v){
                    //Quitamos los vacios                    
                    unset($famHistory[$v]);
                }

                //Save Family History
                $this->FamilyHistories = TableRegistry::get('FamilyHistories');
                $historyEntities = $this->FamilyHistories->newEntities($famHistory);
                $resultHistory = $this->FamilyHistories->saveMany($historyEntities);
            }

            if(!isset($this->request->data["no-digestive"])){
                //get Digestive Problems
                $digestiveProblems = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"digestivefood")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $digestiveProblems[$key]['food_drink'] = $v;
                        $digestiveProblems[$key]['user_id'] = $this->Auth->user("id");
                        $digestiveProblems[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"digestiveresult")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $digestiveProblems[$key]['result_of_eating'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptydigestiveProblems = array();
                foreach($digestiveProblems as $key => $value){
                    if($value['food_drink'] == "" && $value['result_of_eating'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptydigestiveProblems,$key);
                    }
                }
                
                foreach($emptydigestiveProblems as $v){
                    //Quitamos los vacios                    
                    unset($digestiveProblems[$v]);
                }

                //Save Digestive Problems
                $this->DigestiveProblems = TableRegistry::get('DigestiveProblems');
                $digestiveProblemsEntities = $this->DigestiveProblems->newEntities($digestiveProblems);
                $resultDigestiveProblems = $this->DigestiveProblems->saveMany($digestiveProblemsEntities);
            }

            if(!isset($this->request->data["no-bone"])){
                //get Bone Problems
                $boneProblems = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"bonedoctor")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['doctor'] =  $v;
                        $boneProblems[$key]['user_id'] = $this->Auth->user("id");
                        $boneProblems[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"bonedate")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['date_treatment'] = date('Y-m-d', strtotime($v));
                    }
                    if(strpos($k,"bonediagnosis")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['diagnosis'] = $v;
                    }
                }
            
                //Sacar entities vacios
                $emptyboneProblems = array();
                foreach($boneProblems as $key => $value){
                    if($value['doctor'] == "" && $value['date_treatment'] == "" && $value['diagnosis']){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyboneProblems,$key);
                    }
                }
                
                foreach($emptyboneProblems as $v){
                    //Quitamos los vacios                    
                    unset($boneProblems[$v]);
                }

                //Save Bone Problems
                $this->BoneProblems = TableRegistry::get('BoneProblems');
                $boneProblemsEntities = $this->BoneProblems->newEntities($boneProblems);
                $resultBoneProblems = $this->BoneProblems->saveMany($boneProblemsEntities);
            }

            //Poner fijos Doctor y Ubicacion
            $this->request->data["doctor_id"] = 1;
            $this->request->data["location"] = "Tijuana";
            //Save Questionnaire
            $this->request->data["date_of_birth"] = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $this->request->data["suggested_date"] = date('Y-m-d', strtotime($this->request->data["suggested_date"]));
            $this->request->data["date_of_referral"] = date('Y-m-d', strtotime($this->request->data["date_of_referral"]));
            $this->request->data["signature_date"] = date('Y-m-d', strtotime($this->request->data["signature_date"]));
            $this->request->data['user_id'] = $this->Auth->user("id");
            $this->request->data['patient_id'] = $resultPatients->id;

        $healthQuestionnaire = $this->HealthQuestionnaires->patchEntity($healthQuestionnaire, $this->request->data());
            
            if ($healthQuestionnaire->errors()) {
                    echo "Errores Questionario: ";
                print_r($healthQuestionnaire->errors());
            }
            
            $savedQuestionnaire = $this->HealthQuestionnaires->save($healthQuestionnaire);
            if ($savedQuestionnaire->id > 0) {
                //Si se salvan los datos de cuestionario
                $questionnaireId = $savedQuestionnaire->id;                
                
                 $this->Flash->success(__('The {0} has been saved.', 'Health Questionnaire'));

                 return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Health Questionnaire'));
        }

        $doctors = $this->HealthQuestionnaires->Doctors->find('list', [
            'conditions'=>['doctor_type_id'=>1],
            'limit' => 200]);
        $procedures = $this->HealthQuestionnaires->Procedures->find('list', ['limit' => 200]);
        $this->set(compact('healthQuestionnaire', 'procedures','doctors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Health Questionnaire id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $healthQuestionnaire = $this->HealthQuestionnaires->get($id, [
            'contain' => []
        ]);
            
        $this->Patients = TableRegistry::get('Patients');
        $patient = $this->Patients->get($healthQuestionnaire['patient_id'], [
            'contain' => []
        ]);

        $this->Emails = TableRegistry::get('Emails');
        $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "patient_id"=> $patient->id]]);

        $emailsData = $EmailsQuery->toArray();

        $this->InfoMedications = TableRegistry::get('InfoMedications');
        $medicationsQuery = $this->InfoMedications->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $medicationsData = $medicationsQuery->toArray();

        $this->MajorIllnesses = TableRegistry::get('MajorIllnesses');
        $ilnessesQuery = $this->MajorIllnesses->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $ilnessesData = $ilnessesQuery->toArray();

        $this->PatientSurgeries = TableRegistry::get('PatientSurgeries');
        $patientSurgeriesQuery = $this->PatientSurgeries->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $patientSurgeriesData = $patientSurgeriesQuery->toArray();

        $this->OtherHospitalizations = TableRegistry::get('OtherHospitalizations');
        $hospitalizationsQuery = $this->OtherHospitalizations->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $hospitalizationsData = $hospitalizationsQuery->toArray();

        $this->FamilyHistories = TableRegistry::get('FamilyHistories');
        $famHistoryQuery = $this->FamilyHistories->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $famHistoryData = $famHistoryQuery->toArray();

        $this->DigestiveProblems = TableRegistry::get('DigestiveProblems');
        $digestiveProblemsQuery = $this->DigestiveProblems->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $digestiveProblemsData = $digestiveProblemsQuery->toArray();

        $this->BoneProblems = TableRegistry::get('BoneProblems');
        $boneProblemsQuery = $this->BoneProblems->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $boneProblemsData = $boneProblemsQuery->toArray();

        $this->set(compact("emailsData","patient","medicationsData","ilnessesData","patientSurgeriesData","hospitalizationsData","famHistoryData","digestiveProblemsData","boneProblemsData"));

        if ($this->request->is(['patch', 'post', 'put'])) {

            $emailID = $this->request->data['email_id'];
            $oldEmail = $this->request->data['old_email'];
            $newEmail = $this->request->data['email'];

            unset($this->request->data['email_id']);
            unset($this->request->data['old_email']);
            unset($this->request->data['email']);

            if($newEmail != "" && $newEmail != $oldEmail){

                $this->Emails = TableRegistry::get('Emails');

                $emailRecord = $this->Emails->get($emailID);

                $emailRecord->email = $newEmail;

                $this->Emails->save($emailRecord);
            }

            $patient_id = $this->request->data['patient_id'];

            $this->Patients = TableRegistry::get('Patients');
            
            $patient = $this->Patients->get($patient_id);

            $patient->location = $this->request->data["location"];
            $patient->doctor_id = $this->request->data["doctor_id"];
            $patient->name = $this->request->data["name"];
            $patient->address = $this->request->data["address"];
            $patient->city = $this->request->data["city"];
            $patient->state = $this->request->data["state"];
            $patient->zipcode = $this->request->data["zip_code"];
            $patient->home_phone = $this->request->data["home_phone"];
            $patient->work_phone = $this->request->data["work_phone"];
            $patient->cell_phone = $this->request->data["cell_phone"];
            $patient->date_of_birth = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $patient->gender_id = $this->request->data["gender"];
            $patient->marital_status_id = $this->request->data["marital_status"];
            $patient->age = $this->request->data["age"];
            $patient->height = $this->request->data["height"];
            $patient->weight = $this->request->data["weight"];
            $patient->bmi = $this->request->data["BMI"];
            $patient->neck = $this->request->data["neck"];
            $patient->wrist = $this->request->data["wrist"];
            $patient->waist = $this->request->data["waist"];
            $patient->hip = $this->request->data["hip"];
            $patient->thigh = $this->request->data["thigh"];
            $patient->shirt_size = $this->request->data["size_shirt"];
            $patient->pants_size = $this->request->data["size_pants"];
            $patient->user_id = $this->Auth->user("id");

            $resultPatients = $this->Patients->save($patient);

            //get medications
            if(!isset($this->request->data["no-medication"])){
                $medications = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"medicationname")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['name'] = $v;
                        $medications[$key]['user_id'] = $this->Auth->user("id");
                        $medications[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"dose")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['dose'] = $v;
                    }
                    if(strpos($k,"oftentaken")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['often_taken'] = $v;
                    }
                    if(strpos($k,"purpose")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['purpose'] = $v;
                    }
                    if(strpos($k,"medication_brand")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['brand'] = $v;
                    }
                    if(strpos($k,"started")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['started_use'] = date('Y-m-d', strtotime($v));
                    }                    
                }

                //Sacar entities vacios
                $emptyMedications = array();
                foreach($medications as $key => $value){
                    if($value['name'] == "" && $value['dose'] == "" && 
                        $value['often_taken'] == "" && $value['purpose'] == "" &&
                        $value['brand'] == "" && $value['started_use'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyMedications,$key);
                    }
                }
                
                foreach($emptyMedications as $v){
                    //Quitamos los vacios                    
                    unset($medications[$v]);
                }

                //Save Medications
                $this->InfoMedications = TableRegistry::get('InfoMedications');
                $MedicationEntities = $this->InfoMedications->newEntities($medications);
                $resultMedications = $this->InfoMedications->saveMany($MedicationEntities);
            }

            if(!isset($this->request->data["no-ilness"])){
                //get illnesses
                $ilnesses = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"dateillness")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['date'] =  date('Y-m-d', strtotime($v));
                        $ilnesses[$key]['user_id'] = $this->Auth->user("id");
                        $ilnesses[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"illness")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['illness'] = $v;
                    }
                    if(strpos($k,"treatment")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['treatment'] = $v;
                    }
                    if(strpos($k,"outcome")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['outcome'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyilnesses = array();
                foreach($ilnesses as $key => $value){
                    if($value['date'] == "" && $value['illness'] == "" && 
                        $value['treatment'] == "" && $value['outcome'] == "" ){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyilnesses,$key);
                    }
                }
                
                foreach($emptyilnesses as $v){
                    //Quitamos los vacios                    
                    unset($ilnesses[$v]);
                }

                //Save illnesses
                $this->MajorIllnesses = TableRegistry::get('MajorIllnesses');
                $MajorIllnessesEntities = $this->MajorIllnesses->newEntities($ilnesses);
                $resultMajorIllnesses = $this->MajorIllnesses->saveMany($MajorIllnessesEntities);
            }

            if(!isset($this->request->data["no-surgery"])){
                //get Surgeries
                $surgeries = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"surgeryname")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['surgery'] = $v;
                        $surgeries[$key]['user_id'] = $this->Auth->user("id");
                        $surgeries[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"datesurgery")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['date'] =  date('Y-m-d', strtotime($v));                    
                    }                
                    if(strpos($k,"reason")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['reason'] = $v;
                    }
                    if(strpos($k,"hospital")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['hospital'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptysurgeries = array();
                foreach($surgeries as $key => $value){
                    if($value['surgery'] == "" && $value['date'] == "" && 
                        $value['reason'] == "" && $value['hospital'] == "" ){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptysurgeries,$key);
                    }
                }
                
                foreach($emptysurgeries as $v){
                    //Quitamos los vacios                    
                    unset($surgeries[$v]);
                }

                //Save Surgeries
                $this->PatientSurgeries = TableRegistry::get('PatientSurgeries');
                $surgeriesEntities = $this->PatientSurgeries->newEntities($surgeries);
                $resultSurgeries = $this->PatientSurgeries->saveMany($surgeriesEntities);
            }

            if(!isset($this->request->data["no-hospitalizations"])){
                //get Hospitalizations
                $hospitalizations = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"datehospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['date'] =  date('Y-m-d', strtotime($v));
                        $hospitalizations[$key]['user_id'] = $this->Auth->user("id");
                        $hospitalizations[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"reasonhospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['reason'] = $v;
                    }
                    if(strpos($k,"hospitalhospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['hospital'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyhospitalizations = array();
                foreach($hospitalizations as $key => $value){
                    if($value['date'] == "" && $value['reason'] == "" && $value['hospital'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyhospitalizations,$key);
                    }
                }
                
                foreach($emptyhospitalizations as $v){
                    //Quitamos los vacios                    
                    unset($hospitalizations[$v]);
                }

                //Save Hospitalizations
                $this->OtherHospitalizations = TableRegistry::get('OtherHospitalizations');
                $hospitalizationsEntities = $this->OtherHospitalizations->newEntities($hospitalizations);
                $resulthospitalizations = $this->OtherHospitalizations->saveMany($hospitalizationsEntities);
            }

            if(!isset($this->request->data["no-family"])){
                //get Family History
                $famHistory = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"family_member")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['family_member'] =  $v;
                        $famHistory[$key]['user_id'] = $this->Auth->user("id");
                        $famHistory[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"familyage")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['age'] = $v;
                    }
                    if(strpos($k,"deceased")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['deceased'] = $v;
                    }
                    if(strpos($k,"cause_of_death")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['cause_of_death'] = $v;
                    }
                    if(strpos($k,"familycomplexion")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['complexion'] = $v;
                    }
                    if(strpos($k,"familyhealth")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['health_problems'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyfamHistory = array();
                foreach($famHistory as $key => $value){
                    if($value['family_member'] == "" && $value['age'] == "" && $value['deceased'] == "" &&
                    $value['cause_of_death'] == "" && $value['complexion'] == "" && $value['health_problems'] == ""
                    ){
                        //Esta vacio, agregamos el key a un arreglo
                        array_push($emptyfamHistory,$key);
                    }
                }
                
                foreach($emptyfamHistory as $v){
                    //Quitamos los vacios                    
                    unset($famHistory[$v]);
                }

                //Save Family History
                $this->FamilyHistories = TableRegistry::get('FamilyHistories');
                $historyEntities = $this->FamilyHistories->newEntities($famHistory);
                $resultHistory = $this->FamilyHistories->saveMany($historyEntities);
            }

            if(!isset($this->request->data["no-digestive"])){
                //get Digestive Problems
                $digestiveProblems = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"digestivefood")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $digestiveProblems[$key]['food_drink'] = $v;
                        $digestiveProblems[$key]['user_id'] = $this->Auth->user("id");
                        $digestiveProblems[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"digestiveresult")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $digestiveProblems[$key]['result_of_eating'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptydigestiveProblems = array();
                foreach($digestiveProblems as $key => $value){
                    if($value['food_drink'] == "" && $value['result_of_eating'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptydigestiveProblems,$key);
                    }
                }
                
                foreach($emptydigestiveProblems as $v){
                    //Quitamos los vacios                    
                    unset($digestiveProblems[$v]);
                }

                //Save Digestive Problems
                $this->DigestiveProblems = TableRegistry::get('DigestiveProblems');
                $digestiveProblemsEntities = $this->DigestiveProblems->newEntities($digestiveProblems);
                $resultDigestiveProblems = $this->DigestiveProblems->saveMany($digestiveProblemsEntities);
            }

            if(!isset($this->request->data["no-bone"])){
                //get Bone Problems
                $boneProblems = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"bonedoctor")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['doctor'] =  $v;
                        $boneProblems[$key]['user_id'] = $this->Auth->user("id");
                        $boneProblems[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"bonedate")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['date_treatment'] = date('Y-m-d', strtotime($v));
                    }
                    if(strpos($k,"bonediagnosis")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['diagnosis'] = $v;
                    }
                }
            
                //Sacar entities vacios
                $emptyboneProblems = array();
                foreach($boneProblems as $key => $value){
                    if($value['doctor'] == "" && $value['date_treatment'] == "" && $value['diagnosis']){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyboneProblems,$key);
                    }
                }
                
                foreach($emptyboneProblems as $v){
                    //Quitamos los vacios                    
                    unset($boneProblems[$v]);
                }

                //Save Bone Problems
                $this->BoneProblems = TableRegistry::get('BoneProblems');
                $boneProblemsEntities = $this->BoneProblems->newEntities($boneProblems);
                $resultBoneProblems = $this->BoneProblems->saveMany($boneProblemsEntities);
            }
            
            //Poner fijos Doctor y Ubicacion
            $this->request->data["doctor_id"] = 1;
            $this->request->data["location"] = "Tijuana";
            //Save Questionnaire
            $this->request->data["date_of_birth"] = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $this->request->data["suggested_date"] = date('Y-m-d', strtotime($this->request->data["suggested_date"]));
            $this->request->data["date_of_referral"] = date('Y-m-d', strtotime($this->request->data["date_of_referral"]));
            $this->request->data["signature_date"] = date('Y-m-d', strtotime($this->request->data["signature_date"]));

            $healthQuestionnaire = $this->HealthQuestionnaires->patchEntity($healthQuestionnaire, $this->request->data());
            if ($this->HealthQuestionnaires->save($healthQuestionnaire)) {
                $this->Flash->success(__('The {0} has been saved.', 'Health Questionnaire'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Health Questionnaire'));
        }

        $doctors = $this->HealthQuestionnaires->Doctors->find('list', [
            'conditions'=>['doctor_type_id'=>1],
            'limit' => 200]);
        $procedures = $this->HealthQuestionnaires->Procedures->find('list', ['limit' => 200]);
        $this->set(compact('healthQuestionnaire', 'procedures','doctors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Health Questionnaire id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $healthQuestionnaire = $this->HealthQuestionnaires->get($id);
        if ($this->HealthQuestionnaires->delete($healthQuestionnaire)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Health Questionnaire'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Health Questionnaire'));
        }

        return $this->redirect(['action' => 'index']);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addfrompatient($patientid =  null)
    {
        $healthQuestionnaire = $this->HealthQuestionnaires->newEntity();

        $this->Patients = TableRegistry::get('Patients');
        $patient = $this->Patients->get($patientid, [
            'contain' => []
        ]);

        $this->set(compact('patient'));

        if ($this->request->is('post')) {
            
            $patient_id = $this->request->data['patient_id'];

            $this->Patients = TableRegistry::get('Patients');
            
            $patient = $this->Patients->get($patient_id);

            $patient->location = $this->request->data["location"];
            $patient->doctor_id = $this->request->data["doctor_id"];
            $patient->name = $this->request->data["name"];
            $patient->address = $this->request->data["address"];
            $patient->city = $this->request->data["city"];
            $patient->state = $this->request->data["state"];
            $patient->zipcode = $this->request->data["zip_code"];
            $patient->home_phone = $this->request->data["home_phone"];
            $patient->work_phone = $this->request->data["work_phone"];
            $patient->cell_phone = $this->request->data["cell_phone"];
            $patient->date_of_birth = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $patient->gender_id = $this->request->data["gender"];
            $patient->marital_status_id = $this->request->data["marital_status"];
            $patient->age = $this->request->data["age"];
            $patient->height = $this->request->data["height"];
            $patient->weight = $this->request->data["weight"];
            $patient->bmi = $this->request->data["BMI"];
            $patient->neck = $this->request->data["neck"];
            $patient->wrist = $this->request->data["wrist"];
            $patient->waist = $this->request->data["waist"];
            $patient->hip = $this->request->data["hip"];
            $patient->thigh = $this->request->data["thigh"];
            $patient->shirt_size = $this->request->data["size_shirt"];
            $patient->pants_size = $this->request->data["size_pants"];
            $patient->user_id = $this->Auth->user("id");

            $resultPatients = $this->Patients->save($patient);

            $emailAddress = $this->request->data["email"];
            unset($this->request->data["email"]);

            if($resultPatients->id > 0 && $emailAddress != ""){
                //Salvar Email
                $newEmail = array();
                $newEmail["email"] = $emailAddress;
                $newEmail["patient_id"] = $resultPatients->id;         
                
                $this->Emails = TableRegistry::get('Emails');
                $email = $this->Emails->newEntity();
                $email = $this->Emails->patchEntity($email, $newEmail);

                $this->Emails->save($email);
            }

            //get medications
            if(!isset($this->request->data["no-medication"])){
                $medications = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"medicationname")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['name'] = $v;
                        $medications[$key]['user_id'] = $this->Auth->user("id");
                        $medications[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"dose")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['dose'] = $v;
                    }
                    if(strpos($k,"oftentaken")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['often_taken'] = $v;
                    }
                    if(strpos($k,"purpose")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['purpose'] = $v;
                    }
                    if(strpos($k,"medication_brand")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['brand'] = $v;
                    }
                    if(strpos($k,"started")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['started_use'] = date('Y-m-d', strtotime($v));
                    }                    
                }

                //Sacar entities vacios
                $emptyMedications = array();
                foreach($medications as $key => $value){
                    if($value['name'] == "" && $value['dose'] == "" && 
                        $value['often_taken'] == "" && $value['purpose'] == "" &&
                        $value['brand'] == "" && $value['started_use'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyMedications,$key);
                    }
                }
                
                foreach($emptyMedications as $v){
                    //Quitamos los vacios                    
                    unset($medications[$v]);
                }

                //Save Medications
                $this->InfoMedications = TableRegistry::get('InfoMedications');
                $MedicationEntities = $this->InfoMedications->newEntities($medications);
                $resultMedications = $this->InfoMedications->saveMany($MedicationEntities);
            }

            if(!isset($this->request->data["no-ilness"])){
                //get illnesses
                $ilnesses = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"dateillness")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['date'] =  date('Y-m-d', strtotime($v));
                        $ilnesses[$key]['user_id'] = $this->Auth->user("id");
                        $ilnesses[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"illness")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['illness'] = $v;
                    }
                    if(strpos($k,"treatment")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['treatment'] = $v;
                    }
                    if(strpos($k,"outcome")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['outcome'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyilnesses = array();
                foreach($ilnesses as $key => $value){
                    if($value['date'] == "" && $value['illness'] == "" && 
                        $value['treatment'] == "" && $value['outcome'] == "" ){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyilnesses,$key);
                    }
                }
                
                foreach($emptyilnesses as $v){
                    //Quitamos los vacios                    
                    unset($ilnesses[$v]);
                }

                //Save illnesses
                $this->MajorIllnesses = TableRegistry::get('MajorIllnesses');
                $MajorIllnessesEntities = $this->MajorIllnesses->newEntities($ilnesses);
                $resultMajorIllnesses = $this->MajorIllnesses->saveMany($MajorIllnessesEntities);
            }

            if(!isset($this->request->data["no-surgery"])){
                //get Surgeries
                $surgeries = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"surgeryname")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['surgery'] = $v;
                        $surgeries[$key]['user_id'] = $this->Auth->user("id");
                        $surgeries[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"datesurgery")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['date'] =  date('Y-m-d', strtotime($v));                    
                    }                
                    if(strpos($k,"reason")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['reason'] = $v;
                    }
                    if(strpos($k,"hospital")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['hospital'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptysurgeries = array();
                foreach($surgeries as $key => $value){
                    if($value['surgery'] == "" && $value['date'] == "" && 
                        $value['reason'] == "" && $value['hospital'] == "" ){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptysurgeries,$key);
                    }
                }
                
                foreach($emptysurgeries as $v){
                    //Quitamos los vacios                    
                    unset($surgeries[$v]);
                }

                //Save Surgeries
                $this->PatientSurgeries = TableRegistry::get('PatientSurgeries');
                $surgeriesEntities = $this->PatientSurgeries->newEntities($surgeries);
                $resultSurgeries = $this->PatientSurgeries->saveMany($surgeriesEntities);
            }

            if(!isset($this->request->data["no-hospitalizations"])){
                //get Hospitalizations
                $hospitalizations = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"datehospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['date'] =  date('Y-m-d', strtotime($v));
                        $hospitalizations[$key]['user_id'] = $this->Auth->user("id");
                        $hospitalizations[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"reasonhospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['reason'] = $v;
                    }
                    if(strpos($k,"hospitalhospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['hospital'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyhospitalizations = array();
                foreach($hospitalizations as $key => $value){
                    if($value['date'] == "" && $value['reason'] == "" && $value['hospital'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyhospitalizations,$key);
                    }
                }
                
                foreach($emptyhospitalizations as $v){
                    //Quitamos los vacios                    
                    unset($hospitalizations[$v]);
                }

                //Save Hospitalizations
                $this->OtherHospitalizations = TableRegistry::get('OtherHospitalizations');
                $hospitalizationsEntities = $this->OtherHospitalizations->newEntities($hospitalizations);
                $resulthospitalizations = $this->OtherHospitalizations->saveMany($hospitalizationsEntities);
            }

            if(!isset($this->request->data["no-family"])){
                //get Family History
                $famHistory = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"family_member")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['family_member'] =  $v;
                        $famHistory[$key]['user_id'] = $this->Auth->user("id");
                        $famHistory[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"familyage")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['age'] = $v;
                    }
                    if(strpos($k,"deceased")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['deceased'] = $v;
                    }
                    if(strpos($k,"cause_of_death")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['cause_of_death'] = $v;
                    }
                    if(strpos($k,"familycomplexion")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['complexion'] = $v;
                    }
                    if(strpos($k,"familyhealth")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['health_problems'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyfamHistory = array();
                foreach($famHistory as $key => $value){
                    if($value['family_member'] == "" && $value['age'] == "" && $value['deceased'] == "" &&
                    $value['cause_of_death'] == "" && $value['complexion'] == "" && $value['health_problems'] == ""
                    ){
                        //Esta vacio, agregamos el key a un arreglo
                        array_push($emptyfamHistory,$key);
                    }
                }
                
                foreach($emptyfamHistory as $v){
                    //Quitamos los vacios                    
                    unset($famHistory[$v]);
                }

                //Save Family History
                $this->FamilyHistories = TableRegistry::get('FamilyHistories');
                $historyEntities = $this->FamilyHistories->newEntities($famHistory);
                $resultHistory = $this->FamilyHistories->saveMany($historyEntities);
            }

            if(!isset($this->request->data["no-digestive"])){
                //get Digestive Problems
                $digestiveProblems = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"digestivefood")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $digestiveProblems[$key]['food_drink'] = $v;
                        $digestiveProblems[$key]['user_id'] = $this->Auth->user("id");
                        $digestiveProblems[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"digestiveresult")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $digestiveProblems[$key]['result_of_eating'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptydigestiveProblems = array();
                foreach($digestiveProblems as $key => $value){
                    if($value['food_drink'] == "" && $value['result_of_eating'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptydigestiveProblems,$key);
                    }
                }
                
                foreach($emptydigestiveProblems as $v){
                    //Quitamos los vacios                    
                    unset($digestiveProblems[$v]);
                }

                //Save Digestive Problems
                $this->DigestiveProblems = TableRegistry::get('DigestiveProblems');
                $digestiveProblemsEntities = $this->DigestiveProblems->newEntities($digestiveProblems);
                $resultDigestiveProblems = $this->DigestiveProblems->saveMany($digestiveProblemsEntities);
            }

            if(!isset($this->request->data["no-bone"])){
                //get Bone Problems
                $boneProblems = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"bonedoctor")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['doctor'] =  $v;
                        $boneProblems[$key]['user_id'] = $this->Auth->user("id");
                        $boneProblems[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"bonedate")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['date_treatment'] = date('Y-m-d', strtotime($v));
                    }
                    if(strpos($k,"bonediagnosis")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['diagnosis'] = $v;
                    }
                }
            
                //Sacar entities vacios
                $emptyboneProblems = array();
                foreach($boneProblems as $key => $value){
                    if($value['doctor'] == "" && $value['date_treatment'] == "" && $value['diagnosis']){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyboneProblems,$key);
                    }
                }
                
                foreach($emptyboneProblems as $v){
                    //Quitamos los vacios                    
                    unset($boneProblems[$v]);
                }

                //Save Bone Problems
                $this->BoneProblems = TableRegistry::get('BoneProblems');
                $boneProblemsEntities = $this->BoneProblems->newEntities($boneProblems);
                $resultBoneProblems = $this->BoneProblems->saveMany($boneProblemsEntities);
            }
            
            
            //Poner fijos Doctor y Ubicacion
            $this->request->data["doctor_id"] = 1;
            $this->request->data["location"] = "Tijuana";
            //Save Questionnaire
            $this->request->data["date_of_birth"] = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $this->request->data["suggested_date"] = date('Y-m-d', strtotime($this->request->data["suggested_date"]));
            $this->request->data["date_of_referral"] = date('Y-m-d', strtotime($this->request->data["date_of_referral"]));
            $this->request->data["signature_date"] = date('Y-m-d', strtotime($this->request->data["signature_date"]));
            $this->request->data['user_id'] = $this->Auth->user("id");
            $this->request->data['patient_id'] = $resultPatients->id;

            $healthQuestionnaire = $this->HealthQuestionnaires->patchEntity($healthQuestionnaire, $this->request->data());
            
            if ($healthQuestionnaire->errors()) {
                    echo "Errores Questionario: ";
                print_r($healthQuestionnaire->errors());
            }
            
            $savedQuestionnaire = $this->HealthQuestionnaires->save($healthQuestionnaire);
            if ($savedQuestionnaire->id > 0) {
                //Si se salvan los datos de cuestionario
                $questionnaireId = $savedQuestionnaire->id;                
                
                 $this->Flash->success(__('The {0} has been saved.', 'Health Questionnaire'));

                 return $this->redirect(['action' => 'index']);
            }
            //$this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Health Questionnaire'));
        }

        $doctors = $this->HealthQuestionnaires->Doctors->find('list', [
            'conditions'=>['doctor_type_id'=>1],
            'limit' => 200]);
        $procedures = $this->HealthQuestionnaires->Procedures->find('list', ['limit' => 200]);
        $this->set(compact('healthQuestionnaire', 'procedures','doctors'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addfromemail($email_id =  null)
    {
        $healthQuestionnaire = $this->HealthQuestionnaires->newEntity();

        //Agregar verified al registro de la tabla email.
        $this->Emails = TableRegistry::get('Emails');

        $email_record = $this->Emails->get($email_id);
    
        $email_record->verified = 1;

        $email = $email_record->email;

        $this->Emails->save($email_record);

        $this->set(compact('email_id','email'));

        if ($this->request->is('post')) {

            $email_id = $this->request->data['email_id'];            

            unset($this->request->data['email_id']);            

            $this->Patients = TableRegistry::get('Patients');
            
            $patient = array();

            $patient["location"] = $this->request->data["location"];
            $patient["doctor_id"] = $this->request->data["doctor_id"];
            $patient["name"] = $this->request->data["name"];
            $patient["address"] = $this->request->data["address"];
            $patient["city"] = $this->request->data["city"];
            $patient["state"] = $this->request->data["state"];
            $patient["zipcode"] = $this->request->data["zip_code"];
            $patient["home_phone"] = $this->request->data["home_phone"];
            $patient["work_phone"] = $this->request->data["work_phone"];
            $patient["cell_phone"] = $this->request->data["cell_phone"];
            $patient["date_of_birth"] = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $patient["gender_id"] = $this->request->data["gender"];
            $patient["marital_status_id"] = $this->request->data["marital_status"];
            $patient["age"] = $this->request->data["age"];
            $patient["height"] = $this->request->data["height"];
            $patient["weight"] = $this->request->data["weight"];
            $patient["bmi"] = $this->request->data["BMI"];
            $patient["neck"] = $this->request->data["neck"];
            $patient["wrist"] = $this->request->data["wrist"];
            $patient["waist"] = $this->request->data["waist"];
            $patient["hip"] = $this->request->data["hip"];
            $patient["thigh"] = $this->request->data["thigh"];
            $patient["shirt_size"] = $this->request->data["size_shirt"];
            $patient["pants_size"] = $this->request->data["size_pants"];
            $patient['user_id'] = $this->Auth->user("id");

            $this->Patients = TableRegistry::get('Patients');
            $patientEntity = $this->Patients->newEntity();
            $patientEntity = $this->Patients->patchEntity($patientEntity, $patient);

            if ($patientEntity->errors()) {
                echo "Errores Patient: ";
                print_r($patientEntity->errors());
                echo "<br>";
            }

            $resultPatients = $this->Patients->save($patientEntity);

            //get medications
            if(!isset($this->request->data["no-medication"])){
                $medications = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"medicationname")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['name'] = $v;
                        $medications[$key]['user_id'] = $this->Auth->user("id");
                        $medications[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"dose")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['dose'] = $v;
                    }
                    if(strpos($k,"oftentaken")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['often_taken'] = $v;
                    }
                    if(strpos($k,"purpose")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['purpose'] = $v;
                    }
                    if(strpos($k,"medication_brand")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['brand'] = $v;
                    }
                    if(strpos($k,"started")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $medications[$key]['started_use'] = date('Y-m-d', strtotime($v));
                    }                    
                }

                //Sacar entities vacios
                $emptyMedications = array();
                foreach($medications as $key => $value){
                    if($value['name'] == "" && $value['dose'] == "" && 
                        $value['often_taken'] == "" && $value['purpose'] == "" &&
                        $value['brand'] == "" && $value['started_use'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyMedications,$key);
                    }
                }
                
                foreach($emptyMedications as $v){
                    //Quitamos los vacios                    
                    unset($medications[$v]);
                }

                //Save Medications
                $this->InfoMedications = TableRegistry::get('InfoMedications');
                $MedicationEntities = $this->InfoMedications->newEntities($medications);
                $resultMedications = $this->InfoMedications->saveMany($MedicationEntities);
            }

            if(!isset($this->request->data["no-ilness"])){
                //get illnesses
                $ilnesses = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"dateillness")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['date'] =  date('Y-m-d', strtotime($v));
                        $ilnesses[$key]['user_id'] = $this->Auth->user("id");
                        $ilnesses[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"illness")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['illness'] = $v;
                    }
                    if(strpos($k,"treatment")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['treatment'] = $v;
                    }
                    if(strpos($k,"outcome")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $ilnesses[$key]['outcome'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyilnesses = array();
                foreach($ilnesses as $key => $value){
                    if($value['date'] == "" && $value['illness'] == "" && 
                        $value['treatment'] == "" && $value['outcome'] == "" ){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyilnesses,$key);
                    }
                }
                
                foreach($emptyilnesses as $v){
                    //Quitamos los vacios                    
                    unset($ilnesses[$v]);
                }

                //Save illnesses
                $this->MajorIllnesses = TableRegistry::get('MajorIllnesses');
                $MajorIllnessesEntities = $this->MajorIllnesses->newEntities($ilnesses);
                $resultMajorIllnesses = $this->MajorIllnesses->saveMany($MajorIllnessesEntities);
            }

            if(!isset($this->request->data["no-surgery"])){
                //get Surgeries
                $surgeries = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"surgeryname")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['surgery'] = $v;
                        $surgeries[$key]['user_id'] = $this->Auth->user("id");
                        $surgeries[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"datesurgery")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['date'] =  date('Y-m-d', strtotime($v));                    
                    }                
                    if(strpos($k,"reason")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['reason'] = $v;
                    }
                    if(strpos($k,"hospital")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $surgeries[$key]['hospital'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptysurgeries = array();
                foreach($surgeries as $key => $value){
                    if($value['surgery'] == "" && $value['date'] == "" && 
                        $value['reason'] == "" && $value['hospital'] == "" ){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptysurgeries,$key);
                    }
                }
                
                foreach($emptysurgeries as $v){
                    //Quitamos los vacios                    
                    unset($surgeries[$v]);
                }

                //Save Surgeries
                $this->PatientSurgeries = TableRegistry::get('PatientSurgeries');
                $surgeriesEntities = $this->PatientSurgeries->newEntities($surgeries);
                $resultSurgeries = $this->PatientSurgeries->saveMany($surgeriesEntities);
            }

            if(!isset($this->request->data["no-hospitalizations"])){
                //get Hospitalizations
                $hospitalizations = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"datehospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['date'] =  date('Y-m-d', strtotime($v));
                        $hospitalizations[$key]['user_id'] = $this->Auth->user("id");
                        $hospitalizations[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"reasonhospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['reason'] = $v;
                    }
                    if(strpos($k,"hospitalhospitalization")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $hospitalizations[$key]['hospital'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyhospitalizations = array();
                foreach($hospitalizations as $key => $value){
                    if($value['date'] == "" && $value['reason'] == "" && $value['hospital'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyhospitalizations,$key);
                    }
                }
                
                foreach($emptyhospitalizations as $v){
                    //Quitamos los vacios                    
                    unset($hospitalizations[$v]);
                }

                //Save Hospitalizations
                $this->OtherHospitalizations = TableRegistry::get('OtherHospitalizations');
                $hospitalizationsEntities = $this->OtherHospitalizations->newEntities($hospitalizations);
                $resulthospitalizations = $this->OtherHospitalizations->saveMany($hospitalizationsEntities);
            }

            if(!isset($this->request->data["no-family"])){
                //get Family History
                $famHistory = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"family_member")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['family_member'] =  $v;
                        $famHistory[$key]['user_id'] = $this->Auth->user("id");
                        $famHistory[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"familyage")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['age'] = $v;
                    }
                    if(strpos($k,"deceased")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['deceased'] = $v;
                    }
                    if(strpos($k,"cause_of_death")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['cause_of_death'] = $v;
                    }
                    if(strpos($k,"familycomplexion")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['complexion'] = $v;
                    }
                    if(strpos($k,"familyhealth")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $famHistory[$key]['health_problems'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptyfamHistory = array();
                foreach($famHistory as $key => $value){
                    if($value['family_member'] == "" && $value['age'] == "" && $value['deceased'] == "" &&
                    $value['cause_of_death'] == "" && $value['complexion'] == "" && $value['health_problems'] == ""
                    ){
                        //Esta vacio, agregamos el key a un arreglo
                        array_push($emptyfamHistory,$key);
                    }
                }
                
                foreach($emptyfamHistory as $v){
                    //Quitamos los vacios                    
                    unset($famHistory[$v]);
                }

                //Save Family History
                $this->FamilyHistories = TableRegistry::get('FamilyHistories');
                $historyEntities = $this->FamilyHistories->newEntities($famHistory);
                $resultHistory = $this->FamilyHistories->saveMany($historyEntities);
            }

            if(!isset($this->request->data["no-digestive"])){
                //get Digestive Problems
                $digestiveProblems = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"digestivefood")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $digestiveProblems[$key]['food_drink'] = $v;
                        $digestiveProblems[$key]['user_id'] = $this->Auth->user("id");
                        $digestiveProblems[$key]['patient_id'] = $resultPatients->id;
                    }                
                    if(strpos($k,"digestiveresult")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $digestiveProblems[$key]['result_of_eating'] = $v;
                    }
                }

                //Sacar entities vacios
                $emptydigestiveProblems = array();
                foreach($digestiveProblems as $key => $value){
                    if($value['food_drink'] == "" && $value['result_of_eating'] == ""){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptydigestiveProblems,$key);
                    }
                }
                
                foreach($emptydigestiveProblems as $v){
                    //Quitamos los vacios                    
                    unset($digestiveProblems[$v]);
                }

                //Save Digestive Problems
                $this->DigestiveProblems = TableRegistry::get('DigestiveProblems');
                $digestiveProblemsEntities = $this->DigestiveProblems->newEntities($digestiveProblems);
                $resultDigestiveProblems = $this->DigestiveProblems->saveMany($digestiveProblemsEntities);
            }

            if(!isset($this->request->data["no-bone"])){
                //get Bone Problems
                $boneProblems = array();
                foreach($this->request->data as $k => $v){
                    if(strpos($k,"bonedoctor")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['doctor'] =  $v;
                        $boneProblems[$key]['user_id'] = $this->Auth->user("id");
                        $boneProblems[$key]['patient_id'] = $resultPatients->id;
                    }
                    if(strpos($k,"bonedate")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['date_treatment'] = date('Y-m-d', strtotime($v));
                    }
                    if(strpos($k,"bonediagnosis")>-1){
                        $key = preg_replace('/[^0-9]/', '', $k);
                        $boneProblems[$key]['diagnosis'] = $v;
                    }
                }
            
                //Sacar entities vacios
                $emptyboneProblems = array();
                foreach($boneProblems as $key => $value){
                    if($value['doctor'] == "" && $value['date_treatment'] == "" && $value['diagnosis']){
                            //Esta vacio, agregamos el key a un arreglo
                            array_push($emptyboneProblems,$key);
                    }
                }
                
                foreach($emptyboneProblems as $v){
                    //Quitamos los vacios                    
                    unset($boneProblems[$v]);
                }

                //Save Bone Problems
                $this->BoneProblems = TableRegistry::get('BoneProblems');
                $boneProblemsEntities = $this->BoneProblems->newEntities($boneProblems);
                $resultBoneProblems = $this->BoneProblems->saveMany($boneProblemsEntities);
            }
            
            //Poner fijos Doctor y Ubicacion
            $this->request->data["doctor_id"] = 1;
            $this->request->data["location"] = "Tijuana";
            //Save Questionnaire
            $this->request->data["date_of_birth"] = date('Y-m-d', strtotime($this->request->data["date_of_birth"]));
            $this->request->data["suggested_date"] = date('Y-m-d', strtotime($this->request->data["suggested_date"]));
            $this->request->data["date_of_referral"] = date('Y-m-d', strtotime($this->request->data["date_of_referral"]));
            $this->request->data["signature_date"] = date('Y-m-d', strtotime($this->request->data["signature_date"]));
            $this->request->data['user_id'] = $this->Auth->user("id");
            $this->request->data['patient_id'] = $resultPatients->id;

            $healthQuestionnaire = $this->HealthQuestionnaires->patchEntity($healthQuestionnaire, $this->request->data());
            
            if ($healthQuestionnaire->errors()) {
                    echo "Errores Questionario: ";
                print_r($healthQuestionnaire->errors());
            }
            
            $savedQuestionnaire = $this->HealthQuestionnaires->save($healthQuestionnaire);
            if ($savedQuestionnaire->id > 0) {
                //Si se salvan los datos de cuestionario
                $questionnaireId = $savedQuestionnaire->id;

                $this->Emails = TableRegistry::get('Emails');

                $email_record = $this->Emails->get($email_id);
            
                $email_record->patient_id = $resultPatients->id;

                $email_record->verified = 1;

                $this->Emails->save($email_record);

                //$this->Flash->success(__('The {0} has been saved.', 'Health Questionnaire'));

                return $this->redirect(['action' => 'thanks']);
            }
            //$this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Health Questionnaire'));
        }

        $doctors = $this->HealthQuestionnaires->Doctors->find('list', [
            'conditions'=>['doctor_type_id'=>1],
            'limit' => 200]);
        $procedures = $this->HealthQuestionnaires->Procedures->find('list', ['limit' => 200]);
        $this->set(compact('healthQuestionnaire', 'procedures','doctors'));
    }

    public function thanks(){


    }


    public function callinfo($id = null)
    {
        $healthQuestionnaire = $this->HealthQuestionnaires->get($id, [
            'contain' => []
        ]);
            
        $this->Patients = TableRegistry::get('Patients');
        $patient = $this->Patients->get($healthQuestionnaire['patient_id'], [
            'contain' => []
        ]);

        $this->Emails = TableRegistry::get('Emails');
        $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "patient_id"=> $patient->id]]);

        $emailsData = $EmailsQuery->toArray();

        $this->InfoMedications = TableRegistry::get('InfoMedications');
        $medicationsQuery = $this->InfoMedications->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $medicationsData = $medicationsQuery->toArray();

        $this->MajorIllnesses = TableRegistry::get('MajorIllnesses');
        $ilnessesQuery = $this->MajorIllnesses->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $ilnessesData = $ilnessesQuery->toArray();

        $this->PatientSurgeries = TableRegistry::get('PatientSurgeries');
        $patientSurgeriesQuery = $this->PatientSurgeries->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $patientSurgeriesData = $patientSurgeriesQuery->toArray();

        $this->OtherHospitalizations = TableRegistry::get('OtherHospitalizations');
        $hospitalizationsQuery = $this->OtherHospitalizations->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $hospitalizationsData = $hospitalizationsQuery->toArray();

        $this->FamilyHistories = TableRegistry::get('FamilyHistories');
        $famHistoryQuery = $this->FamilyHistories->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $famHistoryData = $famHistoryQuery->toArray();

        $this->DigestiveProblems = TableRegistry::get('DigestiveProblems');
        $digestiveProblemsQuery = $this->DigestiveProblems->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $digestiveProblemsData = $digestiveProblemsQuery->toArray();

        $this->BoneProblems = TableRegistry::get('BoneProblems');
        $boneProblemsQuery = $this->BoneProblems->find('all',['conditions'=>[ "patient_id"=> $healthQuestionnaire['patient_id']]]);

        $boneProblemsData = $boneProblemsQuery->toArray();
    
        $doctors = $this->HealthQuestionnaires->Doctors->find('list', [
            'conditions'=>['doctor_type_id'=>1],
            'limit' => 200]);
            
        $procedures = $this->HealthQuestionnaires->Procedures->find('list', ['limit' => 200]);
        
        $this->set(compact('healthQuestionnaire', 'procedures','doctors',"emailsData","patient","medicationsData","ilnessesData","patientSurgeriesData","hospitalizationsData","famHistoryData","digestiveProblemsData","boneProblemsData"));
    }
}
