<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FamilyHistories Controller
 *
 * @property \App\Model\Table\FamilyHistoriesTable $FamilyHistories
 *
 * @method \App\Model\Entity\FamilyHistory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FamilyHistoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Users']
        ];
        $familyHistories = $this->paginate($this->FamilyHistories);

        $this->set(compact('familyHistories'));
    }

    /**
     * View method
     *
     * @param string|null $id Family History id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $familyHistory = $this->FamilyHistories->get($id, [
            'contain' => ['Patients', 'Users']
        ]);

        $this->set('familyHistory', $familyHistory);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $familyHistory = $this->FamilyHistories->newEntity();
        if ($this->request->is('post')) {
            $familyHistory = $this->FamilyHistories->patchEntity($familyHistory, $this->request->data());
            if ($this->FamilyHistories->save($familyHistory)) {
                $this->Flash->success(__('The {0} has been saved.', 'Family History'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Family History'));
        }
        $patients = $this->FamilyHistories->Patients->find('list', ['limit' => 200]);
        $users = $this->FamilyHistories->Users->find('list', ['limit' => 200]);
        $this->set(compact('familyHistory', 'patients', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Family History id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $familyHistory = $this->FamilyHistories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $familyHistory = $this->FamilyHistories->patchEntity($familyHistory, $this->request->data());
            if ($this->FamilyHistories->save($familyHistory)) {
                $this->Flash->success(__('The {0} has been saved.', 'Family History'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Family History'));
        }
        $patients = $this->FamilyHistories->Patients->find('list', ['limit' => 200]);
        $users = $this->FamilyHistories->Users->find('list', ['limit' => 200]);
        $this->set(compact('familyHistory', 'patients', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Family History id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $familyHistory = $this->FamilyHistories->get($id);
        if ($this->FamilyHistories->delete($familyHistory)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Family History'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Family History'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
