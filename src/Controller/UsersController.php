<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $rolesQuery = $this->Users->Roles->find('list');

        $roles = $rolesQuery->toArray();

        $this->set(compact('users','roles'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        

        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The {0} has been saved.', 'User'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'User'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user','roles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);     
        
        if ($this->request->is(['patch', 'post', 'put'])) {

            $user = $this->Users->patchEntity($user, $this->request->data());

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The {0} has been saved.', 'User'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'User'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user','roles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The {0} has been deleted.', 'User'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'User'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function login(){
        if($this->request->is('post')){
            $user = $this->Auth->Identify();
            if($user){
                $this->Auth->setUser($user);

                $current_user = $this->Users->get($user["id"]);

                $this->Emails = TableRegistry::get('Emails');
                $EmailsQuery = $this->Emails->find('all',['conditions'=>[ "email"=> $this->Auth->user("email")]]);

                $emailsData = $EmailsQuery->first();

                $this->PatientsPictures = TableRegistry::get('PatientsPictures');

                $pictureQuery = $this->PatientsPictures->find('all',['conditions'=>[ 
                    "patient_id"=> $emailsData->patient_id,
                    "active"=>1
                    ]]);

                $pictureData = $pictureQuery->first();
                    
                $this->request->getSession()->write('current', $current_user);
                if($pictureData != null){
                    //Si el paciente ya ha subido fotos
                    $picture = str_replace("/home1/chtechla/cmtechlab.com/sandbox/medfile","",$pictureData->path);
                    
                }else{
                    //Si no tiene fotos ponemos un placeholder
                    $picture = "/img/user.png";
                }

                $this->request->getSession()->write('photo', $picture);

                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error("Wrong username or password");
        }
    }

    public function logout(){
        $this->Flash->success("You have been logged out");
        
        //Desloguar
        $this->Auth->logout();

        //Regresar a la pagina de login
        return $this->redirect(['action' => 'login']);
    }

    public function changepassword($userid){
        if($this->Auth->user("id") == $userid || in_array($this->Auth->user("role_id"),array(1,2))){
            $user = $this->Users->get($userid);

            $this->set(compact("user"));

            if($this->request->is(['patch', 'post', 'put'])){
                //print_r($this->request->data);

                $this->request->data["password"] = $this->request->data['new_password'];

                $user = $this->Users->patchEntity($user, $this->request->data());

                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The {0} has been saved.', 'User'));

                    //return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'User'));
            }
        }
        else{
            $this->Flash->error(__($this->Auth->user("id")."-".$this->Auth->user("role_id")."-You cannot change another user's password."));
            
            return $this->redirect($this->referer());
        }
    }
}
