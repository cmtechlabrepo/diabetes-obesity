<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BoneProblems Controller
 *
 * @property \App\Model\Table\BoneProblemsTable $BoneProblems
 *
 * @method \App\Model\Entity\BoneProblem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BoneProblemsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Users']
        ];
        $boneProblems = $this->paginate($this->BoneProblems);

        $this->set(compact('boneProblems'));
    }

    /**
     * View method
     *
     * @param string|null $id Bone Problem id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $boneProblem = $this->BoneProblems->get($id, [
            'contain' => ['Patients', 'Users']
        ]);

        $this->set('boneProblem', $boneProblem);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $boneProblem = $this->BoneProblems->newEntity();
        if ($this->request->is('post')) {
            $boneProblem = $this->BoneProblems->patchEntity($boneProblem, $this->request->data());
            if ($this->BoneProblems->save($boneProblem)) {
                $this->Flash->success(__('The {0} has been saved.', 'Bone Problem'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Bone Problem'));
        }
        $patients = $this->BoneProblems->Patients->find('list', ['limit' => 200]);
        $users = $this->BoneProblems->Users->find('list', ['limit' => 200]);
        $this->set(compact('boneProblem', 'patients', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bone Problem id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $boneProblem = $this->BoneProblems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $boneProblem = $this->BoneProblems->patchEntity($boneProblem, $this->request->data());
            if ($this->BoneProblems->save($boneProblem)) {
                $this->Flash->success(__('The {0} has been saved.', 'Bone Problem'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Bone Problem'));
        }
        $patients = $this->BoneProblems->Patients->find('list', ['limit' => 200]);
        $users = $this->BoneProblems->Users->find('list', ['limit' => 200]);
        $this->set(compact('boneProblem', 'patients', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bone Problem id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $boneProblem = $this->BoneProblems->get($id);
        if ($this->BoneProblems->delete($boneProblem)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Bone Problem'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Bone Problem'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
