<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PatientSurgeries Controller
 *
 * @property \App\Model\Table\PatientSurgeriesTable $PatientSurgeries
 *
 * @method \App\Model\Entity\PatientSurgery[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PatientSurgeriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Users']
        ];
        $patientSurgeries = $this->paginate($this->PatientSurgeries);

        $this->set(compact('patientSurgeries'));
    }

    /**
     * View method
     *
     * @param string|null $id Patient Surgery id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $patientSurgery = $this->PatientSurgeries->get($id, [
            'contain' => ['Patients', 'Users']
        ]);

        $this->set('patientSurgery', $patientSurgery);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $patientSurgery = $this->PatientSurgeries->newEntity();
        if ($this->request->is('post')) {
            $patientSurgery = $this->PatientSurgeries->patchEntity($patientSurgery, $this->request->data());
            if ($this->PatientSurgeries->save($patientSurgery)) {
                $this->Flash->success(__('The {0} has been saved.', 'Patient Surgery'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patient Surgery'));
        }
        $patients = $this->PatientSurgeries->Patients->find('list', ['limit' => 200]);
        $users = $this->PatientSurgeries->Users->find('list', ['limit' => 200]);
        $this->set(compact('patientSurgery', 'patients', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Patient Surgery id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $patientSurgery = $this->PatientSurgeries->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $patientSurgery = $this->PatientSurgeries->patchEntity($patientSurgery, $this->request->data());
            if ($this->PatientSurgeries->save($patientSurgery)) {
                $this->Flash->success(__('The {0} has been saved.', 'Patient Surgery'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Patient Surgery'));
        }
        $patients = $this->PatientSurgeries->Patients->find('list', ['limit' => 200]);
        $users = $this->PatientSurgeries->Users->find('list', ['limit' => 200]);
        $this->set(compact('patientSurgery', 'patients', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Patient Surgery id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $patientSurgery = $this->PatientSurgeries->get($id);
        if ($this->PatientSurgeries->delete($patientSurgery)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Patient Surgery'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Patient Surgery'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
