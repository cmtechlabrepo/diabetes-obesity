<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SurgeryChanges Controller
 *
 * @property \App\Model\Table\SurgeryChangesTable $SurgeryChanges
 *
 * @method \App\Model\Entity\SurgeryChange[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SurgeryChangesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Patients']
        ];
        $surgeryChanges = $this->paginate($this->SurgeryChanges);

        $this->set(compact('surgeryChanges'));
    }

    /**
     * View method
     *
     * @param string|null $id Surgery Change id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $surgeryChange = $this->SurgeryChanges->get($id, [
            'contain' => ['Users', 'Patients']
        ]);

        $this->set('surgeryChange', $surgeryChange);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $surgeryChange = $this->SurgeryChanges->newEntity();
        if ($this->request->is('post')) {
            $surgeryChange = $this->SurgeryChanges->patchEntity($surgeryChange, $this->request->data());
            if ($this->SurgeryChanges->save($surgeryChange)) {
                $this->Flash->success(__('The {0} has been saved.', 'Surgery Change'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Surgery Change'));
        }
        $users = $this->SurgeryChanges->Users->find('list', ['limit' => 200]);
        $patients = $this->SurgeryChanges->Patients->find('list', ['limit' => 200]);
        $this->set(compact('surgeryChange', 'users', 'patients'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Surgery Change id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $surgeryChange = $this->SurgeryChanges->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $surgeryChange = $this->SurgeryChanges->patchEntity($surgeryChange, $this->request->data());
            if ($this->SurgeryChanges->save($surgeryChange)) {
                $this->Flash->success(__('The {0} has been saved.', 'Surgery Change'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Surgery Change'));
        }
        $users = $this->SurgeryChanges->Users->find('list', ['limit' => 200]);
        $patients = $this->SurgeryChanges->Patients->find('list', ['limit' => 200]);
        $this->set(compact('surgeryChange', 'users', 'patients'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Surgery Change id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $surgeryChange = $this->SurgeryChanges->get($id);
        if ($this->SurgeryChanges->delete($surgeryChange)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Surgery Change'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Surgery Change'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
