<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Procedures Controller
 *
 * @property \App\Model\Table\ProceduresTable $Procedures
 *
 * @method \App\Model\Entity\Procedure[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProceduresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $procedures = $this->paginate($this->Procedures);

        $this->set(compact('procedures'));
    }

    /**
     * View method
     *
     * @param string|null $id Procedure id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $procedure = $this->Procedures->get($id, [
            'contain' => ['HealthQuestionnaires']
        ]);

        $this->set('procedure', $procedure);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $procedure = $this->Procedures->newEntity();
        if ($this->request->is('post')) {
            $procedure = $this->Procedures->patchEntity($procedure, $this->request->data());
            if ($this->Procedures->save($procedure)) {
                $this->Flash->success(__('The {0} has been saved.', 'Procedure'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Procedure'));
        }
        $this->set(compact('procedure'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Procedure id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $procedure = $this->Procedures->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $procedure = $this->Procedures->patchEntity($procedure, $this->request->data());
            if ($this->Procedures->save($procedure)) {
                $this->Flash->success(__('The {0} has been saved.', 'Procedure'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Procedure'));
        }
        $this->set(compact('procedure'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Procedure id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $procedure = $this->Procedures->get($id);
        if ($this->Procedures->delete($procedure)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Procedure'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Procedure'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
