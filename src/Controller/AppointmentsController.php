<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Appointments Controller
 *
 * @property \App\Model\Table\AppointmentsTable $Appointments
 *
 * @method \App\Model\Entity\Appointment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AppointmentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Doctors', 'Users']
        ];
        $appointments = $this->paginate($this->Appointments);

        $this->set(compact('appointments'));
    }

    /**
     * View method
     *
     * @param string|null $id Appointment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $appointment = $this->Appointments->get($id, [
            'contain' => ['Patients', 'Doctors', 'Users']
        ]);

        $this->set('appointment', $appointment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $appointment = $this->Appointments->newEntity();
        if ($this->request->is('post')) {

            $this->request->getData['date'] = date('Y-m-d',strtotime($this->request->getData('date')));
            $this->request->getData['time'] = date('H:i:s',strtotime($this->request->getData('time')));
            $this->request->getData['user_id'] =$this->Auth->user("id");

            $appointment = $this->Appointments->patchEntity($appointment, $this->request->getData());
            if ($this->Appointments->save($appointment)) {
                $this->Flash->success(__('The {0} has been saved.', 'Appointment'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Appointment'));
        }
        $patients = $this->Appointments->Patients->find('list', ['limit' => 200]);
        $doctors = $this->Appointments->Doctors->find('list', ['limit' => 200]);
        $users = $this->Appointments->Users->find('list', ['limit' => 200]);
        $this->set(compact('appointment', 'patients', 'doctors', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Appointment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $appointment = $this->Appointments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->request->getData['date'] = date('Y-m-d',strtotime($this->request->getData('date')));
            $this->request->getData['time'] = date('H:i:s',strtotime($this->request->getData('time')));
            $this->request->getData['user_id'] =$this->Auth->user("id");

            $appointment = $this->Appointments->patchEntity($appointment, $this->request->getData());
            if ($this->Appointments->save($appointment)) {
                $this->Flash->success(__('The {0} has been saved.', 'Appointment'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Appointment'));
        }
        $patients = $this->Appointments->Patients->find('list', ['limit' => 200]);
        $doctors = $this->Appointments->Doctors->find('list', ['limit' => 200]);
        $users = $this->Appointments->Users->find('list', ['limit' => 200]);
        $this->set(compact('appointment', 'patients', 'doctors', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Appointment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $id = $this->request->getData('id');
        
        $this->request->allowMethod(['post', 'delete']);

        $appointment = $this->Appointments->get($id);
        if ($this->Appointments->delete($appointment)) {
            //$this->Flash->success(__('The {0} has been deleted.', 'Appointment'));
            //regresar una respuesta al ajax
            header('Content-Type: application/json');
            echo json_encode(array("ok"=>1));
            exit;
        } 
        // else {
        //     $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Appointment'));
        // }

        // return $this->redirect(['action' => 'index']);
    }


    public function schedule(){
        $patients = $this->Appointments->Patients->find('list', ['limit' => 200]);
        $appointment = $this->Appointments->newEntity();
        $this->set(compact('patients','appointment'));
        
        if($this->request->is('post')){
            $this->request->getData['date'] = date('Y-m-d',strtotime($this->request->getData('date')));
            $this->request->getData['time'] = date('H:i:s',strtotime($this->request->getData('time')));
            $this->request->getData['user_id'] =$this->Auth->user("id");

            $email = $this->Auth->user("email");

            $this->Doctors = TableRegistry::get('Doctors');

            $doctorQuery = $this->Doctors->find('all',['conditions'=>[ "email"=> $email]]);

            $doctorData = $doctorQuery->first();

            $this->request->getData['doctor_id'] =$doctorData['id'];

            $appointment = $this->Appointments->patchEntity($appointment, $this->request->getData());
            if ($this->Appointments->save($appointment)) {
                $this->Flash->success(__('The {0} has been saved.', 'Appointment'));

                return $this->redirect(['action' => 'schedule']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Appointment'));
        }
    }

    public function getappointments(){
        $start = date("Y-m-d",strtotime($this->request->data['start']));
        $end = date("Y-m-d",strtotime($this->request->data['end']));

        $role = $this->Auth->user("role_id");
        $id = $this->Auth->user("id");
        $email = $this->Auth->user("email");

        $events = array();

        $this->Doctors = TableRegistry::get('Doctors');

        //Si es Medico, mostrar calendario medico
        if(in_array($role,array(4,5,6,7,8,9,11))){            
            $doctorQuery = $this->Doctors->find('all',['conditions'=>[ "email"=> $email]]);

            $doctorData = $doctorQuery->first();

            $appointmentsQuery = $this->Appointments->find('all',[
                'conditions'=>[ 
                    "doctor_id"=> $doctorData['id'],
                    'date >= ' => $start,
                    'date <=' => $end,
                    'had_appointment'=>0
                ]]);

            $appointmentsData = $appointmentsQuery->toArray();

        }else{
            if($role == 12){
                //Calendario paciente
                $this->Emails = TableRegistry::get('Emails');
                $emailsQuery = $this->Emails->find('all',['conditions'=>[ "email"=> $email]]);

                $emailData = $emailsQuery->first();

                $appointmentsQuery = $this->Appointments->find('all',['conditions'=>[
                    "patient_id"=> $emailData['patient_id'],
                    //'date between ? and ?' => array($start,$end)
                    'date >= ' => $start,
                    'date <=' => $end,
                    'had_appointment'=>0
                ]]);

                $appointmentsData = $appointmentsQuery->toArray();

                foreach($appointmentsData as $a){
                    $doctors = $this->Doctors->get($a['doctor_id']);
        
                    //echo $patients['name'];
                    $start = date("Y-m-d",strtotime($a['date']))."T".date("H:i:s",strtotime($a['time']));
        
                    $newEvent= array(
                        'id'=>$a['id'],
                        "title"=>$doctors['name'],
                        "start"=>$start,
                        "allDay"=>false,
                        "backgroundColor"=>'#00a65a',
                        "borderColor"=>'#00a65a'
                    );
        
                    array_push($events,$newEvent);                    
                }
                //regresar una respuesta al ajax
                header('Content-Type: application/json');
                echo json_encode($events);
                exit;
            }

            //Si no es doctor ni paciente es admin
            $appointmentsQuery = $this->Appointments->find('all',[
                'conditions'=>[ 
                    'date >= ' => $start,
                    'date <=' => $end,
                    'had_appointment'=>0
                ]]);

            $appointmentsData = $appointmentsQuery->toArray();
            
        }

        $this->Patients = TableRegistry::get('Patients');

        foreach($appointmentsData as $a){
            $patients = $this->Patients->get($a['patient_id']);

            //echo $patients['name'];
            $start = date("Y-m-d",strtotime($a['date']))."T".date("H:i:s",strtotime($a['time']));

            $newEvent= array(
                'id'=>$a['id'],
                "title"=>$patients['name'],
                "start"=>$start,
                "allDay"=>false,
                "backgroundColor"=>'#00a65a',
                "borderColor"=>'#00a65a'
            );

            array_push($events,$newEvent);
        }

        //regresar una respuesta al ajax
        header('Content-Type: application/json');
        echo json_encode($events);
        exit;
    }


    public function change(){
        $id = $this->request->getData('id');
        $date = date("Y-m-d",strtotime($this->request->getData('date')));
        $time = date("H:i:s",strtotime($this->request->getData('time')));

        $record = $this->Appointments->get($id);

        $record->date = $date;
        $record->time = $time;

        $this->Appointments->save($record);

        //regresar una respuesta al ajax
        header('Content-Type: application/json');
        echo json_encode(array("ok"=>1));
        exit;    
    }

    public function hadappoinment(){
        $id = $this->request->getData('id');

        $record = $this->Appointments->get($id);

        $record->had_appointment = 1;

        $this->Appointments->save($record);

        //regresar una respuesta al ajax
        header('Content-Type: application/json');
        echo json_encode(array("ok"=>1));
        exit;    
    }

    public function patientschedule(){

    }    

    public function adminschedule(){
        if($this->request->is('post')){
            $date = date('Y-m-d',strtotime($this->request->getData('date')));
            $time = date('H:i:s',strtotime($this->request->getData('time')));
            $patient = $this->request->getData('patient_id');
            $doctor = $this->request->getData('doctor_id');
            
            //Agregar cita                
            if($patient != "" && $doctor !="" && $date != "" && $time != ""){
                $this->request->getData['date'] = date('Y-m-d',strtotime($this->request->getData('date')));
                $this->request->getData['time'] = date('H:i:s',strtotime($this->request->getData('time')));
                $this->request->getData['user_id'] =$this->Auth->user("id");

                $appointment = $this->Appointments->patchEntity($appointment, $this->request->getData());
                if ($this->Appointments->save($appointment)) {
                    $this->Flash->success(__('The {0} has been saved.', 'Appointment'));

                    return $this->redirect(['action' => 'adminschedule']);
                }
            }
            $this->Flash->error(__('The {0} could not be saved, you need to fill the whole form. Please, try again.', 'Appointment'));
            
        }
        $patients = $this->Appointments->Patients->find('list', ['limit' => 200]);
        $doctors = $this->Appointments->Doctors->find('list', ['limit' => 200]);
        $appointment = $this->Appointments->newEntity();

        $this->set(compact('patients','doctors','appointment'));
    }


    public function filterappointments(){        
        $start = date('Y-m-d',strtotime($this->request->getData('start_date')));
        $end = date('Y-m-d',strtotime($this->request->getData('end_date')));
        $patient = $this->request->getData('patient_id');
        $doctor = $this->request->getData('doctor_id');
    
        $filterDates = false;

        if($start != "" && $end != ""){
            $filterDates = true;
        }
        
        if($patient != "" && $doctor !="" && $filterDates){
            //Buscar con todo
            //echo "filtro 1";
            $appointmentsQuery = $this->Appointments->find('all',['conditions'=>[
                "patient_id"=> $patient,
                "doctor_id"=> $doctor,
                'date >= ' => $start,
                'date <=' => $end,
                //'had_appointment'=>0
            ]]);    
            
        }else if($patient != "" && $doctor != "" && !$filterDates){
            //Buscar por doctor y paciente
            //echo "filtro 2";
            $appointmentsQuery = $this->Appointments->find('all',['conditions'=>[
                "patient_id"=> $patient,
                "doctor_id"=> $doctor,
                // 'date >= ' => $start,
                // 'date <=' => $end,
                //'had_appointment'=>0
            ]]);   
        }else if($patient != "" && $doctor == "" && !$filterDates){
            //Buscar por paciente
            //echo "filtro 3";
            $appointmentsQuery = $this->Appointments->find('all',['conditions'=>[
                "patient_id"=> $patient,
                // "dcctor_id"=> $doctor,
                // 'date >= ' => $start,
                // 'date <=' => $end,
                //'had_appointment'=>0
            ]]);   
        }else if($patient == "" && $doctor != "" && !$filterDates){
            //Buscar por doctor
            //echo "filtro 4";
            $appointmentsQuery = $this->Appointments->find('all',['conditions'=>[
                //"patient_id"=> $patient,
                "doctor_id"=> $doctor,
                // 'date >= ' => $start,
                // 'date <=' => $end,
                //'had_appointment'=>0
            ]]);   
        }else if($patinent == "" && $doctor == "" && $filterDates){
            //Buscar por fechas
            //echo "filtro 5";
            $appointmentsQuery = $this->Appointments->find('all',['conditions'=>[
                // "patient_id"=> $patient,
                // "dcctor_id"=> $doctor,
                'date >= ' => $start,
                'date <=' => $end,
                //'had_appointment'=>0
            ]]);   
        }

        $appointmentsData = $appointmentsQuery->toArray();
        
        $events = array();

        foreach($appointmentsData as $a){
            $this->Doctors = TableRegistry::get('Doctors');
            $doctors = $this->Doctors->get($a['doctor_id']);

            //echo $patients['name'];
            $start = date("Y-m-d",strtotime($a['date']))."T".date("H:i:s",strtotime($a['time']));

            $newEvent= array(
                'id'=>$a['id'],
                "title"=>$doctors['name'],
                "start"=>$start,
                "allDay"=>false,
                "backgroundColor"=>'#00a65a',
                "borderColor"=>'#00a65a'
            );

            array_push($events,$newEvent);                    
        }

        //regresar una respuesta al ajax
        header('Content-Type: application/json');
        echo json_encode($events);
        exit;

    }
}
