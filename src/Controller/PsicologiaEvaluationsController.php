<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PsicologiaEvaluations Controller
 *
 * @property \App\Model\Table\PsicologiaEvaluationsTable $PsicologiaEvaluations
 *
 * @method \App\Model\Entity\PsicologiaEvaluation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PsicologiaEvaluationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Patients', 'Doctors', 'Procedures', 'Users']
        ];
        $psicologiaEvaluations = $this->paginate($this->PsicologiaEvaluations);

        $this->set(compact('psicologiaEvaluations'));
    }

    /**
     * View method
     *
     * @param string|null $id Psicologia Evaluation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $psicologiaEvaluation = $this->PsicologiaEvaluations->get($id, [
            'contain' => ['Patients', 'Doctors', 'Procedures', 'Users']
        ]);

        $this->set('psicologiaEvaluation', $psicologiaEvaluation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $psicologiaEvaluation = $this->PsicologiaEvaluations->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['date'] = date('Y-m-d');
            $this->request->data['user_id'] =$this->Auth->user("id");

            $psicologiaEvaluation = $this->PsicologiaEvaluations->patchEntity($psicologiaEvaluation, $this->request->data());
            if ($this->PsicologiaEvaluations->save($psicologiaEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Psicologia Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Psicologia Evaluation'));
        }
        $patients = $this->PsicologiaEvaluations->Patients->find('list', ['limit' => 200]);
        $doctors = $this->PsicologiaEvaluations->Doctors->find('list', ['limit' => 200]);
        $procedures = $this->PsicologiaEvaluations->Procedures->find('list', ['limit' => 200]);
        $users = $this->PsicologiaEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('psicologiaEvaluation', 'patients', 'doctors', 'procedures', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Psicologia Evaluation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $psicologiaEvaluation = $this->PsicologiaEvaluations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $psicologiaEvaluation = $this->PsicologiaEvaluations->patchEntity($psicologiaEvaluation, $this->request->data());
            if ($this->PsicologiaEvaluations->save($psicologiaEvaluation)) {
                $this->Flash->success(__('The {0} has been saved.', 'Psicologia Evaluation'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Psicologia Evaluation'));
        }
        $patients = $this->PsicologiaEvaluations->Patients->find('list', ['limit' => 200]);
        $doctors = $this->PsicologiaEvaluations->Doctors->find('list', ['limit' => 200]);
        $procedures = $this->PsicologiaEvaluations->Procedures->find('list', ['limit' => 200]);
        $users = $this->PsicologiaEvaluations->Users->find('list', ['limit' => 200]);
        $this->set(compact('psicologiaEvaluation', 'patients', 'doctors', 'procedures', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Psicologia Evaluation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $psicologiaEvaluation = $this->PsicologiaEvaluations->get($id);
        if ($this->PsicologiaEvaluations->delete($psicologiaEvaluation)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Psicologia Evaluation'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Psicologia Evaluation'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
