<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Doctors Controller
 *
 * @property \App\Model\Table\DoctorsTable $Doctors
 *
 * @method \App\Model\Entity\Doctor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DoctorsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['DoctorTypes', 'Users']
        ];
        $doctors = $this->paginate($this->Doctors);

        $this->set(compact('doctors'));
    }

    /**
     * View method
     *
     * @param string|null $id Doctor id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $doctor = $this->Doctors->get($id, [
            'contain' => ['DoctorTypes', 'Users']
        ]);

        $this->set('doctor', $doctor);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $doctor = $this->Doctors->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user("id");
            // $this->request->data['date'] = date("Y-m-d");
            $doctor = $this->Doctors->patchEntity($doctor, $this->request->data());
            if ($this->Doctors->save($doctor)) {
                $this->Flash->success(__('The {0} has been saved.', 'Doctor'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Doctor'));
        }
        $doctorTypes = $this->Doctors->DoctorTypes->find('list', ['limit' => 200]);
        $users = $this->Doctors->Users->find('list', ['limit' => 200]);
        $this->set(compact('doctor', 'doctorTypes', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Doctor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $doctor = $this->Doctors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $doctor = $this->Doctors->patchEntity($doctor, $this->request->data());
            if ($this->Doctors->save($doctor)) {
                $this->Flash->success(__('The {0} has been saved.', 'Doctor'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Doctor'));
        }
        $doctorTypes = $this->Doctors->DoctorTypes->find('list', ['limit' => 200]);
        $users = $this->Doctors->Users->find('list', ['limit' => 200]);
        $this->set(compact('doctor', 'doctorTypes', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Doctor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $doctor = $this->Doctors->get($id);
        if ($this->Doctors->delete($doctor)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Doctor'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Doctor'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
