<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FollowUpsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FollowUpsTable Test Case
 */
class FollowUpsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FollowUpsTable
     */
    public $FollowUps;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FollowUps',
        'app.Patients',
        'app.Doctors',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FollowUps') ? [] : ['className' => FollowUpsTable::class];
        $this->FollowUps = TableRegistry::getTableLocator()->get('FollowUps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FollowUps);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
