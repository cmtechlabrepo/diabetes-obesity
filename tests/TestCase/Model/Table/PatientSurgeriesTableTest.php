<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PatientSurgeriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PatientSurgeriesTable Test Case
 */
class PatientSurgeriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PatientSurgeriesTable
     */
    public $PatientSurgeries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PatientSurgeries',
        'app.Patients',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PatientSurgeries') ? [] : ['className' => PatientSurgeriesTable::class];
        $this->PatientSurgeries = TableRegistry::getTableLocator()->get('PatientSurgeries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PatientSurgeries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
