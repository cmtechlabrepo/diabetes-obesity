<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PsicologiaEvaluationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PsicologiaEvaluationsTable Test Case
 */
class PsicologiaEvaluationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PsicologiaEvaluationsTable
     */
    public $PsicologiaEvaluations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PsicologiaEvaluations',
        'app.Patients',
        'app.Doctors',
        'app.Procedures',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PsicologiaEvaluations') ? [] : ['className' => PsicologiaEvaluationsTable::class];
        $this->PsicologiaEvaluations = TableRegistry::getTableLocator()->get('PsicologiaEvaluations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PsicologiaEvaluations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
