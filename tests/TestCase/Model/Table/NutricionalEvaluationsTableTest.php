<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NutricionalEvaluationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NutricionalEvaluationsTable Test Case
 */
class NutricionalEvaluationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NutricionalEvaluationsTable
     */
    public $NutricionalEvaluations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.NutricionalEvaluations',
        'app.Patients',
        'app.Doctors',
        'app.Procedures',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('NutricionalEvaluations') ? [] : ['className' => NutricionalEvaluationsTable::class];
        $this->NutricionalEvaluations = TableRegistry::getTableLocator()->get('NutricionalEvaluations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NutricionalEvaluations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
