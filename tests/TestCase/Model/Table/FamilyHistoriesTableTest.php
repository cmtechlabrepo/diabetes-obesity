<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FamilyHistoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FamilyHistoriesTable Test Case
 */
class FamilyHistoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FamilyHistoriesTable
     */
    public $FamilyHistories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FamilyHistories',
        'app.Patients',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FamilyHistories') ? [] : ['className' => FamilyHistoriesTable::class];
        $this->FamilyHistories = TableRegistry::getTableLocator()->get('FamilyHistories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FamilyHistories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
