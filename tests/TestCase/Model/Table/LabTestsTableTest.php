<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LabTestsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LabTestsTable Test Case
 */
class LabTestsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LabTestsTable
     */
    public $LabTests;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.LabTests'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LabTests') ? [] : ['className' => LabTestsTable::class];
        $this->LabTests = TableRegistry::getTableLocator()->get('LabTests', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LabTests);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
