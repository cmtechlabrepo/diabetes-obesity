<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OtherHospitalizationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OtherHospitalizationsTable Test Case
 */
class OtherHospitalizationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OtherHospitalizationsTable
     */
    public $OtherHospitalizations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OtherHospitalizations',
        'app.Patients',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OtherHospitalizations') ? [] : ['className' => OtherHospitalizationsTable::class];
        $this->OtherHospitalizations = TableRegistry::getTableLocator()->get('OtherHospitalizations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OtherHospitalizations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
