<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PatientsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PatientsTable Test Case
 */
class PatientsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PatientsTable
     */
    public $Patients;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Patients',
        'app.Doctors',
        'app.Genders',
        'app.MaritalStatuses',
        'app.Users',
        'app.AnestesiaEvaluations',
        'app.Appointments',
        'app.BoneProblems',
        'app.DigestiveProblems',
        'app.Emails',
        'app.FamilyHistories',
        'app.HealthQuestionnaires',
        'app.InfoMedications',
        'app.InternistaEvaluations',
        'app.MajorIllnesses',
        'app.NutricionalEvaluations',
        'app.OtherHospitalizations',
        'app.OtorrinoEvaluations',
        'app.PatientSurgeries',
        'app.PsicologiaEvaluations',
        'app.Referrals',
        'app.SurgeryChanges',
        'app.Surveys',
        'app.LabTests'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Patients') ? [] : ['className' => PatientsTable::class];
        $this->Patients = TableRegistry::getTableLocator()->get('Patients', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Patients);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
