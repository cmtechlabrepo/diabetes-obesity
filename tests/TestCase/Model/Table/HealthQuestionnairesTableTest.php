<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HealthQuestionnairesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HealthQuestionnairesTable Test Case
 */
class HealthQuestionnairesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HealthQuestionnairesTable
     */
    public $HealthQuestionnaires;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HealthQuestionnaires',
        'app.Procedures',
        'app.Patients',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HealthQuestionnaires') ? [] : ['className' => HealthQuestionnairesTable::class];
        $this->HealthQuestionnaires = TableRegistry::getTableLocator()->get('HealthQuestionnaires', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HealthQuestionnaires);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
