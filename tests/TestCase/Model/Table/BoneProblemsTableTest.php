<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BoneProblemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BoneProblemsTable Test Case
 */
class BoneProblemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BoneProblemsTable
     */
    public $BoneProblems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.BoneProblems',
        'app.Patients',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BoneProblems') ? [] : ['className' => BoneProblemsTable::class];
        $this->BoneProblems = TableRegistry::getTableLocator()->get('BoneProblems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BoneProblems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
