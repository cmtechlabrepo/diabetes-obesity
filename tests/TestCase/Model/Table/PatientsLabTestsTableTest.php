<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PatientsLabTestsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PatientsLabTestsTable Test Case
 */
class PatientsLabTestsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PatientsLabTestsTable
     */
    public $PatientsLabTests;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PatientsLabTests',
        'app.Patients',
        'app.LabTests',
        'app.Doctors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PatientsLabTests') ? [] : ['className' => PatientsLabTestsTable::class];
        $this->PatientsLabTests = TableRegistry::getTableLocator()->get('PatientsLabTests', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PatientsLabTests);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
