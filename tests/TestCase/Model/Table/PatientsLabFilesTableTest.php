<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PatientsLabFilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PatientsLabFilesTable Test Case
 */
class PatientsLabFilesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PatientsLabFilesTable
     */
    public $PatientsLabFiles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PatientsLabFiles',
        'app.Patients',
        'app.Doctors',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PatientsLabFiles') ? [] : ['className' => PatientsLabFilesTable::class];
        $this->PatientsLabFiles = TableRegistry::getTableLocator()->get('PatientsLabFiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PatientsLabFiles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
