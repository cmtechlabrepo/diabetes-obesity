<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InfoMedicationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InfoMedicationsTable Test Case
 */
class InfoMedicationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InfoMedicationsTable
     */
    public $InfoMedications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.InfoMedications',
        'app.Patients',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InfoMedications') ? [] : ['className' => InfoMedicationsTable::class];
        $this->InfoMedications = TableRegistry::getTableLocator()->get('InfoMedications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InfoMedications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
