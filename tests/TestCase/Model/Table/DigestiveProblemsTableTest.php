<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DigestiveProblemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DigestiveProblemsTable Test Case
 */
class DigestiveProblemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DigestiveProblemsTable
     */
    public $DigestiveProblems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DigestiveProblems',
        'app.Patients',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DigestiveProblems') ? [] : ['className' => DigestiveProblemsTable::class];
        $this->DigestiveProblems = TableRegistry::getTableLocator()->get('DigestiveProblems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DigestiveProblems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
