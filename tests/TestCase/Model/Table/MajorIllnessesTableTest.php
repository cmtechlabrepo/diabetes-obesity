<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MajorIllnessesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MajorIllnessesTable Test Case
 */
class MajorIllnessesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MajorIllnessesTable
     */
    public $MajorIllnesses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MajorIllnesses',
        'app.Patients',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MajorIllnesses') ? [] : ['className' => MajorIllnessesTable::class];
        $this->MajorIllnesses = TableRegistry::getTableLocator()->get('MajorIllnesses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MajorIllnesses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
